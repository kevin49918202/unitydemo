﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour {
    Rigidbody rbody;
    float h;
    float v;
    int i = 0;
    [SerializeField] GameObject meteorParent;
    List<GameObject>  Meteoris ;
	// Use this for initialization
	void Start () {
        //Meteoris = GameObject.FindGameObjectsWithTag("Meteor") ;
        rbody = GetComponent<Rigidbody>();
        //this.InvokeRepeating("MeteorIns", 0f, 2.0f);
    }
	
	// Update is called once per frame
	void Update () {
        h = Input.GetAxisRaw("Horizontal");
        v = Input.GetAxisRaw("Vertical");
        rbody.velocity = new Vector3(h*20f, 0, v*20f);
    }
    void MeteorIns() {
        Meteoris[i].SetActive(true);
        Meteoris[i + 1].SetActive(true);
        Meteoris[i + 2].SetActive(true);
        Meteoris[i + 3].SetActive(true);
        Meteoris[i + 4].SetActive(true);
        this.CancelInvoke();
        //i++;
    }
}
