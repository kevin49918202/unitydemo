﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAI : MonoBehaviour
{
    //MFSM FSM;
    public AIData m_Data;
    public BossIdleState MIS;
    public BossChaseState MCS;
    public BossAttackState MAS;
    public BossDeadState MDS;
    public BossCastMetorState BCM;
    public BossFireSpikeState BFS;
    public BossBigStompState BBS;
    public BossBigTeleportState BBT;

    
    // Use this for initialization
    void Awake()
    {
        m_Data.Me_Object = this.gameObject;
        m_Data.BornPoint = m_Data.Me_Object.transform.position;
        m_Data.m_currentState = new BossState();
        m_Data.Monsteranim = new MonsterAnimScript();
        m_Data.bossAI = GetComponent<BossAI>();

        MIS = new BossIdleState();
        MCS = new BossChaseState();
        MAS = new BossAttackState();
        MDS = new BossDeadState();
        BCM = new BossCastMetorState();
        BFS = new BossFireSpikeState();
        BBS = new BossBigStompState();
        BBT = new BossBigTeleportState();


        m_Data.TransferState(m_Data, MIS);

        m_Data.Monsteranim.anim = GetComponent<Animator>();
        m_Data.rbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (m_Data.m_currentState != null)
        {
            m_Data.Monsteranim.MonsterAnimController(m_Data);
            m_Data.CheckEnemyInSight();
            m_Data.m_currentState.Do(m_Data);
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            m_Data.TransferState(m_Data, m_Data.bossAI.BCM);
            //m_Data.IsDead = true;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            m_Data.TransferState(m_Data, m_Data.bossAI.BFS);
            //m_Data.IsDead = true;
        }
        if (Input.GetKeyDown(KeyCode.T))
        {
            m_Data.TransferState(m_Data, m_Data.bossAI.BBS);
            //m_Data.IsDead = true;
        }
        if (Input.GetKeyDown(KeyCode.Y))
        {
            m_Data.TransferState(m_Data, m_Data.bossAI.BBT);
            //m_Data.IsDead = true;
        }

        if (m_Data.IsDead && m_Data.m_currentState != m_Data.monsterAI.MDS)
        {
            m_Data.TransferState(m_Data, m_Data.monsterAI.MDS);
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        if (m_Data.Enemy.Length > 0)
        {
            Gizmos.color = Color.red;
        }
        Gizmos.DrawWireSphere(transform.position, m_Data.DetectRadius);

        if (m_Data.steeringPoint != Vector3.zero)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(m_Data.BornPoint, m_Data.SteeringRange);
        }
        Gizmos.color = Color.black;
        Gizmos.DrawWireSphere(transform.position,m_Data.BossJumpRange);
    }
}
