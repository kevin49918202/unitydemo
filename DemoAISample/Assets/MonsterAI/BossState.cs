﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class BossState : MonsterState
{
    public BossState()
    {
    }
    public override void DoBeforeEnter(AIData Data)
    {
        Debug.Log("In Monster State");

    }
    public override void DoBeforeLeave(AIData Data)
    {
        Debug.Log("Leave Monster State");
    }
    public override void Do(AIData Data)
    {
        Debug.Log("Monster State");
    }
}

public class BossIdleState : MonsterState
{
    public override void DoBeforeEnter(AIData Data)
    {
        Data.steeringPoint = Data.Me_Object.transform.position;
    }
    public override void DoBeforeLeave(AIData Data)
    {
    }
    public override void Do(AIData Data)
    {

        if (Data.steeringIntervalCount < Time.time && Data.rbody.velocity == Vector3.zero)
        {
            Data.steeringIntervalCount = Time.time + Data.steeringInterval;
            if (Data.SteeringRange == 0)
            {
                Debug.LogError("沒有SteeringRange");
                return;
            }

            Data.steeringPoint = Data.BornPoint + Random.insideUnitSphere * Data.SteeringRange;
            Data.steeringPoint.y = Data.Me_Object.transform.position.y;
            Data.OriginalPoint = Data.rbody.position;
        }
        if (Vector3.Distance(Data.steeringPoint, Data.Me_Object.transform.position) > 0.15f)
        {
            MonsterRotate(Data, Data.steeringPoint);
            Data.rbody.velocity = Vector3.Normalize(Data.steeringPoint - Data.rbody.transform.position) * Data.MoveSpeed * Mathf.Clamp(Vector3.Distance(Data.steeringPoint, Data.rbody.transform.position) / Vector3.Distance(Data.steeringPoint, Data.OriginalPoint) * 2, 0.6f, 1f);
            Data.rbody.velocity = new Vector3(Data.rbody.velocity.x, 0, Data.rbody.velocity.z);
            Data.CanRun = true;
        }
        else
        {
            Data.rbody.velocity = new Vector3(0, 0, 0);
            Data.Me_Object.transform.position = Data.steeringPoint;
            Data.OriginalPoint = Data.Me_Object.transform.position;
            Data.CanRun = false;
        }
        if (Data.CheckEnemyInSight() == true)
        {
            Data.TransferState(Data, Data.bossAI.MCS);
        }
    }
}
public class BossChaseState : MonsterState
{
    public override void DoBeforeEnter(AIData Data)
    {
        Data.CanRun = true;
    }
    public override void DoBeforeLeave(AIData Data)
    {
        Data.rbody.velocity = new Vector3(0, 0, 0);
        Data.CanRun = false;
    }
    public override void Do(AIData Data)
    {
        if (Data.Enemy.Length > 0)
        {
            //每秒根據角度大小轉身－角度越大轉越多－反之則越小
            MonsterRotate(Data, Data.Enemy[0].transform.position);
            //if (Data.Distance(Data.Me_Object.transform, Data.Enemy[0].transform) > Data.BossJumpRange)
            //{
            //    Data.TransferState(Data,Data.bossAI.BBT);
            //}
            /*else */if (Data.Distance(Data.Me_Object.transform, Data.Enemy[0].transform) > Data.AttackRange)
            {
                if (Data.Monsteranim.anim.GetCurrentAnimatorStateInfo(0).IsName("Run"))
                {
                    Data.rbody.velocity = Vector3.Normalize(Data.Enemy[0].transform.position - Data.rbody.transform.position) * Data.MoveSpeed;
                }
            }
            else
            {
                Data.TransferState(Data, Data.bossAI.MAS);
            }
        }
        else
        {
            Data.TransferState(Data, Data.bossAI.MIS);
        }
    }
}
public class BossAttackState : MonsterState
{
    public override void DoBeforeEnter(AIData Data)
    {
    }
    public override void DoBeforeLeave(AIData Data)
    {
        Data.CanAttack = false;
    }
    public override void Do(AIData Data)
    {
        if (Data.Enemy.Length > 0)
        {
            if (Data.Distance(Data.Me_Object.transform, Data.Enemy[0].transform) > Data.AttackRange+1f)
            {
                Data.TransferState(Data, Data.bossAI.MCS);
            }
            else
            {
                Data.CanAttack = true;
                MonsterRotate(Data, Data.Enemy[0].transform.position);
            }
        }
    }
}
public class BossDeadState : MonsterState
{
    public override void DoBeforeEnter(AIData Data)
    {
        Data.rbody.velocity = new Vector3(0, 0, 0);
        Data.DeadDisapearTimeCount = Data.DeadDisapearTime;

    }
    public override void DoBeforeLeave(AIData Data)
    {
        Data.IsDead = false;
        Data.DeadDisapearTimeCount = Data.DeadDisapearTime;
    }
    public override void Do(AIData Data)
    {
        if (Data.DeadDisapearTimeCount > 0)
        {
            Data.DeadDisapearTimeCount -= Time.deltaTime;
            //shaderdisapeartime = Data.DeadDisapearTime;
        }
        else if (Data.DeadDisapearTimeCount < 0)
        {
            //傳訊息給物件池
            Data.Me_Object.transform.position = Data.BornPoint;
            Data.TransferState(Data, Data.bossAI.MIS);
        }
    }
}
public class BossCastMetorState : MonsterState
{
    public override void DoBeforeEnter(AIData Data)
    {
        Debug.Log("In Boss Cast Metor State");
        Data.CastSpellCount = Data.CastSpellTime;
        Data.Monsteranim.anim.SetTrigger("CastSpell");
    }
    public override void DoBeforeLeave(AIData Data)
    {
        Data.Monsteranim.anim.SetBool("CastComplete", false);
    }
    public override void Do(AIData Data)
    {
        if (Data.CastSpellCount <= 0 && Data.CastComplete && Data.Monsteranim.anim.GetCurrentAnimatorStateInfo(0).IsName("Stand"))
        {
            if (Data.Enemy.Length > 0)
            {
                Data.TransferState(Data, Data.bossAI.MCS);
            }
            else {
                Data.TransferState(Data, Data.bossAI.MIS);
            }
        }
    }
}
public class BossFireSpikeState : MonsterState
{
    public override void DoBeforeEnter(AIData Data)
    {
        Debug.Log("In Boss FireSpike State");
        Data.rbody.velocity = new Vector3(0, 0, 0);
        Data.CastSpellCount = Data.CastSpellTime;
        Data.Monsteranim.anim.SetTrigger("FireSpike");
    }
    public override void DoBeforeLeave(AIData Data)
    {
        Data.CastSpellCount = Data.CastSpellTime;
    }
    public override void Do(AIData Data)
    {
        if (Data.CastSpellCount > 0)
        {
            Debug.Log("In Boss FireSpike State");
        }
        else if (Data.CastSpellCount < 0 )
        {
            Data.TransferState(Data, Data.bossAI.MIS);
        }
    }
}
public class BossBigStompState : MonsterState
{
    public override void DoBeforeEnter(AIData Data)
    {
        Data.rbody.velocity = new Vector3(0, 0, 0);
        Data.Monsteranim.anim.SetTrigger("BigStomp");
        Data.CastSpellCount = 1;
    }
    public override void DoBeforeLeave(AIData Data)
    {
        Data.Monsteranim.anim.SetBool("CastComplete", false);
    }
    public override void Do(AIData Data)
    {
        if (Data.CastSpellCount <= 0 && Data.CastComplete && Data.Monsteranim.anim.GetCurrentAnimatorStateInfo(0).IsName("Stand"))
        {
            if (Data.Enemy.Length > 0)
            {
                Data.TransferState(Data, Data.bossAI.MCS);
            }
            else
            {
                Data.TransferState(Data, Data.bossAI.MIS);
            }
        }
    }
}
public class BossBigTeleportState : MonsterState
{
    public override void DoBeforeEnter(AIData Data)
    {
        Data.rbody.velocity = new Vector3(0, 0, 0);
        Data.Monsteranim.anim.SetTrigger("FarRangeStomp");
        Data.CastSpellCount = 1;
    }
    public override void DoBeforeLeave(AIData Data)
    {
        Data.Monsteranim.anim.SetBool("CastComplete", false);
    }
    public override void Do(AIData Data)
    {
        if (Data.CastSpellCount <= 0 && Data.CastComplete && Data.Monsteranim.anim.GetCurrentAnimatorStateInfo(0).IsName("Stand"))
        {
            if (Data.Enemy.Length > 0)
            {
                Data.TransferState(Data, Data.bossAI.MCS);
            }
            else
            {
                Data.TransferState(Data, Data.bossAI.MIS);
            }
        }
    }
}

