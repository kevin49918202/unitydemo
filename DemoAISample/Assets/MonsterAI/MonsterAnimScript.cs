﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAnimScript  {
    public Animator anim;
    public void MonsterAnimController(AIData Data) {
        anim.SetBool("CanRun",Data.CanRun);
        anim.SetBool("CanAttack", Data.CanAttack);
        anim.SetBool("Dead", Data.IsDead);
    }
}
