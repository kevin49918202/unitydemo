﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class AIData {
    [HideInInspector]
    public MonsterState m_currentState;
    public BossState m_currentBossState;
    [HideInInspector]
    public MonsterAI monsterAI;
    public BossAI bossAI;
    //public MFSM FSM;
    [HideInInspector]
    public GameObject Me_Object ;
    [HideInInspector]
    public Rigidbody rbody;
    public MonsterAnimScript Monsteranim;
    /// <summary>
    /// 遊走(Steering Behavior)
    /// </summary>
    [HideInInspector]
    public Vector3 BornPoint ;
    [HideInInspector]
    public Vector3 OriginalPoint;
    [HideInInspector]
    public Vector3 steeringPoint;
    public float steeringInterval;
    public float SteeringRange;
    [HideInInspector]
    public float steeringIntervalCount;

    /// <summary>
    /// 偵測範圍敵人
    /// </summary>
    [HideInInspector]
    public bool HasEnemy;
    public Collider[] Enemy;
    public float DetectRadius;
    public LayerMask EnemyLayer;
    /// <summary>
    /// AI素質
    /// </summary>
    [HideInInspector]
    public bool CanAttack;
    public float AttackRange;
    [HideInInspector]
    public bool CanRun;
    public float MoveSpeed;
    public float RotateSpeed;
    public int CastSpellTime;
    //[HideInInspector]
    public int CastSpellCount;
    [HideInInspector]
    public bool CastComplete;
    [HideInInspector]
    public Quaternion targetRotation;
    public float BossJumpRange;
    public bool jumpMove;
    public Vector3 JumpOriginPos;
    public Vector3 JumpGoal;

    [HideInInspector]
    public float EnemyAngle;
    /// <summary>
    /// 死亡
    /// </summary>
    [HideInInspector]
    public bool IsDead;
    public float DeadDisapearTime;
    [HideInInspector]
    public float DeadDisapearTimeCount;

    public bool CheckEnemyInSight() {
        Enemy = Physics.OverlapSphere(Me_Object.transform.position,DetectRadius, EnemyLayer);
        if (Enemy.Length>0)
        {
            HasEnemy = true;
            
        }
        else {
            HasEnemy = false;
        }
        return HasEnemy;
    }
    public void TransferState(AIData Data, MonsterState ChangeMS)
    {
        Data.m_currentState.DoBeforeLeave(Data);
        Data.m_currentState = ChangeMS;
        Data.m_currentState.DoBeforeEnter(Data);
    }
    public void TransferState(AIData Data, BossState ChangeMS)
    {
        Data.m_currentBossState.DoBeforeLeave(Data);
        Data.m_currentBossState = ChangeMS;
        Data.m_currentBossState.DoBeforeEnter(Data);
    }
    public float Distance(Transform monstertrans,Transform Enemytrans) {
        float distance = Vector3.Distance(monstertrans.position, Enemytrans.position);
        return distance;
    }
}
