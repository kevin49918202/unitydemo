﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAI : MonoBehaviour {
    //MFSM FSM;
    public AIData m_Data;
    public MonsterIdleState MIS;
    public MonsterChaseState MCS;
    public MonsterAttackState MAS;
    public MonsterDeadState MDS;
	// Use this for initialization
	void Awake () {
        m_Data.Me_Object = this.gameObject;
        m_Data.BornPoint = m_Data.Me_Object.transform.position;
        m_Data.m_currentState = new MonsterState();
        m_Data.Monsteranim = new MonsterAnimScript();
        m_Data.monsterAI = GetComponent<MonsterAI>();

        MIS = new MonsterIdleState();
        MCS = new MonsterChaseState();
        MAS = new MonsterAttackState();
        MDS = new MonsterDeadState();


        m_Data.TransferState(m_Data, MIS);

        m_Data.Monsteranim.anim = GetComponent<Animator>();
        m_Data.rbody = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (m_Data.m_currentState != null )
        {
            m_Data.Monsteranim.MonsterAnimController(m_Data);
            m_Data.CheckEnemyInSight();
            m_Data.m_currentState.Do(m_Data);
        }
        if (Input.GetKey(KeyCode.E)) {
            m_Data.IsDead = true;
        }
        if(m_Data.IsDead && m_Data.m_currentState != m_Data.monsterAI.MDS ){
            m_Data.TransferState(m_Data, m_Data.monsterAI.MDS);
        }
	}
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        if (m_Data.Enemy.Length > 0) {
            Gizmos.color = Color.red;
        }
        if (m_Data.Me_Object == null) {
            return;
        }
        Gizmos.DrawWireSphere(m_Data.Me_Object.transform.position, m_Data.DetectRadius);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(m_Data.BornPoint, m_Data.SteeringRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(m_Data.Me_Object.transform.position, m_Data.AttackRange);
    }
}
