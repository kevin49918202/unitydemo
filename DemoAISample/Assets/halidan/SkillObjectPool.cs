﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillObjectPool : MonoBehaviour {
    [System.Serializable]
    public class pool
    {
        public string tag;
        public GameObject prefab;
        public int size;
    }
    public List<pool> pools;
    public Dictionary<string, Queue<GameObject>> poolDictionary;

    // Use this for initialization
    void Start() {
        poolDictionary = new Dictionary<string, Queue<GameObject>>();
        foreach (pool pool in pools)
        {
            Queue<GameObject> objectpool = new Queue<GameObject>();

            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab,pool.prefab.transform.position, pool.prefab.transform.rotation);
                obj.SetActive(false);
                objectpool.Enqueue(obj);
            }

            poolDictionary.Add(pool.tag, objectpool);
            InvokeRepeating("SpawnMetor",0f,2f);
        }
    }
    public GameObject SpawnFromPool(string tag, Vector3 position, Quaternion rotation) {
        if (!poolDictionary.ContainsKey(tag)) {

            return null;
        }
        GameObject objectToSpawn = poolDictionary[tag].Dequeue();

        objectToSpawn.SetActive(true);
        //objectToSpawn.transform.position = position;
        //objectToSpawn.transform.rotation = rotation;

        poolDictionary[tag].Enqueue(objectToSpawn);

        return objectToSpawn;
    }
    void SpawnMetor() {
        SpawnFromPool("Meteor", transform.position, Quaternion.identity);
    }
	
	// Update is called once per frame
	void Update () {
        
	}
}
