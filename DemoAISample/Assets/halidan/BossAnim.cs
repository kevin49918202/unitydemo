﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAnim : MonoBehaviour {
    [SerializeField]
    GameObject effectSavePoint;
    [SerializeField]
    GameObject meteor;
    GameObject[] meteorList;
    [SerializeField]
    int meteorListCount = 0;
    [SerializeField]
    GameObject stompEffect;
    GameObject[] stompEffectList;
    [SerializeField]
    int stompEffectCount = 0;
    [SerializeField]
    GameObject farRangeStomp;
    GameObject[] farRangeStompList;
    [SerializeField]
    int farRangeStompListCount = 0;
    [SerializeField]
    GameObject BossGround;
    [SerializeField]
    float Area;
    public bool jumpMove;
    BossAI bossai;
    void Start()
    {
        bossai = GetComponent<BossAI>();
        meteorList = new GameObject[meteorListCount];
        for (int i = 0; i < meteorList.Length; i++)
        {
            meteorList[i] = Instantiate(meteor, effectSavePoint.transform.position, effectSavePoint.transform.rotation);
            meteorList[i].SetActive(false);
        }

        stompEffectList = new GameObject[stompEffectCount];
        for (int i = 0; i < stompEffectList.Length; i++)
        {
            stompEffectList[i] = Instantiate(stompEffect, effectSavePoint.transform.position, effectSavePoint.transform.rotation);
            stompEffectList[i].SetActive(false);
        }
        farRangeStompList = new GameObject[farRangeStompListCount];
        for (int i = 0; i < farRangeStompList.Length; i++)
        {
            farRangeStompList[i] = Instantiate(farRangeStomp, effectSavePoint.transform.position, effectSavePoint.transform.rotation);
            farRangeStompList[i].SetActive(false);
        }
    }
    void Meteor() {
        Instantiate(meteor, transform.position+new Vector3(Random.Range(Area,-Area),20f,Random.Range(Area, -Area)), meteor.transform.rotation);
    }
    void BigStomp() {

    }
    void FireSpike() {

    }
    void BigJump() {
        bossai.m_Data.jumpMove = true;
    }
    void BigJumpEnd() {
        bossai.m_Data.jumpMove = true;
    }
    void CastSpellCount() {
        bossai.m_Data.CastSpellCount -= 1;
        if (bossai.m_Data.CastSpellCount <= 0) {
            bossai.m_Data.CastComplete = true;
            bossai.m_Data.Monsteranim.anim.SetBool("CastComplete", true);
        }
    }
    void StompEffect() {
        //Instantiate(stompEffect, BossGround.transform.position, BossGround.transform.rotation);
        if (stompEffectCount >= stompEffectList.Length) {
            stompEffectCount = 0;
        }
        stompEffectList[stompEffectCount].transform.position = BossGround.transform.position;
        stompEffectList[stompEffectCount].SetActive(true);
        stompEffectCount++;

    }
    void FarRangeStomp()
    {
        if (farRangeStompListCount >= farRangeStompList.Length) {
            farRangeStompListCount = 0;
        }
        farRangeStompList[farRangeStompListCount].transform.position = bossai.m_Data.Enemy[0].transform.position;
        farRangeStompList[farRangeStompListCount].SetActive(true);
        farRangeStompListCount++;
        //Instantiate(farRangeStomp, bossai.m_Data.Enemy[0].transform.position, bossai.m_Data.Enemy[0].transform.rotation);
    }
}
