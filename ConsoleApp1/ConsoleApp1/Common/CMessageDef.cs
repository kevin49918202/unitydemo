﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp1.Common
{
    public enum ECommand
    {
        LOGIN = 1, EXIT, CHANGE_CHARACTER, ACCOUNT_LOGIN, GET_CHARACTER_INFO, OTHER_LOGIN, OTHER_LOGIN_SET, OTHER_LOGIN_RETURN,

        TEXT, TRADE, TEAM, PLAYER_MONEY,

        PLAYER_MOVEMENT, PLAYER_JUMP, PLAYER_EQUIP, PLAYER_TARGET, PLAYER_CASTSKILL, PLAYER_TAKEDAMAGE,

        PLAYER_LEVELUP, PLAYER_ADDEXP, PLAYER_REVIVAL, PLAYER_CHANGE_HPMP, PLAYER_DIE,

        MONSTERMOVEMENT_Health, MONSTERATTACK, MONSTERFOCUS, MONSTER_TAKEDAMAGE, MONSTER_ACTIVE, MONSTER_DIE
    }

    public enum ELoginCommand
    {
        LOGIN, SET_INDEX, OTHER_LOGIN, OTHER_LOGIN_RETURN, CHARACTER_INFO
    }

    public enum EEquipmentSlot
    {
        Head, Chest, Legs, Feet, Weapon, Shield
    }

    public enum EItemCommand
    {
        ADD, UPDATE, REMOVE
    }
}
