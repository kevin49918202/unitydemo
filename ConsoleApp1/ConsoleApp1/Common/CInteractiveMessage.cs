﻿namespace ConsoleApp1.Common
{
    class CTradeMessage : CMessage
    {
        public CTradeMessage() : base((int)ECommand.TRADE)
        {

        }

        public int m_PlayerIndex;
        public int m_TargetIndex;
        public int m_TradeCommand;
        public int m_ItemID;
        public int m_ItemAmount;
        public int m_MoneyValue;

        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_PlayerIndex);
            _WriteToBuffer(m_TargetIndex);
            _WriteToBuffer(m_TradeCommand);
            _WriteToBuffer(m_ItemID);
            _WriteToBuffer(m_ItemAmount);
            _WriteToBuffer(m_MoneyValue);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_PlayerIndex);
            _ReadFromBuffer(out m_TargetIndex);
            _ReadFromBuffer(out m_TradeCommand);
            _ReadFromBuffer(out m_ItemID);
            _ReadFromBuffer(out m_ItemAmount);
            _ReadFromBuffer(out m_MoneyValue);
        }
    }

    class CTeamMessage : CMessage
    {
        public CTeamMessage() : base((int)ECommand.TEAM)
        {

        }

        public int m_PlayerIndex;
        public int m_TargetIndex;
        public int m_TeamCommand;
        
        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_PlayerIndex);
            _WriteToBuffer(m_TargetIndex);
            _WriteToBuffer(m_TeamCommand);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_PlayerIndex);
            _ReadFromBuffer(out m_TargetIndex);
            _ReadFromBuffer(out m_TeamCommand);
        }
    }
}