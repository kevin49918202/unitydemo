﻿namespace ConsoleApp1.Common
{
    class CPlayerLevelUpMessage : CMessage
    {
        public CPlayerLevelUpMessage() : base((int)ECommand.PLAYER_LEVELUP)
        {

        }

        public int m_PlayerIndex;
        public int m_Level;

        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_PlayerIndex);
            _WriteToBuffer(m_Level);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_PlayerIndex);
            _ReadFromBuffer(out m_Level);
        }
    }

    class CPlayerRevivalMessage : CMessage
    {
        public CPlayerRevivalMessage() : base((int)ECommand.PLAYER_REVIVAL)
        {

        }

        public int m_PlayerIndex;

        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_PlayerIndex);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_PlayerIndex);
        }
    }

    class CPlayerDieMessage : CMessage
    {
        public CPlayerDieMessage() : base((int)ECommand.PLAYER_DIE)
        {

        }

        public int m_PlayerIndex;

        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_PlayerIndex);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_PlayerIndex);
        }
    }

    class CPlayerAddExpMessage : CMessage
    {
        public CPlayerAddExpMessage() : base((int)ECommand.PLAYER_ADDEXP)
        {

        }

        public int m_PlayerIndex;
        public int m_Exp;

        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_PlayerIndex);
            _WriteToBuffer(m_Exp);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_PlayerIndex);
            _ReadFromBuffer(out m_Exp);
        }
    }


    class CPlayerTargetMessage : CMessage
    {
        public CPlayerTargetMessage() : base((int)ECommand.PLAYER_TARGET)
        {

        }

        public int m_PlayerIndex;
        public int m_TargetIndex;

        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_PlayerIndex);
            _WriteToBuffer(m_TargetIndex);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_PlayerIndex);
            _ReadFromBuffer(out m_TargetIndex);
        }
    }

    class CPlayerCastSkillMessage : CMessage
    {
        public CPlayerCastSkillMessage() : base((int)ECommand.PLAYER_CASTSKILL)
        {

        }

        public int m_PlayerIndex;
        public int m_SkillCommand;

        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_PlayerIndex);
            _WriteToBuffer(m_SkillCommand);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_PlayerIndex);
            _ReadFromBuffer(out m_SkillCommand);
        }
    }

    class CPlayerTakeDamageMessage : CMessage
    {
        public CPlayerTakeDamageMessage() : base((int)ECommand.PLAYER_TAKEDAMAGE)
        {

        }

        public int m_PlayerIndex;
        public int m_MonsterNumber;
        public int m_Damage;
        public bool m_Critical;

        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_PlayerIndex);
            _WriteToBuffer(m_MonsterNumber);
            _WriteToBuffer(m_Damage);
            _WriteToBuffer(m_Critical);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_PlayerIndex);
            _ReadFromBuffer(out m_MonsterNumber);
            _ReadFromBuffer(out m_Damage);
            _ReadFromBuffer(out m_Critical);
        }
    }

    class CMonsterTakeDamageMessage : CMessage
    {
        public CMonsterTakeDamageMessage() : base((int)ECommand.MONSTER_TAKEDAMAGE)
        {

        }

        public int m_MonsterNumber;
        public int m_PlayerIndex;
        public int m_Damage;
        public bool m_Critical;
        public int m_DamageType;

        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_MonsterNumber);
            _WriteToBuffer(m_PlayerIndex);
            _WriteToBuffer(m_Damage);
            _WriteToBuffer(m_Critical);
            _WriteToBuffer(m_DamageType);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_MonsterNumber);
            _ReadFromBuffer(out m_PlayerIndex);
            _ReadFromBuffer(out m_Damage);
            _ReadFromBuffer(out m_Critical);
            _ReadFromBuffer(out m_DamageType);
        }
    }

    class CMonsterDieMessage : CMessage
    {
        public CMonsterDieMessage() : base((int)ECommand.MONSTER_DIE)
        {

        }

        public int m_MonsterNumber;

        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_MonsterNumber);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_MonsterNumber);
        }
    }

    class CPlayerChangeHpMpMessage : CMessage
    {
        public CPlayerChangeHpMpMessage() : base((int)ECommand.PLAYER_CHANGE_HPMP)
        {

        }

        public int m_PlayerIndex;
        public int m_CurrentHp;
        public int m_CurrentMp;

        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_PlayerIndex);
            _WriteToBuffer(m_CurrentHp);
            _WriteToBuffer(m_CurrentMp);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_PlayerIndex);
            _ReadFromBuffer(out m_CurrentHp);
            _ReadFromBuffer(out m_CurrentMp);
        }
    }
}