﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp1.Common
{
    class CMonsterMovementMessage : CMessage
    {
        public CMonsterMovementMessage() : base((int)ECommand.MONSTERMOVEMENT_Health)
        {

        }

        public float m_PosX, m_PosZ;
        public float m_Angle;
        public int m_currenthealth;
        public int m_Number;

        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_PosX);
            _WriteToBuffer(m_PosZ);
            _WriteToBuffer(m_Angle);
            _WriteToBuffer(m_Number);
            _WriteToBuffer(m_currenthealth);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_PosX);
            _ReadFromBuffer(out m_PosZ);
            _ReadFromBuffer(out m_Angle);
            _ReadFromBuffer(out m_Number);
            _ReadFromBuffer(out m_currenthealth);
        }
    }
    class CMonsterActiveMessage : CMessage
    {
        public CMonsterActiveMessage() : base((int)ECommand.MONSTER_ACTIVE)
        {

        }

        public int Active;
        public int m_Number;

        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(Active);
            _WriteToBuffer(m_Number);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out Active);
            _ReadFromBuffer(out m_Number);
        }
    }
}
