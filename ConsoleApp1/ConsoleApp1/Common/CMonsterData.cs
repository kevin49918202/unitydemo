﻿using System.Collections;
using System.Collections.Generic;

namespace ConsoleApp1.Common
{
    class CLoginMessage : CMessage
    {
        public CLoginMessage() : base((int)ECommand.LOGIN)
        {

        }

        public int m_PlayerIndex;
        public string m_Name = "";
        
        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_PlayerIndex);
            _WriteToBuffer(m_Name);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_PlayerIndex);
            _ReadFromBuffer(out m_Name);
        }
    }
}

