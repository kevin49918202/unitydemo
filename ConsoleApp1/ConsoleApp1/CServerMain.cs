﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class CServerMain
    {
        static void Main(string[] args)
        {
            CserverApp serverApp = new CserverApp();
            serverApp.Bind(4099);
            serverApp.Run();
        }
    }
}
