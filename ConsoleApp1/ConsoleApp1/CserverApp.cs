﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using ConsoleApp1.Common;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace ConsoleApp1
{
    class CharacterInfo
    {
        public CharacterInfo(string characterName, string job)
        {
            this.characterName = characterName;
            this.job = job;
        }
        public string characterName;
        public string job;
    }
    class CserverApp
    {
        int playerCount = 0;
        /// <summary>
        /// sql帳號，密碼，預設帳號:root，密碼:1111，資料庫名稱:student
        /// </summary>
        string sDBHost = "127.0.0.1";
        int iDBPort = 3306;
        string sDBAcount = "root";
        string sDBPassword = "00000000";
        string sDBName = "unitydemo";
        MySqlConnection m_Connection;

        bool DBConnected = false;

        List<CharacterInfo> characterList = new List<CharacterInfo>();

        public void Bind(int iPort)
        {
            m_Port = iPort;
            m_theListener = new TcpListener(IPAddress.Any, 4099);
            m_theListener.Start();
            Console.WriteLine("Chat Server port " + iPort + " binded...");

            DBConnected = ConnectDB(sDBHost, iDBPort, sDBAcount, sDBPassword, sDBName);

            characterList.Add(new CharacterInfo("煞氣法師", "戰士"));
            characterList.Add(new CharacterInfo("威猛先生", "戰士"));
        }

        bool ConnectDB(string sDBHost, int iDBPort, string sDBAcount, string sDBPassword, string sDBName)
        {
            string sConnect = "server=" + sDBHost + ";user=" + sDBAcount + ";database=" + sDBName + ";port=" + iDBPort + ";password=" + sDBPassword + ";allow zero datetime=true;";

            try
            {
                m_Connection = new MySqlConnection(sConnect);
                m_Connection.Open();

                Console.WriteLine("DB connected...");
                return true;
            }
            catch (MySqlException ex)
            {
                String sCurrentFile = new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileName();
                int iCurrentLine = new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileLineNumber();

                //Debug.Log("# ERR: SQLException in " + sCurrentFile + ":" + iCurrentLine);
                //Debug.Log("# ERR: " + ex.Message);
                //Debug.Log("# ERR: MySQL error code: " + ex.Number);

                return false;
            }
        }
        public void Run() {
            Thread thethread = new Thread(_RunTcpClient);
            thethread.Start();
            while (true)
            {
                TcpClient theClient = m_theListener.AcceptTcpClient();
                Console.WriteLine("A client connected....");

                CTransmitter transmitter = new CTransmitter(theClient);
                Register(transmitter);

                lock (m_setTransmitters)
                {
                    m_setTransmitters.Add(transmitter);
                }
            }
        }
        void _RunTcpClient() {
            while (true)
            {
                if (m_setTransmitters.Count == 0) continue;
                CTransmitter[] aTransmitter = null;
                lock (m_setTransmitters)
                {
                    aTransmitter = new CTransmitter[m_setTransmitters.Count];
                    m_setTransmitters.CopyTo(aTransmitter);
                }
                foreach (CTransmitter transmitter in aTransmitter) {
                    bool bRemove = false;
                    if (transmitter.IsConnected() == false)
                    {
                        bRemove = true;
                    }
                    else
                    {
                        try
                        {
                            transmitter.Run();
                        }
                        catch (Exception e)
                        {

                            Console.WriteLine("Error happened:"+e.ToString()); ;
                            bRemove = true;
                        }
                    }
                    if (bRemove)
                    {
                        lock (m_setTransmitters)
                        {
                            if (m_setTransmitters.Contains(transmitter))
                            {
                                m_setTransmitters.Remove(transmitter);
                                Console.WriteLine("A client has disConnedted...");
                                if (!m_setPlayerInGame.Contains(transmitter)) continue;
                                
                                //廣播斷線msg
                                CExitMessage msg = new CExitMessage();
                                int playerIndex = m_dicTransmitterIndex[transmitter];
                                msg.m_PlayerIndex = playerIndex;
                                BroadcastMessage(transmitter, msg, false);

                                m_dicTransmitterNames.Remove(transmitter);
                                m_dicTransmitterIndex.Remove(transmitter);
                                m_dicTransmitter.Remove(playerIndex);
                                m_setPlayerInGame.Remove(transmitter);
                            }
                            else
                            {
                                Console.WriteLine("A");
                            }
                        }
                    }
                }
            }
        }

        //void SendPlayerItemMessage(CTransmitter transmitter)
        //{
        //    CPlayerItemMessage msg = new CPlayerItemMessage();
        //    string characterName = m_dicTransmitterNames[transmitter];
        //    if (DBConnected)
        //    {
        //        MySqlDataReader reader = ExecuteQuery("select `ItemID`,`Amount` from `playeritem` where `CharacterName`='" + characterName + "'");
        //        if (reader != null)
        //        {
        //            while (reader.Read())
        //            {
        //                msg.m_ItemID = int.Parse(reader.GetValue(0).ToString());
        //                msg.m_Amount = int.Parse(reader.GetValue(1).ToString());
        //                transmitter.Send(msg);
        //            }

        //            reader.Dispose();
        //        }
        //    }
        //}

        void OnChangeCharacterMessage(CTransmitter transmitter, CChangeCharacterMessage msg)
        {
            if (DBConnected)
            {
                string dbCmd;
                if (msg.m_ChangeCommand == true)
                {
                    dbCmd = "insert into characterinfo(`CharacterName`,`Job`) values('" + msg.m_CharacterName + "','" + msg.m_Job + "')";
                }
                else
                {
                    dbCmd = "delete from characterinfo where `CharacterName`='" + msg.m_CharacterName + "'";
                }

                ExecuteNonQuery(dbCmd);
            }
            else
            {
                if (msg.m_ChangeCommand == true)
                {
                    characterList.Add(new CharacterInfo(msg.m_CharacterName, msg.m_Job));
                }
                else
                {
                    CharacterInfo[] tmpList = new CharacterInfo[characterList.Count];
                    characterList.CopyTo(tmpList);
                    foreach (var info in tmpList)
                    {
                        if(info.characterName == msg.m_CharacterName)
                        {
                            characterList.Remove(info);
                            break;
                        }
                    }
                }
            }
        }
        void OnAccountLoginMessage(CTransmitter transmitter, CAccountLoginMessage msg)
        {
            msg.m_LoginResult = 1;
            transmitter.Send(msg);
        }
        void OnGetCharacterInfoMessage(CTransmitter transmitter, CGetCharacterInfoMessage msg)
        {
            CLoginMessage infoMsg = new CLoginMessage();
            infoMsg.m_LoginCommand = (int)ELoginCommand.CHARACTER_INFO;
            if (DBConnected)
            {
                MySqlDataReader reader = ExecuteQuery("select * from `CharacterInfo`");
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        infoMsg.m_Name = reader.GetValue(0).ToString();
                        infoMsg.m_Job = reader.GetValue(1).ToString();
                        infoMsg.m_Level = int.Parse(reader.GetValue(2).ToString());
                        infoMsg.m_Exp = int.Parse(reader.GetValue(3).ToString());
                        infoMsg.m_Money = int.Parse(reader.GetValue(4).ToString());
                        infoMsg.m_Pos_x = float.Parse(reader.GetValue(5).ToString());
                        infoMsg.m_Pos_y = float.Parse(reader.GetValue(6).ToString());
                        infoMsg.m_Pos_z = float.Parse(reader.GetValue(7).ToString());
                        infoMsg.m_Head = int.Parse(reader.GetValue(8).ToString());
                        infoMsg.m_Chest = int.Parse(reader.GetValue(9).ToString());
                        infoMsg.m_Legs = int.Parse(reader.GetValue(10).ToString());
                        infoMsg.m_Feet = int.Parse(reader.GetValue(11).ToString());
                        infoMsg.m_Weapon = int.Parse(reader.GetValue(12).ToString());
                        infoMsg.m_Shield = int.Parse(reader.GetValue(13).ToString());
                        transmitter.Send(infoMsg);
                    }

                    reader.Dispose();
                }
            }
            else
            {
                infoMsg.m_Level = 1;
                infoMsg.m_Exp = 0;
                infoMsg.m_Money = 0;
                infoMsg.m_Pos_x = 153;
                infoMsg.m_Pos_y = 50.1f;
                infoMsg.m_Pos_z = 133;
                foreach(var info in characterList)
                {
                    infoMsg.m_Name = info.characterName;
                    infoMsg.m_Job = info.job;
                    transmitter.Send(infoMsg);
                }
            }
        }
        void OnLoginMessage(CTransmitter transmitter, CLoginMessage msg)
        {
            switch ((ELoginCommand)msg.m_LoginCommand)
            {
                case ELoginCommand.LOGIN:
                    int count = playerCount;
                    playerCount++;
                    m_setPlayerInGame.Add(transmitter);
                    m_dicTransmitterNames.Add(transmitter, msg.m_Name);
                    m_dicTransmitterIndex.Add(transmitter, count);
                    m_dicTransmitter.Add(count, transmitter);

                    msg.m_PlayerIndex = count;
                    if (m_setPlayerInGame.Count == 1) msg.m_Host = true;
                    msg.m_LoginCommand = (int)ELoginCommand.SET_INDEX;
                    transmitter.Send(msg);
                    //SendPlayerItemMessage(transmitter);

                    Console.WriteLine("...and the client Index is: " + count);

                    msg.m_LoginCommand = (int)ELoginCommand.OTHER_LOGIN;
                    BroadcastMessage(transmitter, msg, false);
                    break;
                case ELoginCommand.OTHER_LOGIN_RETURN:
                    msg.m_PlayerIndex = m_dicTransmitterIndex[transmitter];
                    m_dicTransmitter[msg.m_TargetIndex].Send(msg);
                    break;
            }
        }

        void OnExitMessage(CTransmitter transmitter, CExitMessage msg)
        {
            transmitter.Close();

            Console.WriteLine("A client has disconnected: " + m_dicTransmitterNames[transmitter]);
            BroadcastMessage(transmitter, msg, false);

            m_setTransmitters.Remove(transmitter);
            m_dicTransmitterNames.Remove(transmitter);
        }
        
        void OnMonsterMovementMessage(CTransmitter transmitter, CMonsterMovementMessage msg)
        {
            BroadcastMessage(transmitter, msg, false);
        }
        void OnMonsterAttackMessage(CTransmitter transmitter, CMonsterAttackMessage msg)
        {
            BroadcastMessage(transmitter, msg, false);
        }
        void OnMonsterTakeDamageMessage(CTransmitter transmitter, CMonsterTakeDamageMessage msg)
        {
            BroadcastMessage(transmitter, msg, false);
        }
        void OnMonsterDieMessage(CTransmitter transmitter, CMonsterDieMessage msg)
        {
            BroadcastMessage(transmitter, msg, false);
        }
        void OnMonsterFocusMessage(CTransmitter transmitter, CMonsterFocusMessage msg)
        {
            BroadcastMessage(transmitter, msg, false);
        }
        void OnOtherLoginSetMessage(CTransmitter transmitter, COtherLoginSetMessage msg)
        {
            Console.WriteLine("傳送怪物資料");
            m_dicTransmitter[msg.m_PlayerIndex].Send(msg);
        }
        void OnMonsterActiveMessage(CTransmitter transmitter, CMonsterActiveMessage msg) {
            BroadcastMessage(transmitter, msg, false);
        }
        //PlayerMessage
        void OnPlayerLevelUpMessage(CTransmitter transmitter, CPlayerLevelUpMessage msg)
        {
            if (DBConnected)
            {
                string characterName = m_dicTransmitterNames[transmitter];
                string dbCmd = "update characterinfo set `Level`='" + msg.m_Level + "' where `CharacterName` = '" + characterName + "'";
                ExecuteNonQuery(dbCmd);
            }
            BroadcastMessage(transmitter, msg, false);
        }
        void OnPlayerAddExpMessage(CTransmitter transmitter, CPlayerAddExpMessage msg)
        {
            m_dicTransmitter[msg.m_PlayerIndex].Send(msg);
        }
        void OnPlayerDieMessage(CTransmitter transmitter, CPlayerDieMessage msg)
        {
            BroadcastMessage(transmitter, msg, false);
        }
        void OnPlayerRevivalMessage(CTransmitter transmitter, CPlayerRevivalMessage msg)
        {
            BroadcastMessage(transmitter, msg, false);
        }
        void OnPlayerChangeHpMpMessage(CTransmitter transmitter, CPlayerChangeHpMpMessage msg)
        {
            BroadcastMessage(transmitter, msg, false);
        }
        void OnPlayerMovementMessage(CTransmitter transmitter, CPlayerMovementMessage msg)
        {
            BroadcastMessage(transmitter, msg, false);
        }
        void OnPlayerJumpMessage(CTransmitter transmitter, CPlayerJumpMessage msg)
        {
            BroadcastMessage(transmitter, msg, false);
        }
        void OnPlayerEquipMessage(CTransmitter transmitter, CPlayerEquipMessage msg)
        {
            //if (DBConnected)
            //{
            //    string characterName = m_dicTransmitterNames[transmitter];
            //    string dbCmd = "update characterinfo set `Head`='" + msg.m_ObjectID_0 + "',`Chest`='" + msg.m_ObjectID_1 + 
            //        "',`Legs`='" + msg.m_ObjectID_2 + "',`Feet`='" + msg.m_ObjectID_3 + "',`Weapon`='" + msg.m_ObjectID_4 + 
            //        "',`Shield`='" + msg.m_ObjectID_5 +"' where `CharacterName` = '" + characterName + "'";
            //    ExecuteNonQuery(dbCmd);
            //}
            BroadcastMessage(transmitter, msg, false);
        }
        void OnPlayerMoneyMessage(CTransmitter transmitter, CPlayerMoneyMessage msg)
        {
            if (DBConnected)
            {
                string characterName = m_dicTransmitterNames[transmitter];
                string dbCmd = "update characterinfo set `Money`='" + msg.m_Money + "' where `CharacterName` = '" + characterName + "'";
                ExecuteNonQuery(dbCmd);
            }
        }
        //void OnPlayerItemMessage(CTransmitter transmitter, CPlayerItemMessage msg)
        //{
        //    if (DBConnected)
        //    {
        //        string characterName = m_dicTransmitterNames[transmitter];
        //        string itemID = msg.m_ItemID.ToString();
        //        string amount = msg.m_Amount.ToString();
        //        string dbCmd = "";
        //        switch ((EItemCommand)msg.m_ItemCommand)
        //        {
        //            case EItemCommand.UPDATE:
        //                dbCmd = "update playeritem set `Amount`='" + amount + "' where `CharacterName`='" + characterName + "' and `ItemID`='" + itemID + "'";
        //                break;
        //            case EItemCommand.ADD:
        //                dbCmd = "insert into playeritem(`CharacterName`,`ItemID`,`Amount`) values('" + characterName + "','" + itemID + "','" + amount + "')";
        //                break;
        //            case EItemCommand.REMOVE:
        //                dbCmd = "delete from playeritem where `CharacterName`= '" + characterName + "' and `ItemID`= '" + itemID + "'";
        //                break;
        //        }
        //        ExecuteNonQuery(dbCmd);
        //    }
        //}
        void OnPlayerTargetMessage(CTransmitter transmitter, CPlayerTargetMessage msg)
        {
            BroadcastMessage(transmitter, msg, false);
        }
        void OnPlayerCastSkillMessage(CTransmitter transmitter, CPlayerCastSkillMessage msg)
        {
            BroadcastMessage(transmitter, msg, false);
        }
        void OnPlayerTakeDamageMessage(CTransmitter transmitter, CPlayerTakeDamageMessage msg)
        {
            BroadcastMessage(transmitter, msg, false);
        }

        void OnTextMessage(CTransmitter transmitter, CTextMessage msg)
        {
            Console.WriteLine(msg.m_sName + "say:" + msg.m_sText);
            BroadcastMessage(transmitter, msg, true);
        }
        void OnTradeMessage(CTransmitter transmitter, CTradeMessage msg)
        {
            m_dicTransmitter[msg.m_TargetIndex].Send(msg);
        }
        void OnTeamMessage(CTransmitter transmitter, CTeamMessage msg)
        {
            m_dicTransmitter[msg.m_TargetIndex].Send(msg);
        }
        
        void BroadcastMessage(CTransmitter transmitter, CMessage msg, bool plusOwn)
        {
            CTransmitter[] aTransmitters = null;
            lock (m_setPlayerInGame)
            {
                aTransmitters = new CTransmitter[m_setPlayerInGame.Count];
                m_setPlayerInGame.CopyTo(aTransmitters);
            }
            if (plusOwn)
            {
                foreach (CTransmitter theTransmitter in aTransmitters)
                {
                    theTransmitter.Send(msg);
                }
            }
            else
            {
                foreach (CTransmitter theTransmitter in aTransmitters)
                {
                    if (theTransmitter != transmitter)
                    {
                        theTransmitter.Send(msg);
                    }
                }
            }
        }

        void Register(Common.CTransmitter transmitter)
        {
            transmitter.Register<CChangeCharacterMessage>(OnChangeCharacterMessage);
            transmitter.Register<CAccountLoginMessage>(OnAccountLoginMessage);
            transmitter.Register<CGetCharacterInfoMessage>(OnGetCharacterInfoMessage);
            transmitter.Register<CLoginMessage>(OnLoginMessage);
            transmitter.Register<CExitMessage>(OnExitMessage);

            transmitter.Register<CMonsterMovementMessage>(OnMonsterMovementMessage);
            transmitter.Register<CMonsterAttackMessage>(OnMonsterAttackMessage);
            transmitter.Register<CMonsterTakeDamageMessage>(OnMonsterTakeDamageMessage);
            transmitter.Register<CMonsterDieMessage>(OnMonsterDieMessage);
            transmitter.Register<CMonsterFocusMessage>(OnMonsterFocusMessage);
            transmitter.Register<COtherLoginSetMessage>(OnOtherLoginSetMessage);
            transmitter.Register<CMonsterActiveMessage>(OnMonsterActiveMessage);

            transmitter.Register<CPlayerLevelUpMessage>(OnPlayerLevelUpMessage);
            transmitter.Register<CPlayerAddExpMessage>(OnPlayerAddExpMessage);
            transmitter.Register<CPlayerDieMessage>(OnPlayerDieMessage);
            transmitter.Register<CPlayerRevivalMessage>(OnPlayerRevivalMessage);
            transmitter.Register<CPlayerChangeHpMpMessage>(OnPlayerChangeHpMpMessage);
            transmitter.Register<CPlayerMovementMessage>(OnPlayerMovementMessage);
            transmitter.Register<CPlayerJumpMessage>(OnPlayerJumpMessage);
            transmitter.Register<CPlayerEquipMessage>(OnPlayerEquipMessage);
            transmitter.Register<CPlayerMoneyMessage>(OnPlayerMoneyMessage);
            //transmitter.Register<CPlayerItemMessage>(OnPlayerItemMessage);
            transmitter.Register<CPlayerTargetMessage>(OnPlayerTargetMessage);
            transmitter.Register<CPlayerCastSkillMessage>(OnPlayerCastSkillMessage);
            transmitter.Register<CPlayerTakeDamageMessage>(OnPlayerTakeDamageMessage);
            
            transmitter.Register<CTextMessage>(OnTextMessage);
            transmitter.Register<CTeamMessage>(OnTeamMessage);
            transmitter.Register<CTradeMessage>(OnTradeMessage);
        }

        TcpListener m_theListener = null;
        HashSet<Common.CTransmitter> m_setTransmitters = new HashSet<Common.CTransmitter>();
        HashSet<CTransmitter> m_setPlayerInGame = new HashSet<CTransmitter>();
        Dictionary<Common.CTransmitter, string> m_dicTransmitterNames = new Dictionary<Common.CTransmitter, string>();
        int m_Port;

        //Transmitter對應index
        Dictionary<CTransmitter, int> m_dicTransmitterIndex = new Dictionary<CTransmitter, int>();
        //index對應Transmitter
        Dictionary<int, CTransmitter> m_dicTransmitter = new Dictionary<int, CTransmitter>();


        MySqlDataReader ExecuteQuery(string sCommand)
        {
            MySqlCommand sqlCommand = new MySqlCommand("", m_Connection);
            try
            {
                sqlCommand.CommandText = sCommand;
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();
                return dataReader;
            }
            catch (MySqlException ex)
            {
                sqlCommand.Dispose();

                String sCurrentFile = new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileName();
                int iCurrentLine = new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileLineNumber();

                Console.WriteLine("# ERR: SQLException in " + sCurrentFile + ":" + iCurrentLine);
                Console.WriteLine("# ERR: " + ex.Message);
                Console.WriteLine("# ERR: MySQL error code: " + ex.Number);
                return null;
            }
        }

        int ExecuteNonQuery(string sCommand)
        {
            MySqlCommand sqlCommand = new MySqlCommand("", m_Connection);

            try
            {
                sqlCommand.CommandText = sCommand;
                int iNumRows = sqlCommand.ExecuteNonQuery();
                sqlCommand.Dispose();
                return iNumRows;
            }
            catch (MySqlException ex)
            {
                sqlCommand.Dispose();

                String sCurrentFile = new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileName();
                int iCurrentLine = new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileLineNumber();

                Console.WriteLine("# ERR: SQLException in " + sCurrentFile + ":" + iCurrentLine);
                Console.WriteLine("# ERR: " + ex.Message);
                Console.WriteLine("# ERR: MySQL error code: " + ex.Number);
                return 0;
            }
        }
    }
}
