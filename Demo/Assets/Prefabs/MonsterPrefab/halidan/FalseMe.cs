﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FalseMe : MonoBehaviour {
    [SerializeField]
    float destroyTime;
    // Use this for initialization
    void OnEnable () {
        StartCoroutine(DestoyCount());
	}
    IEnumerator DestoyCount() {
        yield return new WaitForSeconds(destroyTime);
        gameObject.SetActive(false);
    }
}
