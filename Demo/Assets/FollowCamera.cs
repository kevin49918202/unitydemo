﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour {

    Transform cam;
    void Awake()
    {
        cam = Camera.main.transform;
    }

    void Update()
    {
        transform.rotation = Quaternion.LookRotation(cam.position - transform.position);
    }

}
