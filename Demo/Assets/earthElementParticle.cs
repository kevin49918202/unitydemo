﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class earthElementParticle : MonoBehaviour {
    [SerializeField] GameObject EDableParticle;
    bool isQuitting;
    [SerializeField] bool firstCreate = true;
    void Awake()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    private void OnEnable()
    {
        if (firstCreate == true) { return; }
        Destroy( Instantiate(EDableParticle, transform.position, transform.rotation),5f);
    }
    void OnApplicationQuit()
    {
        isQuitting = true;
    }
    public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        isQuitting = true;
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnDisable()
    {
        if (isQuitting) return;
        if (firstCreate == true) { firstCreate = false; return; }
        Destroy(Instantiate(EDableParticle, transform.position, transform.rotation), 5f);
    }

}
