﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MissionClass {
    public MissionData missionData;
    public MissionData NextMission;
    public int amount = 0;
    public int goalamount;
    public ItemData[] Reward;

    public void CheckMission()
    {
        amount += 1;

        if (amount >= goalamount)
        {
            if (Reward != null)
            {
                foreach (ItemData reward in Reward)
                {
                    Inventory.instance.AddItem(reward.objectID, 1);
                }
                Reward = null;
            }
            //if (NextMission != null)
            //{
            //    MissionManager.missionmanager.AddMission(NextMission);
            //    Debug.Log("下一個任務是 : " + NextMission);
            //}
            //else
            //{
            //    MissionUI.missionui.mission = null;
            //}
            MissionManager.missionmanager.MissionComplete(this);
            return;
        }
        MissionManager.missionmanager.OnMissionChanged(this);
    }
}

