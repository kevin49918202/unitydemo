﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Projector' with 'unity_Projector'
 
 Shader "Projector/TatooProjector"
 {
     Properties
     {
		 _Color ("Main Color", Color) = (1,1,1,1)
         _ShadowTex ("Cookie", 2D) = "white" { TexGen ObjectLinear }
     }
    
     Subshader
     {
         Tags { "RenderType"="Transparent"  "Queue"="Transparent+100"}
         Pass
         {
             ZWrite Off
             Offset -1, -1
    
             //Fog { Mode Off }
    
             ColorMask RGB
             Blend OneMinusSrcAlpha SrcAlpha
 
 
             CGPROGRAM
             #pragma vertex vert
			 #pragma multi_compile_fog
             #pragma fragment frag
             #pragma fragmentoption ARB_fog_exp2
             #pragma fragmentoption ARB_precision_hint_fastest
             #include "UnityCG.cginc"
            
             struct v2f
             {
                 float4 pos      : SV_POSITION;
                 float4 uv       : TEXCOORD0;
             };
            
                 
                 float4x4 unity_Projector;
                 
            
             v2f vert(appdata_tan v)
             {
                 v2f o;
                 o.pos = UnityObjectToClipPos (v.vertex);
                 o.uv = mul (unity_Projector, v.vertex);
                 return o;
             }

			 sampler2D _ShadowTex;
			 float4 _Color;
            
             half4 frag (v2f i) : COLOR
             {
                 half4 tex = tex2Dproj(_ShadowTex, UNITY_PROJ_COORD(i.uv));
				 tex.rgb *= _Color.rgb;
                 tex.a = 1-tex.a;
                 //if (i.uv.w < 0)
                 //{
                 //    tex = float4(0,0,0,1);
                 //}
                 return tex;
             }
             ENDCG
        
         }
     }
 }
