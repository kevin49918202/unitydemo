﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorSpell : MonoBehaviour
{
    protected CharacterStats owner;
    protected float effectRange;
    protected int damage = 1;
    protected bool hasDamage;

    [SerializeField] protected float delayTakeDamage = 2f;
    LayerMask targetLayer;
    float currentdelay;
    private void Awake()
    {
        targetLayer = 1 << LayerMask.NameToLayer("Player");
    }
    void Update() {
        Collider[] colliders = Physics.OverlapSphere(transform.position, effectRange, targetLayer);
        foreach (Collider collider in colliders)
        {
            CharacterStats stats = collider.GetComponent<CharacterStats>();
            bool critical = Random.Range(0, 100) <= owner.criticalProbability;
            stats.TakeDamage(owner.gameObject, damage, critical, "", true);
        }
    }
}
