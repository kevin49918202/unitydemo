﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimator : MonoBehaviour
{
    Animator animator;
    AnimatorStateInfo currentAnimStateInfo;
    AnimatorStateInfo nextAnimStateInfo;
    
    public bool bMove, bJump;
    public bool bGround;
    public bool bDeath;
    public float fVertical, fHorizontal, fJumpEndVertical, fJumpEndHorizontal, fMoveVertical, fMoveHorizontal;
    public int iAnimAngle;
    string attackAnimName = "Attack2H";
    string castAnimName = "CastEnd";
    [SerializeField] AudioClip slashAudio;
    [SerializeField] AudioClip walkAudio;
    [SerializeField] AudioClip walkAudio2;
    AudioSource playeraudio;
    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        playeraudio = GetComponent<AudioSource>();
        RegisterAction();
    }

    void Update()
    {
        currentAnimStateInfo = animator.GetCurrentAnimatorStateInfo(0);
        nextAnimStateInfo = animator.GetNextAnimatorStateInfo(0);

        animator.SetBool("Move", bMove);
        animator.SetBool("Ground", bGround);
        animator.SetFloat("JumpVertical", fJumpEndVertical);
        animator.SetFloat("JumpHorizontal", fJumpEndHorizontal);
        animator.SetInteger("Angle", iAnimAngle);
        animator.SetFloat("Vertical", fVertical);
        animator.SetFloat("Horizontal", fHorizontal);
        animator.SetBool("Death", bDeath);

        if (bGround)
        {
            animator.SetFloat("MoveVertical", fVertical);
            animator.SetFloat("MoveHorizontal", fHorizontal);
        }

        if (bJump)
        {
            Jump();
            bJump = false;
        }
    }

    void RegisterAction()
    {
        OtherPlayerCombat combat = GetComponent<OtherPlayerCombat>();
        OtherPlayerStats stats = GetComponent<OtherPlayerStats>();

        combat.onAttack += OnAttack;
        combat.onCastNormalSpell += OnCastNormalSpell;
        combat.onCastStart += OnCastStart;
        combat.onCastBreak += OnCastBreak;
        combat.onCastEnd += OnCastEnd;
        stats.onTakeDamage += OnWound;
    }

    void Jump()
    {
        if (!currentAnimStateInfo.IsName(attackAnimName) && !nextAnimStateInfo.IsName(attackAnimName)
            && !currentAnimStateInfo.IsName(castAnimName))
        {
            animator.SetTrigger("Jump_Upbody");
        }
        animator.SetTrigger("Jump_Lowerbody");
    }

    void OnAttack()
    {
        if (!currentAnimStateInfo.IsName(attackAnimName) && !nextAnimStateInfo.IsName(attackAnimName))
        {
            animator.SetTrigger("Attack");
        }
    }

    void OnCastNormalSpell()
    {
        animator.SetTrigger("CastNormalSpell");
    }

    void OnCastStart()
    {
        animator.SetTrigger("CastStart");
    }

    void OnCastBreak()
    {
        if (nextAnimStateInfo.IsName("CastStart") || nextAnimStateInfo.IsName("Casting") || currentAnimStateInfo.IsName("CastStart") || currentAnimStateInfo.IsName("Casting"))
        {
            animator.SetTrigger("CastBreak");
        }
    }

    void OnCastEnd()
    {
        animator.SetTrigger("CastEnd");
    }

    public void OnWound()
    {
        if (!nextAnimStateInfo.IsName("Wound") && !currentAnimStateInfo.IsName("Wound"))
        {
            animator.SetTrigger("Wound");
        }
        
    }

    void OnAttackParticle()
    {
        SpellObjectsManager.instance.CreateAttackParticle(transform);

    }
    void OnAttack2Particle()
    {
        SpellObjectsManager.instance.CreateAttack2Particle(transform);
    }
    void OnAttackSound() {
        playeraudio.PlayOneShot(slashAudio);
    }
}
