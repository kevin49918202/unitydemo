﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMotor : OtherPlayerMotor {

    Transform cam;

    void Awake()
    {
        cam = Camera.main.transform;
        stats = GetComponent<PlayerStats>();
    }

    [SerializeField] float delayTimeForSendMsg = 0.2f;
    float currentTime;

    Vector3 m_LastSendPos;
    float m_LastSendAngle;
    bool m_FollowCam;

    PlayerStats stats;

    private void FixedUpdate()
    {
        SendMovementMsg();
    }

    private void Update()
    {
        HandleAngleInput();
        HandleAngle();
        HandleInput();

        HandleMove();
        SetAnim();
    }

    protected override void HandleInput()
    {
        m_InputVertical = Input.GetAxis("Vertical");
        m_InputHorizontal = Input.GetAxis("Horizontal");

        if (controller.isGrounded && Input.GetKeyDown(KeyCode.Space))
        {
            m_InputJump = true;
            ClientApp.instance.SendPlayerJumpMessage();
        }
    }

    protected override void HandleAngleInput()
    {
        if (Input.GetMouseButton(0))
        {
            m_TmpAngle = m_LastAngle;
            m_FollowCam = false;
        }
        else if (Input.GetMouseButton(1) || m_MovePos.magnitude != 0)
            m_FollowCam = true;

        m_FollowAngle = m_FollowCam ? cam.eulerAngles.y : m_TmpAngle;
    }

    void SendMovementMsg()
    {
        //每經過delayTime發送一次Movement資訊
        if (currentTime > delayTimeForSendMsg)
        {
            m_LastSendPos = transform.position;
            m_LastSendAngle = transform.eulerAngles.y;
            ClientApp.instance.SendPlayerMovementMessage(m_LastSendPos, m_LastSendAngle, stats.currentHp, stats.level);

            currentTime -= delayTimeForSendMsg;
        }
        else
        {
            currentTime += Time.deltaTime;
        }
    }
}
