﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestController : Interactable {
    Animator chestAnim;
    bool ChestOpen;
    [SerializeField] float DestroyTime = 120f;
    [HideInInspector]public List<ItemData> ChestItem;
	// Use this for initialization
	void Start () {
        chestAnim = GetComponent<Animator>();
        Destroy(gameObject, DestroyTime);
    }

    public override void Interact()
    {
        chestAnim.SetBool("ChestActive", true);
        if (ChestItem != null) {
            foreach (ItemData chestitem in ChestItem) {
                Inventory.instance.AddItem(chestitem.objectID,1);
            }
            ChestItem = null;
            Destroy(gameObject);
        }
        StartCoroutine(CheckDistance());
    }

    public override void OnDefocused()
    {
        chestAnim.SetBool("ChestActive", false);
        base.OnDefocused();
    }

    public override void OnFocused(Transform t)
    {
        if (Vector3.Distance(t.position, transform.position) > radius) return;
        base.OnFocused(t);
    }

    IEnumerator CheckDistance()
    {
        while (player != null && IsClose())
        {
            yield return null;
        }
        OnDefocused();
    }
}
