﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class NPCMission_Controller : NPCShop_Controller
{

    MissionData m_mission;
    [SerializeField] Text MissionDescription;
    [SerializeField] Text AcceptButtonText;
    public event System.Action<CharacterStats> onMissionChanged = (a) => { };

    protected override void Awake()
    {

    }

    public override void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (ScrollView.activeSelf)
            {
                DisableShopFrame();
            }
            else {
                EnableShopFrame();
            }
        }
    }

    public void BuildMission(MissionData mission,CharacterStats characterstats) {
        if (MissionManager.missionmanager.MissionDic.ContainsKey(mission))
        {
            MissionDescription.text = MissionManager.missionmanager.DefaultMission.MissionDescription;
            AcceptButtonText.text = "提交任務";
        }
        else {
            m_mission = mission;
            MissionDescription.text = mission.MissionDescription;
            MissionDescription.text = MissionDescription.text.Replace("n", "\n");
            MissionDescription.text = MissionDescription.text.Replace('s', ' ');
            Content.sizeDelta = new Vector2(0, MissionDescription.preferredHeight+10);
        }
        ShopName_Text.text = characterstats.characterName;
        onMissionChanged(characterstats);
        ScrollView.SetActive(true);
    }


    public void AcceptMission() {
        if (m_mission != null) {
            if (MissionManager.missionmanager.MissionDic.ContainsKey(m_mission))
            {
                Debug.Log("你已經接受了任務");
                return;
            }
            MissionManager.missionmanager.AddMission(m_mission);
            DisableShopFrame();
            
            PlayerManager.instance.player.GetComponent<PlayerStats>().AddMoney(100000);
            MissionManager.missionmanager.UpdateQuestIcon();
        }
    }
    public override void EnableShopFrame()
    {
        gameObject.SetActive(true);
    }

    public override void DisableShopFrame()
    {
        ScrollView.SetActive(false);
        onMissionChanged(null);
    }
}
