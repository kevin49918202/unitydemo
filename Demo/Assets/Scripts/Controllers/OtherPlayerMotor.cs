﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherPlayerMotor : MonoBehaviour {

    protected PlayerAnimator playerAnimator;
    protected CharacterController controller;

    [SerializeField] protected float m_Speed = 7.5f;
    [SerializeField] protected float m_BackSpeed = 3.5f;
    [SerializeField] protected float m_Gravity = 5;
    [SerializeField] protected float m_JumpSpeed = 7.5f;
    [SerializeField] protected float m_JumpGravity = 0.35f;
    [SerializeField] protected float m_VelocityDead = 0.5f;

    protected float m_JumpEndVertical, m_JumpEndHorizontal, m_InputVertical, m_InputHorizontal;
    protected float m_FollowAngle, m_TmpAngle, m_LastAngle;
    protected float m_Height;

    protected int m_AnimAngle;
    protected float m_AnimAngleDead = 0.5f;
    public bool m_IsMove = false;
    protected bool m_InputJump;

    protected Vector3 m_MovePos;
    protected Vector3 m_TargetPos;

    protected float m_AngleSmoothTime = 0.08f;
    protected float m_AngleSmoothTime_Back = 0.4f;
    protected float m_CurrentAngleSmoothTime;
    protected float m_CurrentAngle;
    protected float m_AngleSmoothVelocity;

    float m_InputAngle;
    Vector3 m_InputPos;
    float m_TargetVertical;
    float m_TargetHorizontal;
    float m_SmoothTargetVerticalVelocity;
    float m_SmoothTargetHorizontalVelocity;
    [SerializeField] float m_InputSmoothTime = 0.1f;

    protected void Start()
    {
        controller = GetComponent<CharacterController>();
        playerAnimator = GetComponent<PlayerAnimator>();
        OtherPlayerStats playerStats = GetComponent<OtherPlayerStats>();
        playerStats.onCharacterDie += OnPlayerDie;
        playerStats.onCharacterReset += OnPlayerRevival;

        m_InputPos = transform.position;
    }

    void Update()
    {
        HandleAngleInput();
        HandleAngle();
        HandleInput();

        HandleMove();
        SetAnim();
    }

    protected virtual void HandleInput()
    {
        Vector3 m_InputVec = m_InputPos - transform.position;
        if(m_InputVec.magnitude > 8f)
        {
            transform.position = m_InputPos;
        }

        m_InputVec.y = 0;
        if (m_InputVec.magnitude < 0.4f)
        {
            m_InputVertical = 0;
            m_InputHorizontal = 0;
            return;
        }
        else if(m_InputVec.magnitude > 2f)
        {
            m_Speed = 7.5f;
            m_BackSpeed = 3.5f;
        }
        else
        {
            m_Speed = 7;
            m_BackSpeed = 3;
        }

        m_TargetVertical = Mathf.SmoothDamp(m_TargetVertical, Vector3.Dot(m_InputVec, transform.forward), ref m_SmoothTargetVerticalVelocity, m_InputSmoothTime);
        m_TargetHorizontal = Mathf.SmoothDamp(m_TargetHorizontal, Vector3.Dot(m_InputVec, transform.right), ref m_SmoothTargetHorizontalVelocity, m_InputSmoothTime);


        if (m_TargetVertical > -0.01f && m_TargetVertical < 0.01f)
        {
            m_InputVertical = 0;
        }
        else
        {
            m_InputVertical = Mathf.Clamp(m_TargetVertical, -1.0f, 1.0f);
        }

        m_InputHorizontal = Mathf.Clamp(m_TargetHorizontal, -1.0f, 1.0f);
    }

    protected virtual void HandleAngleInput()
    {
        m_FollowAngle = Mathf.Lerp(m_FollowAngle ,m_InputAngle, 10);
    }

    protected void HandleAngle()
    {
        m_CurrentAngleSmoothTime = (m_InputVertical != 0 || m_InputHorizontal != 0) ? m_AngleSmoothTime : m_AngleSmoothTime_Back;
        m_CurrentAngle = Mathf.SmoothDampAngle(m_CurrentAngle, m_FollowAngle, ref m_AngleSmoothVelocity, m_CurrentAngleSmoothTime);
        transform.eulerAngles = new Vector3(0, m_CurrentAngle, 0);

        if (m_CurrentAngle - m_LastAngle > m_AnimAngleDead)
            m_AnimAngle = 1;
        else if (m_CurrentAngle - m_LastAngle < -m_AnimAngleDead)
            m_AnimAngle = -1;
        else
            m_AnimAngle = 0;

        m_LastAngle = m_CurrentAngle;
    }

    protected virtual void HandleMove()
    {
        HandleHeight();
        
        Vector3 totalVec = (m_InputVertical * transform.forward + m_InputHorizontal * transform.right);
        if (totalVec.magnitude > m_VelocityDead)
        {
            if (controller.isGrounded)
                m_MovePos = totalVec.normalized * (m_InputVertical < -0.1f ? m_BackSpeed : m_Speed);
            m_IsMove = true;
        }
        else
        {
            if (controller.isGrounded)
                m_MovePos = Vector3.zero;
            m_IsMove = false;
        }

        m_TargetPos = (m_MovePos + new Vector3(0, m_Height, 0)) * Time.deltaTime;
        controller.Move(m_TargetPos);
    }

    protected void HandleHeight()
    {
        if (controller.isGrounded)
        {
            if (m_InputJump)
            {
                m_Height = m_JumpSpeed;

                playerAnimator.bJump = true;

                m_JumpEndVertical = m_InputVertical;
                m_JumpEndHorizontal = m_InputHorizontal;

                m_InputJump = false;
            }
            else
            {
                m_Height = -m_Gravity ;
            }
        }
        else
        {
            m_Height -= m_JumpGravity *60 * Time.deltaTime;
        } 
    }

    protected void SetAnim()
    {
        playerAnimator.bMove = m_IsMove;
        playerAnimator.bGround = controller.isGrounded;
        playerAnimator.fJumpEndVertical = m_JumpEndVertical;
        playerAnimator.fJumpEndHorizontal = m_JumpEndHorizontal;
        playerAnimator.iAnimAngle = m_AnimAngle;
        playerAnimator.fVertical = m_InputVertical;
        playerAnimator.fHorizontal = m_InputHorizontal;
    }

    void OnPlayerDie()
    {
        playerAnimator.bMove = false;
        playerAnimator.bGround = true;
        playerAnimator.fJumpEndVertical = 0;
        playerAnimator.fJumpEndHorizontal = 0;
        playerAnimator.iAnimAngle = 0;
        playerAnimator.fVertical = 0;
        playerAnimator.fHorizontal = 0;
        playerAnimator.bDeath = true;
        this.enabled = false;
    }

    void OnPlayerRevival()
    {
        playerAnimator.bDeath = false;
        this.enabled = true;
    }

    public void Jump()
    {
        m_InputJump = true;
    }

    public void SetInput(Vector3 pos, float angle)
    {
        m_InputPos = pos;
        m_InputAngle = angle;
    }

    public void SetPos(Vector3 pos)
    {
        transform.position = pos;
    }
}
