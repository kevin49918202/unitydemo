﻿using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    GameObject interactableObject;
    CharacterStats focus;

    Camera cam;
    GameObject tmpHitGameObject;

    public event System.Action<CharacterStats> onFocusStatsChanged = (CharacterStats nullStats) => { };
    public LayerMask hitLayer;
    LayerMask tabLayer;

    HashSet<GameObject> tabTargetHash = new HashSet<GameObject>();
    
    void Awake()
    {
        cam = Camera.main;
        hitLayer =  1 << LayerMask.NameToLayer("Monster") | 1 << LayerMask.NameToLayer("Player")
                  | 1 << LayerMask.NameToLayer("Npc")     | 1 << LayerMask.NameToLayer("Chest");
        tabLayer = 1 << LayerMask.NameToLayer("Monster");
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<PlayerManager>().onOtherPlayerOffline += OnOtherPlayerOffline;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            Collider[] colliders = Physics.OverlapBox(transform.position + transform.forward * 20, new Vector3(15, 10, 20), transform.rotation, tabLayer);
            if(colliders.Length > 0)
            {
                bool successTab = false;
                foreach (Collider collider in colliders)
                {
                    GameObject go = collider.gameObject;
                    if (go.activeSelf)
                    {
                        if (!tabTargetHash.Contains(go))
                        {
                            successTab = true;
                            SetFocus(go);
                            SetInteractalbeObject(go);
                            tabTargetHash.Add(go);
                            break;
                        }
                    }
                }
                if (!successTab)
                {
                    tabTargetHash.Clear();
                    GameObject go = colliders[0].gameObject;
                    if (go.activeSelf)
                    {
                        SetFocus(go);
                        SetInteractalbeObject(go);
                        tabTargetHash.Add(go);
                    }
                }
            }
        }

        if ((Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(0)) && !EventSystem.current.IsPointerOverGameObject())
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 200 , hitLayer))
            {
                tmpHitGameObject = hit.collider.gameObject;
            }
        }

        if ((Input.GetMouseButtonUp(1) || Input.GetMouseButtonUp(0)) && !EventSystem.current.IsPointerOverGameObject())
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 200, hitLayer))
            {
                if(hit.collider.gameObject == tmpHitGameObject)
                {
                    SetFocus(hit.collider.gameObject);
                    SetInteractalbeObject(hit.collider.gameObject);
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            RemoveInteractableObject();
            RemoveFocus();
        }
    }

    //設定交互對象
    public void SetInteractalbeObject(GameObject newFocus)
    {
        Interactable newInteractable = newFocus.GetComponent<Interactable>();
        if (newInteractable != null)
        {
            if (newFocus != interactableObject)
            {
                RemoveInteractableObject();
                interactableObject = newFocus;
            }
            newInteractable.OnFocused(transform);
        }
    }
    //移除交互對象
    void RemoveInteractableObject()
    {
        if(interactableObject != null)
        {
            Interactable oldInteractable = interactableObject.GetComponent<Interactable>();
            oldInteractable.OnDefocused();
        }
        interactableObject = null;
    }

    //鎖定目標
    public void SetFocus(GameObject newFocus)
    {
        CharacterStats newStats = newFocus.GetComponent<CharacterStats>();
        if (newStats != null && newStats != focus)
        {
            RemoveInteractableObject();
            RemoveFocus();
            focus = newStats;
            focus.onCharacterDie += OnCharacterDie;
            focus.FocusOutline();
            onFocusStatsChanged(focus);
        }
    }
    //解除目標
    public void RemoveFocus()
    {
        if (focus)
        {
            focus.onCharacterDie -= OnCharacterDie;
            focus.DeleteOutline();
            focus = null;
            onFocusStatsChanged(focus);
        }
    }

    void OnCharacterDie()
    {
        StartCoroutine(RemoveDieFocus(focus));
        focus.onCharacterDie -= OnCharacterDie;
    }

    IEnumerator RemoveDieFocus(CharacterStats stats)
    {
        yield return new WaitForSeconds(4.2f);
        if(focus == stats)
        {
            RemoveFocus();
        }
    }

    void OnOtherPlayerOffline(GameObject otherPlayer)
    {
        if (focus == null) return;
        if(otherPlayer == focus.gameObject)
        {
            RemoveFocus();
        }
    }

    public void Disable()
    {
        RemoveInteractableObject();
        RemoveFocus();
        this.enabled = false;
    }
}
