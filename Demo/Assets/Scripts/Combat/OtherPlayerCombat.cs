﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherPlayerCombat : MonoBehaviour
{
    [SerializeField] protected CharacterStats targetStats;
    protected OtherPlayerStats myStats;

    public event System.Action onAttack = () => { };
    public event System.Action onCastStart = () => { };
    public event System.Action onCastBreak = () => { };
    public event System.Action onCastEnd = () => { };
    public event System.Action onCastNormalSpell = () => { };

    [SerializeField] protected float castMotionDelay = 0.2f;
    [SerializeField] protected float cast2HMotionDelay = 0.7f;
    [SerializeField] protected float attackDelay = 0.5f;

    public SkillData currentCastingSkill { get; protected set; }

    void Start()
    {
        myStats = GetComponent<OtherPlayerStats>();
    }

    public void CastEnd()
    {
        SpellObjectsManager spellObjectManager = SpellObjectsManager.instance;
        switch (currentCastingSkill.type)
        {
            case SkillData.Type.Attack:
                StartCoroutine(DoDamage(targetStats, attackDelay, myStats.damage * currentCastingSkill.damageRatio));
                onAttack();
                break;
            case SkillData.Type.AreaSpell:
                spellObjectManager.StartCreateAreaSpell(myStats, currentCastingSkill, castMotionDelay);
                onCastEnd();
                break;
            case SkillData.Type.AreaSkill:
                spellObjectManager.StartCreateAreaSpell(myStats, currentCastingSkill, cast2HMotionDelay);
                onCastEnd();
                break;
            case SkillData.Type.TargetedSpell:
                spellObjectManager.StartCreateTargetedSpell(myStats, targetStats, currentCastingSkill, castMotionDelay);
                onCastEnd();
                break;
            case SkillData.Type.normalSpell:
                spellObjectManager.StartCreateTargetedSpell(myStats, targetStats, currentCastingSkill, castMotionDelay);
                onCastNormalSpell();
                break;
        }
    }

    public void CastBreak()
    {
        onCastBreak();
    }

    public bool CastStart(SkillData skillData)
    {
        currentCastingSkill = skillData;

        //如果不是順發法術
        if (skillData.castTime > 0)
        {
            onCastStart();
            SpellObjectsManager.instance.CreateCastingObject(myStats, currentCastingSkill);
        }

        return true;
    }

    public virtual void Target(GameObject target)
    {
        targetStats = target.GetComponent<CharacterStats>();
    }

    public virtual void UnTarget()
    {
        targetStats = null;
    }

    IEnumerator DoDamage(CharacterStats targetStats, float delay, int damage)
    {
        yield return new WaitForSeconds(delay);
        if (targetStats == null) yield break;
        if (targetStats.currentHp != 0 && myStats.currentHp != 0)
        {
            bool critical = Random.Range(0, 100) <= myStats.criticalProbability;
            targetStats.TakeDamage(gameObject, damage, critical, "", true);
        }
    }

    protected void ActionOnCastStart()
    {
        onCastStart();
    }

    protected void ActionOnCastBreak()
    {
        onCastBreak();
    }

    public void OnPlayerTargetReviece(int index)
    {
        if(index < 0)
        {
            UnTarget();
        }
        else
        {
            GameObject monster = MonsterManager.monsterInstance.GetMonsterGameObject(index);
            Target(monster);
        }  
    }

    public void OnPlayerCastSkillReviece(int command)
    {
        if (command == -1)
        {
            CastEnd();
        }
        else if (command == -2)
        {
            CastBreak();
        }
        else
        {
            SkillData skillData = UseableObjectDataManager.instance.GetSkillData(command);
            CastStart(skillData);
        }
    }
}
