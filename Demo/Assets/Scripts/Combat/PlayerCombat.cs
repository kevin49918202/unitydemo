﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : OtherPlayerCombat
{
    public SkillDataList skillDataList;
    PlayerMotor motor;

    public enum Status { Standby, Combat, CastingSkill, Dead }
    public Status status;

    float cosToAngle = 0;

    CharacterStats currentCastingTarget;
    Vector3 castingPos;
    public float currentCastingTime { get; private set; }

    void Start()
    {
        myStats = GetComponent<PlayerStats>();
        motor = GetComponent<PlayerMotor>();

        myStats.onTakeDamage += OnTakeDamage;
    }

    void LateUpdate()
    {
        switch (status)
        {
            case Status.Standby:
                StandbyStatus();
                break;
            case Status.Combat:
                CombatStatus();
                break;
            case Status.CastingSkill:
                CastingSkillStatus();
                break;
            case Status.Dead:
                DeadStatus();
                break;
        }
    }

    void StandbyStatus()
    {
        if (myStats.currentHp == 0)
        {
            status = Status.Dead;
            return;
        }
        if (targetStats != null)
        {
            status = Status.Combat;
            return;
        }
    }

    void CombatStatus()
    {
        if (myStats.currentHp == 0)
        {
            status = Status.Dead;
            return;
        }
        if (targetStats == null)
        {
            status = Status.Standby;
            return;
        }
        if (targetStats.currentHp == 0)
        {
            UnTarget();
            status = Status.Standby;
            return;
        }
    }

    void CastingSkillStatus()
    {
        if (myStats.currentHp == 0)
        {
            ActionOnCastBreak();
            status = Status.Dead;
            return;
        }
        if (CheckCastingObstacle() == false)
        {
            ClientApp.instance.SendPlayerCastSkillMessage(-2);
            ActionOnCastBreak();
            status = Status.Combat;
            return;
        }
        
        if (currentCastingTime > currentCastingSkill.castTime)
        {
            CastEnd();

            CoolDownManager.instance.StartCoolDown(currentCastingSkill);
            myStats.CostMp(currentCastingSkill.costMana);
            ClientApp.instance.SendPlayerCastSkillMessage(-1);

            status = Status.Combat;
        }
        else
        {
            currentCastingTime += Time.deltaTime;
        } 
    }

    void DeadStatus()
    {
        if (myStats.currentHp > 0)
        {
            status = Status.Standby;
        }
    }

    public bool CastSkill(SkillData skillData)
    {
        if (CheckCastObstacle(skillData) == false) return false;

        //Debug.Log("施放 " + skillData.objectName);

        currentCastingTarget = targetStats;
        castingPos = transform.position;
        currentCastingTime = 0;
        currentCastingSkill = skillData;
        ClientApp.instance.SendPlayerCastSkillMessage(skillData.objectID);
        //如果不是順發法術
        if (skillData.castTime > 0)
        {
            ActionOnCastStart();
            SpellObjectsManager.instance.CreateCastingObject(myStats, currentCastingSkill);
        }
        status = Status.CastingSkill;

        return true;
    }

    public override void Target(GameObject target)
    {
        base.Target(target);

        int monsterIndex = MonsterManager.monsterInstance.GetMonsterNumber(target);
        ClientApp.instance.SendPlayerTargetMessage(monsterIndex);
    }

    public override void UnTarget()
    {
        base.UnTarget();
        ClientApp.instance.SendPlayerTargetMessage(-1);
    }

    

    float GetDistance()
    {
        return Vector3.Distance(targetStats.transform.position, transform.position);
    }

    bool CheckFasingTarget()
    {
        Vector3 dir = targetStats.transform.position - myStats.transform.position;
        return Vector3.Dot(transform.forward, dir.normalized) > cosToAngle;
    }

    //檢查能否施法
    bool CheckCastObstacle(SkillData skillData)
    {
        if (skillData.targetNeeded)
        {
            if (targetStats == null)
            {
                Debug.Log("你沒有目標");
                FloatingTextManager.instance.CreateWarningText("你沒有目標");
                return false;
            }
            else if (!CheckFasingTarget())
            {
                Debug.Log("請面對目標");
                FloatingTextManager.instance.CreateWarningText("請面對目標");
                return false;
            }
            else if (GetDistance() > skillData.castRange + targetStats.GetModelRadius())
            {
                Debug.Log("超出距離");
                FloatingTextManager.instance.CreateWarningText("超出距離");
                return false;
            }
            else if (targetStats.currentHp == 0)
            {
                Debug.Log("目標死亡!");
                FloatingTextManager.instance.CreateWarningText("目標死亡");
                return false;
            }
        }
        
        if (motor.m_IsMove && skillData.castTime > 0)
        {
            Debug.Log("移動中");
            FloatingTextManager.instance.CreateWarningText("移動中");
            return false;
        }
        else if (status == Status.CastingSkill)
        {
            Debug.Log("忙碌中");
            return false;
        }
        else if (!CoolDownManager.instance.CheckCoolDown(skillData))
        {
            Debug.Log("技能冷卻中");
            FloatingTextManager.instance.CreateWarningText("技能冷卻中");
            return false;
        }
        else if(myStats.currentMp < skillData.costMana)
        {
            Debug.Log("魔力不足!");
            FloatingTextManager.instance.CreateWarningText("魔力不足");
            return false;
        }

        return true;
    }

    //檢查能否繼續施法
    bool CheckCastingObstacle()
    {
        if (currentCastingSkill.targetNeeded)
        {
            if (targetStats == null)
            {
                Debug.Log("你沒有目標");
                FloatingTextManager.instance.CreateWarningText("你沒有目標");
                return false;
            }
            else if (currentCastingTarget != targetStats)
            {
                Debug.Log("目標改變");
                FloatingTextManager.instance.CreateWarningText("目標改變");
                return false;
            }
            else if (GetDistance() > currentCastingSkill.castRange + targetStats.GetModelRadius())
            {
                Debug.Log("超出距離");
                FloatingTextManager.instance.CreateWarningText("超出距離");
                return false;
            }
            else if (targetStats.currentHp == 0)
            {
                Debug.Log("目標死亡!");
                FloatingTextManager.instance.CreateWarningText("目標死亡");
                return false;
            }
            else if (!CheckFasingTarget())
            {
                Debug.Log("請面對目標");
                FloatingTextManager.instance.CreateWarningText("請面對目標");
                return false;
            }
        }

        if (Vector3.Distance(castingPos, transform.position) > 0.1f && currentCastingSkill.castTime > 0)
        {
            Debug.Log("詠唱中斷!");
            return false;
        }
        
        return true;
    }

    void OnTakeDamage()
    {
        currentCastingTime -= 0.2f;
    }
}