﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AccountLoginManager : MonoBehaviour {

    public static AccountLoginManager instance { get; private set; }
    bool isConnecting;

    void Awake()
    {
        instance = this;
    }

    public void OnAccountLoginButton()
    {
        if (isConnecting) return;
        isConnecting = true;
        if (ClientApp.instance.Connect() == true)
        {
            SystemLog.instance.EnableLog("連線中");
            ClientApp.instance.SendAccountLoginMessage("", "");
        }
        else
        {
            SystemLog.instance.EnableLog("連線失敗");
            isConnecting = false;
        }
    }

    public void OnAccountLoginReceive(int loginResult)
    {
        if (loginResult == 1)
        {
            SystemLog.instance.EnableLog("登入成功");
            //讀角色場景
            SceneManage.instance.StartLoadCharacterMenuSence();
        }
        else
        {
            SystemLog.instance.EnableLog("帳號密碼錯誤");
            ClientApp.instance.Disconnect();
            isConnecting = false;
        }
    }

    public void OnExitButton()
    {
        SceneManage.instance.ExitGame();
    }
}
