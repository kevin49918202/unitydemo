﻿namespace ConsoleApp1.Common
{
    class CPlayerEquipMessage : CMessage
    {
        public CPlayerEquipMessage() : base((int)ECommand.PLAYER_EQUIP)
        {

        }

        public int m_PlayerIndex;
        public int m_ObjectID_0;
        public int m_ObjectID_1;
        public int m_ObjectID_2;
        public int m_ObjectID_3;
        public int m_ObjectID_4;
        public int m_ObjectID_5;

        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_PlayerIndex);
            _WriteToBuffer(m_ObjectID_0);
            _WriteToBuffer(m_ObjectID_1);
            _WriteToBuffer(m_ObjectID_2);
            _WriteToBuffer(m_ObjectID_3);
            _WriteToBuffer(m_ObjectID_4);
            _WriteToBuffer(m_ObjectID_5);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_PlayerIndex);
            _ReadFromBuffer(out m_ObjectID_0);
            _ReadFromBuffer(out m_ObjectID_1);
            _ReadFromBuffer(out m_ObjectID_2);
            _ReadFromBuffer(out m_ObjectID_3);
            _ReadFromBuffer(out m_ObjectID_4);
            _ReadFromBuffer(out m_ObjectID_5);
        }
    }

    //class CPlayerItemMessage : CMessage
    //{
    //    public CPlayerItemMessage() : base((int)ECommand.PLAYER_ITEM)
    //    {

    //    }

    //    public int m_ItemCommand;
    //    public int m_ItemID;
    //    public int m_Amount;

    //    public override void Serialize()
    //    {
    //        base.Serialize();
    //        _WriteToBuffer(m_ItemCommand);
    //        _WriteToBuffer(m_ItemID);
    //        _WriteToBuffer(m_Amount);
    //    }
    //    public override void Unserialize()
    //    {
    //        base.Unserialize();
    //        _ReadFromBuffer(out m_ItemCommand);
    //        _ReadFromBuffer(out m_ItemID);
    //        _ReadFromBuffer(out m_Amount);
    //    }
    //}

    class CPlayerMoneyMessage : CMessage
    {
        public CPlayerMoneyMessage() : base((int)ECommand.PLAYER_MONEY)
        {

        }

        public int m_Money;

        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_Money);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_Money);
        }
    }
}