﻿using System.Collections;
using System.Collections.Generic;

namespace ConsoleApp1.Common
{
    class CLoginMessage : CMessage
    {
        public CLoginMessage() : base((int)ECommand.LOGIN)
        {

        }

        public int m_LoginCommand;
        public bool m_Host;
        public int m_PlayerIndex;
        public int m_TargetIndex;
        public string m_Name = "";
        public string m_Job;
        public int m_Level;
        public int m_Exp;
        public int m_Money;
        public int m_CurrentHp;
        public int m_CurrentMp;
        public float m_Pos_x;
        public float m_Pos_y;
        public float m_Pos_z;
        public int m_Head = -1;
        public int m_Chest = -1;
        public int m_Legs = -1;
        public int m_Feet = -1;
        public int m_Weapon = -1;
        public int m_Shield = -1;

        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_LoginCommand);
            _WriteToBuffer(m_Host);
            _WriteToBuffer(m_PlayerIndex);
            _WriteToBuffer(m_TargetIndex);
            _WriteToBuffer(m_Name);
            _WriteToBuffer(m_Job);
            _WriteToBuffer(m_Level);
            _WriteToBuffer(m_Exp);
            _WriteToBuffer(m_Money);
            _WriteToBuffer(m_CurrentHp);
            _WriteToBuffer(m_CurrentMp);
            _WriteToBuffer(m_Pos_x);
            _WriteToBuffer(m_Pos_y);
            _WriteToBuffer(m_Pos_z);
            _WriteToBuffer(m_Head);
            _WriteToBuffer(m_Chest);
            _WriteToBuffer(m_Legs);
            _WriteToBuffer(m_Feet);
            _WriteToBuffer(m_Weapon);
            _WriteToBuffer(m_Shield);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_LoginCommand);
            _ReadFromBuffer(out m_Host);
            _ReadFromBuffer(out m_PlayerIndex);
            _ReadFromBuffer(out m_TargetIndex);
            _ReadFromBuffer(out m_Name);
            _ReadFromBuffer(out m_Job);
            _ReadFromBuffer(out m_Level);
            _ReadFromBuffer(out m_Exp);
            _ReadFromBuffer(out m_Money);
            _ReadFromBuffer(out m_CurrentHp);
            _ReadFromBuffer(out m_CurrentMp);
            _ReadFromBuffer(out m_Pos_x);
            _ReadFromBuffer(out m_Pos_y);
            _ReadFromBuffer(out m_Pos_z);
            _ReadFromBuffer(out m_Head);
            _ReadFromBuffer(out m_Chest);
            _ReadFromBuffer(out m_Legs);
            _ReadFromBuffer(out m_Feet);
            _ReadFromBuffer(out m_Weapon);
            _ReadFromBuffer(out m_Shield);
        }
    }

    class CAccountLoginMessage : CMessage
    {
        public CAccountLoginMessage() : base((int)ECommand.ACCOUNT_LOGIN)
        {

        }

        public string m_Account;
        public string m_Password;
        public int m_LoginResult;

        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_Account);
            _WriteToBuffer(m_Password);
            _WriteToBuffer(m_LoginResult);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_Account);
            _ReadFromBuffer(out m_Password);
            _ReadFromBuffer(out m_LoginResult);
        }
    }

    class CGetCharacterInfoMessage : CMessage
    {
        public CGetCharacterInfoMessage() : base((int)ECommand.GET_CHARACTER_INFO)
        {

        }

        public override void Serialize()
        {
            base.Serialize();
        }
        public override void Unserialize()
        {
            base.Unserialize();
        }
    }

    class CChangeCharacterMessage : CMessage
    {
        public CChangeCharacterMessage() : base((int)ECommand.CHANGE_CHARACTER)
        {

        }
        public bool m_ChangeCommand;
        public string m_CharacterName;
        public string m_Job;

        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_ChangeCommand);
            _WriteToBuffer(m_CharacterName);
            _WriteToBuffer(m_Job);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_ChangeCommand);
            _ReadFromBuffer(out m_CharacterName);
            _ReadFromBuffer(out m_Job);
        }
    }


}

