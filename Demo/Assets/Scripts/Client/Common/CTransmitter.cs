﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

namespace ConsoleApp1.Common
{
    class CTransmitter
    {
        public CTransmitter(TcpClient theclient = null) {
            m_theClient = theclient;
        }
        public bool Connect(string sAddress, int iPort)
        {
            m_sAddress = sAddress;
            m_iPort = iPort;

            m_theClient = new TcpClient();

            try
            {
                //IPHostEntry host = Dns.GetHostEntry(sAddress);
                //var address = (from h in host.AddressList where h.AddressFamily == AddressFamily.InterNetwork select h).First();
                //IPAddress address = null;
                /*foreach( IPAddress addr in host.AddressList )
                {
                    if( addr.AddressFamily == AddressFamily.InterNetwork )
                    {
                        address = addr;
                        break;
                    }
                }*/

                m_theClient.Connect(m_sAddress, m_iPort);
                Console.WriteLine("Connected to server: " + m_sAddress + ":" + m_iPort + "\n");

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception happened: " + e.ToString());
                return false;
            }
        }
        public bool IsConnected()
        {
            if (m_theClient.Connected == false) return false;
            try
            {
                if (m_theClient.Client.Poll(0, SelectMode.SelectRead))
                {
                    byte[] buff = new byte[1];
                    if (m_theClient.Client.Receive(buff, SocketFlags.Peek) == 0) return false;
                }
            }
            catch (Exception)
            {

                return false;
            }
            return true;
        }

        public void Register<T>(Action<CTransmitter, T> theAction) where T : CMessage, new()
        {
            T t = new T();
            m_mapCommandTypes.Add(t.m_iCommand, t.GetType());
            m_mapCommandActions.Add(t.m_iCommand, theAction);
        }

        public void Close()
        {
            if(m_theClient != null)
                m_theClient.Close();
        }
        public void Send(Common.CMessage msg)
        {
            if (!ClientApp.instance.isConnect) return;
            msg.Serialize();

            int iPacketLength;
            byte[] aPacketData = msg.SealPacketBuffer(out iPacketLength);
            m_theClient.GetStream().Write(aPacketData, 0, iPacketLength);
        }
        
        public void Run() {
            if (m_theClient.Available > 0)
            {
                _HandleReceiveMessages();
            }
        }
        void _HandleReceiveMessages()
        {
            int iNumBytes = m_theClient.Available;
            byte[] aPacketBuffer = new byte[iNumBytes];
            int iBytesRead = m_theClient.GetStream().Read(aPacketBuffer, 0, iNumBytes);
            if (iBytesRead != iNumBytes)
            {
                Console.WriteLine("Error reading stream buffer...");
                return;
            }

            int iPos = 0;

            while (iPos < iBytesRead)
            {
                int iLength, idCommand;
                CMessage.FetchHeader(out iLength, out idCommand, aPacketBuffer, iPos);
                if ((iPos+iLength) > iBytesRead) {
                    Debug.LogError("錯誤" + (iPos + iLength) + " " + iBytesRead);
                    break;
                }
                if (m_mapCommandTypes.ContainsKey(idCommand))
                {
                    Type t = m_mapCommandTypes[idCommand];
                    CMessage msg = (CMessage)Activator.CreateInstance(t);
                    msg.UnSealPacketBuffer(aPacketBuffer, iPos);
                    msg.Unserialize();

                    try
                    {
                        Delegate actionObj = m_mapCommandActions[idCommand];
                        actionObj.DynamicInvoke(new object[] { this, msg });

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }

                iPos += iLength;
            }

        }
        TcpClient m_theClient = null;
        string m_sAddress = "";
        int m_iPort;

        Dictionary<int, Type> m_mapCommandTypes = new Dictionary<int, Type>();
        Dictionary<int, Delegate> m_mapCommandActions = new Dictionary<int, Delegate>();
    }
}
