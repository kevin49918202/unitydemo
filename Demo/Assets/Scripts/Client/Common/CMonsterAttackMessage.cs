﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp1.Common
{
    class CMonsterAttackMessage : CMessage
    {
        public CMonsterAttackMessage() : base((int)ECommand.MONSTERATTACK)
        {

        }
       

        public int m_Number;

        public int SkillCommand;


        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_Number);
            _WriteToBuffer(SkillCommand);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_Number);
            _ReadFromBuffer(out SkillCommand);
        }
    }
}
