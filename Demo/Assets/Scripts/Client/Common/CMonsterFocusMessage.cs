﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp1.Common
{
    class CMonsterFocusMessage : CMessage
    {
        public CMonsterFocusMessage() : base((int)ECommand.MONSTERFOCUS)
        {

        }

        public int m_PlayerIndex = 0;

        public int m_Number;

        public bool isFocus;

        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_PlayerIndex);
            _WriteToBuffer(m_Number);
            _WriteToBuffer(isFocus);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_PlayerIndex);
            _ReadFromBuffer(out m_Number);
            _ReadFromBuffer(out isFocus);
        }
    }
}
