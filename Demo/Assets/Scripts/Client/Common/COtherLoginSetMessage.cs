﻿namespace ConsoleApp1.Common
{
    class COtherLoginSetMessage : CMessage
    {
        public COtherLoginSetMessage() : base((int)ECommand.OTHER_LOGIN_SET)
        {

        }
        public int m_PlayerIndex;
        public int m_Number;
        public float Posx;
        public float Posy;
        public float Posz;
        public int SkillCommand;

        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_Number);
            _WriteToBuffer(m_PlayerIndex);
            _WriteToBuffer(Posx);
            _WriteToBuffer(Posy);
            _WriteToBuffer(Posz);
            _WriteToBuffer(SkillCommand);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_Number);
            _ReadFromBuffer(out m_PlayerIndex);
            _ReadFromBuffer(out Posx);
            _ReadFromBuffer(out Posy);
            _ReadFromBuffer(out Posz);
            _ReadFromBuffer(out SkillCommand);
        }
    }
}