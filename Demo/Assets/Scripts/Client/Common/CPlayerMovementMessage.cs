﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp1.Common
{
    class CPlayerMovementMessage : CMessage
    {
        public CPlayerMovementMessage() : base((int)ECommand.PLAYER_MOVEMENT)
        {

        }

        public int m_PlayerIndex = 0;
        public float m_PosX, m_PosY, m_PosZ;
        public float m_Angle;
        public int m_CurrentHp;

        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_PlayerIndex);
            _WriteToBuffer(m_PosX);
            _WriteToBuffer(m_PosY);
            _WriteToBuffer(m_PosZ);
            _WriteToBuffer(m_Angle);
            _WriteToBuffer(m_CurrentHp);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_PlayerIndex);
            _ReadFromBuffer(out m_PosX);
            _ReadFromBuffer(out m_PosY);
            _ReadFromBuffer(out m_PosZ);
            _ReadFromBuffer(out m_Angle);
            _ReadFromBuffer(out m_CurrentHp);
        }
    }

    class CPlayerJumpMessage : CMessage
    {
        public CPlayerJumpMessage() : base((int)ECommand.PLAYER_JUMP)
        {

        }

        public int m_PlayerIndex;
        //public string m_Name = "";

        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_PlayerIndex);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_PlayerIndex);
        }
    }
}
