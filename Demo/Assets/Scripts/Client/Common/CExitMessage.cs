﻿using System.Collections;
using System.Collections.Generic;

namespace ConsoleApp1.Common
{
    class CExitMessage : CMessage
    {
        public CExitMessage() : base((int)ECommand.EXIT)
        {

        }

        public int m_PlayerIndex;

        public override void Serialize()
        {
            base.Serialize();
            _WriteToBuffer(m_PlayerIndex);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_PlayerIndex);
        }
    }
}

