﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp1.Common
{
    class CTextMessage : CMessage
    {
        public CTextMessage() : base((int)ECommand.TEXT) {

        }
        public override void Serialize()
        {
            base.Serialize();

            // write command
            _WriteToBuffer(m_sName);
            // write command
            _WriteToBuffer(m_sText);
        }
        public override void Unserialize()
        {
            base.Unserialize();
            _ReadFromBuffer(out m_sName);
            // read command
            _ReadFromBuffer(out m_sText);
        }
        public string m_sName = "";
        public string m_sText = "";
    }
}
