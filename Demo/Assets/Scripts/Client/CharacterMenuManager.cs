﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMenuManager : MonoBehaviour {
    public static CharacterMenuManager instance { get; private set; }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    AccountCharacterInfo mainCharacter;

    public event System.Action<AccountCharacterInfo> onCharacterInfoMessageReceive;

    public AccountCharacterInfo GetMainPlayer()
    {
        return mainCharacter;
    }

    public void SendGetCharacterInfo()
    {
        ClientApp.instance.SendGetCharacterInfoMessage();
    }

    public void SetCharacter(AccountCharacterInfo info)
    {
        mainCharacter = info;
    }

    public void OnCharacterInfoMessageReceive(string characterName, string job, int level, int exp, int money, 
        int[] equipments, Vector3 position)
    {
        AccountCharacterInfo newCharacter = new AccountCharacterInfo(characterName, job, level, exp, money, 0, 0,
            null, equipments, position);
        onCharacterInfoMessageReceive(newCharacter);
    }
}

public class AccountCharacterInfo
{
    public AccountCharacterInfo(string characterName, string job, int level, int exp, int money, int currentHp, int currentMp,
        List<int> items, int[] equipments, Vector3 position)
    {
        this.characterName = characterName;
        this.job = job;
        this.position = position;
        this.level = level;
        this.exp = exp;
        this.money = money;
        this.currentHp = currentHp;
        this.currentMp = currentMp;
        this.items = items;
        this.equipments = equipments;
    }
    public string characterName;
    public string job;
    public Vector3 position;
    public int level;
    public int exp;
    public int money;
    public int currentHp;
    public int currentMp;
    public List<int> items;
    public int[] equipments;
}
