﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ConsoleApp1.Common;
using System.IO;

public class ClientApp : MonoBehaviour {

    //singleton by 白
    static ClientApp m_Instance;
    public static ClientApp instance { get { return m_Instance; } }
    public bool host;
    private void Awake()
    {
        if(m_Instance == null)
        {
            m_Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }
        Register(m_theTransmitter);
    }

    /// <summary>
    /// 帳號，密碼
    /// </summary>
    public InputField AccountField;
    public InputField PasswordField;
    public InputField IPField;
    public InputField SendMessageField;

    //After loading Scene,This bool will be true, and you can start load your gameObject
    public bool isConnect = false;

    private void Start()
    {
        if (AccountLoginManager.instance || CharacterSence.instance) return;

        if (Connect() == true)
        {
            int[] equipments = new int[] { -1, -1, -1, -1, -1, -1 };
            AccountCharacterInfo mainPlayerInfo = new AccountCharacterInfo("沒登入用資料", "Witch", 1, 0, 9999999, 0, 0, null, equipments, new Vector3(153, 50.1f, 133));
            SendLoginMessage(mainPlayerInfo);
        }
        else
        {
            host = true;
            MonsterManager.monsterInstance.SpawnMonster();
        }
    }

    private void Update()
    {
        if (isConnect ) {
            m_theTransmitter.Run();
        }
    }

    private void OnApplicationQuit()
    {
        m_theTransmitter.Close();
    }

    public bool Connect()
    {
        var parentInfo = new System.IO.DirectoryInfo(Application.dataPath).Parent;
        FileStream fs = new FileStream(parentInfo + "/ServerIp.txt", FileMode.Open);
        StreamReader sr = new StreamReader(fs);
        string sAddress = sr.ReadLine();
        if (System.Text.RegularExpressions.Regex.Match(sAddress, @"//").Success) {
            sAddress = sr.ReadLine();
        }
        int iPort = int.Parse(sr.ReadLine());
        fs.Close();
        sr.Close();

        if (m_theTransmitter.Connect(sAddress, iPort) == false) {
            Debug.Log("Connect to server failed!");
            return false;
        }
        Debug.Log("Connect Succesful");
        isConnect = true;
        return true;
    }
    public void Disconnect()
    {
        m_theTransmitter.Close();
        isConnect = false;
    }

    public void SendChangeCharacterMessage(bool cmd, string characterName, string job)
    {
        CChangeCharacterMessage msg = new CChangeCharacterMessage();
        msg.m_ChangeCommand = cmd;
        msg.m_CharacterName = characterName;
        msg.m_Job = job;
        m_theTransmitter.Send(msg);
    }
    public void SendAccountLoginMessage(string account, string password)
    {
        CAccountLoginMessage msg = new CAccountLoginMessage();
        msg.m_Account = account;
        msg.m_Password = password;
        m_theTransmitter.Send(msg);
    }
    public void SendGetCharacterInfoMessage()
    {
        CGetCharacterInfoMessage msg = new CGetCharacterInfoMessage();
        m_theTransmitter.Send(msg);
    }
    public void SendLoginMessage(AccountCharacterInfo info)
    {
        CLoginMessage msg = new CLoginMessage();
        msg.m_LoginCommand = (int)ELoginCommand.LOGIN;
        msg.m_Name = info.characterName;
        msg.m_Job = info.job;
        msg.m_Level = info.level;
        msg.m_Pos_x = info.position.x;
        msg.m_Pos_y = info.position.y;
        msg.m_Pos_z = info.position.z;
        msg.m_Head = info.equipments[0];
        msg.m_Chest = info.equipments[1];
        msg.m_Legs = info.equipments[2];
        msg.m_Feet = info.equipments[3];
        msg.m_Weapon = info.equipments[4];
        msg.m_Shield = info.equipments[5];
        m_theTransmitter.Send(msg);
    }
    public void SendOtherLoginReturnMessage(int targetIndex)
    {
        AccountCharacterInfo info = PlayerManager.instance.GetPlayerInfo();
        CLoginMessage msg = new CLoginMessage();
        msg.m_LoginCommand = (int)ELoginCommand.OTHER_LOGIN_RETURN;
        msg.m_PlayerIndex = PlayerManager.instance.myIndex;
        msg.m_TargetIndex = targetIndex;
        msg.m_Name = info.characterName;
        msg.m_Job = info.job;
        msg.m_Level = info.level;
        msg.m_Pos_x = info.position.x;
        msg.m_Pos_y = info.position.y;
        msg.m_Pos_z = info.position.z;
        msg.m_Head = info.equipments[0];
        msg.m_Chest = info.equipments[1];
        msg.m_Legs = info.equipments[2];
        msg.m_Feet = info.equipments[3];
        msg.m_Weapon = info.equipments[4];
        msg.m_Shield = info.equipments[5];

        m_theTransmitter.Send(msg);
    }

    public void SendMonsterMovementMessage(int monsterNumber,Vector3 pos, float angle,int m_currenthealth/*,bool CanAttack*/)
    {
        CMonsterMovementMessage msg = new CMonsterMovementMessage();
        msg.m_PosX = pos.x;
        msg.m_PosZ = pos.z;
        msg.m_Angle = angle;
        msg.m_currenthealth = m_currenthealth;
        msg.m_Number = monsterNumber;
        //IsAttack = CanAttack

        m_theTransmitter.Send(msg);
    }
    public void SendMonsterAttackAnimMessage(int Number,int AttackCommand)
    {
        CMonsterAttackMessage msg = new CMonsterAttackMessage();
        msg.m_Number = Number;
        msg.SkillCommand = AttackCommand;
        m_theTransmitter.Send(msg);
    }
    public void SendMonsterFocusMessage(int Number,int playerNumber) {
        CMonsterFocusMessage msg = new CMonsterFocusMessage();
        msg.isFocus = true;
        msg.m_Number = Number;
        msg.m_PlayerIndex = playerNumber;
        m_theTransmitter.Send(msg);
        Debug.Log("SendFocus");
    }
    public void SendMonsterDisFocusMessage(int Number, bool isFocus)
    {
        CMonsterFocusMessage msg = new CMonsterFocusMessage();
        msg.isFocus = isFocus;
        msg.m_Number = Number;
        m_theTransmitter.Send(msg);
        Debug.Log("SenddisFocus");
    }
    public void SendMonsterTakeDamageMessage(GameObject monster, GameObject player, int damage, bool critical, string damageType) {
        CMonsterTakeDamageMessage msg = new CMonsterTakeDamageMessage();
        msg.m_PlayerIndex = PlayerManager.instance.GetPlayerIndex(player);
        msg.m_MonsterNumber = MonsterManager.monsterInstance.GetMonsterNumber(monster);
        msg.m_Damage = damage;
        msg.m_Critical = critical;
        msg.m_DamageType = damageType == "燃燒"? 1:0;

        m_theTransmitter.Send(msg);
    }
    public void SendMonsterDieMessage(GameObject monster)
    {
        CMonsterDieMessage msg = new CMonsterDieMessage();
        msg.m_MonsterNumber = MonsterManager.monsterInstance.GetMonsterNumber(monster);
        m_theTransmitter.Send(msg);
    }
    public void SendOtherLoginSetMessage(int m_PlayerIndex, int MonsterNumber, Vector3 Pos,int SkillCommand) {
        COtherLoginSetMessage msg = new COtherLoginSetMessage();
        msg.Posx = Pos.x;
        msg.Posy = Pos.y;
        msg.Posz = Pos.z;
        msg.m_PlayerIndex = m_PlayerIndex;
        msg.m_Number = MonsterNumber;
        msg.SkillCommand = SkillCommand;
        m_theTransmitter.Send(msg);
        Debug.Log("傳出怪物資料");
    }
    public void SendMonsterActiveMessage(int Number,bool active) {
        CMonsterActiveMessage msg = new CMonsterActiveMessage();
        msg.Active = active ? 1 : 0;
        msg.m_Number = Number;
        m_theTransmitter.Send(msg);
    }


    public void SendPlayerLevelUpMessage()
    {
        CPlayerLevelUpMessage msg = new CPlayerLevelUpMessage();
        msg.m_PlayerIndex = PlayerManager.instance.myIndex;
        msg.m_Level = PlayerManager.instance.player.GetComponent<PlayerStats>().level;
        m_theTransmitter.Send(msg);
    }
    public void SendPlayerAddExpMessage(GameObject player, int exp)
    {
        CPlayerAddExpMessage msg = new CPlayerAddExpMessage();
        msg.m_PlayerIndex = PlayerManager.instance.GetPlayerIndex(player);
        msg.m_Exp = exp;
        m_theTransmitter.Send(msg);
    }
    public void SendPlayerDieMessage()
    {
        CPlayerDieMessage msg = new CPlayerDieMessage();
        msg.m_PlayerIndex = PlayerManager.instance.myIndex;
        m_theTransmitter.Send(msg);
    }
    public void SendPlayerRevivalMessage()
    {
        CPlayerRevivalMessage msg = new CPlayerRevivalMessage();
        msg.m_PlayerIndex = PlayerManager.instance.myIndex;
        m_theTransmitter.Send(msg);
    }
    public void SendPlayerChangeHpMpMessage(int currentHp, int currentMp)
    {
        CPlayerChangeHpMpMessage msg = new CPlayerChangeHpMpMessage();
        msg.m_PlayerIndex = PlayerManager.instance.myIndex;
        msg.m_CurrentHp = currentHp;
        msg.m_CurrentMp = currentMp;
        m_theTransmitter.Send(msg);
    }
    public void SendPlayerMovementMessage(Vector3 pos, float angle, int currentHp, int level)
    {
        CPlayerMovementMessage msg = new CPlayerMovementMessage();
        msg.m_PlayerIndex = PlayerManager.instance.myIndex;
        msg.m_PosX = pos.x;
        msg.m_PosY = pos.y;
        msg.m_PosZ = pos.z;
        msg.m_Angle = angle;
        msg.m_CurrentHp = currentHp;

        m_theTransmitter.Send(msg);
    }
    public void SendPlayerJumpMessage()
    {
        CPlayerJumpMessage msg = new CPlayerJumpMessage();
        msg.m_PlayerIndex = PlayerManager.instance.myIndex;
        m_theTransmitter.Send(msg);
    }
    public void SendPlayerEquipMessage(int[] objectIDs)
    {
        CPlayerEquipMessage msg = new CPlayerEquipMessage();
        msg.m_PlayerIndex = PlayerManager.instance.myIndex;
        msg.m_ObjectID_0 = objectIDs[0];
        msg.m_ObjectID_1 = objectIDs[1];
        msg.m_ObjectID_2 = objectIDs[2];
        msg.m_ObjectID_3 = objectIDs[3];
        msg.m_ObjectID_4 = objectIDs[4];
        msg.m_ObjectID_5 = objectIDs[5];

        m_theTransmitter.Send(msg);
    }
    public void SendPlayerMoneyMessage(int money)
    {
        CPlayerMoneyMessage msg = new CPlayerMoneyMessage();
        msg.m_Money = money;
        m_theTransmitter.Send(msg);
    }
    //public void SendPlayerItemMessage(EItemCommand cmd, int itemID, int amount)
    //{
    //    CPlayerItemMessage msg = new CPlayerItemMessage();
    //    msg.m_ItemCommand = (int)cmd;
    //    msg.m_ItemID = itemID;
    //    msg.m_Amount = amount;

    //    m_theTransmitter.Send(msg);
    //}
    public void SendPlayerTakeDamageMessage(GameObject player, GameObject monster, int damage, bool critical)
    {
        CPlayerTakeDamageMessage msg = new CPlayerTakeDamageMessage();
        msg.m_PlayerIndex = PlayerManager.instance.GetPlayerIndex(player);
        msg.m_MonsterNumber = monster == null? -1:MonsterManager.monsterInstance.GetMonsterNumber(monster);
        msg.m_Damage = damage;
        msg.m_Critical = critical;

        m_theTransmitter.Send(msg);
    }
    public void SendPlayerTargetMessage(int targetIndex)
    {
        CPlayerTargetMessage msg = new CPlayerTargetMessage();
        msg.m_PlayerIndex = PlayerManager.instance.myIndex;
        msg.m_TargetIndex = targetIndex;

        m_theTransmitter.Send(msg);
    }
    public void SendPlayerCastSkillMessage(int skillCommand)
    {
        CPlayerCastSkillMessage msg = new CPlayerCastSkillMessage();
        msg.m_PlayerIndex = PlayerManager.instance.myIndex;
        msg.m_SkillCommand = skillCommand;

        m_theTransmitter.Send(msg);
    }
    
    public void SendTradeMessage(int targetIndex, int tradeCommand, int itemID, int itemAmount, int moneyValue)
    {
        CTradeMessage msg = new CTradeMessage();
        msg.m_PlayerIndex = PlayerManager.instance.myIndex;
        msg.m_TargetIndex = targetIndex;
        msg.m_TradeCommand = tradeCommand;
        msg.m_ItemID = itemID;
        msg.m_ItemAmount = itemAmount;
        msg.m_MoneyValue = moneyValue;

        m_theTransmitter.Send(msg);
    }
    public void SendTeamMessage(GameObject player, GameObject target, int teamCommand)
    {
        int playerIndex = PlayerManager.instance.GetPlayerIndex(player);
        int targetIndex = PlayerManager.instance.GetPlayerIndex(target);
        CTeamMessage msg = new CTeamMessage();
        msg.m_PlayerIndex = playerIndex;
        msg.m_TargetIndex = targetIndex;
        msg.m_TeamCommand = teamCommand;

        m_theTransmitter.Send(msg);
    }
    public void SendMessageBoard(string MessageText)
    {
         CTextMessage msgText = new CTextMessage();
         msgText.m_sName = PlayerManager.instance.player.GetComponent<PlayerStats>().characterName;
         msgText.m_sText = MessageText;
         m_theTransmitter.Send(msgText);
    }

    //LoginMessage
    void OnAccountLoginMessage(CTransmitter transmitter, CAccountLoginMessage msg)
    {
        AccountLoginManager.instance.OnAccountLoginReceive(msg.m_LoginResult);
    }
    void OnLoginMessage(CTransmitter transmitter, CLoginMessage msg)
    {
        int[] equipments = new int[] { msg.m_Head, msg.m_Chest, msg.m_Legs, msg.m_Feet, msg.m_Weapon, msg.m_Shield };
        switch ((ELoginCommand)msg.m_LoginCommand)
        {
            case ELoginCommand.SET_INDEX:
                Debug.Log("Your Index is : " + msg.m_PlayerIndex + " Host:" + msg.m_Host);
                host = msg.m_Host;
                PlayerManager.instance.SetMyIndex(msg.m_PlayerIndex);
                MonsterManager.monsterInstance.SpawnMonster();
                break;
            case ELoginCommand.OTHER_LOGIN:
            case ELoginCommand.OTHER_LOGIN_RETURN:
                Debug.Log("player " + msg.m_PlayerIndex + " login");
                AccountCharacterInfo returnInfo = new AccountCharacterInfo(msg.m_Name, msg.m_Job, msg.m_Level, 0, 0,
                     msg.m_CurrentHp, msg.m_CurrentMp, null, equipments, new Vector3(msg.m_Pos_x, msg.m_Pos_y, msg.m_Pos_z));
                PlayerManager.instance.AddOtherPlayer(msg.m_PlayerIndex, returnInfo);
                break;
            case ELoginCommand.CHARACTER_INFO:
                CharacterMenuManager.instance.OnCharacterInfoMessageReceive(msg.m_Name, msg.m_Job, msg.m_Level, msg.m_Exp, msg.m_Money,
                equipments, new Vector3(msg.m_Pos_x, msg.m_Pos_y, msg.m_Pos_z));
                break;
        }

        if ((ELoginCommand)msg.m_LoginCommand == ELoginCommand.OTHER_LOGIN)
        {
            if (host)
            {
                //可能爆傳導致當機
                Debug.Log("傳出怪物資訊");
                //MonsterManager.monsterInstance.SendMonsterState(msg.m_PlayerIndex);
            }
            SendOtherLoginReturnMessage(msg.m_PlayerIndex);
        }
    }
    void OnExitMessage(CTransmitter transmitter, CExitMessage msg)
    {
        Debug.Log("player offline");
        PlayerManager.instance.RemoveOtherPlayer(msg.m_PlayerIndex);
    }

    //MonsterMessage
    void OnMonsterMovementMessage(CTransmitter transmitter, CMonsterMovementMessage msg)
    {
        MonsterManager.monsterInstance.OnMonsterMovementRecieve(msg.m_Number,new Vector3(msg.m_PosX,0,msg.m_PosZ),msg.m_Angle,msg.m_currenthealth/*,msg.IsAttack*/);
    }
    void OnMonsterFocusMessage(CTransmitter transmitter,CMonsterFocusMessage msg) {
        MonsterManager.monsterInstance.OnMonsterFocus(msg.m_Number,msg.m_PlayerIndex,msg.isFocus);
        //MonsterManager.monsterInstance.DetectEnemyIcon(MonsterManager.monsterInstance.monsterPool[msg.m_Number].m_Data);
    }
    void OnMonsterAttackMessage(CTransmitter transmitter, CMonsterAttackMessage msg)
    {
        MonsterManager.monsterInstance.OnMonsterATR(msg.m_Number,msg.SkillCommand);
    }
    void OnMonsterTakeDamageMessage(CTransmitter transmitter, CMonsterTakeDamageMessage msg) {
        Debug.Log(msg.m_MonsterNumber + "號 受到了" + msg.m_Damage + "點傷害");
        GameObject monster = MonsterManager.monsterInstance.GetMonsterGameObject(msg.m_MonsterNumber);
        GameObject player = PlayerManager.instance.GetPlayerGameObject(msg.m_PlayerIndex);
        string damageType = msg.m_DamageType == 1 ? "燃燒" : "";
        monster.GetComponent<CharacterStats>().TakeDamage(player, msg.m_Damage, msg.m_Critical, damageType, false);
    }
    void OnMonsterDieMessage(CTransmitter transmitter, CMonsterDieMessage msg)
    {
        GameObject monster = MonsterManager.monsterInstance.GetMonsterGameObject(msg.m_MonsterNumber);
        monster.GetComponent<AIStats>().OnMonsterDieReceive();
    }
    void OnOtherLoginSetMessage(CTransmitter transmitter,COtherLoginSetMessage msg) {
        //MonsterManager.monsterInstance.monsterPool[msg.m_Number].transform.position = new Vector3(msg.Posx, msg.Posy, msg.Posz);
        //MonsterManager.monsterInstance.OnMonsterATR(msg.m_Number, msg.SkillCommand);
        //Debug.Log("接收怪物資料");
    }
    void OnMonsterActiveMessage(CTransmitter transmitter, CMonsterActiveMessage msg) {
        MonsterManager.monsterInstance.monsterPool[msg.m_Number].gameObject.SetActive((msg.Active == 1)? true: false);
        //Debug.Log("Active = "+((msg.Active == 1) ? true : false));
    }

    //PlayerMessage
    void OnPlayerLevelUpMessage(CTransmitter transmitter, CPlayerLevelUpMessage msg)
    {
        GameObject player = PlayerManager.instance.GetPlayerGameObject(msg.m_PlayerIndex);
        player.GetComponent<OtherPlayerStats>().OnLevelUpReceive(msg.m_Level);
    }
    void OnPlayerAddExpMessage(CTransmitter transmitter, CPlayerAddExpMessage msg)
    {
        GameObject player = PlayerManager.instance.player;
        player.GetComponent<PlayerStats>().AddExp(msg.m_Exp);
    }
    void OnPlayerDieMessage(CTransmitter transmitter, CPlayerDieMessage msg)
    {
        GameObject player = PlayerManager.instance.GetPlayerGameObject(msg.m_PlayerIndex);
        player.GetComponent<OtherPlayerStats>().Die();
    }
    void OnPlayerRevivalMessage(CTransmitter transmitter, CPlayerRevivalMessage msg)
    {
        GameObject player = PlayerManager.instance.GetPlayerGameObject(msg.m_PlayerIndex);
        Revival.instance.RevivalPlayer(player);
    }
    void OnPlayerChangeHpMpMessage(CTransmitter transmitter, CPlayerChangeHpMpMessage msg)
    {
        GameObject player = PlayerManager.instance.GetPlayerGameObject(msg.m_PlayerIndex);
        player.GetComponent<OtherPlayerStats>().OnChangeHpMpReceive(msg.m_CurrentHp, msg.m_CurrentMp);
    }
    void OnPlayerMovementMessage(CTransmitter transmitter, CPlayerMovementMessage msg)
    {
        Vector3 pos = new Vector3(msg.m_PosX, msg.m_PosY, msg.m_PosZ);
        float angle = msg.m_Angle;
        PlayerManager.instance.GetPlayerMotor(msg.m_PlayerIndex).SetInput(pos, angle);
        OtherPlayerStats stats = PlayerManager.instance.GetPlayerStats(msg.m_PlayerIndex);
        stats.SetHp(msg.m_CurrentHp);
    }
    void OnPlayerJumpMessage(CTransmitter transmitter, CPlayerJumpMessage msg)
    {
        PlayerManager.instance.GetPlayerMotor(msg.m_PlayerIndex).Jump();
    }
    void OnPlayerEquipMessage(CTransmitter transmitter, CPlayerEquipMessage msg)
    {
        GameObject player = PlayerManager.instance.GetPlayerGameObject(msg.m_PlayerIndex);
        int[] objects = new int[] { msg.m_ObjectID_0, msg.m_ObjectID_1, msg.m_ObjectID_2, msg.m_ObjectID_3, msg.m_ObjectID_4, msg.m_ObjectID_5 };
        player.GetComponent<OtherPlayerEquipment>().SetEquipment(objects);
    }
    //void OnPlayerItemMessage(CTransmitter transmitter, CPlayerItemMessage msg)
    //{
    //    Inventory.instance.OnPlayerItemMessageReceive(msg.m_ItemID, msg.m_Amount);
    //}
    void OnPlayerTakeDamageMessage(CTransmitter transmitter, CPlayerTakeDamageMessage msg)
    {
        GameObject player = PlayerManager.instance.GetPlayerGameObject(msg.m_PlayerIndex);
        GameObject monster = msg.m_MonsterNumber == -1?null :MonsterManager.monsterInstance.GetMonsterGameObject(msg.m_MonsterNumber);
        player.GetComponent<CharacterStats>().TakeDamage(monster, msg.m_Damage, msg.m_Critical, "", false);
    }
    void OnPlayerTargetMessage(CTransmitter transmitter, CPlayerTargetMessage msg)
    {
        GameObject player = PlayerManager.instance.GetPlayerGameObject(msg.m_PlayerIndex);
        player.GetComponent<OtherPlayerCombat>().OnPlayerTargetReviece(msg.m_TargetIndex);
    }
    void OnPlayerCastSkillMessage(CTransmitter transmitter, CPlayerCastSkillMessage msg)
    {
        GameObject player = PlayerManager.instance.GetPlayerGameObject(msg.m_PlayerIndex);
        player.GetComponent<OtherPlayerCombat>().OnPlayerCastSkillReviece(msg.m_SkillCommand);
    }
    //InteractiveMessage
    void OnTextMessage(CTransmitter transmitter, CTextMessage msg)
    {
        ChatBoxUIFunc.chatboxuifunc.AcceptMessageToBoard(msg.m_sName, msg.m_sText);
    }
    void OnTradeMessage(CTransmitter transmitter, CTradeMessage msg)
    {
        TradeFrame.instance.OnTradeMessageReceive(msg.m_PlayerIndex, msg.m_TradeCommand, msg.m_ItemID, msg.m_ItemAmount, msg.m_MoneyValue);
    }
    void OnTeamMessage(CTransmitter transmitter, CTeamMessage msg)
    {
        GameObject player = PlayerManager.instance.GetPlayerGameObject(msg.m_PlayerIndex);
        TeamManager.instance.OnTeamMessageReceive(player, msg.m_TeamCommand);
    }

    void Register(CTransmitter transmitter)
    {
        transmitter.Register<CAccountLoginMessage>(OnAccountLoginMessage);
        transmitter.Register<CLoginMessage>(OnLoginMessage);
        transmitter.Register<CExitMessage>(OnExitMessage);

        transmitter.Register<CMonsterMovementMessage>(OnMonsterMovementMessage);
        transmitter.Register<CMonsterAttackMessage>(OnMonsterAttackMessage);
        transmitter.Register<CMonsterTakeDamageMessage>(OnMonsterTakeDamageMessage);
        transmitter.Register<CMonsterDieMessage>(OnMonsterDieMessage);
        transmitter.Register<CMonsterFocusMessage>(OnMonsterFocusMessage);
        transmitter.Register<COtherLoginSetMessage>(OnOtherLoginSetMessage);
        transmitter.Register<CMonsterActiveMessage>(OnMonsterActiveMessage);

        transmitter.Register<CPlayerLevelUpMessage>(OnPlayerLevelUpMessage);
        transmitter.Register<CPlayerAddExpMessage>(OnPlayerAddExpMessage);
        transmitter.Register<CPlayerDieMessage>(OnPlayerDieMessage);
        transmitter.Register<CPlayerRevivalMessage>(OnPlayerRevivalMessage);
        transmitter.Register<CPlayerChangeHpMpMessage>(OnPlayerChangeHpMpMessage);
        transmitter.Register<CPlayerMovementMessage>(OnPlayerMovementMessage);
        transmitter.Register<CPlayerJumpMessage>(OnPlayerJumpMessage);
        transmitter.Register<CPlayerEquipMessage>(OnPlayerEquipMessage);
        //transmitter.Register<CPlayerItemMessage>(OnPlayerItemMessage);
        transmitter.Register<CPlayerTakeDamageMessage>(OnPlayerTakeDamageMessage);
        transmitter.Register<CPlayerTargetMessage>(OnPlayerTargetMessage);
        transmitter.Register<CPlayerCastSkillMessage>(OnPlayerCastSkillMessage);

        transmitter.Register<CTextMessage>(OnTextMessage);
        transmitter.Register<CTeamMessage>(OnTeamMessage);
        transmitter.Register<CTradeMessage>(OnTradeMessage);
    }
    CTransmitter m_theTransmitter = new CTransmitter();   
}

class ServerIp
{
    string serverIp;
    string port;
}