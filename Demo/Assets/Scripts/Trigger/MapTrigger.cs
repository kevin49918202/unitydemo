﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;
using UnityEngine.Audio;
public class MapTrigger : MonoBehaviour
{
    [SerializeField] string mapname;
    [SerializeField] MiniMap minimap;

    public event System.Action onTrigger = () => { };
    public event System.Action resetTrigger = () => { };
    bool IsTrigger = false;
    public GameObject Timeline;
    public bool canTrig;
    Camera cam;
    [SerializeField]AudioClip m_Clip;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == PlayerManager.instance.player)
        {
            if (m_Clip != null)
            {
                StartCoroutine(AudioManager.audiomanager.ChangeAudio(m_Clip));
            }
            minimap.SetMapName(mapname);
            if (IsTrigger == false)
            {
                onTrigger();
                IsTrigger = true;
                if (Timeline != null)
                {
                    Timeline.SetActive(true);
                }
            }
        }
    }
    public void ResetTrigger() {
        IsTrigger = false;
        if (Timeline != null) {
            Timeline.SetActive(false);
        }
        resetTrigger();
    }
}
