﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RitualCounter : MonoBehaviour
{
    public float RitualSpeed;
    float currentRitualCount;
    Material ritualMat;
    MapTrigger maptrig;
    public bool RitualStop = false;
    private void Start()
    {
        ritualMat = GetComponent<Renderer>().material;
        maptrig = GetComponentInParent<MapTrigger>();
        maptrig.onTrigger += RitualTrig;
        maptrig.resetTrigger += ResetRitual;
        this.gameObject.SetActive(false);
    }
    void RitualTrig()
    {
        MonsterManager.monsterInstance.SpawnEarth_Element();
        MonsterManager.monsterInstance.DisBug();
        gameObject.SetActive(true);
        if (ritualMat == null)
        {
            return;
        }
        currentRitualCount = RitualSpeed;
        StartCoroutine(RitualCountFunc());
    }
    IEnumerator RitualCountFunc()
    {
        ritualMat.SetFloat("_Alpha", 1);
        RitualStop = false;
        while (currentRitualCount > 0 && RitualStop == false)
        {
            if ((float)(currentRitualCount / RitualSpeed) > 0.5)
            {
                currentRitualCount -= Time.deltaTime * 2;
            }
            else {
                currentRitualCount -= Time.deltaTime / 2;
            }
            ritualMat.SetFloat("_SliceAmount", (float)(currentRitualCount/ RitualSpeed));
            ritualMat.SetFloat("_HDR", (1 + (float)(1 - currentRitualCount / RitualSpeed) * 39));
            yield return 0;
        }
        if (RitualStop == false)
        {
            PlayerManager.instance.player.GetComponent<CharacterStats>().TakeDamage(null, 99999, true, "", true);
        }
        else if (RitualStop == true)
        {
            float alpha = ritualMat.GetFloat("_Alpha");
            while (alpha >= 0) {
                alpha -= Time.deltaTime/3;
                ritualMat.SetFloat("_Alpha",alpha);
                yield return 0;
            }
        }

    }
    void ResetRitual() {
        RitualStop = true;
        ritualMat.SetFloat("_SliceAmount", 1);
        ritualMat.SetFloat("_Alpha",1);
        Debug.Log("ResetRitual");
    }
}
