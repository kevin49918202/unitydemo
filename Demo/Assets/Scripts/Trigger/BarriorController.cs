﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarriorController : MonoBehaviour {
    MeshRenderer barriorMat;
    MapTrigger maptrig;
    void Start () {
        barriorMat = GetComponent<MeshRenderer>();
        maptrig = GetComponentInParent<MapTrigger>();
        maptrig.resetTrigger += ResetBarrior;
    }
	
    public void StartClose()
    {
        if(gameObject.activeSelf == true)
            StartCoroutine(BarriorClose());
    }
	// Update is called once per frame
    public IEnumerator BarriorClose() {
        float barroirFade = barriorMat.material.GetFloat("_InvFade");
        barroirFade -= Time.deltaTime;
        barriorMat.material.SetFloat("_InvFade", barroirFade);
        yield return new WaitForFixedUpdate();
        if (barroirFade > 0)
        {
            StartCoroutine(BarriorClose());
        }
        else {
            gameObject.SetActive(false);
        }
    }
    private void ResetBarrior() {
        gameObject.SetActive(true);
        barriorMat.material.SetFloat("_fade", 1);
    }
}
