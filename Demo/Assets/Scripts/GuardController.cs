﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GuardController : MonoBehaviour {

    NavMeshAgent agent;
    Animator animator;

    [SerializeField] Transform[] patrolPoints;
    int pointIndex=0;
    int nextIndexDir;
    float agentSpeed = 2;
    bool hasSetLookAroundTrigger;
    

    float waitTime = 10;
    float currentTime;
    

	// Use this for initialization
	void Start ()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        transform.position = patrolPoints[pointIndex].position;
    }

    // Update is called once per frame
    void Update ()
    {
        if (agent.remainingDistance < 0.1f)
        {
            //在頭尾設LookAroundTrigger,且只設一次
            if ((pointIndex == 0 || pointIndex == patrolPoints.Length - 1) && !hasSetLookAroundTrigger)
            {
                animator.SetTrigger("LookAround");
                hasSetLookAroundTrigger = true;
            }

            //在頭尾等waitTime,如果不是頭尾直接設下一點
            if (currentTime > waitTime || (pointIndex != 0 && pointIndex != patrolPoints.Length - 1)) 
            {
                Vector3 nextPoint = GetNextPatrolPoint();

                agent.SetDestination(nextPoint);

                hasSetLookAroundTrigger = false;
                currentTime = 0;
            }
            else
            {
                currentTime += Time.deltaTime;
            }
        }
            
        animator.SetFloat("Speed", agent.velocity.magnitude); //velocity(速度)
    }

    //Vector3 GetNextPatrolPoint()
    //{
    //    if (pointIndex == 0)
    //    {
    //        wanderBack = false;
    //    }
    //    if (pointIndex == patrolPoints.Length - 1)
    //        wanderBack = true;

    //    if (wanderBack == true)
    //    {
    //        pointIndex--;
    //    }
    //    else
    //    {
    //        pointIndex++;
    //    }

    //    return patrolPoints[pointIndex].position;
    //}

    Vector3 GetNextPatrolPoint()
    {
        if (pointIndex == 0)
            nextIndexDir = 1;
        else if(pointIndex == patrolPoints.Length - 1)
            nextIndexDir = -1;

        pointIndex += nextIndexDir;

        return patrolPoints[pointIndex].position;
    }

    public void StopPatrol()
    {
        agent.speed = 0;
    }
    public void Patrol()
    {
        agent.speed = agentSpeed;
    }

    void OnDrawGizmosSelected()
    {
        if(patrolPoints != null && patrolPoints.Length >= 2)
        {
            Gizmos.color = Color.blue;
            for (int i = 0; i < patrolPoints.Length - 1; i++)
            {
                Gizmos.DrawLine(patrolPoints[i].position, patrolPoints[i + 1].position);
            }
        }
    }
}
