﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UseableObjectDataManager : MonoBehaviour {

    public static UseableObjectDataManager instance { get; private set; }

    void Awake()
    {
        instance = this;
        RegisterItemData();
        RegisterItemType();
    }

    Dictionary<int, Type> m_dictItemTypes = new Dictionary<int, Type>();
    Dictionary<int, ItemData> m_dictItemData = new Dictionary<int, ItemData>();
    Dictionary<int, SkillData> m_dictSkillData = new Dictionary<int, SkillData>();
    Dictionary<string, int> m_dictItemID = new Dictionary<string, int>();
    [SerializeField] ItemDataList itemDataList;
    [SerializeField] SkillDataList skillDataList;

    public ItemData GetItemData(int objectID)
    {
        if (m_dictItemData.ContainsKey(objectID))
        {
            return m_dictItemData[objectID];
        }
        else
        {
            return null;
        }
    }
    public EquipmentData GetEquipmentData(int objectID)
    {
        if (m_dictItemData.ContainsKey(objectID))
        {
            ItemData data = m_dictItemData[objectID];
            if (data.GetType() == typeof(EquipmentData))
                return (EquipmentData)data;
            else
                return null;
        }
        else
        {
            return null;
        }
    }
    public SkillData GetSkillData(int objectID)
    {
        return m_dictSkillData[objectID];
    }

    //根據itemID建立對應ItemClass回傳
    public Item CreateItem(int itemID)
    {
        Type t = m_dictItemTypes[itemID];
        Item newItem = (Item)Activator.CreateInstance(t);
        newItem.data = m_dictItemData[itemID];
        return newItem;
    }

    //建立TypeDict(Key:ID, Value:Type)
    void RegisterType<T>(int itemID) where T : Item, new()
    {
        T t = new T();
        m_dictItemTypes.Add(itemID, t.GetType());
    }

    //註冊所有ItemClass
    void RegisterItemType()
    {
        RegisterType<RedPotion>(m_dictItemID["RedPotion"]);
        RegisterType<BluePotion>(m_dictItemID["BluePotion"]);
        RegisterType<Equipment>(m_dictItemID["Head_1"]); 
        RegisterType<Equipment>(m_dictItemID["Head_2"]);
        RegisterType<Equipment>(m_dictItemID["Chest_1"]);
        RegisterType<Equipment>(m_dictItemID["Chest_2"]);
        RegisterType<Equipment>(m_dictItemID["Legs_1"]);
        RegisterType<Equipment>(m_dictItemID["Legs_2"]);
        RegisterType<Equipment>(m_dictItemID["Feet_1"]);
        RegisterType<Equipment>(m_dictItemID["Feet_2"]);
        RegisterType<Equipment>(m_dictItemID["Weapon_1"]);
        RegisterType<Equipment>(m_dictItemID["Weapon_2"]);
        RegisterType<Equipment>(m_dictItemID["Staff_1"]);
        RegisterType<Equipment>(m_dictItemID["Staff_2"]);
        RegisterType<Equipment>(m_dictItemID["Shield_1"]);
        RegisterType<Equipment>(m_dictItemID["Shield_2"]);
    }

    //註冊ItemData
    void RegisterItemData()
    {
        foreach (ItemData data in itemDataList.dataList)
        {
            m_dictItemData.Add(data.objectID, data);
            m_dictItemID.Add(data.name, data.objectID);
        }
        foreach (SkillData data in skillDataList.dataList)
        {
            m_dictSkillData.Add(data.objectID, data);
        }
    }
}
