﻿using UnityEngine;

[CreateAssetMenu(fileName = "Mission", menuName = "UseableObject/Mission")]
public class MissionData : UseableObjectData
{
    public string MissionDescription;
    public GameObject[] target;
    public int Amount;
    public ItemData[] Reward;
    public MissionData NextMission;
    
    public override void Use()
    {
        Inventory.instance.UseItem(objectID);
    }
}
