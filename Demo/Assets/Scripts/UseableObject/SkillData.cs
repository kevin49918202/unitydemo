﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New SkillData", menuName = "UseableObject/SkillData")]
public class SkillData : UseableObjectData {

    public float castTime = 0;//詠唱時間
    public float castRange = 15;//施放範圍
    public float effectRange = 0;//影響範圍
    public int costMana = 0;//魔力消耗
    public int damageRatio;//乘上damage當做傷害
    public int levelNeeded;
    public bool targetNeeded;
    
    public enum Type {Attack, normalSpell, TargetedSpell, AreaSpell, AreaSkill }
    public Type type;

	public override void Use()
    {
        PlayerManager.instance.player.GetComponent<PlayerCombat>().CastSkill(this);
    }
}
