﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New EquiptmentData" , menuName = "UseableObject/EquipmentData")]
public class EquipmentData : ItemData {

    public EquipmentSlotIndex EquipSlot;

    public int hpModifier;
    public int mpModifier;
    public int armorModifier;
    public int damageModifier;
    public int criticalModifier;
}

public enum EquipmentSlotIndex { Head , Chest, Legs, Feet, Weapon, Shield };
