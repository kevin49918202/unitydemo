﻿using UnityEngine;

[CreateAssetMenu(fileName = "New ItemData", menuName = "UseableObject/ItemData")]
public class ItemData : UseableObjectData
{
    public int shopCost;
    public int sealValue;
    public int stackLimit = 1;

    public override void Use()
    {
        Inventory.instance.UseItem(objectID);
    }
}
