﻿using UnityEngine;

public abstract class UseableObjectData : ScriptableObject
{
    public string objectName = "New Object";
    public string description = "This is a UseableObject";
    public Sprite icon = null;

    public int objectID;
    public float coolDownTime;

    public abstract void Use();
}