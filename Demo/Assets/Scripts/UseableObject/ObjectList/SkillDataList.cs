﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Create/SkillDataList")]
public class SkillDataList : ScriptableObject
{
    public List<SkillData> dataList = new List<SkillData>();
}