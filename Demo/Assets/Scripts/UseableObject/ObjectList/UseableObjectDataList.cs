﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Create/UseableObjectDataList")]
public class UseableObjectDataList : ScriptableObject
{
    public List<UseableObjectData> dataList = new List<UseableObjectData>();
}
