﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create/ItemDataList")]
public class ItemDataList : ScriptableObject
{
    public List<ItemData> dataList = new List<ItemData>();
}
