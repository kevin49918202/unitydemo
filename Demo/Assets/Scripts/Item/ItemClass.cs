﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item {

    public ItemData data;
    public int amount = 1;

    public abstract bool Use();
}

public class RedPotion : Item
{
    float value = 0.5f;

    public override bool Use()
    {
        PlayerStats playerStats = PlayerManager.instance.player.GetComponent<PlayerStats>();
        if (playerStats.currentHp > 0)
        {
            playerStats.AddHp((int)(playerStats.maxHp * value));
            return true;
        }

        return false;
    }
}

public class BluePotion : Item
{
    float value = 0.5f;

    public override bool Use()
    {
        PlayerStats playerStats = PlayerManager.instance.player.GetComponent<PlayerStats>();
        if (playerStats.currentMp > 0)
        {
            playerStats.AddMp((int)(playerStats.maxMp * value));
            return true;
        }

        return false;
    }
}


