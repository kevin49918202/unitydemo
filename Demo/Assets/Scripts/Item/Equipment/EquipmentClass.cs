﻿public class Equipment : Item
{
    public override bool Use()
    {
        PlayerManager.instance.player.GetComponent<PlayerEquipment>().Equip((EquipmentData)data);
        return true;
    }
}


