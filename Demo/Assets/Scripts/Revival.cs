﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Revival : MonoBehaviour {

    public static Revival instance { get; private set; }
    private void Awake()
    {
        instance = this;
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<PlayerManager>().onMainPlayerCreated += OnMainPlayerCreated;
    }

    [SerializeField] Transform revivalPoint;
    [SerializeField] GameObject revivalUI_View;
    GameObject player;
    public bool isEnableUI;
    public event System.Action onEnableUI = () => {};

    void OnMainPlayerCreated(GameObject player)
    {
        this.player = player;
        player.GetComponent<PlayerStats>().onCharacterDie += StartEnableUI;
    }

    public void StartEnableUI()
    {
        Invoke("EnableUI", 3);
    }

    void EnableUI()
    {
        revivalUI_View.SetActive(true);
        isEnableUI = true;
        onEnableUI();

        player.GetComponent<PlayerController>().Disable();
        CursorController.instance.Disable();
    }

    public void OnRevivalButton()
    {
        RevivalPlayer(PlayerManager.instance.player);
        ClientApp.instance.SendPlayerRevivalMessage();
        revivalUI_View.SetActive(false);
        isEnableUI = false;

        player.GetComponent<PlayerController>().enabled = true;
        CursorController.instance.enabled = true;
    }

    public void RevivalPlayer(GameObject otherPlayer)
    {
        otherPlayer.transform.position = revivalPoint.position;
        otherPlayer.GetComponent<OtherPlayerStats>().Reset();
    }
}
