﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaSpell : MonoBehaviour
{
    protected CharacterStats owner;
    protected float effectRange;
    protected int damage = 1;
    protected bool hasDamage;

    [SerializeField] protected float delayTakeDamage = 2f;
    [SerializeField] protected float delayDestroy = 3f;
    LayerMask targetLayer;
    float currentdelay = 100;

    protected virtual void OnEnable()
    {
        currentdelay = delayTakeDamage;
    }

    void Update()
    {
        if(currentdelay > 0)
        {
            currentdelay -= Time.deltaTime;
        }
        else
        {
            OnCollide();
            currentdelay = 100;
        }
    }

    public virtual void OnCollide()
    {
        if (owner == null)
        {
            return;
        }
            targetLayer = 1 << LayerMask.NameToLayer("Player");
        if (owner.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            Debug.Log("1");
            targetLayer = 1 << LayerMask.NameToLayer("Monster");
            ObjectPool.instance.StartDelayUnloadObject(gameObject, delayDestroy);
        }
        else if (!ClientApp.instance.host)
        {
            damage = 0;
        }

        Collider[] colliders = Physics.OverlapSphere(transform.position, effectRange, targetLayer);
        foreach (Collider collider in colliders)
        {
            CharacterStats stats = collider.GetComponent<CharacterStats>();
            bool critical = Random.Range(0, 100) <= owner.criticalProbability;
            stats.TakeDamage(owner.gameObject, damage, critical, "", true);
            TakeDot(stats);
            CreateBurning(stats);
        }
    }

    public void Set(CharacterStats owner, SkillData skillData)
    {
        this.owner = owner;
        this.damage = (int)(owner.damage * skillData.damageRatio);
        this.effectRange = skillData.effectRange;
        currentdelay = delayTakeDamage;
    }

    protected virtual void TakeDot(CharacterStats stats) { }
    protected virtual void CreateBurning(CharacterStats stats) { }
}
