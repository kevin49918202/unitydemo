﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalFire : TargetedSpell
{
    Vector3 currentVec;
    Vector3 targetHeight;
    [SerializeField] float heightForce = 0.5f;
    [SerializeField] float gravity = 5;

    protected override void CreateBurning()
    {
        SpellObjectsManager.instance.CreateFireBurning(target);
    }

    public override void Set(CharacterStats owner, CharacterStats target, SkillData skillData)
    {
        base.Set(owner, target, skillData);
        targetHeight.y = (GetTargetPos() - transform.position).magnitude * heightForce;
    }

    protected override void Move()
    {
        currentVec = (GetTargetPos() - transform.position + targetHeight).normalized;
        transform.position += currentVec * Time.deltaTime * navigateSpeed;
        if(targetHeight.y > 0)
        {
            targetHeight.y -= gravity * Time.deltaTime;
        }
        else
        {
            targetHeight.y = 0;
        }
    }

}
