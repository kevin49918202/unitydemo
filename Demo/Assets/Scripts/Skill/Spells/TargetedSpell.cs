﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetedSpell : MonoBehaviour {

    protected CharacterStats owner;
    protected CharacterStats target;
    
    protected int damage = 1;
    protected bool hasDamage;
    [SerializeField] protected float navigateSpeed = 15;
    [SerializeField] protected float collideDistance = 0.1f;

    private void OnEnable()
    {
        StartCoroutine(TrackTarget());
    }

    IEnumerator TrackTarget()
    {
        while (Vector3.Distance(GetTargetPos(), transform.position) > collideDistance)
        {
            Move();
            yield return null;
        }

        bool critical = Random.Range(0, 100) <= owner.criticalProbability;
        target.TakeDamage(owner.gameObject, damage, critical, "", true);
        TakeDot(target);
        CreateBoom();
        CreateBurning();
        ObjectPool.instance.StartDelayUnloadObject(gameObject, 1);
        yield return 0;
    }

    public virtual void Set(CharacterStats owner, CharacterStats target, SkillData skillData)
    {
        this.owner = owner;
        this.target = target;
        this.damage = (int)(owner.damage * skillData.damageRatio);
    }

    protected virtual void Move()
    {
        transform.position += (GetTargetPos() - transform.position).normalized * Time.deltaTime * navigateSpeed;
    }

    protected virtual void TakeDot(CharacterStats stats){ }

    protected virtual void CreateBoom(){ }

    protected virtual void CreateBurning(){ }

    protected Vector3 GetTargetPos()
    {
        return target.transform.position + new Vector3(0, target.GetModelRadius() / 2);
    }
}
