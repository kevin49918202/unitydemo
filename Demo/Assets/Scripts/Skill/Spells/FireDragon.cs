﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireDragon : AreaSpell {

    [SerializeField] int burningTimes = 3;

    protected override void TakeDot(CharacterStats stats)
    {
        StartCoroutine(TakeBurningDamage(stats));
    }

    protected override void CreateBurning(CharacterStats stats)
    {
        SpellObjectsManager.instance.CreateFireBurning(stats);
    }

    IEnumerator TakeBurningDamage(CharacterStats stats)
    {
        int times = burningTimes;
        while(times > 0)
        {
            yield return new WaitForSeconds(0.5f);
            bool critical = Random.Range(0, 100) <= owner.criticalProbability;
            stats.TakeDamage(owner.gameObject, damage, critical, "燃燒", true);
            times--;
        }
        yield return null;
    }
}
