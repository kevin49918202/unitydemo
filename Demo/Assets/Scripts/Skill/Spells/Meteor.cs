﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : AreaSpell {

    [SerializeField] GameObject projector;

    protected override void OnEnable()
    {
        base.OnEnable();
        projector.SetActive(true);
    }
    public override void OnCollide()
    {
        base.OnCollide();
        projector.SetActive(false);
    }
}
