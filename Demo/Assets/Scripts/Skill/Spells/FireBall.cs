﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : TargetedSpell {

    protected override void CreateBoom()
    {
        SpellObjectsManager.instance.CreateFireBoom(target);
    }

    protected override void CreateBurning()
    {
        SpellObjectsManager.instance.CreateFireBurning(target);
    }

    protected override void TakeDot(CharacterStats stats)
    {
        base.TakeDot(stats);
    }

    IEnumerator TakeBurningDamage(CharacterStats stats)
    {
        int times = 3;
        while (times > 0)
        {
            yield return new WaitForSeconds(1f);
            bool critical = Random.Range(0, 100) <= owner.criticalProbability;
            stats.TakeDamage(owner.gameObject, (int)(damage * 0.5f), critical, "燃燒", true);
            times--;
        }
        yield return null;
    }
}
