﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneMonster : AreaSpell {

    public override void OnCollide()
    {
        LayerMask targetLayer = 1 << LayerMask.NameToLayer("Player");
        if (owner.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            targetLayer = 1 << LayerMask.NameToLayer("Monster");
            Destroy(this.gameObject, delayDestroy);
        }

        Collider[] colliders = Physics.OverlapSphere(transform.position + transform.forward * effectRange, effectRange, targetLayer);
        foreach (Collider collider in colliders)
        {
            CharacterStats stats = collider.GetComponent<CharacterStats>();
            stats.TakeDamage(owner.gameObject, damage, false, "", true);
            TakeDot(stats);
            CreateBurning(stats);
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position + transform.forward * 5, 5);
    }
}
