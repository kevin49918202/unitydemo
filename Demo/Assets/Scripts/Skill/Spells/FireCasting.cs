﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireCasting : MonoBehaviour {

    OtherPlayerCombat playerCombat;
    public void Register(GameObject player)
    {
        this.playerCombat = player.GetComponent<OtherPlayerCombat>();
        if(playerCombat.GetType() == typeof(PlayerCombat))
        {
            playerCombat.onCastBreak += OnCastBreak;
            playerCombat.onCastEnd += OnCastEnd;
        }
    }

    void Unregister()
    {
        playerCombat.onCastBreak -= OnCastBreak;
        playerCombat.onCastEnd -= OnCastEnd;
    }

    void OnCastEnd()
    {
        Unregister();
        transform.SetParent(transform.parent.parent);
        ObjectPool.instance.StartDelayUnloadObject(this.gameObject, 3);
    }

    void OnCastBreak()
    {
        ParticleSystem[] particleSystems = GetComponentsInChildren<ParticleSystem>();
        foreach(ParticleSystem ps in particleSystems)
        {
            ps.Stop();
        }
        Unregister();
        transform.SetParent(transform.parent.parent);
        ObjectPool.instance.StartDelayUnloadObject(this.gameObject, 3);
    }
}
