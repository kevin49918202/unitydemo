﻿using UnityEngine;

public class ItemPickup : Interactable {

    public ItemData itemData;

    public override void Interact()
    {
        PickUp();
    }

    void PickUp()
    {
        Debug.Log("Picking up " + itemData.objectName);

        bool wasPickedUp = Inventory.instance.AddItem(itemData.objectID, 1);

        if (wasPickedUp)
            Destroy(gameObject);  
    }
}
