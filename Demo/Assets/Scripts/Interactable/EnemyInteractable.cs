﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyInteractable : Interactable{

    PlayerCombat playerCombat;
    public override void Interact()
    {
        playerCombat = player.GetComponent<PlayerCombat>();
        playerCombat.Target(gameObject);
    }

    public override void OnFocused(Transform t)
    {
        base.OnFocused(t);
    }

    public override void OnDefocused()
    {
        if (player!= null)
        {
            player.GetComponent<PlayerCombat>().UnTarget();
        }
        
        base.OnDefocused();
    }
}
