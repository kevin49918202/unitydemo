﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardInteractable : Interactable {

    bool sayingHi = false;
    float sayHiTime = 5;
    Quaternion targetRotation;
    float rotationSmoothSpeed = 5;

    void Update()
    {
        if (sayingHi)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSmoothSpeed * Time.deltaTime);
        }
    }

    public override void Interact()
    {
        if (!IsClose()) return;
        if (!sayingHi)
        {
            StartCoroutine(SayHi());
        }
    }

    IEnumerator SayHi()
    {
        targetRotation = Quaternion.LookRotation(player.position - transform.position);
        sayingHi = true;

        GuardController controller = GetComponent<GuardController>();
        controller.StopPatrol();
        GetComponent<Animator>().SetTrigger("SayHi");

        yield return new WaitForSeconds(sayHiTime);

        controller.Patrol();
        sayingHi = false;
    }
}
