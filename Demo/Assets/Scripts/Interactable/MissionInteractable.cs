﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionInteractable : Interactable
{
    NPCMission_Controller missionController;
    [SerializeField] MissionData m_mission;

    Quaternion targetRotation;
    float rotationSmoothSpeed = 5;

    void Awake()
    {
        //if (false) {
            missionController = GameObject.Find("Canvas/Mission_Panel").GetComponent<NPCMission_Controller>();
        //}
    }

    void Update()
    {
        //if (false) {
            if (player != null)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSmoothSpeed * Time.deltaTime);
                if (!IsClose())
                {
                    missionController.DisableShopFrame();
                    player = null;
                }
            }
        //}
    }

    public override void OnFocused(Transform t)
    {
        if (Vector3.Distance(t.position, transform.position) > radius) return;
        base.OnFocused(t);
    }

    public override void OnDefocused()
    {
    }

    public override void Interact()
    {
        targetRotation = Quaternion.LookRotation(player.position - transform.position);
        if (MissionManager.missionmanager.MissionDic.ContainsKey(m_mission)) {
            Debug.Log("你已經接受任務了");
            return;
        }
        missionController.BuildMission(m_mission, GetComponent<CharacterStats>());
        GetComponent<Animator>().SetTrigger("Talk");
        //shopController.BuildShop(shopList, GetComponent<CharacterStats>());
    }
}
