﻿using System.Collections.Generic;
using UnityEngine;

public abstract class Interactable : MonoBehaviour {
    public float radius = 3f;

    public Transform interactTransform;

    protected Transform player;

    public abstract void Interact();

    public virtual void OnFocused(Transform t)
    {
        player = t;
        Interact();
    }

    public virtual void OnDefocused()
    {
        player = null;
    }

    protected bool IsClose()
    {
        return Vector3.Distance(player.position, transform.position) < radius;
    }

    //protected void OnMouseOver()
    //{
    //    Transform player = PlayerManager.instance.player.transform;  
    //    if (Vector3.Distance(player.position, transform.position) < radius)
    //    {
    //        CursorController.instance.SetCursor(gameObject.tag, true);
    //    }
    //    else
    //    {
    //        CursorController.instance.SetCursor(gameObject.tag, false);
    //    }
    //}
    //protected void OnMouseExit()
    //{
    //    CursorController.instance.SetDefaultCursor();
    //}

    void OnDrawGizmosSelected()
    {
        if (interactTransform == null)
            interactTransform = transform; 
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
