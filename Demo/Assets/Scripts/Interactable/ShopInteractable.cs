﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopInteractable : Interactable {

    NPCShop_Controller shopController;
    [SerializeField] ItemDataList shopList;

    Quaternion targetRotation;
    float rotationSmoothSpeed = 5;

    void Awake()
    {
        shopController = GameObject.Find("Canvas/Shop_Panel").GetComponent<NPCShop_Controller>();
    }

    void Update()
    {
        if (player != null)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSmoothSpeed * Time.deltaTime);
            if (!IsClose())
            {
                shopController.DisableShopFrame();
                player = null;
            }
        }
    }

    public override void OnFocused(Transform t)
    {
        if (Vector3.Distance(t.position, transform.position) > radius) return;
        base.OnFocused(t);
    }

    public override void OnDefocused()
    {
    }

    public override void Interact()
    {
        targetRotation = Quaternion.LookRotation(player.position - transform.position);
        shopController.BuildShop(shopList, GetComponent<CharacterStats>());
        GetComponent<Animator>().SetTrigger("Talk");
    }
}
