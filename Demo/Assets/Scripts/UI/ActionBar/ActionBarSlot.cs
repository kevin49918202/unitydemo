﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ActionBarSlot : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    , IBeginDragHandler, IDragHandler, IEndDragHandler, IDropHandler
{
    [SerializeField] KeyCode keycode;
    [SerializeField] Image object_Image;
    [SerializeField] Image coolDown_Image;
    [SerializeField] Image highlight_Image;
    [SerializeField] Text coolDown_Text;
    [SerializeField] Text amount_Text;
    [SerializeField] Image unusable_Image;

    public int slotNumber;
    bool StartCountTimer = false;
    public UseableObjectData useableObject { get; private set; }

    Vector3 startPosition;
    Transform startParent;
    [SerializeField] Transform itemBeingDragged;
    [SerializeField] Transform top_Panel;

    bool isRunningUpdateUi = false;

    IEnumerator UpdateUi()
    {
        isRunningUpdateUi = true;
        while(useableObject != null)
        {
            if (Input.GetKeyDown(keycode))
            {
                Highlight();
                if(PlayerManager.instance.player.GetComponent<PlayerStats>().currentHp > 0)
                {
                    useableObject.Use();
                }
            }
            else if (Input.GetKeyUp(keycode))
            {
                Unhighlight();
            }

            if (StartCountTimer)
            {
                if (!CoolDownManager.instance.CheckCoolDown(useableObject))
                {
                    float coolDownCount = CoolDownManager.instance.GetTimeCount(useableObject);
                    coolDown_Image.fillAmount = coolDownCount / useableObject.coolDownTime;
                    coolDown_Text.text = (int)coolDownCount + "s";
                }
                else
                {
                    EndCoolDown();
                }
            }

            yield return null;
        }

        isRunningUpdateUi = false;
    }

    public void AddObject(UseableObjectData useableObject)
    {
        this.useableObject     = useableObject;
        object_Image.sprite    = useableObject.icon;
        coolDown_Image.sprite  = useableObject.icon;
        object_Image.enabled   = true;
        coolDown_Image.enabled = true;
        unusable_Image.enabled = false;

        RegisterCheckAmount();
        CheckObjectAmount();
        StartCoolDown();

        if(!isRunningUpdateUi)
            StartCoroutine(UpdateUi());
    }

    public void RemoveObject()
    {
        UnregisterCheckAmount();

        this.useableObject     = null;
        object_Image.sprite    = null;
        coolDown_Image.sprite  = null;
        object_Image.enabled   = false;
        coolDown_Image.enabled = false;
        unusable_Image.enabled = false;

        amount_Text.text = "";
        EndCoolDown();
    }

    public void StartCoolDown()
    {
        StartCountTimer = true;
    }

    void EndCoolDown()
    {
        coolDown_Image.fillAmount = 0;
        coolDown_Text.text = "";
        StartCountTimer = false;
    }

    void RegisterCheckAmount()
    {
        if (useableObject.GetType() == typeof(ItemData))
        {
            Inventory.instance.onItemChanged += CheckObjectAmount;
        }
    }

    void OnEnable()
    {
        if(useableObject!= null)
        {
            if (isRunningUpdateUi)
            {
                StopAllCoroutines();
            }
            StartCoroutine(UpdateUi());
        }
    }

    void UnregisterCheckAmount()
    {
        if (useableObject.GetType() == typeof(ItemData))
        {
            Inventory.instance.onItemChanged -= CheckObjectAmount;
        }
    }

    void CheckObjectAmount()
    {
        if (useableObject.GetType() == typeof(ItemData))
        {
            int amount = Inventory.instance.GetItemAmount(useableObject.objectID);
            if (amount == 0)
            {
                amount_Text.text = "";
                unusable_Image.enabled = true;
            }
            else
            {
                amount_Text.text = amount.ToString();
                unusable_Image.enabled = false;
            }
        }
    }

    void Highlight()
    {
        highlight_Image.enabled = true;
    }

    void Unhighlight()
    {
        highlight_Image.enabled = false;
    }

    //Mouse事件
    public void OnPointerClick(PointerEventData eventData)
    {
        if (useableObject == null) return;
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            useableObject.Use();
        }
        else if(eventData.button == PointerEventData.InputButton.Middle)
        {
            GetComponentInParent<ActionBarManager>().RemoveObjectFromSlot(slotNumber);
            DataInfoUI.instance.Disable(DataInfoUI.EFrameType.ActionBar);
        }
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        highlight_Image.enabled = true;
        if(useableObject != null)
        {
            DataInfoUI.instance.Enable(useableObject, DataInfoUI.EFrameType.ActionBar);
        }
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        highlight_Image.enabled = false;
        DataInfoUI.instance.Disable(DataInfoUI.EFrameType.ActionBar);
    }

    //拖曳事件
    public void OnBeginDrag(PointerEventData eventData)
    {
        startPosition = itemBeingDragged.position;
        startParent = transform;
        itemBeingDragged.SetParent(top_Panel);
    }
    public void OnDrag(PointerEventData eventData)
    {
        itemBeingDragged.position = Input.mousePosition;
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        if (itemBeingDragged.parent == top_Panel)
        {
            itemBeingDragged.position = startPosition;
            itemBeingDragged.SetParent(startParent);
            itemBeingDragged.SetAsFirstSibling();
        }
    }
    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log(eventData.pointerDrag.name + " Drop to : " + gameObject.name);
        InventorySlot draggedInventorySlot = eventData.pointerDrag.GetComponent<InventorySlot>();
        ActionBarSlot draggedActionBarSlot = eventData.pointerDrag.GetComponent<ActionBarSlot>();
        SkillBookSlot draggedSkillBookSlot = eventData.pointerDrag.GetComponent<SkillBookSlot>();
        //從物品拖曳到ActionBar
        if (draggedInventorySlot != null)
        {
            if (draggedInventorySlot.item == null) return;

            GetComponentInParent<ActionBarManager>().AddObjectToSlot(slotNumber, draggedInventorySlot.item.data);
            return;
        }
        //從ActionBar拖曳到ActionBar
        else if (draggedActionBarSlot != null)
        {
            if (draggedActionBarSlot.useableObject == null) return;
            GetComponentInParent<ActionBarManager>().Swap(slotNumber, draggedActionBarSlot.slotNumber);
            return;
        }
        //從SkillBook拖曳到ActionBar
        else if (draggedSkillBookSlot != null)
        {
            if (draggedSkillBookSlot.skillData == null) return;
            GetComponentInParent<ActionBarManager>().AddObjectToSlot(slotNumber, draggedSkillBookSlot.skillData);
            return;
        }
    }
}
