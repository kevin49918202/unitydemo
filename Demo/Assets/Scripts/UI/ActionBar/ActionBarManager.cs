﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ActionBarManager : MonoBehaviour {

    public GameObject slotsParent;
    ActionBarSlot[] actionBarSlots;
    UseableObjectData[] actionBarSavedArray;

    Dictionary<UseableObjectData, ActionBarSlot> dictObjectToSlot = new Dictionary<UseableObjectData, ActionBarSlot>();
    // Use this for initialization
    void Awake()
    {
        actionBarSlots = slotsParent.GetComponentsInChildren<ActionBarSlot>();
        for (int i = 0; i < actionBarSlots.Length; i++)
        {
            //設定各欄位編號
            actionBarSlots[i].slotNumber = i;
        }

        actionBarSavedArray = new UseableObjectData[actionBarSlots.Length];
        GameObject gameManager = GameObject.Find("GameManager");
        gameManager.GetComponent<CoolDownManager>().onCoolDownStart += OnObjectCoolDownStart;
        gameManager.GetComponent<PlayerManager>().onMainPlayerCreated += OnMainPlayerCreated;
    }

    void OnMainPlayerCreated(GameObject player)
    {
        //List<SkillData> list = player.GetComponent<PlayerCombat>().skillDataList.dataList;
        //for (int i = 0; i < list.Count; i++)
        //{
        //    AddObjectToSlot(i, list[i]);
        //}
    }

    void OnObjectCoolDownStart(UseableObjectData useableObject)
    {
        if(dictObjectToSlot.ContainsKey(useableObject))
            dictObjectToSlot[useableObject].StartCoolDown();
    }

    public void AddObjectToSlot(int index, UseableObjectData useableObject)
    {
        //如果丟進來的物件=null 清空欄位
        if (useableObject == null)
        {
            RemoveObjectFromSlot(index);
            return;
        }

        //如果已經有欄位存放此物件
        if (dictObjectToSlot.ContainsKey(useableObject))
        {
            //清除原欄位物件
            RemoveObjectFromSlot(dictObjectToSlot[useableObject].slotNumber);
        }

        //如果欄位已經有物件
        if(actionBarSlots[index].useableObject != null)
        {
            RemoveObjectFromSlot(index);
        }

        actionBarSlots[index].AddObject(useableObject);
        dictObjectToSlot.Add(useableObject, actionBarSlots[index]);
    }

    public void RemoveObjectFromSlot(int index)
    {
        UseableObjectData useableObject = actionBarSlots[index].useableObject;
        if(useableObject != null)
        {
            actionBarSlots[index].RemoveObject();
            dictObjectToSlot.Remove(useableObject);
        }   
    }

    public void Swap(int indexA, int indexB)
    {
        UseableObjectData objectA = actionBarSlots[indexA].useableObject;
        UseableObjectData objectB = actionBarSlots[indexB].useableObject;

        AddObjectToSlot(indexA, objectB);
        AddObjectToSlot(indexB, objectA);
    }

    public void OnChangeActionBarButton()
    {
        //把預存列Array暫存下來
        UseableObjectData[] tmp = new UseableObjectData[actionBarSavedArray.Length];
        actionBarSavedArray.CopyTo(tmp,0);

        for (int i = 0; i < actionBarSavedArray.Length; i++)
        {
            //把顯示列現有物件存進另ㄧ列Array
            actionBarSavedArray[i] = actionBarSlots[i].useableObject;
        }

        for (int i = 0; i < actionBarSavedArray.Length; i++)
        {
            //暫存的物件放進顯示列
            AddObjectToSlot(i, tmp[i]);
        }
    }
}
