﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataInfoUI : MonoBehaviour {

    public static DataInfoUI instance { get; private set; }

    void Awake()
    {
        instance = this;
    }

    [SerializeField] GameObject dataInfo_View;
    [SerializeField] RectTransform background;
    [SerializeField] RectTransform background2;
    [SerializeField] Text dataInfo_Text;
    [SerializeField] Vector2 spacing;
    Vector3 cursorOffset;
    Vector3 totalOffset;

    Dictionary<EquipmentSlotIndex, string> dictEquipmentType = new Dictionary<EquipmentSlotIndex, string>();
    event System.Action updateUI = () => { };

    public enum EFrameType { Inventory, SkillBook, Shop, EquipmentUI, Trade, ActionBar, MenuButton}
    EFrameType currentFrameType;

    void Update()
    {
        updateUI();
    }

    void Start()
    {
        cursorOffset = new Vector3((float)Screen.width / 30, (float)Screen.height / 30);
        dictEquipmentType.Add(EquipmentSlotIndex.Chest, "胸甲");
        dictEquipmentType.Add(EquipmentSlotIndex.Feet, "鞋子");
        dictEquipmentType.Add(EquipmentSlotIndex.Head, "頭盔");
        dictEquipmentType.Add(EquipmentSlotIndex.Legs, "護腿");
        dictEquipmentType.Add(EquipmentSlotIndex.Shield, "盾牌");
        dictEquipmentType.Add(EquipmentSlotIndex.Weapon, "武器");
    }

    public void Enable(UseableObjectData objectData, EFrameType frameType)
    {
        currentFrameType = frameType;
        dataInfo_Text.text = objectData.objectName + "\n\n" + objectData.description;

        if (objectData.GetType() == typeof(EquipmentData))
        {
            EquipmentData data = (EquipmentData)objectData;
            dataInfo_Text.text += "\n\n<" + dictEquipmentType[data.EquipSlot] + ">";
            dataInfo_Text.text += "\n  <color=#00FF41FF>裝備素質</color>";
            if (data.hpModifier != 0)
                dataInfo_Text.text += "\n血量: " + data.hpModifier;
            if (data.mpModifier != 0)
                dataInfo_Text.text += "\n魔力: " + data.mpModifier;
            if (data.damageModifier != 0)
                dataInfo_Text.text += "\n攻擊力: " + data.damageModifier;
            if (data.armorModifier != 0)
                dataInfo_Text.text += "\n防禦力: " + data.armorModifier;
            if (data.criticalModifier != 0)
                dataInfo_Text.text += "\n爆擊率: " + data.criticalModifier + "%";
        }

        //對齊
        background.sizeDelta = new Vector2(dataInfo_Text.preferredWidth, dataInfo_Text.preferredHeight) + spacing;
        background2.sizeDelta = new Vector2(dataInfo_Text.preferredWidth, dataInfo_Text.preferredHeight) + spacing;
        dataInfo_Text.transform.localPosition = new Vector3((dataInfo_Text.rectTransform.sizeDelta.x - background.sizeDelta.x + spacing.x) / 2, 0);
        //background.localPosition = backgroundOffset;


        totalOffset = cursorOffset + new Vector3(background.sizeDelta.x / 2, background.sizeDelta.y / 2);

        updateUI += FollowCursor;
        dataInfo_View.SetActive(true);
    }

    public void Enable(string text, EFrameType frameType)
    {
        currentFrameType = frameType;
        dataInfo_Text.text = text;

        //對齊
        background.sizeDelta = new Vector2(dataInfo_Text.preferredWidth, dataInfo_Text.preferredHeight) + spacing;
        background2.sizeDelta = new Vector2(dataInfo_Text.preferredWidth, dataInfo_Text.preferredHeight) + spacing;
        dataInfo_Text.transform.localPosition = new Vector3((dataInfo_Text.rectTransform.sizeDelta.x - background.sizeDelta.x + spacing.x) / 2, 0);
        //background.localPosition = backgroundOffset;


        totalOffset = cursorOffset + new Vector3(background.sizeDelta.x / 2, background.sizeDelta.y / 2);

        updateUI += FollowCursor;
        dataInfo_View.SetActive(true);
    }

    public void Disable(EFrameType frameType)
    {
        if(frameType == currentFrameType)
        {
            updateUI -= FollowCursor;
            dataInfo_View.SetActive(false);
        }
    }

    void FollowCursor()
    {
        Vector3 finalOffset = totalOffset;
        Vector3 newPos = Input.mousePosition + cursorOffset + new Vector3(background.sizeDelta.x, background.sizeDelta.y);
        if (newPos.x > Screen.width)
        {
            finalOffset.x = -totalOffset.x;
        }
        if (newPos.y > Screen.height)
        {
            finalOffset.y = -totalOffset.y;
        }
        transform.position = Input.mousePosition + finalOffset;
    }
}
