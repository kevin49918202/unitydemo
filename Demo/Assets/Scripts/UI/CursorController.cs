﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CursorController : MonoBehaviour {

    static CursorController m_Instance;
    public static CursorController instance { get { return m_Instance; } }

    void Awake()
    {
        m_Instance = this;
    }

    [SerializeField] Texture2D defaultIcon;
    [SerializeField] Texture2D attackIcon;
    [SerializeField] Texture2D unableAttackIcon;
    [SerializeField] Texture2D ShopIcon;
    [SerializeField] Texture2D unableShopIcon;
    [SerializeField] Texture2D ChestIcon;
    [SerializeField] Texture2D unableChestIcon;
    [SerializeField] Texture2D MissionIcon;
    [SerializeField] Texture2D unableMissionIcon;
    Dictionary<string, Texture2D> m_dicTag2Texture = new Dictionary<string, Texture2D>();
    Dictionary<string, Texture2D> m_dicTag2UnableTexture = new Dictionary<string, Texture2D>();
    HashSet<string> m_hashSetMonster = new HashSet<string>();

    public LayerMask hitLayer;
    bool hasSetDefault = true;

    Camera cam;

    void Start()
    {
        cam = Camera.main;

        RegisterTextureDict("Enemy", attackIcon, unableAttackIcon);
        RegisterTextureDict("Bug", attackIcon, unableAttackIcon);
        RegisterTextureDict("Wolf", attackIcon, unableAttackIcon);
        RegisterTextureDict("EarthElement", attackIcon, unableAttackIcon);
        RegisterTextureDict("Boss", attackIcon, unableAttackIcon);
        RegisterTextureDict("Shop", ShopIcon, unableShopIcon);
        RegisterTextureDict("Chest", ShopIcon, unableShopIcon);
        RegisterTextureDict("Mission", MissionIcon, unableMissionIcon);

        m_hashSetMonster.Add("Enemy");
        m_hashSetMonster.Add("Bug");
        m_hashSetMonster.Add("Wolf");
        m_hashSetMonster.Add("EarthElement");
        m_hashSetMonster.Add("Boss");

        hitLayer =  1 << LayerMask.NameToLayer("Monster") | 1 << LayerMask.NameToLayer("Npc") | 1 << LayerMask.NameToLayer("Chest");
    }

    void Update()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if(Physics.Raycast(ray, out hit, 200, hitLayer))
        {
            float distance = Vector3.Distance(hit.collider.transform.position, PlayerManager.instance.player.transform.position);
            bool enable = distance < hit.collider.GetComponent<Interactable>().radius;
            if (m_hashSetMonster.Contains(tag))
            {
                if (hit.collider.GetComponent<CharacterStats>().currentHp == 0) return;
            }
            SetCursor(hit.collider.tag, enable);
        }
        else if(!hasSetDefault)
        {
            SetDefaultCursor();
        }
    }

    void RegisterTextureDict(string tag, Texture2D enableTexture, Texture2D unableTexture)
    {
        m_dicTag2Texture.Add(tag, enableTexture);
        m_dicTag2UnableTexture.Add(tag, unableTexture);
    }

    public void SetCursor(string tag, bool enable)
    {
        if (EventSystem.current.IsPointerOverGameObject()) return;


        if (m_dicTag2Texture.ContainsKey(tag))
        {
            if (enable)
            {
                Cursor.SetCursor(m_dicTag2Texture[tag], Vector2.zero, CursorMode.Auto);
            }
            else
            {
                Cursor.SetCursor(m_dicTag2UnableTexture[tag], Vector2.zero, CursorMode.Auto);
            }

            hasSetDefault = false;
        } 
    }
    
    public void SetDefaultCursor()
    {
        Cursor.SetCursor(defaultIcon, Vector2.zero, CursorMode.Auto);
        hasSetDefault = true;
    }

    public void Disable()
    {
        SetDefaultCursor();
        this.enabled = false;
    }
}
