﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddItemUi : MonoBehaviour {

    [SerializeField] Image item_Image;

    private void OnEnable()
    {
        AnimatorClipInfo[] clipInfo = GetComponent<Animator>().GetCurrentAnimatorClipInfo(0);
        Invoke("Disable", clipInfo.Length);
    }

    void Disable()
    {
        gameObject.SetActive(false);
    }

    public void AddItem(ItemData itemData)
    {
        item_Image.sprite = itemData.icon;
        gameObject.SetActive(true);
    }
}
