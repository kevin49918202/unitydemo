﻿using UnityEngine;
using UnityEngine.EventSystems;

public class InventoryUI : DraggableObject
{
    public static InventoryUI instance { get; private set; }
    public enum UIMode { BASE, TRADE }

    void Awake()
    {
        instance = this;
    }

    [SerializeField] Transform slotsParent;
    [SerializeField] GameObject inventoryView;

    MoneyObserver moneyObserver;
    Inventory inventory;
    InventorySlot[] slots;

    CanvasGroup canvasGroup;

	void Start () {
        inventory = Inventory.instance;
        canvasGroup = GetComponent<CanvasGroup>();
        moneyObserver = GetComponent<MoneyObserver>();

        slots = slotsParent.GetComponentsInChildren<InventorySlot>();
        for (int i = 0; i < slots.Length; i++)
        {
            slots[i].slotNumber = i;
        }
    }

	void Update () {
        if (Input.GetButtonDown("Inventory"))
        {
            SetUI();
        }  
	}

    public void SetUI()
    {
        if (inventoryView.activeSelf == false)
        {
            EnableUI();
        }
        else
        {
            DisableUI();
            DataInfoUI.instance.Disable(DataInfoUI.EFrameType.Inventory);
        }
    }

    public void EnableUI()
    {
        UpdateUI();
        moneyObserver.RegisterToPlayerStats();
        inventory.onItemChanged += UpdateUI;
        inventoryView.SetActive(true);
        canvasGroup.blocksRaycasts = true;
    }

    public void DisableUI()
    {
        moneyObserver.UnregisterFromPlayerStats();
        inventory.onItemChanged -= UpdateUI;
        inventoryView.SetActive(false);
        canvasGroup.blocksRaycasts = false;
    }

    void UpdateUI()
    {
        for(int i = 0; i < slots.Length; i++)
        {
            if(i < inventory.items.Length)
            {
                slots[i].UpdateSlot(inventory.items[i]);
            }
        }
    }

    public void ChangeMode(UIMode mode)
    {
        foreach(InventorySlot slot in slots)
        {
            slot.mode = mode;
        }
    }
}
