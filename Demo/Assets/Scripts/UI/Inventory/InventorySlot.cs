﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InventorySlot : MonoBehaviour, IBeginDragHandler, IDragHandler,
     IEndDragHandler, IDropHandler, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    public Item item { get; private set; }
    public int slotNumber;
    [SerializeField] Image itemIcon_Image;
    [SerializeField] Image highLight_Image;
    [SerializeField] Text amount_Text;
  
    Vector3 startPosition;
    Transform startParent;
    [SerializeField] Transform itemBeingDragged;
    [SerializeField] Transform top_Panel;

    public InventoryUI.UIMode mode = InventoryUI.UIMode.BASE;
    
    public void UpdateSlot(Item newItem)
    {
        if (newItem == null)
            ClearSlot();
        else
            AddItem(newItem);
    }

    void AddItem(Item newItem)
    {
        item = newItem;
        
        itemIcon_Image.sprite = item.data.icon;
        itemIcon_Image.enabled = true;
        highLight_Image.enabled = false;
        amount_Text.text = item.amount.ToString();
    }

    void ClearSlot()
    {
        item = null;

        itemIcon_Image.sprite = null;
        itemIcon_Image.enabled = false;
        highLight_Image.enabled = false;
        amount_Text.text = "";
    }

    void UseItem()
    {
        if(item != null)
        {
            Inventory.instance.UseItem(item.data.objectID);
        }
    }

    void RemoveItem()
    {
        if(item != null)
        {
            Inventory.instance.RemoveItem(item.data.objectID, item.amount);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (item == null) return;

        if (eventData.button == PointerEventData.InputButton.Right)
        {
            if(mode == InventoryUI.UIMode.BASE)
            {
                UseItem();
                if (item == null)
                {
                    DataInfoUI.instance.Disable(DataInfoUI.EFrameType.Inventory);
                }
            }
            else if(mode == InventoryUI.UIMode.TRADE)
            {
                TradeFrame.instance.AddItem(item.data.objectID, item.amount);
            }
        }
        else if(eventData.button == PointerEventData.InputButton.Middle)
        {
            if (mode == InventoryUI.UIMode.BASE)
            {
                RemoveItem();
                DataInfoUI.instance.Disable(DataInfoUI.EFrameType.Inventory);
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        highLight_Image.enabled = true;
        if(item != null)
        {
            DataInfoUI.instance.Enable(item.data, DataInfoUI.EFrameType.Inventory);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        highLight_Image.enabled = false;
        DataInfoUI.instance.Disable(DataInfoUI.EFrameType.Inventory);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        startPosition = itemBeingDragged.position;
        startParent = transform;
        itemBeingDragged.SetParent(top_Panel);
    }

    public void OnDrag(PointerEventData eventData)
    {
        itemBeingDragged.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        
        if (itemBeingDragged.parent == top_Panel)
        {
            itemBeingDragged.position = startPosition;
            itemBeingDragged.SetParent(startParent);
            itemBeingDragged.SetAsFirstSibling();
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log(eventData.pointerDrag.name + " Drop to : " + gameObject.name);
        InventorySlot draggedInventorySlot = eventData.pointerDrag.GetComponent<InventorySlot>();
        if(draggedInventorySlot != null)
        {
            if (draggedInventorySlot.item == null) return;
            Inventory.instance.SwapItem(draggedInventorySlot.slotNumber, this.slotNumber);
        }
    }
}
