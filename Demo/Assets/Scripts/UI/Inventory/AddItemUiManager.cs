﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddItemUiManager : MonoBehaviour {

    [SerializeField] AddItemUi[] addItemUiArray;

	void Start () {
        Inventory.instance.onAddItem += OnAddItem;
    }
	
    void OnAddItem(int itemID, int itemAmount)
    {
        ItemData itemData = UseableObjectDataManager.instance.GetItemData(itemID);
        AddItemUi addItemUi = GetIdleObject();
        if(addItemUi)
            addItemUi.AddItem(itemData);
    }

    AddItemUi GetIdleObject()
    {
        foreach(var ui in addItemUiArray)
        {
            if (!ui.gameObject.activeSelf)
                return ui;
        }
        return null;
    }
}
