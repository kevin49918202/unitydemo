﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using ConsoleApp1;
using ConsoleApp1.Common;

public class Inventory : MonoBehaviour {

    static Inventory m_Instance;
    public static Inventory instance { get { return m_Instance; } }
    [SerializeField]AudioClip ItemAudio;
    AudioSource itemAudioSource;
    Animator BagIcon;
    void Awake()
    {
        m_Instance = this;
        items = new Item[space];
        itemAudioSource = GetComponent<AudioSource>();
        BagIcon = GameObject.FindGameObjectWithTag("BagPanel").GetComponent<Animator>();
    }

    public event System.Action onItemChanged = ()=>{ };
    public event System.Action<int, int> onAddItem = (a,b) => { };
    
    public int space = 20;

    [SerializeField] public Item[] items;
    Dictionary<int, Item> m_dictItemInInvertory = new Dictionary<int, Item>();

    public bool AddItem(int itemID, int itemAmount)
    {
        //如果已經存在物品欄內則增加數量
        if (m_dictItemInInvertory.ContainsKey(itemID))
        {
            Item item = m_dictItemInInvertory[itemID];
            if (item.amount + itemAmount > item.data.stackLimit)
            {
                Debug.Log(item.data.objectName + " 堆疊滿");
                return false;
            }
            else
            {
                item.amount += itemAmount;
                //ClientApp.instance.SendPlayerItemMessage(EItemCommand.UPDATE, itemID, item.amount);
            }   
        }
        else
        {
            bool success = false;
            for (int i = 0; i < items.Length; i++)
            {
                if (items[i] == null)
                {
                    //跟DataManager索取Item物件
                    Item newItem = UseableObjectDataManager.instance.CreateItem(itemID);
                    newItem.amount = itemAmount;
                    items[i] = newItem;
                    m_dictItemInInvertory.Add(itemID, newItem);
                    success = true;
                    //ClientApp.instance.SendPlayerItemMessage(EItemCommand.ADD, itemID, itemAmount);
                    break;
                }
            }
            
            if (!success)
            {
                Debug.Log("空間不夠");
                return false;
            }
        }

        foreach (var item in items)
        {
            if (item == null) continue;
            if (item.amount > item.data.stackLimit)
            {
                item.amount = item.data.stackLimit;
                Debug.Log("物品數量錯誤 已修正");
            }
        }

        onItemChanged();
        onAddItem(itemID, itemAmount);
        BagIcon.SetTrigger("AddItem");
        itemAudioSource.PlayOneShot(ItemAudio);
        
        return true;
    }

    public void RemoveItem(int itemID, int itemAmount)
    {
        if (m_dictItemInInvertory.ContainsKey(itemID))
        {
            Item item = m_dictItemInInvertory[itemID];
            item.amount -= itemAmount;
            if (item.amount <= 0)
            {
                for (int i = 0; i < items.Length; i++)
                {
                    if (items[i] == item)
                    {
                        items[i] = null;
                        m_dictItemInInvertory.Remove(itemID);
                        break;
                    }
                }

                //ClientApp.instance.SendPlayerItemMessage(EItemCommand.REMOVE, itemID, 0);
            }
            else
            {
                //ClientApp.instance.SendPlayerItemMessage(EItemCommand.UPDATE, itemID, item.amount);
            }

            onItemChanged();
        }
        else
        {
            Debug.Log("找不到物品");
        }
    }

    public void OnPlayerItemMessageReceive(int itemID, int itemAmount)
    {
        AddItem(itemID, itemAmount);
        //for (int i = 0; i < items.Length; i++)
        //{
        //    if (items[i] == null)
        //    {
        //        Item newItem = UseableObjectDataManager.instance.CreateItem(itemID);
        //        newItem.amount = itemAmount;
        //        items[i] = newItem;
        //        m_dictItemInInvertory.Add(itemID, newItem);
        //        onItemChanged();
        //        break;
        //    }
        //}
    }

    public void SwapItem(int indexA, int indexB)
    {
        Item tmp = items[indexA];
        items[indexA] = items[indexB];
        items[indexB] = tmp;
        onItemChanged();
    }

    public void UseItem(int itemID)
    {
        if (m_dictItemInInvertory.ContainsKey(itemID))
        {
            ItemData itemData = UseableObjectDataManager.instance.GetItemData(itemID);
            if (CoolDownManager.instance.CheckCoolDown(itemData))
            {
                if (m_dictItemInInvertory[itemID].Use())
                {
                    RemoveItem(itemID, 1);
                    CoolDownManager.instance.StartCoolDown(itemData);
                }
            }
            else
            {
                FloatingTextManager.instance.CreateWarningText("道具冷卻中");
            }
        }
        else
        {
            Debug.Log("沒有該道具");
        }
    }

    public int GetItemAmount(int itemID)
    {
        if (m_dictItemInInvertory.ContainsKey(itemID))
        {
            return m_dictItemInInvertory[itemID].amount;
        }
        else return 0;
    }

    public void SortOut()
    {
        var newItems = m_dictItemInInvertory.Values.ToArray().OrderBy(i => i.data.objectID);
        items = new Item[space];
        int count = 0;
        foreach (Item item in newItems)
        {
            items[count] = item;
            count++;
        }

        Debug.Log("整理包包 整理了 " + count + " 件物品");
        onItemChanged();
    }

    public void WhoIsYourDaddy()
    {
        AddItem(32, 1);
        AddItem(42, 1);
        AddItem(52, 1);
        AddItem(62, 1);
        AddItem(72, 1);
        AddItem(82, 1);
        AddItem(92, 1);
    }
}
