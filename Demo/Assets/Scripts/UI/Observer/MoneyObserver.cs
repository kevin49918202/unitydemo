﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyObserver : MonoBehaviour {

    [SerializeField] Text GoldCoin_Text;
    [SerializeField] Text SilverCoin_Text;
    [SerializeField] Text CopperCoin_Text;
    PlayerStats playerStats;

    void Awake()
    {
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<PlayerManager>().onMainPlayerCreated += OnMainPlayerCreate;
    }

    void OnMainPlayerCreate(GameObject player)
    {
        playerStats = player.GetComponent<PlayerStats>();
    }

    void OnMoneyChanged()
    {
        int money = playerStats.money;
        GoldCoin_Text.text   = (money / 10000).ToString();
        SilverCoin_Text.text = (money % 10000 / 100).ToString();
        CopperCoin_Text.text = (money % 100).ToString();
    }

    public void RegisterToPlayerStats()
    {
        playerStats.onMoneyChanged += OnMoneyChanged;
        OnMoneyChanged();
    }

    public void UnregisterFromPlayerStats()
    {
        playerStats.onMoneyChanged -= OnMoneyChanged;
    }
}
