﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExpObserver : MonoBehaviour {

    [SerializeField] Image exp_Image;
    [SerializeField] Text exp_Text;
    PlayerStats playerStats;

    private void Awake()
    {
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<PlayerManager>().onMainPlayerCreated += OnMainPlayerCreated;
    }

    void OnMainPlayerCreated(GameObject player)
    {
        playerStats = player.GetComponent<PlayerStats>();
        playerStats.onExpChanged += OnExpChanged;
        OnExpChanged();
    }

    void OnExpChanged()
    {
        exp_Image.fillAmount = (float)playerStats.exp / playerStats.GetExpNeededCurrentLevel();
        float expPercent = ((float)playerStats.exp / playerStats.GetExpNeededCurrentLevel())*100;
        exp_Text.text = expPercent.ToString("f2") + "%";
    }

}
