﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HpObserver : MonoBehaviour {

    [SerializeField] GameObject hpObserver_View;
    [SerializeField] Image hp_Image;
    [SerializeField] Text name_Text;
    [SerializeField] Color player_Color;
    [SerializeField] Color monster_Color;
    [SerializeField] Color npc_Color;
    Vector3 offset = Vector3.zero;

    CharacterStats focusStats;
    Dictionary<LayerMask, Color> dictLayerColor = new Dictionary<LayerMask, Color>();

    float currentSmoothHp = 0;
    float smoothSpeed = 0.1f;

    bool isRunningFollowTarget = false;
    bool isRunningUpdateHp = false;

    void Awake()
    {
        dictLayerColor.Add(LayerMask.NameToLayer("Player"), player_Color);
        dictLayerColor.Add(LayerMask.NameToLayer("Monster"), monster_Color);
        dictLayerColor.Add(LayerMask.NameToLayer("Npc"), npc_Color);

        GameObject.FindGameObjectWithTag("GameManager").GetComponent<PlayerManager>().onMainPlayerCreated += RegisterToPlayerController;
    }

    IEnumerator FollowTarget()
    {
        isRunningFollowTarget = true;
        while (focusStats)
        {
            Vector3 dir = focusStats.transform.position - Camera.main.transform.position;
            if (Vector3.Dot(dir.normalized, Camera.main.transform.forward) > 0)
            {
                transform.position = Camera.main.WorldToScreenPoint(focusStats.transform.position + offset);
                hpObserver_View.SetActive(true);
            }
            else
            {
                hpObserver_View.SetActive(false);
            }

            yield return null;
        }
        isRunningFollowTarget = false;
    }

    IEnumerator UpdateHp()
    {
        isRunningUpdateHp = true;
        while (focusStats!= null && Mathf.Abs(currentSmoothHp - focusStats.currentHp) > 1)
        {
            currentSmoothHp = Mathf.Lerp(currentSmoothHp, (float)focusStats.currentHp, smoothSpeed);
            hp_Image.fillAmount = currentSmoothHp / focusStats.maxHp;
            yield return null;
        }
        if(focusStats != null)
        {
            SetHp();
        }
        isRunningUpdateHp = false;
        yield return null;
    }

    void RegisterToPlayerController(GameObject player)
    {
        player.GetComponent<PlayerController>().onFocusStatsChanged += OnFocusStatsChanged;
    }

    void OnFocusStatsChanged(CharacterStats newStats)
    {
        //舊目標反註冊
        if (focusStats != null)
        {
            focusStats.onHpChanged -= OnHpChanged;
        }
        
        focusStats = newStats;

        if (focusStats == null)
        {
            hpObserver_View.SetActive(false);
        }
        else if(focusStats.GetType() == typeof(BossAIStats))
        {
            focusStats = null;
            hpObserver_View.SetActive(false);
        }
        else
        {
            hp_Image.color = dictLayerColor[focusStats.gameObject.layer];
            name_Text.color = dictLayerColor[focusStats.gameObject.layer];
            name_Text.text = focusStats.characterName;
            offset = new Vector3(0, focusStats.GetModelRadius()*0.3f + 2.5f, 0);

            focusStats.onHpChanged += OnHpChanged;

            SetHp();
            if(!isRunningFollowTarget)
                StartCoroutine(FollowTarget());
        }
    }

    void SetHp()
    {
        currentSmoothHp = focusStats.currentHp;
        hp_Image.fillAmount = focusStats.maxHp == 0 ? 0 : currentSmoothHp / focusStats.maxHp;
    }

    void OnHpChanged()
    {
        if (!isRunningUpdateHp)
            StartCoroutine(UpdateHp());
    }
}
