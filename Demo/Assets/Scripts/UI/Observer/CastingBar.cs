﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CastingBar : MonoBehaviour
{
    PlayerCombat playerCombat;

    GameObject castingBarFrame;
    CanvasGroup canvasGroup;
    Image background_Image;
    Image barFill_Image;
    Image finshLight_Image;
    Text skillName_Text;

    bool bCasting;
    bool bFadeOuting;
    float fadeSpeed = 2;
    [SerializeField] Color defaultColor;
    [SerializeField] Color breakColor;

    event System.Action actionCheckUpdate = () => { };

    void Awake()
    {
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<PlayerManager>().onMainPlayerCreated += OnMainPlayerCreated;
        canvasGroup = transform.GetComponent<CanvasGroup>();

        castingBarFrame  = transform.Find("CastingBarFrame").gameObject;
        barFill_Image    = transform.Find("CastingBarFrame/BarFill_Image").GetComponent<Image>();
        finshLight_Image = transform.Find("CastingBarFrame/FinshLight_Image").GetComponent<Image>();
        skillName_Text   = transform.Find("CastingBarFrame/SkillName_Text").GetComponent<Text>();
    }

    void Update()
    {
        actionCheckUpdate();
    }

    void OnMainPlayerCreated(GameObject player)
    {
        playerCombat = player.GetComponent<PlayerCombat>();
        playerCombat.onCastStart += OnCastStart;
        playerCombat.onCastBreak += OnCastBreak;
        playerCombat.onCastEnd   += OnCastEnd;
    }

    void OnCastStart()
    {
        actionCheckUpdate += UpdateFillAmount;
        actionCheckUpdate -= FadeOut;

        canvasGroup.alpha = 1;
        skillName_Text.text = playerCombat.currentCastingSkill.objectName;
        barFill_Image.color = defaultColor;

        finshLight_Image.enabled = false;
        castingBarFrame.SetActive(true);
    }

    void OnCastBreak()
    {
        actionCheckUpdate -= UpdateFillAmount;
        actionCheckUpdate += FadeOut;
        skillName_Text.text = "中斷";
        barFill_Image.fillAmount = 1;
        barFill_Image.color = breakColor;
    }

    void OnCastEnd()
    {
        actionCheckUpdate -= UpdateFillAmount;
        actionCheckUpdate += FadeOut;
        finshLight_Image.enabled = true;
    }

    void UpdateFillAmount() //更新施法條FillAmount
    {
        barFill_Image.fillAmount = playerCombat.currentCastingTime / playerCombat.currentCastingSkill.castTime;
    }

    void FadeOut() //淡出畫面
    {
        if (canvasGroup.alpha > 0)
        {
            canvasGroup.alpha -= fadeSpeed * Time.deltaTime;
        }
        else
        {
            actionCheckUpdate -= FadeOut; //結束FadeOut
            castingBarFrame.SetActive(false);
        }
    }
}
