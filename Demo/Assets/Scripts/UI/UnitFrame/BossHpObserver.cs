﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossHpObserver : MonoBehaviour {

    public static BossHpObserver instance { get; private set; }

    void Awake()
    {
        instance = this;
    }

    [SerializeField] GameObject hpObserver_View;
    [SerializeField] Text name_Text;
    [SerializeField] Image hp_Image;

    CharacterStats focusStats;

    float currentSmoothHp = 0;
    float smoothSpeed = 0.05f;

    bool isRunningUpdateHp = false;

    public void Enable(CharacterStats stats)
    {
        focusStats = stats;
        focusStats.onHpChanged += OnHpChanged;
        name_Text.text = focusStats.characterName;
        SetHp();
        hpObserver_View.SetActive(true);
    }

    public void Disable()
    {
        if (focusStats != null)
        {
            focusStats.onHpChanged -= OnHpChanged;
            focusStats = null;

        }

        if (hpObserver_View != null)
        {
            hpObserver_View.SetActive(false);
        }
    }

    void OnHpChanged()
    {
        if (!isRunningUpdateHp)
            StartCoroutine(UpdateHp());
    }

    void SetHp()
    {
        currentSmoothHp = focusStats.currentHp;
        hp_Image.fillAmount = focusStats.maxHp == 0 ? 0 : currentSmoothHp / focusStats.maxHp;
    }

    IEnumerator UpdateHp()
    {
        isRunningUpdateHp = true;
        while (focusStats != null && Mathf.Abs(currentSmoothHp - focusStats.currentHp) > 1)
        {
            currentSmoothHp = Mathf.Lerp(currentSmoothHp, (float)focusStats.currentHp, smoothSpeed);
            hp_Image.fillAmount = currentSmoothHp / focusStats.maxHp;
            yield return null;
        }
        if (focusStats != null)
        {
            SetHp();
        }
        isRunningUpdateHp = false;
        yield return null;
    }
}
