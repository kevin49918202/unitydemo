﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DropdownMenu : MonoBehaviour {

    [SerializeField] RectTransform dropdownMenu_View;
    [SerializeField] CanvasGroup canvasGroup;
    [SerializeField] GameObject name_Text;
    [SerializeField] GameObject cancel_Button;
    [SerializeField] GameObject trade_Button;

    [SerializeField] GameObject teamInvite_Button;
    [SerializeField] GameObject teamChangeLeader_Button;
    [SerializeField] GameObject teamKick_Button;
    [SerializeField] GameObject teamQuit_Button;
    [SerializeField] GameObject teamDisband_Button;

    List<GameObject> elements = new List<GameObject>();
    CharacterStats stats;
    bool frameIsClose = true;
    bool isRunngingCoroutine = false;

    void Start()
    {
        for (int i = 0; i < dropdownMenu_View.transform.childCount; i++)
        {
            elements.Add(dropdownMenu_View.transform.GetChild(i).gameObject);
        }
    }

    IEnumerator CheckMouse()
    {
        isRunngingCoroutine = true;
        while (!frameIsClose)
        {
            if ((Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(0)) && !EventSystem.current.IsPointerOverGameObject())
            {
                Disable();
                isRunngingCoroutine = false;
                yield break;
            }
            yield return null;
        }
        yield return null;
    }

    public void Enable(CharacterStats stats)
    {
        ClearAll();
        this.stats = stats;
        
        if (stats.GetType() == typeof(OtherPlayerStats))
        {
            AddMenuElement(trade_Button);
            SetOtherTeamButton(stats.gameObject);
        }
        else if(stats.GetType() == typeof(PlayerStats))
        {
            SetMyTeamButton();
        }

        name_Text.GetComponent<Text>().text = stats.characterName;
        AddMenuElement(name_Text);
        AddMenuElement(cancel_Button);

        dropdownMenu_View.gameObject.SetActive(true);
        SetMenuSize();

        canvasGroup.blocksRaycasts = true;
        frameIsClose = false;
        if(!isRunngingCoroutine)
            StartCoroutine(CheckMouse());
    }

    void Disable()
    {
        stats = null;

        dropdownMenu_View.gameObject.SetActive(false);
        canvasGroup.blocksRaycasts = false;

        frameIsClose = true;
    }

    void SetMenuSize()
    {
        Canvas.ForceUpdateCanvases();
        VerticalLayoutGroup layout = dropdownMenu_View.GetComponent<VerticalLayoutGroup>();
        float height = layout.preferredHeight;
        float weight = layout.preferredWidth;

        dropdownMenu_View.sizeDelta = new Vector2(weight, height);
        Vector3 offset = new Vector3(weight / 2 + 10, -height / 2);
        dropdownMenu_View.position = Input.mousePosition + offset;
    }

    void AddMenuElement(GameObject element)
    {
        element.SetActive(true);
    }

    void ClearAll()
    {
        foreach(GameObject element in elements)
        {
            element.SetActive(false);
        }
    }

    void SetMyTeamButton()
    {
        TeamManager teamManager = TeamManager.instance;

        if (teamManager.CheckBuilt())
        {
            if (teamManager.CheckLeader())
            {
                AddMenuElement(teamDisband_Button);
            }
            else
            {
                AddMenuElement(teamQuit_Button);
            }
        }
    }

    void SetOtherTeamButton(GameObject target)
    {
        TeamManager teamManager = TeamManager.instance;

        //確認自己是否有隊伍
        if (teamManager.CheckBuilt())
        {
            //確認自己是否為隊長
            if (teamManager.CheckLeader())
            {
                //確認目標是否為隊員
                if (teamManager.CheckTargetInTeam(target))
                {
                    AddMenuElement(teamChangeLeader_Button);
                    AddMenuElement(teamKick_Button);
                }
                else
                {
                    AddMenuElement(teamInvite_Button);
                }
            }
        }
        else
        {
            AddMenuElement(teamInvite_Button);
        }
        
    }

    public void OnExitButton()
    {
        Disable();
    }

    public void OnTradeButton()
    {
        TradeFrame.instance.InviteTrade(stats.gameObject);
        Disable();
    }

    public void OnTeamInviteButton()
    {
        TeamManager.instance.InviteJoinTeam(stats.gameObject);
        Disable();
    }

    public void OnTeamChangeLeaderButton()
    {
        TeamManager.instance.ChangeLeader(stats.gameObject);
        Disable();
    }

    public void OnTeamKickButton()
    {
        TeamManager.instance.KickMember(stats.gameObject);
        Disable();
    }

    public void OnTeamQuitButton()
    {
        TeamManager.instance.QuitTeam();
        Disable();
    }

    public void OnTeamDisbandButton()
    {
        TeamManager.instance.DisbandTeam();
        Disable();
    }
}
