﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UnitFrame : MonoBehaviour, IPointerClickHandler {

    [SerializeField] Image hp_Image;
    [SerializeField] Image mp_Image;
    [SerializeField] Text hp_Text;
    [SerializeField] Text mp_Text;
    [SerializeField] Text name_Text;
    [SerializeField] Text level_Text;

    CharacterStats m_TargetStats;
    public CharacterStats targetStats { get { return m_TargetStats; } }

    float currentSmoothHp;
    float currentSmoothMp;
    float smoothSpeed = 0.1f;

    bool isRunningUpdateHp = false;
    bool isRunningUpdateMp = false;

    public void SetUI()
    {
        name_Text.text = m_TargetStats.characterName;
        SetHp();
        SetMp();
        OnLevelChanged();
    }

    void SetHp()
    {
        currentSmoothHp = m_TargetStats.currentHp;
        hp_Image.fillAmount = m_TargetStats.maxHp == 0 ? 0 : currentSmoothHp / m_TargetStats.maxHp;
        hp_Text.text = (int)currentSmoothHp + "/" + m_TargetStats.maxHp;
    }

    void SetMp()
    {
        currentSmoothMp = m_TargetStats.currentMp;
        mp_Image.fillAmount = m_TargetStats.maxMp == 0 ? 0 : currentSmoothMp / m_TargetStats.maxMp;
        mp_Text.text = (int)currentSmoothMp + "/" + m_TargetStats.maxMp;
    }

    void OnHpChange()
    {
        if (!isRunningUpdateHp)
            StartCoroutine(UpdateHp());
    }

    void OnMpChange()
    {
        if (!isRunningUpdateMp)
            StartCoroutine(UpdateMp());
    }

    IEnumerator UpdateHp()
    {
        isRunningUpdateHp = true;
        while(m_TargetStats != null && Mathf.Abs(currentSmoothHp - m_TargetStats.currentHp) > 1)
        {
            currentSmoothHp = Mathf.Lerp(currentSmoothHp, (float)m_TargetStats.currentHp, smoothSpeed);
            hp_Image.fillAmount = currentSmoothHp / m_TargetStats.maxHp;
            hp_Text.text = (int)currentSmoothHp + "/" + m_TargetStats.maxHp;
            yield return null;
        }
        if (m_TargetStats != null)
        {
            SetHp();
        }
        isRunningUpdateHp = false;
        yield return null;
    }

    IEnumerator UpdateMp()
    {
        isRunningUpdateMp = true;
        while (m_TargetStats != null && Mathf.Abs(currentSmoothMp - m_TargetStats.currentMp) > 1)
        {
            currentSmoothMp = Mathf.Lerp(currentSmoothMp, (float)m_TargetStats.currentMp, smoothSpeed);
            mp_Image.fillAmount = currentSmoothMp / m_TargetStats.maxMp;
            mp_Text.text = (int)currentSmoothMp + "/" + m_TargetStats.maxMp;
            yield return null;
        }
        if(m_TargetStats != null)
        {
            SetMp();
        }
        isRunningUpdateMp = false;
        yield return null;
    }

    void OnLevelChanged()
    {
        level_Text.text = m_TargetStats.level.ToString();
    }

    public void Target(CharacterStats stats)
    {
        if (stats.GetType() == typeof(BossAIStats)) return;

        if (stats != m_TargetStats)
        {
            StopAllCoroutines();
            isRunningUpdateHp = false;
            isRunningUpdateMp = false;

            UnregisterActionFromStats(m_TargetStats);
            RegisterActionToStats(stats);

            m_TargetStats = stats;
            gameObject.SetActive(true);
            SetUI();
        }
    }

    public void Untarget()
    {
        UnregisterActionFromStats(m_TargetStats);
        m_TargetStats = null;

        gameObject.SetActive(false);
    }

    void RegisterActionToStats(CharacterStats stats)
    {
        if (stats == null) return;
        stats.onHpChanged    += OnHpChange;
        stats.onMpChanged    += OnMpChange;
        stats.onLevelChanged += OnLevelChanged;
    }

    void UnregisterActionFromStats(CharacterStats stats)
    {
        if (stats == null) return;
        stats.onHpChanged    -= OnHpChange;
        stats.onMpChanged    -= OnMpChange;
        stats.onLevelChanged -= OnLevelChanged;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            GetComponentInParent<DropdownMenu>().Enable(m_TargetStats);
        }
        else if(eventData.button == PointerEventData.InputButton.Left)
        {
            PlayerController controller = PlayerManager.instance.player.GetComponent<PlayerController>();
            controller.SetFocus(m_TargetStats.gameObject);
            controller.SetInteractalbeObject(m_TargetStats.gameObject);
        }
    }
}
