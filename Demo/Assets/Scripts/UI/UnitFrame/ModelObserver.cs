﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModelObserver : MonoBehaviour
{
    [SerializeField] Transform m_modelsParent;
    GameObject[] m_UnitFrameModelsMap;//依序存放各Model,CharacterStats內Index指向對應Model

    [SerializeField] Transform m_PlayerRenderCamera;
    [SerializeField] Transform m_FocusRenderCamera;
    [SerializeField] Transform m_ShopRenderCamera;
    [SerializeField] Transform m_TradeRenderCamera;
    [SerializeField] Transform m_MissionRenderCamera;
    [SerializeField] Transform m_TeamInviteRenderCamera;
    [SerializeField] Transform[] m_TeamRenderCameras;

    //紀錄各Frame當前觀看Model
    GameObject m_PlayerFrameCurrentModel;
    GameObject m_FocusFrameCurrentModel;
    GameObject m_ShopFrameCurrentModel;
    GameObject m_TradeFrameCurrentModel;
    GameObject m_MissionFrameCurrentModel;
    GameObject m_TeamInviteFrameCurrentModel;
    GameObject[] m_TeamFrameCurrentModels = new GameObject[4];

    void Awake()
    {
        m_UnitFrameModelsMap = new GameObject[m_modelsParent.childCount];
        for (int i = 0; i < m_UnitFrameModelsMap.Length; i++)
        {
            m_UnitFrameModelsMap[i] = m_modelsParent.GetChild(i).gameObject;
        }

        RegisterAction();
    }

    void RegisterAction()
    {
        GameObject.Find("Canvas/Shop_Panel").GetComponent<NPCShop_Controller>().onShopChanged += OnShopChanged;
        GameObject.Find("Canvas/Trade_Panel").GetComponent<TradeFrame>().onTradeChanged += OnTradeChanged;
        GameObject.Find("Canvas/Mission_Panel").GetComponent<NPCMission_Controller>().onMissionChanged += OnMissionChanged;

        GameObject gameManager = GameObject.FindGameObjectWithTag("GameManager");
        gameManager.GetComponent<PlayerManager>().onMainPlayerCreated += OnMainPlayerCreated;
        TeamManager teamManager = gameManager.GetComponent<TeamManager>();
        teamManager.onTeamInviteChanged += OnTeamInviteChanged;
        teamManager.onTeamChanged += OnTeamChanged;
    }

    public void OnMainPlayerCreated(GameObject newTarget)
    {
        CharacterStats newStats = newTarget.GetComponent<CharacterStats>();   
        m_PlayerFrameCurrentModel = EnableFrameModel(m_PlayerFrameCurrentModel, newStats);
        m_PlayerRenderCamera.position = m_PlayerFrameCurrentModel.transform.Find("ScreenPoint").position;
        m_PlayerRenderCamera.gameObject.SetActive(true);

        newTarget.GetComponent<PlayerController>().onFocusStatsChanged += OnFocusStatsChanged;
    }

    public void OnFocusStatsChanged(CharacterStats newStats)
    {
        if(newStats != null)
        {
            m_FocusFrameCurrentModel = EnableFrameModel(m_FocusFrameCurrentModel, newStats);
            m_FocusRenderCamera.position = m_FocusFrameCurrentModel.transform.Find("ScreenPoint").position;
            m_FocusRenderCamera.gameObject.SetActive(true);
        }
        else
        {
            DisableFrameModel(m_FocusFrameCurrentModel);
            m_FocusFrameCurrentModel = null;
            m_FocusRenderCamera.gameObject.SetActive(false);
        }
    }

    public void OnShopChanged(CharacterStats newStats)
    {
        if (newStats != null)
        {
            m_ShopFrameCurrentModel = EnableFrameModel(m_ShopFrameCurrentModel, newStats);
            m_ShopRenderCamera.position = m_ShopFrameCurrentModel.transform.Find("ScreenPoint").position;
            m_ShopRenderCamera.gameObject.SetActive(true);
        }
        else
        {
            DisableFrameModel(m_ShopFrameCurrentModel);
            m_ShopFrameCurrentModel = null;
            m_ShopRenderCamera.gameObject.SetActive(false);
        }
    }

    void OnTradeChanged(CharacterStats newStats)
    {
        if (newStats != null)
        {
            m_TradeFrameCurrentModel = EnableFrameModel(m_TradeFrameCurrentModel, newStats);
            m_TradeRenderCamera.position = m_TradeFrameCurrentModel.transform.Find("ScreenPoint").position;
            m_TradeRenderCamera.gameObject.SetActive(true);
        }
        else
        {
            DisableFrameModel(m_TradeFrameCurrentModel);
            m_TradeFrameCurrentModel = null;
            m_TradeRenderCamera.gameObject.SetActive(false);
        }
    }

    void OnMissionChanged(CharacterStats newStats)
    {
        if (newStats != null)
        {
            m_MissionFrameCurrentModel = EnableFrameModel(m_MissionFrameCurrentModel, newStats);
            m_MissionRenderCamera.position = m_MissionFrameCurrentModel.transform.Find("ScreenPoint").position;
            m_MissionRenderCamera.gameObject.SetActive(true);
        }
        else
        {
            DisableFrameModel(m_MissionFrameCurrentModel);
            m_MissionFrameCurrentModel = null;
            m_MissionRenderCamera.gameObject.SetActive(false);
        }
    }

    void OnTeamInviteChanged(CharacterStats newStats)
    {
        if (newStats != null)
        {
            m_TeamInviteFrameCurrentModel = EnableFrameModel(m_TeamInviteFrameCurrentModel, newStats);
            m_TeamInviteRenderCamera.position = m_TeamInviteFrameCurrentModel.transform.Find("ScreenPoint").position;
            m_TeamInviteRenderCamera.gameObject.SetActive(true);
        }
        else
        {
            DisableFrameModel(m_TeamInviteFrameCurrentModel);
            m_TeamInviteFrameCurrentModel = null;
            m_TeamInviteRenderCamera.gameObject.SetActive(false);
        }
    }

    void OnTeamChanged(List<GameObject> members, GameObject leader)
    {
        int count = 0;
        int frameCount = 0;
        while (count < members.Count)
        {
            if (members[count] != PlayerManager.instance.player)
            {
                CharacterStats memberStats = members[count].GetComponent<CharacterStats>();
                m_TeamFrameCurrentModels[frameCount] = EnableFrameModel(m_TeamFrameCurrentModels[frameCount], memberStats);
                m_TeamRenderCameras[frameCount].position = m_TeamFrameCurrentModels[frameCount].transform.Find("ScreenPoint").position;
                m_TeamRenderCameras[frameCount].gameObject.SetActive(true);
                frameCount++;
            }
            count++;
        }

        for (int i = count; i < m_TeamFrameCurrentModels.Length; i++)
        {
            DisableFrameModel(m_TeamFrameCurrentModels[i]);
            m_TeamFrameCurrentModels[i] = null;
            m_TeamRenderCameras[i].gameObject.SetActive(false);
        }
    }

    //抓Stats內unitFrameModelIndex去查表並enable
    GameObject EnableFrameModel(GameObject frameCurrentModel, CharacterStats newStats)
    {
        if (frameCurrentModel)//如果原本有Target目標 > Disable舊目標
        {
            DisableFrameModel(frameCurrentModel);
        }

        GameObject newTarget = m_UnitFrameModelsMap[newStats.unitFrameModelIndex].gameObject;
        newTarget.SetActive(true);

        return newTarget;
    }

    void DisableFrameModel(GameObject frameCurrentModel)
    {
        if (frameCurrentModel == null) return;

        if (CheckSafeDisable(frameCurrentModel))//沒有其他Frame Target此目標 > 安全disable
        {
            frameCurrentModel.SetActive(false);
        }
    }

    //確認其他人有無Target此目標
    bool CheckSafeDisable(GameObject frameTargetModel)
    {
        int count = 0;
        if (frameTargetModel == m_PlayerFrameCurrentModel) count++;
        if (frameTargetModel == m_FocusFrameCurrentModel) count++;
        if (frameTargetModel == m_ShopFrameCurrentModel) count++;
        if (frameTargetModel == m_TradeFrameCurrentModel) count++;
        if (frameTargetModel == m_TeamInviteFrameCurrentModel) count++;
        if (frameTargetModel == m_MissionFrameCurrentModel) count++;
        foreach (GameObject teamCurrentModel in m_TeamFrameCurrentModels)
        {
            if (frameTargetModel == teamCurrentModel) count++;
        }

        if (count > 1) return false; //Disable之前只有自己Target count=1

        return true;
    }
}
