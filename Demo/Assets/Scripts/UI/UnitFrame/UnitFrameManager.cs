﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitFrameManager : MonoBehaviour
{
    [SerializeField] UnitFrame m_MainPlayerFrame;
    [SerializeField] UnitFrame m_FocusFrame;
    [SerializeField] UnitFrame[] m_TeamMembers;

    void Awake()
    {
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<PlayerManager>().onMainPlayerCreated += OnMainPlayerCreated;
    }
    void Start()
    {
        TeamManager.instance.onTeamChanged += OnTeamChanged;
    }

    //MainPlayer創建時觸發
    void OnMainPlayerCreated(GameObject player)
    {
        CharacterStats newStats = player.GetComponent<CharacterStats>();

        if (newStats != null)
        {
            //target新目標
            m_MainPlayerFrame.Target(newStats);
        }
        else
        {
            Debug.LogError("PlayerStats is not found!");
        }

        //註冊focus改變事件
        PlayerController playerController = newStats.transform.GetComponent<PlayerController>();
        playerController.onFocusStatsChanged += OnFocusStatsChanged;
    }

    //玩家focus目標改變時觸發
    public void OnFocusStatsChanged(CharacterStats newStats)
    {
        if (newStats != null) //PlayerController focus新目標
        {
            //target新目標
            m_FocusFrame.Target(newStats);
        }
        else //PlayerController取消focus
        {
            //Untarget原目標
            m_FocusFrame.Untarget();
        }
    }

    void OnTeamChanged(List<GameObject> members, GameObject leader)
    {
        int count = 0;
        int frameCount = 0;
        while(count < members.Count)
        {
            if (members[count] != PlayerManager.instance.player)
            {
                CharacterStats memberStats = members[count].GetComponent<CharacterStats>();
                m_TeamMembers[frameCount].Target(memberStats);
                m_TeamMembers[frameCount].gameObject.SetActive(true);
                frameCount++;
            }
            count++;
        }

        for (int i = frameCount; i < m_TeamMembers.Length; i++)
        {
            m_TeamMembers[i].Untarget();
            m_TeamMembers[i].gameObject.SetActive(false);
        }
    }
}
