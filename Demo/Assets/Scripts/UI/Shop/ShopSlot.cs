﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ShopSlot : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{

    ItemData itemData;

    Image ItemIcon_Image;
    Text  ItemName_Text, GoldCoin_Text, SilverCoin_Text, CopperCoin_Text;
    GameObject GoldCoin, SilverCoin, CopperCoin;

    public void Initialize()
    {
        ItemIcon_Image   = transform.Find("ItemIcon_Image").GetComponent<Image>();
        ItemName_Text    = transform.Find("ItemName_Text").GetComponent<Text>();

        GoldCoin         = transform.Find("Cost/GoldCoin").gameObject;
        GoldCoin_Text    = transform.Find("Cost/GoldCoin/GoldCoin_Text").GetComponent<Text>();

        SilverCoin       = transform.Find("Cost/SilverCoin").gameObject;
        SilverCoin_Text  = transform.Find("Cost/SilverCoin/SilverCoin_Text").GetComponent<Text>();

        CopperCoin       = transform.Find("Cost/CopperCoin").gameObject;
        CopperCoin_Text  = transform.Find("Cost/CopperCoin/CopperCoin_Text").GetComponent<Text>();
    }

    public void AddItem(ItemData newitem) {
        itemData = newitem;
        ItemIcon_Image.sprite = itemData.icon;
        ItemName_Text.text    = itemData.objectName;
        int goldCoin          = itemData.shopCost / 10000;
        int silverCoin        = itemData.shopCost % 10000 / 100;
        int copperCoin        = itemData.shopCost % 100;

        GoldCoin.SetActive((goldCoin == 0)     ? false : true);
        SilverCoin.SetActive((silverCoin == 0) ? false : true);
        CopperCoin.SetActive((copperCoin == 0) ? false : true);

        GoldCoin_Text.text   = goldCoin.ToString();
        SilverCoin_Text.text = silverCoin.ToString();
        CopperCoin_Text.text = copperCoin.ToString();

        this.gameObject.SetActive(true);
    }
    public void ClearItem() {
        itemData = null;
        ItemIcon_Image.sprite = null;
        this.gameObject.SetActive(false);
    }

    public void BuyItem()
    {
        PlayerStats playerStats = PlayerManager.instance.player.GetComponent<PlayerStats>();
        if (playerStats.money >= itemData.shopCost)
        {
            bool success = Inventory.instance.AddItem(itemData.objectID, 1);
            if (success)
            {
                Debug.Log("購買了商品 : " + itemData.objectName);
                playerStats.CostMoney(itemData.shopCost);
            }
        }
        else
        {
            Debug.Log("錢不夠!");
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            GetComponent<ShopSlot>().BuyItem();
        }
        else if (eventData.button == PointerEventData.InputButton.Left && Input.GetKey(KeyCode.LeftShift))
        {
            NPCShop_Controller.instance.EnableAmountFrame(itemData);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        transform.Find("Highlight_Image").GetComponent<Image>().enabled = true;
        if(itemData != null)
            DataInfoUI.instance.Enable(itemData, DataInfoUI.EFrameType.Shop);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        transform.Find("Highlight_Image").GetComponent<Image>().enabled = false;
        DataInfoUI.instance.Disable(DataInfoUI.EFrameType.Shop);
    }

}
