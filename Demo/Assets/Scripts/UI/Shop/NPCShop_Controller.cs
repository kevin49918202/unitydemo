﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class NPCShop_Controller : MonoBehaviour {

    public static NPCShop_Controller instance { get; private set; }

    public event System.Action<CharacterStats> onShopChanged;
    //edit by 查理(讓mission也可以使用有被public的東西)
    public GameObject ScrollView;
    [SerializeField] Scrollbar scrollbar;
    [SerializeField] Transform itemParent;
    [SerializeField] GameObject amountFrame;
    [SerializeField] InputField amount_InputField;
    public RectTransform Content;
    public Text ShopName_Text;

    MoneyObserver moneyObserver;
    CharacterStats currentShopMan;

    ShopSlot[] slots;

    ItemData amountBuyData;

    // Use this for initialization
    protected virtual void Awake () {
        instance = this;
        slots = itemParent.GetComponentsInChildren<ShopSlot>();
        //initialize by白
        foreach(ShopSlot shopSlot in slots)
        {
            shopSlot.Initialize();
        }

        moneyObserver = GetComponent<MoneyObserver>();
    }

    public virtual void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (ScrollView.activeSelf)
            {
                DisableShopFrame();
            }
        }
    }

    public virtual void BuildShop(ItemDataList shopList, CharacterStats newShopMan)
    {
        currentShopMan = newShopMan;

        ShopName_Text.text = currentShopMan.characterName;
        onShopChanged(currentShopMan);

        for (int i = 0; i < slots.Length; i++)
        {
            if (i < shopList.dataList.Count)
            {
                slots[i].AddItem(shopList.dataList[i]);
            }
            else {
                slots[i].ClearItem();
            }
        }
        if (shopList.dataList.Count > 12)
        {
            Content.sizeDelta = new Vector2(0, 180+(shopList.dataList.Count % 2 + shopList.dataList.Count - 12) / 2 * (30f ));
        }
        else {
            Content.sizeDelta = new Vector2(0, 185);
        }

        EnableShopFrame();
    }

    //enable shop table by白
    public virtual void EnableShopFrame()
    {
        moneyObserver.RegisterToPlayerStats();
        scrollbar.value = 1;//拉軸初始化 
        ScrollView.SetActive(true);
    }

    public virtual void DisableShopFrame()
    {
        currentShopMan = null;
        onShopChanged(currentShopMan);

        if (ScrollView.activeSelf)
        {
            moneyObserver.UnregisterFromPlayerStats();
            ScrollView.SetActive(false);
        }
        DataInfoUI.instance.Disable(DataInfoUI.EFrameType.Shop);
        DisableAmountFrame();
    }

    public void EnableAmountFrame(ItemData itemData)
    {
        amount_InputField.text = "";
        amount_InputField.ActivateInputField();
        amountBuyData = itemData;
        amountFrame.transform.position = Input.mousePosition;
        amountFrame.SetActive(true);
    }

    public void DisableAmountFrame()
    {
        amountBuyData = null;
        amountFrame.SetActive(false);
    }

    public void AmountBuy()
    {
        if (amount_InputField.text == "") return; 
        int amount = int.Parse(amount_InputField.text);
        PlayerStats playerStats = PlayerManager.instance.player.GetComponent<PlayerStats>();
        if (playerStats.money >= amountBuyData.shopCost * amount)
        {
            bool success = Inventory.instance.AddItem(amountBuyData.objectID, amount);
            if (success)
            {
                Debug.Log("購買了商品 : " + amountBuyData.objectName + " * " + amount);
                playerStats.CostMoney(amountBuyData.shopCost * amount);
            }
            else
            {
                Debug.Log("購買失敗");
                FloatingTextManager.instance.CreateWarningText("購買失敗");
            }
        }
        else
        {
            Debug.Log("錢不夠!");
        }
        DisableAmountFrame();
    }
}
