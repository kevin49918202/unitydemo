﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniMap : MonoBehaviour
{
    [SerializeField] Text time;
    [SerializeField] Text mapName;
    [SerializeField] Transform miniCamera;
    Transform player;
    bool enable;

    private void Awake()
    {
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<PlayerManager>().onMainPlayerCreated += OnMainPlayerCreated;
        enable = true;
    }

    void OnEnable()
    {
        enable = true;
        StartCoroutine(UpdateClock());
    }

    void OnDisable()
    {
        enable = false;
    }

    void OnMainPlayerCreated(GameObject player)
    {
        this.player = player.transform;
    }

    void Update()
    {
        if (player)
        {
            //跟隨
            miniCamera.position = new Vector3(player.position.x, miniCamera.position.y, player.position.z);
        }
    }

    IEnumerator UpdateClock()
    {
        while (enable)
        {
            int hour = System.DateTime.Now.Hour;
            string sHour = hour.ToString();
            if (hour < 10)
            {
                sHour = "0" + sHour;
            }
            int minute = System.DateTime.Now.Minute;
            string sMinute = minute.ToString();
            if (minute < 10)
            {
                sMinute = "0" + sMinute;
            }

            time.text = sHour + ":" + sMinute;
            yield return new WaitForSeconds(60);
        }
        yield return 0;
    }

    public void SetMapName(string name)
    {
        mapName.text = name;
    }
}
