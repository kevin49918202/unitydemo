﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossMiniMapIcon : MonoBehaviour {

    Transform target;
    void Update()
    {
        if (target)
        {
            transform.position = new Vector3(target.position.x, transform.position.y, target.position.z);
        }
    }

    public void SetTarget(Transform target)
    {
        this.target = target;
    }
}
