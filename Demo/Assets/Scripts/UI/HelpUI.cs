﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpUI : MonoBehaviour {

    [SerializeField] GameObject helpUI;

    public void SetUI()
    {
        if (helpUI.activeSelf)
        {
            DisableUI();
        }
        else
        {
            EnableUI();
        }
    }

    public void EnableUI()
    {
        helpUI.SetActive(true);
    }
    public void DisableUI()
    {
        helpUI.SetActive(false);
    }
}
