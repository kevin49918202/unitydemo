﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MenuButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] string text;

    public void OnPointerEnter(PointerEventData eventData)
    {
        DataInfoUI.instance.Enable(text, DataInfoUI.EFrameType.MenuButton);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        DataInfoUI.instance.Disable(DataInfoUI.EFrameType.MenuButton);
    }
}
