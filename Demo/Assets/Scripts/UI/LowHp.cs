﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LowHp : MonoBehaviour {

    [SerializeField] Image lowHp_Image;
    PlayerStats stats;

    void Awake()
    {
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<PlayerManager>().onMainPlayerCreated += OnMainPlayerCreated;
        lowHp_Image.enabled = false;
    }

    void OnMainPlayerCreated(GameObject player)
    {
        stats = player.GetComponent<PlayerStats>();
        stats.onHpChanged += OnHpChanged;
    }

    void OnHpChanged()
    {
        if((float)stats.currentHp / stats.maxHp < 0.3f)
        {
            lowHp_Image.enabled = true;
        }
        else
        {
            lowHp_Image.enabled = false;
        }
    }
}
