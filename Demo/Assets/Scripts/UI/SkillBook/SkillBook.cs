﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillBook : DraggableObject {

    SkillBookSlot[] slots;
    [SerializeField] Transform skillSlotsParent;
    [SerializeField] GameObject skillBook_View;
    [SerializeField] CanvasGroup canvasGroup;

    private void Awake()
    {
        slots = skillSlotsParent.GetComponentsInChildren<SkillBookSlot>();
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<PlayerManager>().onMainPlayerCreated += OnMainPlayerCreated;
    }

    private void Update()
    {
        if (Input.GetButtonDown("SkillBook"))
        {
            SetUI();
        }
    }

    void OnMainPlayerCreated(GameObject player)
    {
        BuildUI(player.GetComponent<PlayerCombat>().skillDataList.dataList, player.GetComponent<PlayerStats>());
    }

    public void SetUI()
    {
        if (skillBook_View.activeSelf == false)
        {
            EnableUI();
        }
        else
        {
            DisableUI();
        }
    }

    public void EnableUI()
    {
        skillBook_View.SetActive(true);
        canvasGroup.blocksRaycasts = true;
    }

    public void DisableUI()
    {
        skillBook_View.SetActive(false);
        canvasGroup.blocksRaycasts = false;
        DataInfoUI.instance.Disable(DataInfoUI.EFrameType.SkillBook);
    }

    void BuildUI(List<SkillData> skillDataList, PlayerStats playerStats)
    {
        for (int i = 0; i < skillDataList.Count; i++)
        {
            slots[i].AddSkillData(skillDataList[i], playerStats);
        }

        int count = skillDataList.Count;
        for (int i = count; i < slots.Length; i++)
        {
            slots[i].gameObject.SetActive(false);
        }
    }
}
