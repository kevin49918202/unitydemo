﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SkillBookSlot : MonoBehaviour, IBeginDragHandler, IDragHandler,
     IEndDragHandler, IPointerEnterHandler, IPointerExitHandler
{
    public SkillData skillData { get; private set; }
    PlayerStats playerStats;
    [SerializeField] Image skill_Image;
    [SerializeField] Image drag_Image;
    [SerializeField] Image highlight_Image;
    [SerializeField] Text name_Text;
    [SerializeField] Text levelNeeded_Text;

    bool enable;
    Vector3 startPosition;
    Transform startParent;
    [SerializeField] Transform itemBeingDragged;
    [SerializeField] Transform top_Panel;


    public void AddSkillData(SkillData skillData, PlayerStats playerStats)
    {
        this.skillData = skillData;
        skill_Image.sprite = skillData.icon;
        drag_Image.sprite = skillData.icon;
        name_Text.text = skillData.objectName;

        this.playerStats = playerStats;
        this.playerStats.onLevelChanged += OnLevelChanged;
        OnLevelChanged();
    }

    void OnLevelChanged()
    {
        if(playerStats.level >= skillData.levelNeeded)
        {
            Enable();
        }
        else
        {
            Disable();
        }
    }

    void Enable()
    {
        skill_Image.color = Color.white;
        skill_Image.raycastTarget = true;
        levelNeeded_Text.text = "";
    }

    void Disable()
    {
        skill_Image.color = Color.red;
        skill_Image.raycastTarget = false;
        levelNeeded_Text.text = "等級需求：" + skillData.levelNeeded;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        highlight_Image.enabled = true;
        if (skillData != null)
        {
            DataInfoUI.instance.Enable(skillData, DataInfoUI.EFrameType.SkillBook);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        highlight_Image.enabled = false;
        DataInfoUI.instance.Disable(DataInfoUI.EFrameType.SkillBook);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        startPosition = itemBeingDragged.position;
        startParent = transform;
        itemBeingDragged.SetParent(top_Panel);
    }

    public void OnDrag(PointerEventData eventData)
    {
        itemBeingDragged.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (itemBeingDragged.parent == top_Panel)
        {
            itemBeingDragged.position = startPosition;
            itemBeingDragged.SetParent(startParent);
            itemBeingDragged.SetAsFirstSibling();
        }
    }
}
