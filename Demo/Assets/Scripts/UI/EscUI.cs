﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscUI : MonoBehaviour {

    [SerializeField] GameObject esc_View;
    [SerializeField] CanvasGroup canvasGroup;

    public void SetUI()
    {
        if (esc_View.activeSelf)
        {
            esc_View.SetActive(false);
            canvasGroup.blocksRaycasts = false;
        }
        else
        {
            esc_View.SetActive(true);
            canvasGroup.blocksRaycasts = true;
        }
    }

    public void OnBackToLoginButton()
    {
        SceneManage.instance.StartLoadLoginSence();
    }

    public void OnExitButton()
    {
        ClientApp.instance.Disconnect();
        SceneManage.instance.ExitGame();
    }

    public void OnCancelButton()
    {
        SetUI();
    }

}
