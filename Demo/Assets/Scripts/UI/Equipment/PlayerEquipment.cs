﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEquipment : OtherPlayerEquipment {

    public override void Equip(EquipmentData newItem)
    {
        if (newItem == null) return;
        base.Equip(newItem);

        
        ClientApp.instance.SendPlayerEquipMessage(GetCurrentID());
    }

    public override EquipmentData Unequip(int slotIndex)
    {
        if (currentEquipment[slotIndex] != null)
        {
            Inventory.instance.AddItem(currentEquipment[slotIndex].objectID, 1);
        }
        
        base.Unequip(slotIndex);
        ClientApp.instance.SendPlayerEquipMessage(GetCurrentID());
        return currentEquipment[slotIndex];
    }

    public int[] GetCurrentID()
    {
        int[] objectIDs = new int[6];
        for (int i = 0; i < objectIDs.Length; i++)
        {
            if(currentEquipment[i] == null)
            {
                objectIDs[i] = -1;
            }
            else
            {
                objectIDs[i] = currentEquipment[i].objectID;
            }
        }
        return objectIDs;
    }
}
