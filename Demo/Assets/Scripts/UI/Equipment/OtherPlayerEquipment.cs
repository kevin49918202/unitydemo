﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherPlayerEquipment : MonoBehaviour
{
    public EquipmentData[] currentEquipment { get; private set; }

    public event System.Action<EquipmentData, EquipmentData> onEquipmentChanged = (a, b) => { };

    protected void Awake()
    {
        int numSlots = System.Enum.GetNames(typeof(EquipmentSlotIndex)).Length;
        currentEquipment = new EquipmentData[numSlots];
    }

    public virtual void Equip(EquipmentData newItem)
    {
        if (newItem == null) return;
        int slotIndex = (int)newItem.EquipSlot;
        Unequip(slotIndex);
        Debug.Log("穿上:" + newItem.objectID + " to " + newItem.EquipSlot);
        currentEquipment[slotIndex] = newItem;

        onEquipmentChanged(newItem, null);
    }

    public virtual EquipmentData Unequip(int slotIndex)
    {
        if (currentEquipment[slotIndex] != null)
        {
            Debug.Log("脫下:" + currentEquipment[slotIndex].objectID + " from " + (EquipmentSlotIndex)slotIndex);
            EquipmentData oldItem = currentEquipment[slotIndex];

            currentEquipment[slotIndex] = null;

            onEquipmentChanged(null, oldItem);

            return oldItem;
        }
        return null;
    }

    public void SetEquipment(int[] objectIDs)
    {
        for (int i = 0; i < objectIDs.Length; i++)
        {
            if (objectIDs[i] == -1)
            {
                Unequip(i);
            }
            else
            {
                int currentID = currentEquipment[i] == null ? -1 : currentEquipment[i].objectID;
                if (objectIDs[i] != currentID)
                {
                    EquipmentData equipmentData = (EquipmentData)UseableObjectDataManager.instance.GetItemData(objectIDs[i]);
                    Equip(equipmentData);
                }
            }
        }
    }
}

