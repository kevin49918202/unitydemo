﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EquipmentUI : DraggableObject
{
    [SerializeField] Transform slotsParent;
    [SerializeField] GameObject equipment_View;

    [SerializeField] Text name_Text;
    [SerializeField] Text maxHp_Text;
    [SerializeField] Text maxMp_Text;
    [SerializeField] Text damage_Text;
    [SerializeField] Text armor_Text;
    [SerializeField] Text critical_Text;

    PlayerStats playerStats;
    PlayerEquipment playerEquipment;
    EquipmentSlot[] slots;

    CanvasGroup canvasGroup;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();

        slots = slotsParent.GetComponentsInChildren<EquipmentSlot>();
        for (int i = 0; i < slots.Length; i++)
        {
            slots[i].slotNumber = i;
        }

        GameObject.FindGameObjectWithTag("GameManager").GetComponent<PlayerManager>().onMainPlayerCreated += OnMainPlayerCreated;
    }

    void Update()
    {
        if (Input.GetButtonDown("Equipment"))
        {
            SetUI();
        }
    }

    void OnMainPlayerCreated(GameObject player)
    {
        playerEquipment = player.GetComponent<PlayerEquipment>();
        
        playerStats = player.GetComponent<PlayerStats>();
        
        name_Text.text = playerStats.characterName;
    }

    public void SetUI()
    {
        if (equipment_View.activeSelf == false)
        {
            EnableUI();
        }
        else
        {
            DisableUI();
            DataInfoUI.instance.Disable(DataInfoUI.EFrameType.EquipmentUI);
        }
    }

    public void EnableUI()
    {
        UpdateEquipmentSlots(null, null);
        UpdatePlayerStatsInfo();
        playerEquipment.onEquipmentChanged += UpdateEquipmentSlots;
        playerStats.onValueChanged += UpdatePlayerStatsInfo;
        equipment_View.SetActive(true);
        canvasGroup.blocksRaycasts = true;
    }

    public void DisableUI()
    {
        playerEquipment.onEquipmentChanged -= UpdateEquipmentSlots;
        playerStats.onValueChanged -= UpdatePlayerStatsInfo;
        equipment_View.SetActive(false);
        canvasGroup.blocksRaycasts = false;
    }

    void UpdateEquipmentSlots(EquipmentData newItem, EquipmentData oldItem)
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (i < playerEquipment.currentEquipment.Length)
            {
                slots[i].UpdateSlot(playerEquipment.currentEquipment[i]);
            }
        }
    }

    void UpdatePlayerStatsInfo()
    {
        maxHp_Text.text = playerStats.maxHp.ToString();
        maxMp_Text.text = playerStats.maxMp.ToString();
        damage_Text.text = playerStats.damage.ToString();
        armor_Text.text = playerStats.armor.ToString();
        critical_Text.text = playerStats.criticalProbability.ToString() + "%";
    }
}
