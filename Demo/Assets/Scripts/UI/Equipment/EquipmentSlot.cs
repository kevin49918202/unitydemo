﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class EquipmentSlot : MonoBehaviour, IDropHandler, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    public EquipmentData equipmentData { get; private set; }
    public int slotNumber;
    [SerializeField] Image equipment_Image;
    [SerializeField] Image highLight_Image;

    Vector3 startPosition;
    Transform startParent;
    [SerializeField] Transform itemBeingDragged;
    [SerializeField] Transform top_Panel;

    public void UpdateSlot(EquipmentData newEquipment)
    {
        if (newEquipment == null)
            ClearSlot();
        else
            AddItem(newEquipment);
    }

    void AddItem(EquipmentData newEquipment)
    {
        equipmentData = newEquipment;

        equipment_Image.sprite = equipmentData.icon;
        equipment_Image.enabled = true;
        highLight_Image.enabled = false;
    }

    void ClearSlot()
    {
        equipmentData = null;

        equipment_Image.sprite = null;
        equipment_Image.enabled = false;
        highLight_Image.enabled = false;
    }

    void Unequip()
    {
        if (equipmentData != null)
        {
            PlayerManager.instance.player.GetComponent<PlayerEquipment>().Unequip(slotNumber);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            Unequip();
            DataInfoUI.instance.Disable(DataInfoUI.EFrameType.EquipmentUI);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        highLight_Image.enabled = true;
        if(equipmentData != null)
        {
            DataInfoUI.instance.Enable(equipmentData, DataInfoUI.EFrameType.EquipmentUI);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        highLight_Image.enabled = false;
        DataInfoUI.instance.Disable(DataInfoUI.EFrameType.EquipmentUI);
    }

    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log(eventData.pointerDrag.name + " Drop to : " + gameObject.name);
        InventorySlot draggedInventorySlot = eventData.pointerDrag.GetComponent<InventorySlot>();
        if (draggedInventorySlot != null)
        {
            Item draggedItem = draggedInventorySlot.item;
            if (draggedItem == null) return;
            
            if(draggedItem.data.GetType() == typeof(EquipmentData))
            {
                EquipmentData equipmentData = (EquipmentData)draggedItem.data;
                if ((int)equipmentData.EquipSlot == slotNumber)
                {
                    Inventory.instance.UseItem(equipmentData.objectID);
                }
            }
        }
    }
}
