﻿using UnityEngine;
using UnityEngine.EventSystems;

public class TradeDropArea : MonoBehaviour, IDropHandler
{
    public void OnDrop(PointerEventData eventData)
    {
        InventorySlot draggedInventorySlot = eventData.pointerDrag.GetComponent<InventorySlot>();
        //從物品拖曳到交易欄
        if (draggedInventorySlot != null)
        {
            if (draggedInventorySlot.item == null) return;

            GetComponentInParent<TradeFrame>().AddItem(draggedInventorySlot.item.data.objectID, draggedInventorySlot.item.amount);
            return;
        }
    }
}
