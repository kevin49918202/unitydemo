﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TradeFrame : MonoBehaviour {

    public static TradeFrame instance { get; private set; }
    enum TradeCommand { INVITE, ACCEPT, REFUSE, ADD_ITEM, REMOVE_ITEM, CHANGE_MONEY, READY, UNREADY, CANCEL }

    void Awake()
    {
        instance = this;
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<PlayerManager>().onOtherPlayerOffline += OnOtherPlayerOffline;
    }

    [SerializeField] GameObject inviteFrame_View;
    [SerializeField] GameObject tradeFrame_View;
    [SerializeField] Transform myTradeSlotsParent;
    [SerializeField] Transform targetTradeSlotsParent;

    [SerializeField] Text inviteLog_Text;
    [SerializeField] Text readyButton_Text;
    [SerializeField] Image myReady_Image;
    [SerializeField] Image targetReady_Image;
    [SerializeField] Text myName_Text;
    [SerializeField] Text targetName_Text;

    [SerializeField] TradeMoneyInput myMoneyInput;
    [SerializeField] TradeMoneyInput targetMoneyInput;

    MyTradeSlot[] myTradeSlots;
    TradeSlot[] targetTradeSlots;

    int targetIndex;
    CharacterStats targetStats;
    bool myReady;
    bool targetReady;

    public event System.Action<CharacterStats> onTradeChanged = (a) => { };

    void Start()
    {
        myTradeSlots = myTradeSlotsParent.GetComponentsInChildren<MyTradeSlot>();
        targetTradeSlots = targetTradeSlotsParent.GetComponentsInChildren<TradeSlot>();
    }

    //申請交易
    public void InviteTrade(GameObject target)
    {
        targetIndex = PlayerManager.instance.GetPlayerIndex(target);
        targetStats = target.GetComponent<CharacterStats>();
        ClientApp.instance.SendTradeMessage(targetIndex, (int)TradeCommand.INVITE, -1, -1, -1);
    }

    //回應交易邀請
    public void ReplyInvite(bool reply)
    {
        if (reply)
        {
            ClientApp.instance.SendTradeMessage(targetIndex, (int)TradeCommand.ACCEPT, -1, -1, -1);
            EnableTradeFrame();
        }
        else
        {
            ClientApp.instance.SendTradeMessage(targetIndex, (int)TradeCommand.REFUSE, -1, -1, -1);
            DisableTradeFrame();
        }

        inviteFrame_View.SetActive(false);
    }

    //改變金額
    public void OnChangeMoney()
    {
        int moneyValue = myMoneyInput.GetMoneyValue();
        int playerMoney = PlayerManager.instance.player.GetComponent<PlayerStats>().money;
        //確認是否超過身上金額
        if (moneyValue > playerMoney)
        {
            moneyValue = playerMoney;
            myMoneyInput.SetMoneyValue(playerMoney);
        }
        ClientApp.instance.SendTradeMessage(targetIndex, (int)TradeCommand.CHANGE_MONEY, -1, -1, moneyValue);
        MyUnready();
    }
    //新增交易物品
    public void AddItem(int itemID, int itemAmount)
    {
        //確認道具是否已經存在交易欄
        foreach (TradeSlot slot in myTradeSlots)
        {
            if (slot.item == null) continue;

            if (slot.item.data.objectID == itemID)
            {
                return;
            }
        }
        //加入到具到slots
        if(AddItemToSlots(itemID, itemAmount, myTradeSlots))
        {
            ClientApp.instance.SendTradeMessage(targetIndex, (int)TradeCommand.ADD_ITEM, itemID, itemAmount, -1);
            MyUnready();
        }
        else
        {
            Debug.Log("交易欄已滿");
        }
    }
    //移除交易物品
    public void RemoveItem(int itemID, int itemAmount)
    {
        RemoveItemFromSlots(itemID, itemAmount, myTradeSlots);

        ClientApp.instance.SendTradeMessage(targetIndex, (int)TradeCommand.REMOVE_ITEM, itemID, itemAmount, -1);
        MyUnready();
    }
    
    //新增物品至指定Slots
    bool AddItemToSlots(int itemID, int itemAmount, TradeSlot[] slots)
    {
        //找空位放道具
        bool success = false;
        foreach (TradeSlot slot in slots)
        {
            if (slot.item == null)
            {
                slot.BuildItem(itemID, itemAmount);
                success = true;
                break;
            }
        }
        return success;
    }
    //從指定Slots移除物品
    void RemoveItemFromSlots(int itemID, int itemAmount, TradeSlot[] slots)
    {
        foreach (TradeSlot slot in slots)
        {
            if (slot.item == null) continue;

            if (slot.item.data.objectID == itemID)
            {
                slot.RemoveItem(itemAmount);
                break;
            }
        }
    }

    //準備
    void MyReady()
    {
        if (myReady) return;

        myReady = true;
        myReady_Image.enabled = true;
        ClientApp.instance.SendTradeMessage(targetIndex, (int)TradeCommand.READY, -1, -1, -1);
        CheckComplete();
    }
    void MyUnready()
    {
        if (!myReady) return;

        myReady = false;
        myReady_Image.enabled = false;
        ClientApp.instance.SendTradeMessage(targetIndex, (int)TradeCommand.UNREADY, -1, -1, -1);
    }
    //對象準備
    void TargetReady()
    {
        targetReady = true;
        targetReady_Image.enabled = true;
        CheckComplete();
    }
    void TargetUnready()
    {
        targetReady = false;
        targetReady_Image.enabled = false;
        MyUnready();
    }

    //確認完成交易
    void CheckComplete()
    {
        //雙方都準備好
        if(myReady && targetReady)
        {
            Debug.Log("交易完成");
            foreach(TradeSlot slot in myTradeSlots)
            {
                Item item = slot.item;
                if (item == null) continue;
                Inventory.instance.RemoveItem(item.data.objectID, item.amount);
                Debug.Log("失去了 " + item.data.objectID + " * " + item.amount);
            }

            foreach (TradeSlot slot in targetTradeSlots)
            {
                Item item = slot.item;
                if (item == null) continue;
                Inventory.instance.AddItem(slot.item.data.objectID, slot.item.amount);
                Debug.Log("獲得了 " + item.data.objectID + " * " + item.amount);
            }

            PlayerManager.instance.player.GetComponent<PlayerStats>().CostMoney(myMoneyInput.GetMoneyValue());
            Debug.Log("失去了 " + myMoneyInput.GetMoneyValue() + "$");
            PlayerManager.instance.player.GetComponent<PlayerStats>().AddMoney(targetMoneyInput.GetMoneyValue());
            Debug.Log("增加了 " + targetMoneyInput.GetMoneyValue() + "$");

            DisableTradeFrame();
        }
    }

    //交易取消按鈕
    public void OnCancelButton()
    {
        ClientApp.instance.SendTradeMessage(targetIndex, (int)TradeCommand.CANCEL, -1, -1, -1);
        DisableTradeFrame();
    }
    //準備按鈕
    public void OnReadyButton()
    {
        if (myReady)
        {
            MyUnready();
            readyButton_Text.text = "取消準備";
        }
        else
        {
            MyReady();
            readyButton_Text.text = "準備";
        }
    }

    void EnableInviteFrame(int targetIndex)
    {
        this.targetIndex = targetIndex;
        targetStats = PlayerManager.instance.GetPlayerGameObject(targetIndex).GetComponent<CharacterStats>();
        onTradeChanged(targetStats);
        inviteLog_Text.text = targetStats.characterName + " 向你申請交易";
        inviteFrame_View.SetActive(true);
    }
    void EnableTradeFrame()
    {
        InventoryUI.instance.ChangeMode(InventoryUI.UIMode.TRADE);
        InventoryUI.instance.EnableUI();
        onTradeChanged(targetStats);
        tradeFrame_View.SetActive(true);
        myName_Text.text = PlayerManager.instance.player.GetComponent<PlayerStats>().characterName;
        targetName_Text.text = targetStats.characterName;
    }
    void DisableTradeFrame()
    {
        ClearAll();
        InventoryUI.instance.ChangeMode(InventoryUI.UIMode.BASE);
        onTradeChanged(targetStats);
        tradeFrame_View.SetActive(false);
    }

    //清空交易欄
    void ClearAll()
    {
        foreach(TradeSlot slot in myTradeSlots)
        {
            slot.Clear();
        }
        foreach (TradeSlot slot in targetTradeSlots)
        {
            slot.Clear();
        }

        myMoneyInput.Init();
        targetMoneyInput.Init();
        targetIndex = -1;
        targetStats = null;
        myReady = false;
        targetReady = false;
        myReady_Image.enabled = false;
        targetReady_Image.enabled = false;
    }

    public void OnTradeMessageReceive(int playerIndex, int tradeCommand, int itemID, int itemAmount, int moneyValue)
    {
        switch ((TradeCommand)tradeCommand)
        {
            case TradeCommand.INVITE:
                Debug.Log("收到交易邀請");
                EnableInviteFrame(playerIndex);
                break;
            case TradeCommand.ACCEPT:
                Debug.Log("對方接受了");
                EnableTradeFrame();
                break;
            case TradeCommand.REFUSE:
                Debug.Log("對方拒絕了");
                break;
            case TradeCommand.ADD_ITEM:
                Debug.Log("對方新增物品 : " + itemID + " * " + itemAmount);
                AddItemToSlots(itemID, itemAmount, targetTradeSlots);
                break;
            case TradeCommand.REMOVE_ITEM:
                Debug.Log("對方移除物品 : " + itemID + " * " + itemAmount);
                RemoveItemFromSlots(itemID, itemAmount, targetTradeSlots);
                break;
            case TradeCommand.CHANGE_MONEY:
                Debug.Log("對方改變金額 : " + moneyValue);
                targetMoneyInput.SetMoneyValue(moneyValue);
                break;
            case TradeCommand.READY:
                Debug.Log("對方準備好了");
                TargetReady();
                break;
            case TradeCommand.UNREADY:
                Debug.Log("對方取消準備");
                TargetUnready();
                break;
            case TradeCommand.CANCEL:
                Debug.Log("對方取消交易");
                DisableTradeFrame();
                break;
        }
    }

    void OnOtherPlayerOffline(GameObject otherPlayer)
    {
        if (targetStats == null) return;
        if(otherPlayer == targetStats.gameObject)
        {
            Debug.Log("對方下線了");
            DisableTradeFrame();
        }
    }
}
