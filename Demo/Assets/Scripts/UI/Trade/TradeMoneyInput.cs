﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TradeMoneyInput : MonoBehaviour {

    [SerializeField] InputField goldCoin_InputField;
    [SerializeField] InputField silverCoin_InputField;
    [SerializeField] InputField copperCoin_InputField;
    int moneyValue;

    public void SetMoneyValue(int value)
    {
        moneyValue = value;
        goldCoin_InputField.text   = (moneyValue / 10000).ToString();
        silverCoin_InputField.text = (moneyValue % 10000 / 100).ToString();
        copperCoin_InputField.text = (moneyValue % 100).ToString();
    }

    public int GetMoneyValue()
    {
        int goldCoin, silverCoin, copperCoin;
        int.TryParse(goldCoin_InputField.text, out goldCoin);
        int.TryParse(silverCoin_InputField.text, out silverCoin);
        int.TryParse(copperCoin_InputField.text, out copperCoin);

        moneyValue = goldCoin * 10000 + silverCoin * 100 + copperCoin;

        return moneyValue;
    }

    public void Init()
    {
        goldCoin_InputField.text   = "0";
        silverCoin_InputField.text = "0";
        copperCoin_InputField.text = "0";
    }
}
