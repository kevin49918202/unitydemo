﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class TradeSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Item item;

    [SerializeField] Image item_Image;
    [SerializeField] Text name_Text;
    [SerializeField] Text amount_Text;
    [SerializeField] Image highlight_Image;

    public void BuildItem(int itemID, int itemAmount)
    {
        item = UseableObjectDataManager.instance.CreateItem(itemID);
        item.amount = itemAmount;

        item_Image.sprite = item.data.icon;
        item_Image.enabled = true;
        name_Text.text = item.data.objectName;
        amount_Text.text = item.amount.ToString();
    }

    public void RemoveItem(int itemAmount)
    {
        item.amount -= itemAmount;
        amount_Text.text = item.amount.ToString();
        if (item.amount == 0)
        {
            Clear();
        }
    }

    public void Clear()
    {
        item = null;

        item_Image.sprite = null;
        item_Image.enabled = false;
        name_Text.text = "";
        amount_Text.text = "";
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        highlight_Image.enabled = true;
        if (item != null)
        {
            DataInfoUI.instance.Enable(item.data, DataInfoUI.EFrameType.Trade);
        }
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        highlight_Image.enabled = false;
        DataInfoUI.instance.Disable(DataInfoUI.EFrameType.Trade);
    }
}
