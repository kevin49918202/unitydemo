﻿using UnityEngine.EventSystems;

public class MyTradeSlot : TradeSlot ,IPointerClickHandler, IDropHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        if (item == null) return;
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            TradeFrame.instance.RemoveItem(item.data.objectID, item.amount);
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        InventorySlot draggedInventorySlot = eventData.pointerDrag.GetComponent<InventorySlot>();
        //從物品拖曳到交易欄
        if (draggedInventorySlot != null)
        {
            if (draggedInventorySlot.item == null) return;

            GetComponentInParent<TradeFrame>().AddItem(draggedInventorySlot.item.data.objectID, draggedInventorySlot.item.amount);
            return;
        }
    }
}
