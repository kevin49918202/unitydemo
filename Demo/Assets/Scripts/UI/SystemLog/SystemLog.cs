﻿using UnityEngine;
using UnityEngine.UI;

public class SystemLog : MonoBehaviour {

    public static SystemLog instance { get; private set; }

    void Awake()
    {
        instance = this;
    }

    [SerializeField] GameObject systemLog_View;
    [SerializeField] Text log_Text;

    public void EnableLog(string text)
    {
        log_Text.text = text;
        systemLog_View.SetActive(true);
    }
    public void DisableLog()
    {
        log_Text.text = "";
        systemLog_View.SetActive(true);
    }
}
