﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FocusProjectorManager : MonoBehaviour {

    [SerializeField] GameObject monsterFocusProjectorPrefab;
    [SerializeField] GameObject playerFocusProjectorPrefab;
    [SerializeField] GameObject npcFocusProjectorPrefab;
    GameObject monsterFocusProjector;
    GameObject playerFocusProjector;
    GameObject npcFocusProjector;
    GameObject m_CurrentEnableFocusProjector;
    
    float m_FocusProjectorOffset = 5;

    Dictionary<int, GameObject> m_dictProjectorPrefab = new Dictionary<int, GameObject>();

    void Awake()
    {
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<PlayerManager>().onMainPlayerCreated += RegisterActionToPlayer;

        monsterFocusProjector = Instantiate(monsterFocusProjectorPrefab);
        playerFocusProjector = Instantiate(playerFocusProjectorPrefab);
        npcFocusProjector = Instantiate(npcFocusProjectorPrefab);

        m_dictProjectorPrefab.Add(LayerMask.NameToLayer("Monster"), monsterFocusProjector);
        m_dictProjectorPrefab.Add(LayerMask.NameToLayer("Player"), playerFocusProjector);
        m_dictProjectorPrefab.Add(LayerMask.NameToLayer("Npc"), npcFocusProjector);
    }

    void RegisterActionToPlayer(GameObject player)
    {
        player.GetComponent<PlayerController>().onFocusStatsChanged += OnFocusStatsChanged;
    }

    void OnFocusStatsChanged(CharacterStats focusStats)
    {
        if(focusStats != null)
        {
            if(m_CurrentEnableFocusProjector != null)
            {
                DisableCurrentFocusProjector();
            }
            EnableFocusProjector(focusStats);
        }
        else
        {
            DisableCurrentFocusProjector();
        }
    }

    void EnableFocusProjector(CharacterStats focusStats)
    {
        Transform targetTransform = focusStats.transform;
        int targetLayer = focusStats.gameObject.layer;
        m_CurrentEnableFocusProjector = m_dictProjectorPrefab[targetLayer];
        m_CurrentEnableFocusProjector.GetComponent<Projector>().orthographicSize = focusStats.GetModelRadius();
        m_CurrentEnableFocusProjector.transform.parent = focusStats.transform;
        m_CurrentEnableFocusProjector.transform.localPosition = new Vector3(0, m_FocusProjectorOffset, 0);
        m_CurrentEnableFocusProjector.transform.localEulerAngles = new Vector3(90, 0, 0);

        m_CurrentEnableFocusProjector.SetActive(true);       
    }

    void DisableCurrentFocusProjector()
    {
        m_CurrentEnableFocusProjector.SetActive(false);
        m_CurrentEnableFocusProjector.transform.SetParent(null);
        m_CurrentEnableFocusProjector = null;
    }
}
