﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionFinishedText : MonoBehaviour {
    [SerializeField] Text missionFinished_Text;
	public void Set(string text)
    {
        missionFinished_Text.text = text;
    }
}
