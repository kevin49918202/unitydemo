﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamagePopupText : MonoBehaviour {

    [SerializeField] protected Animator animator;
    [SerializeField] protected Text damageText;

    protected Transform target;
    protected Vector3 offset;
    protected float unLoadTime;
    Camera cam;
    private void Awake()
    {
        cam = Camera.main;
    }
    protected void OnEnable()
    {
        AnimatorClipInfo[] clipInfo = animator.GetCurrentAnimatorClipInfo(0);
        unLoadTime = clipInfo[0].clip.length;
        target = null;
    }

    protected void Update()
    {
        if (target != null)
        {
            Vector3 dir = target.position - Camera.main.transform.position ;
            if(Vector3.Dot(dir, Camera.main.transform.forward) > 0)
            {
                damageText.enabled = true;
                UpdatePosition();
            }
            else
            {
                damageText.enabled = false;
            }

            if(unLoadTime > 0)
            {
                unLoadTime -= Time.deltaTime;
            }
            else
            {
                FloatingTextManager.instance.DisableDamageText(gameObject);
            }
        }
    }

    public virtual void Set(string text, Transform transform, Color color, Vector3 offset)
    {
        damageText.color = color;
        damageText.text = text;
        target = transform;
        this.offset = offset;
        UpdatePosition();
    }

    protected void UpdatePosition()
    {
        transform.position = cam.WorldToScreenPoint(target.position) + offset;
    }
}
