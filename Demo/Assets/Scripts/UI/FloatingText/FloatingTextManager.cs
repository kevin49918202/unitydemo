﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingTextManager : MonoBehaviour {

    public static FloatingTextManager instance;

    void Awake()
    {
        instance = this;
    }

    [SerializeField] GameObject damage_Text;
    [SerializeField] GameObject criticalDamage_Text;
    [SerializeField] GameObject warning_Text;
    [SerializeField] GameObject experience_Text;
    [SerializeField] GameObject hpMp_Text;
    [SerializeField] GameObject missionFinished_Text;
    [SerializeField] GameObject levelUp_Text;
    [SerializeField] GameObject learnNewSkill_Text;
    [SerializeField] Transform hpMpText_Panel;
    [SerializeField] Transform warningText_Panel;
    [SerializeField] Transform experienceText_Panel;
    [SerializeField] Transform missionFinished_Panel;
    [SerializeField] Transform levelUp_Panel;
    

    [SerializeField] protected Color player_Color;
    [SerializeField] protected Color monster_Color;

    Dictionary<LayerMask, Color> dictDamageColor = new Dictionary<LayerMask, Color>();

    ObjectPool objectPool;
    int damageSlotNumber;
    int criticalDamageSlotNumber;
    int warningSlotNumber;
    int experienceSlotNumber;
    int hpMpSlotNumber;
    int missionFinishedSlotNumber;
    int levelUpSlotNumber;

    List<GameObject> listWarningTexts = new List<GameObject>();

    void Start()
    {
        objectPool = ObjectPool.instance;

        damageSlotNumber          = objectPool.InitGameObjects(damage_Text, 50, transform);
        criticalDamageSlotNumber  = objectPool.InitGameObjects(criticalDamage_Text, 25, transform);
        hpMpSlotNumber            = objectPool.InitGameObjects(hpMp_Text, 25, hpMpText_Panel);
        warningSlotNumber         = objectPool.InitGameObjects(warning_Text, 5, warningText_Panel);
        experienceSlotNumber      = objectPool.InitGameObjects(experience_Text, 20, experienceText_Panel);
        missionFinishedSlotNumber = objectPool.InitGameObjects(missionFinished_Text, 5, missionFinished_Panel);
        levelUpSlotNumber         = objectPool.InitGameObjects(levelUp_Text, 5, levelUp_Panel);

        dictDamageColor.Add(LayerMask.NameToLayer("Player"), player_Color);
        dictDamageColor.Add(LayerMask.NameToLayer("Monster"), monster_Color);
    }

    public void CreatDamageFloatingText(string text, CharacterStats stats, LayerMask layer, bool critical)
    {
        int slotNumber = critical ? criticalDamageSlotNumber : damageSlotNumber;
        GameObject go = objectPool.LoadGameObjectFromPool(slotNumber);
        Vector3 offset = new Vector3(Random.Range(0, 50), stats.GetModelRadius()*20 + 50);
        go.SetActive(true);
        Color color = critical ? Color.yellow : dictDamageColor[layer];
        go.GetComponent<DamagePopupText>().Set(text, stats.transform, color, offset);
    }

    public void DisableDamageText(GameObject go)
    {
        objectPool.UnLoadObjectToPool(go);
    }

    public void CreateWarningText(string text)
    {
        GameObject go = objectPool.LoadGameObjectFromPool(warningSlotNumber);
        if (go)
        {
            go.SetActive(true);
            go.transform.localPosition = Vector3.zero;
            go.GetComponent<WarningPopupText>().SetText(text);
        }

        if (listWarningTexts.Count == 3)
        {
            objectPool.UnLoadObjectToPool(listWarningTexts[0]);
            listWarningTexts.RemoveAt(0);
        }
        listWarningTexts.Add(go);
    }

    public void DisableWarningText(GameObject go)
    {
        objectPool.UnLoadObjectToPool(go);
        listWarningTexts.Remove(go);
    }

    public void CreateExperienceText(string text)
    {
        GameObject go = objectPool.LoadGameObjectFromPool(experienceSlotNumber);

        go.SetActive(true);
        go.transform.localPosition = new Vector3 (Random.Range(-100,100), Random.Range(-20, 20), 0);
        go.GetComponentInChildren<ExperiencePopupText>().SetText(text);
    }

    public void DisableExperienceText(GameObject go)
    {
        objectPool.UnLoadObjectToPool(go);
    }

    public void CreateHpMpText(string text, bool isHp)
    {
        GameObject go = objectPool.LoadGameObjectFromPool(hpMpSlotNumber);
        if (go)
        {
            go.SetActive(true);
            go.transform.localPosition = new Vector3(0, 0, 0);
            go.GetComponentInChildren<HpMpPopupText>().SetText(text, isHp);
        }
    }

    public void DisableHpMpText(GameObject go)
    {
        objectPool.UnLoadObjectToPool(go);
    }

    public void CreateMissionFinishedText(string text)
    {
        GameObject go = objectPool.LoadGameObjectFromPool(missionFinishedSlotNumber);
        if (go)
        {
            go.GetComponent<MissionFinishedText>().Set(text);
            go.SetActive(true);
            go.transform.localPosition = new Vector3(0, 0, 0);
        }

        objectPool.StartDelayUnloadObject(go, 5);
    }

    public void CreateLevelUpText()
    {
        GameObject go = objectPool.LoadGameObjectFromPool(levelUpSlotNumber);
        if (go)
        {
            go.SetActive(true);
            go.transform.localPosition = new Vector3(0, 0, 0);
        }

        objectPool.StartDelayUnloadObject(go, 5);
    }

    public void CreateLearnNewSkillText()
    {
        learnNewSkill_Text.SetActive(true);
        Destroy(learnNewSkill_Text, 3);
    }
}
