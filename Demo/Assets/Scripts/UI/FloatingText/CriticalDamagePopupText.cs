﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CriticalDamagePopupText : DamagePopupText {

    public override void Set(string text, Transform transform, Color color, Vector3 offset)
    {
        damageText.color = color;
        damageText.text = text;
        target = transform;
        this.offset = offset;
        UpdatePosition();
    }
}
