﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExperiencePopupText : MonoBehaviour {

    [SerializeField] Animator animator;
    [SerializeField] Text experience_Text;
    float unLoadTime;

    void OnEnable()
    {
        AnimatorClipInfo[] clipInfo = animator.GetCurrentAnimatorClipInfo(0);
        unLoadTime = clipInfo[0].clip.length;
    }

    void Update()
    {
        if (unLoadTime > 0)
        {
            unLoadTime -= Time.deltaTime;
        }
        else
        {
            FloatingTextManager.instance.DisableExperienceText(gameObject);
        }
    }

    public void SetText(string text)
    {
        experience_Text.text = text;
    }
}
