﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HpMpPopupText : MonoBehaviour {

    [SerializeField] Animator animator;
    [SerializeField] Text hpMp_Text;
    float unLoadTime;
    Color color;

    void OnEnable()
    {
        AnimatorClipInfo[] clipInfo = animator.GetCurrentAnimatorClipInfo(0);
        float unLoadTime = clipInfo[0].clip.length;
        ObjectPool.instance.StartDelayUnloadObject(gameObject, unLoadTime);
    }

    public void SetText(string text, bool isHp)
    {
        if (isHp)
        {
            color = Color.green;
        }
        else
        {
            color = Color.blue;
        }
        hpMp_Text.text = text;
        hpMp_Text.color = color;
    }
}
