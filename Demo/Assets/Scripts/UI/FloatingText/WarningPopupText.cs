﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WarningPopupText : MonoBehaviour {

    [SerializeField] Animator animator;
    [SerializeField] Text warning_Text;
    float unLoadTime;

    void OnEnable()
    {
        AnimatorClipInfo[] clipInfo = animator.GetCurrentAnimatorClipInfo(0);
        unLoadTime = clipInfo[0].clip.length;
    }

    void Update()
    {
        if(unLoadTime > 0)
        {
            unLoadTime -= Time.deltaTime;
        }
        else
        {
            unLoadTime = 100;
            FloatingTextManager.instance.DisableWarningText(gameObject);
        }
    }

    public void SetText(string text)
    {
        warning_Text.text = text;
    }
}
