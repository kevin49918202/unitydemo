﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoolDownTimer{

	public CoolDownTimer(float time)
    {
        m_TimeCount = time;
    }

    float m_TimeCount;
    public float timeCount { get { return m_TimeCount; } }

    public bool Count()
    {
        if(m_TimeCount < 0)
        {
            return true;
        }
        else
        {
            m_TimeCount -= Time.deltaTime;
            return false;
        }
    }
}
