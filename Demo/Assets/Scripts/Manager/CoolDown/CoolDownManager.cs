﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoolDownManager : MonoBehaviour {

    static CoolDownManager m_Instance;
    public static CoolDownManager instance { get { return m_Instance; } }

    void Awake()
    {
        m_Instance = this;
    }

    Dictionary<UseableObjectData, CoolDownTimer> m_dictCoolDowningTimer = new Dictionary<UseableObjectData, CoolDownTimer>();
    public event System.Action<UseableObjectData> onCoolDownStart = (a) => { };
    public float cdRatio = 1;

    void Update()
    {
        List<UseableObjectData> keys = new List<UseableObjectData>(m_dictCoolDowningTimer.Keys);
        foreach (UseableObjectData uo in keys)
        {
            if (m_dictCoolDowningTimer[uo].Count()) //coolDown完成
            {
                m_dictCoolDowningTimer.Remove(uo);
            }
        }
    }

    public void StartCoolDown(UseableObjectData useableObject)
    {
        if (useableObject.coolDownTime == 0) return;
        CoolDownTimer cdt = new CoolDownTimer(useableObject.coolDownTime * cdRatio);
        m_dictCoolDowningTimer.Add(useableObject, cdt);
        onCoolDownStart(useableObject);
    }

    public void setCDRatio(float ratio)
    {
        cdRatio = ratio;
    }

    //檢查是否正在CD
    public bool CheckCoolDown(UseableObjectData useableObject)
    {
        if (m_dictCoolDowningTimer.ContainsKey(useableObject))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    //Get目前冷卻時間
    public float GetTimeCount(UseableObjectData useableObject)
    {
        if (m_dictCoolDowningTimer.ContainsKey(useableObject))
        {
            return m_dictCoolDowningTimer[useableObject].timeCount;
        }
        else
        {
            return 0;
        }
    }
}
