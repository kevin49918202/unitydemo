﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour {

    #region Singleton

    public static PlayerManager instance;

    void Awake()
    {
        instance = this;
    }

    #endregion
    public GameObject player;
    public int myIndex;
    Dictionary<int, OtherPlayerMotor> m_dictOtherPlayerMotor = new Dictionary<int, OtherPlayerMotor>();
    Dictionary<int, OtherPlayerStats> m_dictOtherPlayerStats = new Dictionary<int, OtherPlayerStats>();
    Dictionary<int, GameObject> m_dictPlayerGameObject = new Dictionary<int, GameObject>();
    Dictionary<GameObject, int> m_dictPlayerIndex = new Dictionary<GameObject, int>();

    [SerializeField] Transform spawnPoint;

    [SerializeField] GameObject witchSource;
    [SerializeField] GameObject warriorSource;
    [SerializeField] GameObject otherWitchSource;
    [SerializeField] GameObject otherWarriorSource;

    public event System.Action<GameObject> onMainPlayerCreated = (a) => { };
    public event System.Action<GameObject> onOtherPlayerOffline = (a) => { };

    public void Start()
    { 
        AccountCharacterInfo mainPlayerInfo = CharacterMenuManager.instance.GetMainPlayer();
        if (mainPlayerInfo == null)
        {
            int[] equipments = new int[] { -1, -1, -1, -1, -1, -1 };
            mainPlayerInfo = new AccountCharacterInfo("Iris", "Witch", 1, 0, 9999999, 0, 0, 
                null, equipments, spawnPoint.position);
        }
        else
        {
            ClientApp.instance.SendLoginMessage(mainPlayerInfo);
        }

        CreateMainPlayer(mainPlayerInfo);
    }

    public void AddOtherPlayer(int index ,AccountCharacterInfo info)
    {
        GameObject otherPlayer = null;
        if (info.job == "Witch")
            otherPlayer = Instantiate(otherWitchSource);
        else if(info.job == "Warrior")
            otherPlayer = Instantiate(otherWarriorSource);

        otherPlayer.GetComponent<OtherPlayerStats>().SetInfo(info);
        otherPlayer.GetComponent<OtherPlayerEquipment>().SetEquipment(info.equipments);
        otherPlayer.transform.position = info.position;
        
        m_dictOtherPlayerMotor.Add(index, otherPlayer.GetComponent<OtherPlayerMotor>());
        m_dictOtherPlayerStats.Add(index, otherPlayer.GetComponent<OtherPlayerStats>());
        m_dictPlayerGameObject.Add(index, otherPlayer);
        m_dictPlayerIndex.Add(otherPlayer, index);
    }

    public void RemoveOtherPlayer(int index)
    {
        GameObject otherPlayer = m_dictPlayerGameObject[index];
        onOtherPlayerOffline(otherPlayer);
        m_dictOtherPlayerMotor.Remove(index);
        m_dictOtherPlayerStats.Remove(index);
        m_dictPlayerGameObject.Remove(index);
        m_dictPlayerIndex.Remove(otherPlayer);
        Destroy(otherPlayer);
    }

    public void CreateMainPlayer(AccountCharacterInfo info)
    {
        if(info.job == "Witch")
        {
            player = Instantiate(witchSource);
        }
        else if(info.job == "Warrior")
        {
            player = Instantiate(warriorSource);
        }
        PlayerStats stats = player.GetComponent<PlayerStats>();
        player.transform.position = info.position;
        stats.SetInfo(info);
        PlayerEquipment equipment = player.GetComponent<PlayerEquipment>();
        if (info.equipments != null) {
            foreach (int id in info.equipments)
            {

                equipment.Equip(UseableObjectDataManager.instance.GetEquipmentData(id));
            }
        }
        

        onMainPlayerCreated(player);
    }

    public void SetMyIndex(int index)
    {
        m_dictPlayerGameObject.Add(index, player);
        m_dictPlayerIndex.Add(player, index);
        myIndex = index;
    }

    public OtherPlayerMotor GetPlayerMotor(int index)
    {
        return m_dictOtherPlayerMotor[index];
    }
    public OtherPlayerStats GetPlayerStats(int index)
    {
        return m_dictOtherPlayerStats[index];
    }
    public GameObject GetPlayerGameObject(int index)
    {
        if (m_dictPlayerGameObject.ContainsKey(index))
        {
            return m_dictPlayerGameObject[index];
        }
        return null;
    }
    public int GetPlayerIndex(GameObject player)
    {
        if (m_dictPlayerIndex.ContainsKey(player))
        {
            return m_dictPlayerIndex[player];
        }
        return -1;
    }
    public AccountCharacterInfo GetPlayerInfo()
    {
        int[] equipments = player.GetComponent<PlayerEquipment>().GetCurrentID();
        PlayerStats stats = player.GetComponent<PlayerStats>();
        AccountCharacterInfo info = new AccountCharacterInfo(stats.characterName, stats.job,
            stats.level, 0, 0, stats.currentHp, stats.currentMp, null, equipments, stats.transform.position);

        return info;
    }

    public List<GameObject> GetAllPlayer()
    {
        List<GameObject> playersList = new List<GameObject>();
        foreach(var data in m_dictPlayerGameObject)
        {
            playersList.Add(data.Value);
        }
        return playersList;
    }
    public int GetPlayerCount()
    {
        return m_dictPlayerGameObject.Count;
    }
}
