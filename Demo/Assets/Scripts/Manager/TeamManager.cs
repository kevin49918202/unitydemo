﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TeamManager : MonoBehaviour {

    public static TeamManager instance { get; private set; }
    enum TeamCommand { INVITE, ACCEPT, REFUSE, JOIN, QUIT, CHANGE_LEADER, KICK, DISBAND}

    void Awake()
    {
        instance = this;
        PlayerManager playerManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<PlayerManager>();
        playerManager.onMainPlayerCreated += OnMainPlayerCreated;
        playerManager.onOtherPlayerOffline += OnOtherPlayerOffline;
    }

    [SerializeField] GameObject inviteFrame_View;
    [SerializeField] Text inviteLog_Text;

    [SerializeField] List<GameObject> members = new List<GameObject>();
    [SerializeField] GameObject leader;
    GameObject inviteTarget;
    GameObject me;
    int teamSize = 5;
    bool isBuilt;

    public event System.Action<List<GameObject>, GameObject> onTeamChanged = (a, b) => { };
    public event System.Action<CharacterStats> onTeamInviteChanged = (a) => { };

    void Update()
    {
        onTeamChanged(members, leader);
    }

    void OnMainPlayerCreated(GameObject player)
    {
        me = player;
    }

    public bool CheckBuilt()
    {
        return isBuilt;
    }

    public bool CheckLeader()
    {
        return leader == PlayerManager.instance.player;
    }

    public bool CheckTargetInTeam(GameObject target)
    {
        return members.Contains(target);
    }

    //回應組隊邀請
    public void ReplyInvite(bool reply)
    {
        if (reply)
        {
            ClientApp.instance.SendTeamMessage(me, inviteTarget, (int)TeamCommand.ACCEPT);
        }
        else
        {
            ClientApp.instance.SendTeamMessage(me, inviteTarget, (int)TeamCommand.REFUSE);
        }

        inviteFrame_View.SetActive(false);
        inviteTarget = null;
        onTeamInviteChanged(null);
    }

    public void InviteJoinTeam(GameObject target)
    {
        if (members.Count == teamSize)
        {
            Debug.Log("隊伍已滿");
            return;
        }
        ClientApp.instance.SendTeamMessage(me, target, (int)TeamCommand.INVITE);
    }

    public void QuitTeam()
    {
        foreach (GameObject member in members)
        {
            //廣播離開隊伍
            ClientApp.instance.SendTeamMessage(me, member, (int)TeamCommand.QUIT);
        }
        ClearTeam();
    }

    public void ChangeLeader(GameObject target)
    {
        foreach (GameObject member in members)
        {
            //廣播更換隊長
            ClientApp.instance.SendTeamMessage(target, member, (int)TeamCommand.CHANGE_LEADER);
        }
        OnChangeLeader(target);
    }

    public void KickMember(GameObject target)
    {
        ClientApp.instance.SendTeamMessage(me, target, (int)TeamCommand.KICK);
    }

    public void DisbandTeam()
    {
        foreach (GameObject member in members)
        {
            //廣播解散隊長
            ClientApp.instance.SendTeamMessage(me, member, (int)TeamCommand.DISBAND);
        }
        ClearTeam();
    }

    void OnTargetAcceptInvite(GameObject target)
    {
        if (members.Contains(target)) return;
        //成立隊伍
        if (!isBuilt)
        {
            OnPlayerJoinTeam(me);
            OnChangeLeader(me);
        }

        //廣播玩家入隊
        foreach (GameObject member in members)
        {
            if (member == me) continue;
            //傳隊友清單給目標
            ClientApp.instance.SendTeamMessage(target, member, (int)TeamCommand.JOIN);
        }

        OnPlayerJoinTeam(target);
        foreach (GameObject member in members)
        {
            //傳隊友清單給目標
            ClientApp.instance.SendTeamMessage(member, target, (int)TeamCommand.JOIN);
        }
        ClientApp.instance.SendTeamMessage(me, target, (int)TeamCommand.CHANGE_LEADER);
    }

    void OnPlayerJoinTeam(GameObject player)
    {
        if (members.Contains(player)) return;
        members.Add(player);
        isBuilt = true;
        onTeamChanged(members, leader);
    }

    void OnPlayerQuitTeam(GameObject player)
    {
        members.Remove(player);
        
        if (members.Count == 1)
        {
            ClearTeam();
        }
        else if (player == leader)
        {
            OnChangeLeader(members[0]);
        }

        onTeamChanged(members, leader);
    }

    void OnChangeLeader(GameObject player)
    {
        leader = player;
        onTeamChanged(members, leader);
    }

    void ClearTeam()
    {
        members = new List<GameObject>();
        leader = null;
        isBuilt = false;
        onTeamChanged(members, leader);
    }

    void EnableInviteFrame(GameObject target)
    {
        if (isBuilt)
        {
            ClientApp.instance.SendTeamMessage(me, target, (int)TeamCommand.REFUSE);
            return;
        }
        inviteTarget = target;

        CharacterStats targetStats = inviteTarget.GetComponent<CharacterStats>();
        onTeamInviteChanged(targetStats);
        inviteLog_Text.text = targetStats.characterName + " 向你申請組隊";
        inviteFrame_View.SetActive(true);
    }

    public int ShareExp(float exp)
    {
        if (CheckBuilt())
        {
            int sharedExp = (int)(exp * (1 + (members.Count - 1) * 0.5f) / members.Count);
            foreach (GameObject member in members)
            {
                if (member == me) continue;

                ClientApp.instance.SendPlayerAddExpMessage(member, sharedExp);
            }

            return sharedExp;
        }
        
        return (int)exp;
    }

    void OnOtherPlayerOffline(GameObject otherPlayer)
    {
        if (members.Contains(otherPlayer))
        {
            OnPlayerQuitTeam(otherPlayer);
        }
    }

    public void OnTeamMessageReceive(GameObject player, int teamIndex)
    {
        switch ((TeamCommand)teamIndex)
        {
            case TeamCommand.INVITE:
                EnableInviteFrame(player);
                break;
            case TeamCommand.ACCEPT:
                Debug.Log("對方接受入隊");
                OnTargetAcceptInvite(player);
                break;
            case TeamCommand.REFUSE:
                Debug.Log("對方拒絕入隊");
                break;
            case TeamCommand.JOIN:
                Debug.Log("玩家加入隊伍");
                OnPlayerJoinTeam(player);
                break;
            case TeamCommand.CHANGE_LEADER:
                Debug.Log("更換隊長");
                OnChangeLeader(player);
                break;
            case TeamCommand.QUIT:
                Debug.Log("玩家退出隊伍");
                OnPlayerQuitTeam(player);
                break;
            case TeamCommand.KICK:
                Debug.Log("玩家被踢出隊伍");
                QuitTeam();
                break;
            case TeamCommand.DISBAND:
                Debug.Log("隊伍解散");
                ClearTeam();
                break;
        }
    }
}
