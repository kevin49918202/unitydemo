using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    public class GameObjectData
    {
        public GameObject m_go;
        public bool m_beUse;
    }

    public static ObjectPool instance;

    public void Awake()
    {
        instance = this;
    }

    public Dictionary<int, List<GameObjectData>> m_dictGameObjectList = new Dictionary<int, List<GameObjectData>>();
    public Dictionary<GameObject, GameObjectData> m_dictGameObjectData = new Dictionary<GameObject, GameObjectData>();
    int slotCount = 0;

    public int InitGameObjects(GameObject go, int iCount, Transform parent)
    {
        slotCount++;
        List<GameObjectData> pList = new List<GameObjectData>();
        m_dictGameObjectList.Add(slotCount, pList);

        for (int i = 0; i < iCount; i++)
        {
            GameObjectData goData = new GameObjectData();

            goData.m_go = Instantiate(go, parent);
            goData.m_beUse = false;
            goData.m_go.SetActive(false);
            pList.Add(goData);

            m_dictGameObjectData.Add(goData.m_go, goData);
        }

        return slotCount;
    }

    public GameObject LoadGameObjectFromPool(int iSlot)
    {
        if (m_dictGameObjectList.ContainsKey(iSlot) == false)
        {
            return null;
        }

        List<GameObjectData> pList = m_dictGameObjectList[iSlot];
        GameObject rgo = null;

        foreach (GameObjectData goData in pList)
        {
            if (goData.m_beUse == false)
            {
                goData.m_beUse = true;
                rgo = goData.m_go;
                break;
            }
        }
        return rgo;
    }

    public void UnLoadObjectToPool(GameObject go)
    {
        if (m_dictGameObjectData.ContainsKey(go))
        {
            GameObjectData goData = m_dictGameObjectData[go];
            go.SetActive(false);
            goData.m_beUse = false;
        }
    }

    public void StartDelayUnloadObject(GameObject go, float time)
    {
        StartCoroutine(DelayUnloadObject(go, time));
    }

    IEnumerator DelayUnloadObject(GameObject go, float time)
    {
        yield return new WaitForSeconds(time);
        UnLoadObjectToPool(go);
    }
}
