﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneManage : MonoBehaviour {

    public static SceneManage instance { get; private set; }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    [SerializeField] GameObject loadingBar;
    [SerializeField] Image loadingBar_Image;
    [SerializeField] GameObject fadeOut_Panel;

    public void StartLoadDemoSence()
    {
        StartCoroutine(LoadScene("Demo"));
    }

    public void StartLoadCharacterMenuSence()
    {
        StartCoroutine(LoadScene("CharacterMenu"));
    }

    public void StartLoadLoginSence()
    {
        ClientApp.instance.Disconnect();
        StartCoroutine(LoadScene("Login"));
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    IEnumerator LoadScene(string sceneName)
    {
        loadingBar_Image.fillAmount = 0;
        loadingBar.SetActive(true);
        AsyncOperation CheckSceneLoad = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
        while (!CheckSceneLoad.isDone)
        {
            float progress = CheckSceneLoad.progress;
            loadingBar_Image.fillAmount = progress /0.9f;
            Debug.Log("Loading Progress : " + (progress * 100) + "%");
            if (progress >= 1f)
            {
                CheckSceneLoad.allowSceneActivation = true;
            }
            yield return 0;
        }
        StartFadeInOut();
        yield return new WaitForSeconds(0.9f);
        loadingBar.SetActive(false);
    }

    public void StartFadeInOut()
    {
        StartCoroutine(FadeInOut());
    }

    IEnumerator  FadeInOut()
    {
        fadeOut_Panel.SetActive(true);
        yield return new WaitForSeconds(1.35f);
        fadeOut_Panel.SetActive(false);
    }
}
