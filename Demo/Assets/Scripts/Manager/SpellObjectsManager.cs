﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellObjectsManager : MonoBehaviour {

    static SpellObjectsManager m_Instance;
    public static SpellObjectsManager instance { get { return m_Instance; } }

    [SerializeField] Transform objectParent;
    [SerializeField] GameObject[] spellPrefabs;
    [SerializeField] GameObject stoneMonster;
    [SerializeField] Dictionary<string, int> m_dictSpellObjectSlot = new Dictionary<string, int>();
    ObjectPool objectPool;

    void Awake()
    {
        m_Instance = this;
    }

    void Start()
    {
        objectPool = ObjectPool.instance;
        foreach (GameObject go in spellPrefabs)
        {
            int iSlot = objectPool.InitGameObjects(go, 20, objectParent);
            m_dictSpellObjectSlot.Add(go.name, iSlot);
        }
    }

    public void StartCreateTargetedSpell(CharacterStats owner, CharacterStats target, SkillData skillData, float delay)
    {
        StartCoroutine(CreateTargetedSpell(owner, target, skillData, delay));
    }

    public void StartCreateAreaSpell(CharacterStats owner, SkillData skillData, float delay)
    {
        StartCoroutine(CreateAreaSpell(owner, skillData, delay));
    }

    IEnumerator CreateTargetedSpell(CharacterStats owner, CharacterStats target, SkillData skillData, float delay)
    {
        yield return new WaitForSeconds(delay);
        int iSlot = m_dictSpellObjectSlot[skillData.name];
        GameObject go = objectPool.LoadGameObjectFromPool(iSlot);
        if (go)
        {
            go.transform.position = owner.transform.Find("SpellSpawnPoint").position;
            go.GetComponent<TargetedSpell>().Set(owner, target, skillData);
            go.SetActive(true);
        }
    }

    IEnumerator CreateAreaSpell(CharacterStats owner, SkillData skillData, float delay)
    {
        yield return new WaitForSeconds(delay);
        GameObject go = null;
        if (skillData.name == "StoneMonster")
        {
            go = Instantiate(stoneMonster);
        }
        else
        {
            int iSlot = m_dictSpellObjectSlot[skillData.name];
            go = objectPool.LoadGameObjectFromPool(iSlot);
        }
        
        if (go)
        {
            Vector3 createPoint = new Vector3(owner.transform.position.x, 50.1f, owner.transform.position.z);
            createPoint += owner.transform.forward * skillData.castRange;
            go.transform.position = createPoint;
            go.transform.rotation = owner.transform.rotation;
            go.GetComponent<AreaSpell>().Set(owner, skillData);
            go.SetActive(true);
        }
    }

    public void CreateFireBoom(CharacterStats target)
    {
        int iSlot = m_dictSpellObjectSlot["FireBoom"];
        GameObject go = objectPool.LoadGameObjectFromPool(iSlot);
        if (go)
        {
            float scale = target.GetModelRadius() * 0.5f + 1.5f;
            go.transform.Find("Fire").localScale = new Vector3(scale, scale, scale);
            go.transform.SetParent(target.transform);
            go.transform.localPosition = new Vector3(0, target.GetModelRadius() / 2 - 1, 0);
            go.SetActive(true);
            objectPool.StartDelayUnloadObject(go, 3f);
        }
    }

    public void CreateFireBurning(CharacterStats target)
    {
        int iSlot = m_dictSpellObjectSlot["FireBurning"];
        GameObject go = objectPool.LoadGameObjectFromPool(iSlot);
        if (go)
        {
            float targetRadius = target.GetModelRadius();
            go.transform.Find("Fire").localScale = new Vector3(targetRadius, targetRadius, targetRadius);
            go.transform.SetParent(target.transform);
            go.transform.localPosition = new Vector3(0, target.GetModelRadius() / 2 - 1, 0);
            go.SetActive(true);
            objectPool.StartDelayUnloadObject(go, 3f);
        }
    }

    public void CreateCastingObject(CharacterStats caster, SkillData skillData)
    {
        if (skillData.name == "FireBall")
        {
            int iSlot = m_dictSpellObjectSlot["FireCasting"];
            GameObject go = objectPool.LoadGameObjectFromPool(iSlot);
            if (go)
            {
                go.transform.position = caster.transform.Find("SpellSpawnPoint").position;
                go.transform.SetParent(caster.transform);
                go.GetComponent<FireCasting>().Register(caster.gameObject);
                go.SetActive(true);
            }
        }
    }

    public void CreateAttackParticle(Transform parent)
    {
        int iSlot = m_dictSpellObjectSlot["Attack_Particle"];
        GameObject go = objectPool.LoadGameObjectFromPool(iSlot);
        if (go)
        {
            go.transform.position = parent.position;
            go.transform.rotation = parent.rotation;
            go.SetActive(true);
            objectPool.StartDelayUnloadObject(go, 0.5f);
        }
    }
    public void CreateAttack2Particle(Transform parent)
    {
        int iSlot = m_dictSpellObjectSlot["Attack2_Particle"];
        GameObject go = objectPool.LoadGameObjectFromPool(iSlot);
        if (go)
        {
            go.transform.position = parent.position;
            go.transform.rotation = parent.rotation;
            go.SetActive(true);
            objectPool.StartDelayUnloadObject(go, 0.5f);
        }
    }
}
