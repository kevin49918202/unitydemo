﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {
    public static AudioManager audiomanager;
    AudioSource m_AudioSource;
    [SerializeField] float volumeSpeed;
    // Use this for initialization
    void Awake () {
        audiomanager = this;
        m_AudioSource = Camera.main.GetComponent<AudioSource>();


    }
    public IEnumerator ChangeAudio(AudioClip m_Clip)
    {
        if (m_AudioSource.clip == null && m_Clip != null)
        {
            m_AudioSource.clip = m_Clip;
            m_AudioSource.Play();
            yield return null;
        }
        if (m_AudioSource.clip != null && m_Clip != null)
        {
            if (m_AudioSource.clip != m_Clip)
            {
                float volume = m_AudioSource.volume;
                while (m_AudioSource.volume > 0)
                {
                    m_AudioSource.volume -= Time.deltaTime / volumeSpeed;
                    yield return 0;
                }
                m_AudioSource.clip = m_Clip;
                m_AudioSource.volume = volume;
                m_AudioSource.Play();
            }
        }
    }
}
