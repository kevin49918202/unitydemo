﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSlot : MonoBehaviour {

    [SerializeField] Image select_Image;
    [SerializeField] Text name_Text;
    [SerializeField] Text jobLevel_Text;
    AccountCharacterInfo characterInfo;

	public void AddCharacter(AccountCharacterInfo info)
    {
        characterInfo = info;
        string characterName = characterInfo.characterName;
        string job = "";
        string level = characterInfo.level.ToString();

        if (characterInfo.job == "Witch") job = "法師";
        else if (characterInfo.job == "Warrior") job = "戰士";

        name_Text.text = characterName;
        jobLevel_Text.text = job + " 等級:" + level;
        gameObject.SetActive(true);
    }

    public void RemoveCharacter()
    {
        characterInfo = null;
        name_Text.text = "";
        jobLevel_Text.text = "";
        select_Image.enabled = false;
        gameObject.SetActive(false);
    }

    public void OnButton()
    {
        CharacterSence.instance.SetCharacter(characterInfo);
        select_Image.enabled = true;
    }

    public void Unselect()
    {
        select_Image.enabled = false;
    }
}
