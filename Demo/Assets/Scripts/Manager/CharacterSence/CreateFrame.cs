﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateFrame : MonoBehaviour {

    [SerializeField] GameObject create_View;
    [SerializeField] Text characterName;
    [SerializeField] Image witch_Image;
    [SerializeField] Image warrior_Image;
    bool isSelectJob;
    string job;

    public void EnableUI()
    {
        isSelectJob = false;
        job = "";
        witch_Image.enabled = false;
        warrior_Image.enabled = false;
        create_View.SetActive(true);
    }

    public void OnCheckButton()
    {
        if(characterName.text != "" && isSelectJob)
        {
            ClientApp.instance.SendChangeCharacterMessage(true, characterName.text, job);
            CharacterSence.instance.UpdateCharacterList();
            create_View.SetActive(false);
        }
    }

    public void OnCancelButton()
    {
        create_View.SetActive(false);
    }

    public void SetJob(bool b)
    {
        if (b)
        {
            job = "Witch";
            witch_Image.enabled = true;
            warrior_Image.enabled = false;
        }
        else
        {
            job = "Warrior";
            witch_Image.enabled = false;
            warrior_Image.enabled = true;
        }
        isSelectJob = true;
    }
}
