﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSence : MonoBehaviour {

    public static CharacterSence instance { get; private set; }

    private void Awake()
    {
        instance = this;
    }

    [SerializeField] Button login_Button;
    [SerializeField] Transform slotsParent;
    [SerializeField] GameObject witch_model;
    [SerializeField] GameObject warrior_model;

    AccountCharacterInfo selectInfo;
    CharacterSlot[] slots;
    List<AccountCharacterInfo> characterInfoList = new List<AccountCharacterInfo>();

    private void Start()
    {
        slots = slotsParent.GetComponentsInChildren<CharacterSlot>();
        foreach(var slot in slots)
        {
            slot.gameObject.SetActive(false);
        }
        CharacterMenuManager.instance.onCharacterInfoMessageReceive += onCharacterInfoMessageReceive;
        CharacterMenuManager.instance.SendGetCharacterInfo();
    }

    void onCharacterInfoMessageReceive(AccountCharacterInfo info)
    {
        characterInfoList.Add(info);
        BuildCharacterList(info);
    }

    public void UpdateCharacterList()
    {
        foreach(CharacterSlot slot in slots)
        {
            slot.RemoveCharacter();
        }
        characterInfoList = new List<AccountCharacterInfo>();
        witch_model.SetActive(false);
        warrior_model.SetActive(false);
        CharacterMenuManager.instance.SendGetCharacterInfo();
    }

    public void SetCharacter(AccountCharacterInfo info)
    {
        foreach(CharacterSlot slot in slots)
        {
            slot.Unselect();
        }
        selectInfo = info;
        CharacterMenuManager.instance.SetCharacter(info);
        if (info.job == "Witch")
        {
            warrior_model.SetActive(false);
            witch_model.SetActive(true);
        }
        else
        {
            witch_model.SetActive(false);
            warrior_model.SetActive(true);
        }
        login_Button.enabled = true;
    }

    void BuildCharacterList(AccountCharacterInfo info)
    {
        slots[characterInfoList.Count - 1].AddCharacter(info);
    }

    public void OnLoginButton()
    {
        CharacterMenuManager.instance.onCharacterInfoMessageReceive -= onCharacterInfoMessageReceive;
        SceneManage.instance.StartLoadDemoSence();
        login_Button.enabled = false;
    }

    public void OnDeleteButton()
    {
        if(selectInfo != null)
        {
            ClientApp.instance.SendChangeCharacterMessage(false, selectInfo.characterName, "");
            UpdateCharacterList();
        }
    }

    public void OnBackToLoginButton()
    {
        CharacterMenuManager.instance.onCharacterInfoMessageReceive -= onCharacterInfoMessageReceive;
        SceneManage.instance.StartLoadLoginSence();
    }
}
