﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionManager : MonoBehaviour {
    public static MissionManager missionmanager;
    public MissionData DefaultMission;
    public Dictionary<MissionData, MissionClass> MissionDic = new Dictionary<MissionData, MissionClass>();
    RitualCounter ritualCounter;
    [SerializeField] GameObject TimeLineManage;
    BarriorController barriorcontrol;
    public AudioSource audiosource;
    [SerializeField] AudioClip completeClip;
    [SerializeField] AudioClip bossCompeleteClip;
    [SerializeField] GameObject EndTimeline;

    [SerializeField] GameObject quest1_Icon;
    [SerializeField] GameObject quest2_Icon;
    [SerializeField] GameObject leader;

    // Use this for initialization
    void Awake () {
        missionmanager = this;
        barriorcontrol = TimeLineManage.GetComponentInChildren<BarriorController>();
        ritualCounter = TimeLineManage.GetComponentInChildren<RitualCounter>();
        audiosource = GetComponent<AudioSource>();

        UpdateQuestIcon();
    }
    //private void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.Q)) {
    //        ResetMission();
    //    }
    //}
    // Update is called once per frame

    public void AddMission(MissionData mission) {
        if (MissionDic.ContainsKey(mission))
        {
            Debug.Log("你已經接受了任務");
            return;
        }
        else {
            MissionClass missionclass = new MissionClass();
            missionclass.missionData = mission;
            missionclass.goalamount = mission.Amount;
            missionclass.Reward = mission.Reward;
            missionclass.NextMission = mission.NextMission;
            MissionDic.Add(mission, missionclass);
            audiosource.PlayOneShot(completeClip);
            //delegate給怪物;
            MonsterManager.monsterInstance.SetMonsterMission(missionclass);
            MissionUI.missionui.OnAddMission(missionclass);
        }
        FloatingTextManager.instance.CreateMissionFinishedText("任務更新");
    }
    public void MissionComplete(MissionClass mission) {
        //if (completeSound != null)
        //{
        //    audiosource.PlayOneShot(completeSound);
        //}
        //輸入對話
        MissionUI.missionui.OnMissionCompleted(mission);
        switch (mission.missionData.name) {
            case "BugMission":
                //Debug.Log("BugMission");
                barriorcontrol.StartClose();
                PlayerManager.instance.player.GetComponent<PlayerStats>().AddExp(500);
                break;
            case "EathElementRitual":
                ritualCounter.RitualStop = true;
                PlayerManager.instance.player.GetComponent<PlayerStats>().AddExp(10000);
                break;
            case "WolfAssault":
                Debug.Log("WolfAssault");
                PlayerManager.instance.player.GetComponent<PlayerStats>().AddExp(50000);
                break;
            case "BossMission":
                Debug.Log("BossMission");
                StartCoroutine(PlayEndTimeline());
                break;
        }

        FloatingTextManager.instance.CreateMissionFinishedText("任務完成");
        MonsterManager.monsterInstance.DeleteMonsterMission(mission);

        if (mission.NextMission != null)
        {
            StartCoroutine(DelayAddMission(mission.NextMission));
            Debug.Log("下一個任務是 : " + mission.NextMission);
        }
        else
        {
            MissionUI.missionui.OnMissionChanged(null);
        }
    }
    public void ResetMission() {
        foreach(var mission in MissionDic)
        {
            MonsterManager.monsterInstance.DeleteMonsterMission(mission.Value);
        }
        MissionDic.Clear();
        //重設所有trigger 
        MapTrigger[] maptrigger = TimeLineManage.GetComponentsInChildren<MapTrigger>();
        foreach (MapTrigger maptrig in maptrigger) {
            maptrig.ResetTrigger();
        }
        MissionUI.missionui.OnMissionChanged(null);
        UpdateQuestIcon();
    }

    public void OnMissionChanged(MissionClass mission)
    {
        MissionUI.missionui.OnMissionChanged(mission);
    }

    public void UpdateQuestIcon()
    {
        if (MissionDic.Count == 0)
        {
            quest1_Icon.SetActive(true);
            quest2_Icon.SetActive(true);
            leader.tag = "Mission";
        }
        else
        {
            quest1_Icon.SetActive(false);
            quest2_Icon.SetActive(false);
            leader.tag = "Untagged";
        }
    }

    IEnumerator DelayAddMission(MissionData missionData)
    {
        yield return new WaitForSeconds(5);
        AddMission(missionData);
    }
    IEnumerator PlayEndTimeline() {
        yield return new WaitForSeconds(5f);
        StartCoroutine(AudioManager.audiomanager.ChangeAudio(bossCompeleteClip));
        SceneManage.instance.StartFadeInOut();
        yield return new WaitForSeconds(0.7f);
        EndTimeline.SetActive(true);
    }
}
