﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterManager : MonoBehaviour
{
    public static MonsterManager monsterInstance;
    public GameObject[] MonsterSpecie;
    public OtherMonsterAI[] monsterPool;
    [SerializeField] GameObject[] spawnPool_Bug;
    [SerializeField]GameObject[] spawnPool_Wolf;
    [SerializeField] GameObject[] spawnPool_EarthElement;
    GameObject spawnBoss;
    public GameObject BugBornPT;
    public GameObject DieExplosion;
    public GameObject DefaultChest;
    public GameObject BugDieParticle;

    public MapTrigger BugTrigger;
    public MapTrigger Earth_ElementTrigger;
    public MapTrigger WolfTrigger;
    public MapTrigger BossTrigger;
    // Use this for initialization
    private void Awake()
    {
        monsterInstance = this;
        spawnPool_Bug = GameObject.FindGameObjectsWithTag("SpawnPointBug");
        spawnPool_Wolf = GameObject.FindGameObjectsWithTag("SpawnPointWolf");
        spawnPool_EarthElement = GameObject.FindGameObjectsWithTag("SpawnPointEarthElement");
        spawnBoss = GameObject.FindGameObjectWithTag("SpawnBoss");
        BugTrigger.onTrigger += SpawnBug;
        BugTrigger.resetTrigger += DisBug;
        Earth_ElementTrigger.onTrigger += SpawnEarth_Element;
        Earth_ElementTrigger.resetTrigger += DisEarth_Element;
        WolfTrigger.resetTrigger += DisWolf;
        BossTrigger.resetTrigger += DisBoss;

    }


    void Start()
    {
        //otherMonsterPool = Monstermamager.GetComponentsInChildren<OtherMonsterAI>();
    }
    private void Update()
    {
    }
    public void SpawnMonster() {
        //monsterPool = new OtherMonsterAI[spawnPool_Bug.Length+1];
        monsterPool = new OtherMonsterAI[spawnPool_Bug.Length + spawnPool_Wolf.Length + spawnPool_EarthElement.Length + 1];
        for (int i = 0; i < (spawnPool_Bug.Length+ spawnPool_Wolf.Length+ spawnPool_EarthElement.Length); i++)
            {
                if (ClientApp.instance.host)
                {
                    if (i < (spawnPool_Bug.Length))
                    {
                        monsterPool[i] = Instantiate(MonsterSpecie[0], spawnPool_Bug[i].transform).GetComponent<OtherMonsterAI>();
                        monsterPool[i].m_Data.monsterNumber = i;
                        monsterPool[i].m_Data.Me_Object.SetActive(false);
                    }
                    else if (i < (spawnPool_Bug.Length + spawnPool_Wolf.Length ))
                    {
                        monsterPool[i] = Instantiate(MonsterSpecie[3], spawnPool_Wolf[i- spawnPool_Bug.Length].transform).GetComponent<OtherMonsterAI>();
                        monsterPool[i].m_Data.monsterNumber = i;
                        monsterPool[i].m_Data.Me_Object.SetActive(false);
                    }
                    else {
                        monsterPool[i] = Instantiate(MonsterSpecie[5], spawnPool_EarthElement[i- spawnPool_Bug.Length- spawnPool_Wolf.Length].transform).GetComponent<OtherMonsterAI>();
                        monsterPool[i].m_Data.monsterNumber = i;
                        monsterPool[i].m_Data.Me_Object.SetActive(false);
                    }
                }
                else
                {
                    if (i < spawnPool_Bug.Length)
                    {
                        monsterPool[i] = Instantiate(MonsterSpecie[1], spawnPool_Bug[i].transform).GetComponent<OtherMonsterAI>();
                        monsterPool[i].m_Data.monsterNumber = i;
                        //other Awake
                        monsterPool[i].m_Data.finalPosition = monsterPool[i].m_Data.Me_Object.transform.position;
                        monsterPool[i].m_Data.Me_Object.SetActive(false);
                    }
                    else if (i < spawnPool_Bug.Length + spawnPool_Wolf.Length)
                    {
                        monsterPool[i] = Instantiate(MonsterSpecie[4], spawnPool_Wolf[i - spawnPool_Bug.Length].transform).GetComponent<OtherMonsterAI>();
                        monsterPool[i].m_Data.monsterNumber = i;
                        //other Awake
                        monsterPool[i].m_Data.finalPosition = monsterPool[i].m_Data.Me_Object.transform.position;
                        monsterPool[i].m_Data.Me_Object.SetActive(false);
                    }
                    else
                    {
                        monsterPool[i] = Instantiate(MonsterSpecie[6], spawnPool_EarthElement[i - spawnPool_Bug.Length - spawnPool_Wolf.Length].transform).GetComponent<OtherMonsterAI>();
                        monsterPool[i].m_Data.monsterNumber = i;
                        //other Awake
                        monsterPool[i].m_Data.finalPosition = monsterPool[i].m_Data.Me_Object.transform.position;
                        monsterPool[i].m_Data.Me_Object.SetActive(false);
                }
            }
            RegisterDict(i, monsterPool[i].gameObject);
        }
        monsterPool[monsterPool.Length - 1] = Instantiate(MonsterSpecie[2], spawnBoss.transform).GetComponent<OtherMonsterAI>();
        monsterPool[monsterPool.Length - 1].m_Data.monsterNumber = (monsterPool.Length - 1);
        monsterPool[monsterPool.Length - 1].m_Data.Me_Object.SetActive(false);
        RegisterDict((monsterPool.Length - 1),monsterPool[monsterPool.Length - 1].gameObject);

    }
    public void OnMonsterMovementRecieve(int Number, Vector3 finalPos, float Angle,int currenthealth/*, bool IsAttack*/)
    {
        monsterPool[Number].m_Data.finalPosition = new Vector3(finalPos.x, monsterPool[Number].m_Data.Me_Object.transform.position.y, finalPos.z);
        monsterPool[Number].m_Data.Angle = Angle;
        monsterPool[Number].m_Data.m_AIStats.SetHp(currenthealth);
    }
    public void OnMonsterATR(int Number,int skillCommand) {
        if (monsterPool[Number].m_Data != null)
        {
            switch (skillCommand)
            {
                case 0:
                    monsterPool[Number].m_Data.CanAttack = false;
                    break;
                case 1:
                    monsterPool[Number].m_Data.CanAttack = true;
                    break;
                case 2:
                    monsterPool[Number].m_Data.TransferState(monsterPool[Number].m_Data, monsterPool[Number].m_Data.bossAI.BCM);
                    break;
                case 3:
                    monsterPool[Number].m_Data.TransferState(monsterPool[Number].m_Data, monsterPool[Number].m_Data.bossAI.BBS);
                    break;
                case 4:
                    monsterPool[Number].m_Data.TransferState(monsterPool[Number].m_Data, monsterPool[Number].m_Data.bossAI.BFR);
                    break;
            }
            if (skillCommand == 1)
            {
                monsterPool[Number].m_Data.CanAttack = true;
            }
            else if (skillCommand == 0)
            {
                monsterPool[Number].m_Data.CanAttack = false;
            }
        }
    }
    public void OnMonsterFocus(int Number,int playerNumber,bool isFocus) {
        if (isFocus)
        {
            monsterPool[Number].m_Data.FocusEnemy = PlayerManager.instance.GetPlayerGameObject(playerNumber);
        }
        else {
            monsterPool[Number].m_Data.FocusEnemy = null;
        }
    }
    public void setUpMonster(int Number,Vector3 MonsterPos,float monsterSpeed) {
        monsterPool[Number].m_Data.Me_Object.transform.position = MonsterPos;
        monsterPool[Number].m_Data.MoveSpeed = monsterSpeed;
    }
    public void SetMonsterMission(MissionClass mission) {
        if (mission.missionData.target.Length <= 0) { return; }
            for (int i = 0; i < (spawnPool_Bug.Length + spawnPool_Wolf.Length + spawnPool_EarthElement.Length+1); i++)
            {
                if (mission.missionData.target[0].tag == monsterPool[i].gameObject.tag)
                {
                    AIStats monster = monsterPool[i].gameObject.GetComponent<AIStats>();
                    monster.missionCheck += mission.CheckMission;
                }
            }
    }
    public void DeleteMonsterMission(MissionClass mission) {
            for (int i = 0; i < spawnPool_Bug.Length + spawnPool_Wolf.Length + spawnPool_EarthElement.Length; i++)
            {
                if (mission.missionData.target[0].tag == monsterPool[i].gameObject.tag)
                {
                    AIStats monster = monsterPool[i].gameObject.GetComponent<AIStats>();
                    monster.missionCheck -= mission.CheckMission;
                }
            }
    }

    Dictionary<GameObject, int> m_dictMonsterNumber = new Dictionary<GameObject, int>();
    void RegisterDict(int number, GameObject monster)
    {
        m_dictMonsterNumber.Add(monster, number);
    }
    public GameObject GetMonsterGameObject(int number)
    {
        return monsterPool[number].gameObject;
    }
    public int GetMonsterNumber(GameObject monster)
    {
        return m_dictMonsterNumber[monster];
    }
    public void SendMonsterState(int m_PlayerIndex) {
        //ClientApp.instance.SendOtherLoginSetMessage(monsterPool[0].m_Data.monsterNumber, monsterPool[0].transform.position, (monsterPool[0].m_Data.CanAttack) ? 1 : 0);
        foreach (OtherMonsterAI OTMon in monsterPool)
        {
            //可能爆傳導致當機
            ClientApp.instance.SendOtherLoginSetMessage(m_PlayerIndex, OTMon.m_Data.monsterNumber, OTMon.transform.position, (OTMon.m_Data.CanAttack) ? 1 : 0);
        }
    }
    public void SpawnBug()
    {
        foreach (var monsterpool in monsterPool) {
            if (monsterpool.gameObject.CompareTag("Bug")) { 
                monsterpool.gameObject.SetActive(true);
            }
        }
    }
    public void DisBug()
    {
        foreach (var monsterpool in monsterPool)
        {
            if (monsterpool.gameObject.CompareTag("Bug"))
            {
                monsterpool.m_Data.AttackDelayCount = 0;
                monsterpool.gameObject.SetActive(false);
            }
        }
    }
    public void SpawnEarth_Element()
    {

        foreach (var monsterpool in monsterPool)
        {
            if (monsterpool.gameObject.tag == "EarthElement")
            {
                monsterpool.gameObject.SetActive(true);
            }
        }
    }
    public void DisEarth_Element()
    {

        foreach (var monsterpool in monsterPool)
        {
            if (monsterpool.gameObject.tag == "EarthElement")
            {
                monsterpool.m_Data.AttackDelayCount = 0;
                monsterpool.gameObject.SetActive(false);
            }
        }
    }
    public void SpawnWolf() {
        foreach (var monsterpool in monsterPool)
        {
            if (monsterpool.gameObject.tag == "Wolf")
            {
                monsterpool.gameObject.SetActive(true);
            }
        }
    }
    public void DisWolf()
    {
        foreach (var monsterpool in monsterPool)
        {
            if (monsterpool.gameObject.tag == "Wolf")
            {
                monsterpool.m_Data.AttackDelayCount = 0;
                monsterpool.gameObject.SetActive(false);
            }
        }
    }
    public void SpawnBoss()
    {
        foreach (var monsterpool in monsterPool)
        {
            if (monsterpool.gameObject.tag == "Boss")
            {
                monsterpool.gameObject.SetActive(true);
            }
        }
    }
    public void DisBoss()
    {
        foreach (var monsterpool in monsterPool)
        {
            if (monsterpool.gameObject.tag == "Boss")
            {
                monsterpool.m_Data.AttackDelayCount = 0;
                monsterpool.gameObject.SetActive(false);
            }
        }
    }
    public void DetectEnemyIcon(AIData Data) {

    }
}
