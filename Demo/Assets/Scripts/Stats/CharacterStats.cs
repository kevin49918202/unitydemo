﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour {

    public int unitFrameModelIndex { protected set { m_UnitFrameModelIndex = (UnitFrameModelIndex)value; } get { return (int)m_UnitFrameModelIndex; } }
    public string characterName { protected set { m_CharacterName = value; } get { return m_CharacterName; } }
    public int level { protected set { m_Level = value; onLevelChanged(); } get { return m_Level; } }
    public int maxHp { protected set { m_MaxHp.SetBaseValue(value); onHpChanged(); } get { return m_MaxHp.GetValue(); } }
    public int maxMp { protected set { m_MaxMp.SetBaseValue(value); onMpChanged(); } get { return m_MaxMp.GetValue(); } }
    public int currentHp { protected set { m_CurrentHp = value; onHpChanged(); } get { return m_CurrentHp; } }
    public int currentMp { protected set { m_CurrentMp = value; onMpChanged(); } get { return m_CurrentMp; } }
    public int damage { protected set { m_Damage.SetBaseValue(value); } get { return m_Damage.GetValue(); } }
    public int armor { protected set { m_Armor.SetBaseValue(value); } get { return m_Armor.GetValue(); } }
    public int criticalProbability { protected set { m_CriticalProbability.SetBaseValue(value); } get { return m_CriticalProbability.GetValue(); } }

    public float GetModelRadius() { return modelRadius; }
    public float SetModelRadius(float s_modelRadius) { modelRadius = s_modelRadius; return modelRadius; }
    public event System.Action onLevelChanged = () => { };
    public event System.Action onHpChanged = () => { };
    public event System.Action onMpChanged = () => { };
    public event System.Action onGameObjectDissappear = () => { };
    public event System.Action onCharacterDie = () => { };
    public event System.Action onCharacterReset = () => { };
    public float damageRatio = 1;

    protected virtual void Awake()
    {
        InitialHpMp();
    }

    protected void Start()
    {
        //materials = new Material[rend.materials.Length + 1];
        materials = GetComponentInChildren<Renderer>().materials;
        outlinedic.Add(LayerMask.NameToLayer("Npc"), Color.green);
        outlinedic.Add(LayerMask.NameToLayer("Monster"), Color.red);
        outlinedic.Add(LayerMask.NameToLayer("Player"), Color.green);
    }

    public virtual void TakeDamage(GameObject owner, int damage, bool critical, string damageType, bool sendToOther)
    {
        if (damage == 0 || currentHp <= 0) return;

        damage -= armor;

        if (critical) damage *= 2;
        damage = (int)(damage * damageRatio);
        damage = Mathf.Clamp(damage, 1, int.MaxValue);
        FloatingTextManager.instance.CreatDamageFloatingText(damageType + damage.ToString(), this, gameObject.layer, critical);

        if (damage >= currentHp)
        {
            currentHp = 0;
            Die();
        }
        else
        {
            currentHp -= damage;
        }
    }

    public virtual void CostMp(int cost)
    {
        if (cost > currentMp)
        {
            currentMp = 0;
        }
        else
        {
            currentMp -= cost;
        }
    }

    public virtual void AddMp(int value)
    {
        Debug.Log("Mp回復 " + value);
        //
        if (value + currentMp > maxMp)
        {
            currentMp = maxMp;
        }
        else
        {
            currentMp += value;
        }
    }

    public virtual void AddHp(int value)
    {
        Debug.Log("Hp回復 " + value);

        if (value + currentHp > maxHp)
        {
            currentHp = maxHp;
        }
        else
        {
            currentHp += value;
        }
    }

    public virtual void Die()
    {
        onCharacterDie();
    }

    //Dissable時 關注此目標的玩家解除關注
    public void OnDisappaer()
    {
        onGameObjectDissappear();
    }

    public void InitialHpMp()
    {
        currentHp = maxHp;
        currentMp = maxMp;
    }
    public virtual void Reset()
    {
        Debug.Log("reset");
        onCharacterReset();
    }

    public void SetHp(int hp) {
        if(currentHp != hp)
        {
            currentHp = hp;
        }
    }

    [SerializeField] UnitFrameModelIndex m_UnitFrameModelIndex;
    [SerializeField] protected string m_CharacterName;
    [SerializeField] protected int m_Level = 1;
    [SerializeField] protected Stat m_MaxHp;
    [SerializeField] protected Stat m_MaxMp;
    protected int m_CurrentHp;
    protected int m_CurrentMp;

    [SerializeField] protected Stat m_Damage;
    [SerializeField] protected Stat m_Armor;
    [SerializeField] protected Stat m_CriticalProbability;
    [SerializeField] protected float modelRadius = 1;

    public enum UnitFrameModelIndex { Witch=0, Warrior, Bug, Guard, Dwarf, Wolf, Element, Captain, Potion};

    bool isTargeted;

    protected void OnMouseEnter()
    {
        if (!isTargeted && !Revival.instance.isEnableUI)
            AddOutline();
    }
    protected void OnMouseExit()
    {
        if (!isTargeted && !Revival.instance.isEnableUI)
            DeleteOutline();
    }

    public void FocusOutline()
    {
        isTargeted = true;
        for (int i = 0; i < materials.Length; i++)
        {
            materials[i].SetFloat("_Outline", OutlineValue * FocusValue);
            if (outlinedic.ContainsKey(gameObject.layer))
            {
                materials[i].SetColor("_OutlineColor", outlinedic[gameObject.layer]);
            }
        }
    }
    public void AddOutline()
    {
        for (int i = 0; i < materials.Length; i++)
        {
            materials[i].SetFloat("_Outline", OutlineValue);
            if (outlinedic.ContainsKey(gameObject.layer))
            {
                materials[i].SetColor("_OutlineColor", outlinedic[gameObject.layer]);
            }
        }

        Revival.instance.onEnableUI += DeleteOutline;
    }
    public void DeleteOutline()
    {
        isTargeted = false;
        for (int i = 0; i < materials.Length; i++)
        {
            materials[i].SetFloat("_Outline", 0);
        }

        Revival.instance.onEnableUI -= DeleteOutline;
    }
    /// <summary>
    /// Outline參數
    /// </summary>
    Material[] materials;
    [SerializeField] float FocusValue = 1.5f;
    [SerializeField] float OutlineValue = 0.025f;
    Dictionary<LayerMask, Color> outlinedic = new Dictionary<LayerMask, Color>();
    bool addoutline;
}


