﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherPlayerStats : CharacterStats
{
    public string job;
    [SerializeField] GameObject bloodParticle;
    [SerializeField] GameObject levelUpParticle;
    
    AudioSource audiosource;
    [SerializeField]AudioClip levelupaudio;

    public event System.Action onTakeDamage = () => { };

    protected override void Awake()
    {
        base.Awake();
        RegisterActionToEquipmentManager();
        Reset();
        audiosource = GetComponent<AudioSource>();
    }

    protected void RegisterActionToEquipmentManager()
    {
        OtherPlayerEquipment playerEquipment = GetComponent<OtherPlayerEquipment>();
        if (playerEquipment)
        {
            playerEquipment.onEquipmentChanged += OnEquipmentChanged;
        }
        else
        {
            Debug.Log("EquipmentManager is not found");
        }
    }

    protected virtual void OnEquipmentChanged(EquipmentData newItem, EquipmentData oldItem)
    {
        if (newItem)
        {
            m_MaxHp.AddModifier(newItem.hpModifier);
            m_MaxMp.AddModifier(newItem.mpModifier);
            m_Armor.AddModifier(newItem.armorModifier);
            currentHp += newItem.hpModifier;
            currentMp += newItem.mpModifier;
        }

        if (oldItem)
        {
            m_MaxHp.RemoveModifier(oldItem.hpModifier);
            m_MaxMp.RemoveModifier(oldItem.mpModifier);
            m_Armor.RemoveModifier(oldItem.armorModifier);

            if (currentHp >= maxHp)
                currentHp = maxHp;
            if (currentMp >= maxMp)
                currentMp = maxMp;
        }
    }

    public override void TakeDamage(GameObject owner, int damage, bool critical, string damageType, bool sendToOther)
    {
        if (damage == 0 || currentHp <= 0) return;
        if (sendToOther)
        {
            ClientApp.instance.SendPlayerTakeDamageMessage(gameObject, owner, damage, critical);
        }

        if (critical) damage *= 2;

        damage -= armor;
        damage = Mathf.Clamp(damage, 1, int.MaxValue);
        FloatingTextManager.instance.CreatDamageFloatingText(damageType + damage.ToString(), this, gameObject.layer, critical);

        if (damage >= currentHp)
        {
            currentHp = 0;
        }
        else
        {
            currentHp -= damage;
        }
        if (bloodParticle != null)
        {
            Instantiate(bloodParticle, transform.position, transform.rotation);
        }

        onTakeDamage();
    }

    public override void Die()
    {
        base.Die();
        GetComponent<Animator>().SetBool("Death", true);
        gameObject.layer = LayerMask.NameToLayer("Default");
        foreach (GameObject player in PlayerManager.instance.GetAllPlayer()) {
            if (player.GetComponent<OtherPlayerStats>().currentHp > 0) {
                return;
            }
        }
        Invoke("ResetMission", 5);
    }

    void ResetMission()
    {
        MissionManager.missionmanager.ResetMission();
    }

    public override void Reset()
    {
        base.Reset();
        GetComponent<Animator>().SetBool("Death", false);
        UpdateLevelBaseValue();
        InitialHpMp();
        Invoke("ReturnMask", 0.3f);
    }
    void ReturnMask() {
        gameObject.layer = LayerMask.NameToLayer("Player");
    }

    public virtual void SetInfo(AccountCharacterInfo info)
    {
        characterName = info.characterName;
        job = info.job;
        level = info.level;
        Reset();
    }

    public void OnLevelUpReceive(int level)
    {
        if(this.level != level)
        {
            LevelUp(level);
        }
    }

    public virtual void LevelUp(int level)
    {
        this.level = level;
        UpdateLevelBaseValue();
        audiosource.PlayOneShot(levelupaudio);
        Destroy(Instantiate(levelUpParticle, transform.position, transform.rotation,transform),5f);
    }

    public void OnChangeHpMpReceive(int receiveHp, int receiveMp)
    {
        currentHp = receiveHp;
        currentMp = receiveMp;
    }

    protected virtual void UpdateLevelBaseValue()
    {
        UpdateMaxHpBasedOnLevel();
        UpdateMaxMpBasedOnLevel();
        UpdateArrmorBasedOnLevel();
    }

    protected void UpdateMaxHpBasedOnLevel()
    {
        maxHp = baseHpValue + perLevelHpValue * level;
    }

    protected void UpdateMaxMpBasedOnLevel()
    {
        maxMp = baseMpValue + perLevelMpValue * level;
    }

    protected void UpdateArrmorBasedOnLevel()
    {
        armor = baseArmorValue + perLevelArmorValue * level;
    }

    protected int levelLimit = 20;
    protected int baseHpValue = 300;
    public int perLevelHpValue = 100;

    protected int baseMpValue = 150;
    public int perLevelMpValue = 50;

    protected int baseDamageValue = 10;
    public int perLevelDamageValue = 15;

    protected int baseArmorValue = 8;
    public int perLevelArmorValue = 8;
}

