﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : OtherPlayerStats {

    public int money { protected set { m_Money = value; onMoneyChanged(); } get { return m_Money; } }
    public int exp { protected set { m_Exp = value; onExpChanged(); } get { return m_Exp; } }
    public event System.Action onMoneyChanged = () => { };
    public event System.Action onExpChanged = () => { };
    public event System.Action onValueChanged = () => { };
    public event System.Action<PlayerStats> onLevelUp = (a) => { };
    [SerializeField] LevelMap levelMap;

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void OnEquipmentChanged(EquipmentData newItem, EquipmentData oldItem)
    {
        base.OnEquipmentChanged(newItem, oldItem);
        if (newItem)
        {
            m_Damage.AddModifier(newItem.damageModifier);
            m_CriticalProbability.AddModifier(newItem.criticalModifier);
        }

        if (oldItem)
        {
            m_Damage.RemoveModifier(oldItem.damageModifier);
            m_CriticalProbability.RemoveModifier(oldItem.criticalModifier);
        }
        onValueChanged();
    }

    public override void TakeDamage(GameObject owner, int damage, bool critical, string damageType, bool sendToOther)
    {
        base.TakeDamage(owner, damage, critical, damageType, sendToOther);
        if(currentHp <= 0)
        {
            Die();
        }
    }

    public override void Die()
    {
        base.Die();
        ClientApp.instance.SendPlayerDieMessage();
    }

    public override void AddHp(int value)
    {
        base.AddHp(value);
        ClientApp.instance.SendPlayerChangeHpMpMessage(currentHp, currentMp);
        FloatingTextManager.instance.CreateHpMpText("+" + value, true);
    }

    public override void AddMp(int value)
    {
        base.AddMp(value);
        ClientApp.instance.SendPlayerChangeHpMpMessage(currentHp, currentMp);
        FloatingTextManager.instance.CreateHpMpText("+" + value, false);
    }

    protected override void UpdateLevelBaseValue()
    {
        base.UpdateLevelBaseValue();
        UpdateDamageBasedOnLevel();
        onValueChanged();
    }

    void UpdateDamageBasedOnLevel()
    {
        damage = baseDamageValue + perLevelDamageValue * level;
    }

    public void AddExp(int value)
    {
        if (level == levelLimit) return;
        if (currentHp == 0) return;
        ChatBoxUIFunc.chatboxuifunc.SetSystemLog("經驗值 +" + value.ToString());
        FloatingTextManager.instance.CreateExperienceText("經驗值 +" + value.ToString());
        exp += value;
        while (exp >= levelMap.levelMap[level])
        {
            exp -= levelMap.levelMap[level];
            LevelUp(level + 1);
            if (level == levelLimit)
            {
                exp = 0;
            }
        }
        onExpChanged();
    }

    public override void LevelUp(int level)
    {
        base.LevelUp(level);
        if(level == 10)
        {
            FloatingTextManager.instance.CreateLearnNewSkillText();
        }
        onLevelUp(this);
        FloatingTextManager.instance.CreateLevelUpText();
    }

    public int GetExpNeededCurrentLevel()
    {
        return levelMap.levelMap[level];
    }

    public void AddMoney(int value)
    {
        money += value;
        ClientApp.instance.SendPlayerMoneyMessage(money);
    }

    public bool CostMoney(int value)
    {
        if(money - value < 0)
        {
            return false;
        }

        money -= value;
        ClientApp.instance.SendPlayerMoneyMessage(money);
        return true;
    }

    public override void SetInfo(AccountCharacterInfo info)
    {
        exp = info.exp;
        money = info.money;
        base.SetInfo(info);
    }

    int m_Exp;
    int m_Money;
}

