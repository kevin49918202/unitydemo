﻿using UnityEngine;

[CreateAssetMenu(fileName = "New LevelMap", menuName = "Create/LevelMap")]
public class LevelMap : ScriptableObject
{
    public int[] levelMap;
}