﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAI : OtherMonsterAI
{
    //MFSM FSM;
    public BossIdleState BIS;
    public BossChaseState BCS;
    public BossAttackState BAS;
    public BossDeadState BDS;
    public BossCastMetorState BCM;
    //public BossFireSpikeState BFS;
    public BossBigStompState BBS;
    public BossFarRangeStompState BFR;

    
    // Use this for initialization
    public override void Awake()
    {
        m_Data.MonsterCollider = GetComponent<Collider>();
        m_Data.MonsterMesh = GetComponentInChildren<Renderer>();
        m_Data.m_AIStats = GetComponent<BossAIStats>();
        m_Data.Me_Object = this.gameObject;
        m_Data.rbody = GetComponent<Rigidbody>();
        m_Data.Monsteranim = GetComponent<MonsterAnimScript>();
        m_Data.Monsteranim.anim = GetComponent<Animator>();
        m_Data.BornPoint = m_Data.Me_Object.transform.position;
        m_Data.monsterAI = GetComponent<OtherMonsterAI>();
        OMS = new OtherMonsterState();

        m_Data.m_currentState = new MonsterState();
       // m_Data.bossAI = GetComponent<BossAI>();
    }
    public override void Start()
    {
        BIS = new BossIdleState();
        BCS = new BossChaseState();
        BAS = new BossAttackState();
        BDS = new BossDeadState();
        BCM = new BossCastMetorState();
        BBS = new BossBigStompState();
        BFR = new BossFarRangeStompState();

        m_Data.TransferState(m_Data, BIS);
    }
    public IEnumerator BigStompCDCount(AIData Data, float CD)
    {
        while (CD > 0) {
            CD -= 0.2f;
            yield return new WaitForSeconds(0.2f);
        }
        Data.CanCastBigStomp = true;
    }
    public IEnumerator FarRangeStompCDCount(AIData Data, float CD)
    {
        while (CD > 0)
        {
            CD -= 0.2f;
            yield return new WaitForSeconds(0.2f);
        }
        Data.CanCastFarRangeStomp = true;
    }
    public IEnumerator MeteorCDCount(AIData Data, float CD)
    {
        while (CD > 0)
        {
            CD -= 0.2f;
            yield return new WaitForSeconds(0.2f);
        }
        Data.CanCastMeteor = true;
    }

    // Update is called once per frame
    //public override void FixedUpdate()
    //{
    //    if (m_Data.m_currentState != null)
    //    {
    //        m_Data.Monsteranim.MonsterAnimController(m_Data);
    //        m_Data.CheckEnemyInSight();
    //        m_Data.m_currentState.Do(m_Data);
    //    }
    //    if (m_Data.IsDead && m_Data.m_currentState != m_Data.bossAI.MDS && !m_Data.InDeadState)
    //    {
    //        m_Data.TransferState(m_Data, m_Data.bossAI.MDS);
    //    }
    //    if (Input.GetKeyDown(KeyCode.E))
    //    {
    //        m_Data.TransferState(m_Data, m_Data.bossAI.BCM);
    //        //m_Data.IsDead = true;
    //    }
    //    if (Input.GetKeyDown(KeyCode.R))
    //    {
    //        m_Data.TransferState(m_Data, m_Data.bossAI.BFS);
    //        //m_Data.IsDead = true;
    //    }
    //    if (Input.GetKeyDown(KeyCode.T))
    //    {
    //        m_Data.TransferState(m_Data, m_Data.bossAI.BBS);
    //        //m_Data.IsDead = true;
    //    }
    //    if (Input.GetKeyDown(KeyCode.Y))
    //    {
    //        m_Data.TransferState(m_Data, m_Data.bossAI.BBT);
    //        //m_Data.IsDead = true;
    //    }

    //}
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        if (m_Data.Enemy.Length > 0 )
        {
            Gizmos.color = Color.red;
        }
        Gizmos.DrawWireSphere(transform.position, m_Data.DetectRadius);

        if (m_Data.steeringPoint != Vector3.zero)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(m_Data.BornPoint, m_Data.SteeringRange);
        }
        Gizmos.color = Color.black;
        Gizmos.DrawWireSphere(transform.position,m_Data.MiddleSkillRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, m_Data.AttackRange);
    }
}
