﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherMonsterAI : MonoBehaviour {

    public AIData m_Data;
    public float Angle;
    public bool FirstLoginCheck;
    public OtherMonsterState OMS;
    public MonsterDeadState MDS;
    public MonsterIdleState MIS;
    public MonsterChaseState MCS;
    public MonsterAttackState MAS;

    float delayTimeForSnedMsg = 1f;
    float delayCurrentTime;
    Vector3 m_LastSendPos;
    float m_LastSendAngle;

    // Use this for initialization
    public virtual void Awake() {
        m_Data.MonsterCollider = GetComponent<Collider>();
        m_Data.MonsterMesh = GetComponentInChildren<Renderer>();
        m_Data.m_AIStats = GetComponent<AIStats>();
        m_Data.Me_Object = this.gameObject;
        m_Data.rbody = GetComponent<Rigidbody>();
        m_Data.Monsteranim = GetComponent<MonsterAnimScript>();
        m_Data.BornPoint = m_Data.Me_Object.transform.position;
        m_Data.m_currentState = new MonsterState();
        MDS = new MonsterDeadState();
        MIS = new MonsterIdleState();
        MCS = new MonsterChaseState();
        MAS = new MonsterAttackState();
        m_Data.monsterAI = GetComponent<OtherMonsterAI>();
        OMS = new OtherMonsterState();
    }
    protected void OnEnable()
    {
        ClientApp.instance.SendMonsterActiveMessage(m_Data.monsterNumber,true);
        m_Data.steeringIntervalCount = Time.time + m_Data.steeringInterval;
        if (gameObject.CompareTag("Bug")) {
            if (m_Data.Monsteranim.anim == null) {
                m_Data.Monsteranim = GetComponent<MonsterAnimScript>();
                m_Data.Monsteranim.anim = GetComponent<Animator>();
            }
            m_Data.Monsteranim.anim.SetTrigger("Emerge");
        }
    }
    public virtual void Start() {
        m_Data.TransferState(m_Data, OMS);
    }

    // Update is called once per frame
    protected void FixedUpdate() {
        if (Input.GetKeyDown(KeyCode.E)) {
            Debug.Log(m_Data.m_currentState);
        }
        if (m_Data.m_currentState != null)
        {
            m_Data.Monsteranim.MonsterAnimController(m_Data);
            m_Data.m_currentState.Do(m_Data);
        }
        //if (m_Data.IsDead && !m_Data.InDeadState)
        //{
        //    if (m_Data.bossAI != null && m_Data.m_currentState != m_Data.bossAI.BDS)
        //    {
        //        m_Data.TransferState(m_Data, m_Data.bossAI.BDS);
        //    }
        //    else if (m_Data.m_currentState != m_Data.monsterAI.MDS && m_Data.monsterAI != null)
        //    {
        //        m_Data.TransferState(m_Data, m_Data.monsterAI.MDS);
        //    }
        //}
        SendMonserMonsterMessage();
    }
    void SendMonserMonsterMessage()
    {
        if (ClientApp.instance.host)
        {
            if (delayCurrentTime > delayTimeForSnedMsg)
            {
                Debug.Log("sendmove");
                //if (Vector3.Distance(m_LastSendPos, m_Data.Me_Object.transform.position) > 0 || (m_LastSendAngle - m_Data.Me_Object.transform.eulerAngles.y) != 0)
                //{
                m_LastSendPos = m_Data.Me_Object.transform.position;
                    m_LastSendAngle = m_Data.Me_Object.transform.eulerAngles.y;
                    ClientApp.instance.SendMonsterMovementMessage(m_Data.monsterNumber, m_LastSendPos, m_LastSendAngle,m_Data.m_AIStats.currentHp/*,m_Data.CanAttack*/);
                //}
                delayCurrentTime -= delayTimeForSnedMsg;
            }
            else
            {
                delayCurrentTime += Time.deltaTime;
            }
        }
    }
    public IEnumerator monsterattackCount(AIData Data,float Time) {
        yield return new WaitForSeconds(Time);
        Data.AttackDelayCount = 0;
    }

}
