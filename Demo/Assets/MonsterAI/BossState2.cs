﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossState2 : MonsterState2
{
    public BossState2()
    {
    }
    public override void DoBeforeEnter(AIData2 Data)
    {
        //Debug.Log("In Monster State");

    }
    public override void DoBeforeLeave(AIData2 Data)
    {
        //Debug.Log("Leave Monster State");
    }
    public override void Do(AIData2 Data)
    {
        //Debug.Log("Monster State");
    }
}

public class BossIdleState2 : MonsterState2
{
    public override void DoBeforeEnter(AIData2 Data)
    {
        Data.steeringPoint = Data.Me_Object.transform.position;
        Data.steeringIntervalCount = Time.time + Data.steeringInterval;
    }
    public override void DoBeforeLeave(AIData2 Data)
    {
    }
    public override void Do(AIData2 Data)
    {
        if (ClientApp.instance.host)
        {
            if (Data.FocusEnemy == null)
            {
                if (Data.steeringIntervalCount < Time.time && Data.rbody.velocity == new Vector3(0, Data.rbody.velocity.y, 0))
                {
                    Data.steeringIntervalCount = Time.time + Data.steeringInterval;
                    //if (Data.SteeringRange == 0)
                    //{
                    //    Debug.LogError("沒有SteeringRange");
                    //    return;
                    //}

                    Data.steeringPoint = Data.BornPoint + Random.insideUnitSphere * Data.SteeringRange;
                    Data.steeringPoint.y = Data.Me_Object.transform.position.y;
                    Data.OriginalPoint = Data.rbody.position;
                }
                if (Vector3.Distance(Data.steeringPoint, Data.Me_Object.transform.position) > 0.15f)
                {
                    MonsterRotate(Data, Data.steeringPoint);
                    Data.rbody.velocity = Vector3.Normalize(Data.steeringPoint - Data.rbody.transform.position) * Data.MoveSpeed * Mathf.Clamp(Vector3.Distance(Data.steeringPoint, Data.rbody.transform.position) / Vector3.Distance(Data.steeringPoint, Data.OriginalPoint) * 2, 0.6f, 1f);
                    Data.rbody.velocity = new Vector3(Data.rbody.velocity.x, Data.rbody.velocity.y, Data.rbody.velocity.z);
                    Data.CanRun = true;
                }
                else
                {
                    Data.rbody.velocity = new Vector3(0, Data.rbody.velocity.y, 0);
                    Data.Me_Object.transform.position = Data.steeringPoint;
                    Data.OriginalPoint = Data.Me_Object.transform.position;
                    Data.CanRun = false;
                }
            }
            //if (Data.HasFocusEnemy() == true)
            //{
            //    Data.TransferState(Data, Data.bossAI.BCS);
            //}
            if (DetectEnemy(Data) || Data.FocusEnemy != null)
            {
                if (Data.Enemy.Length > 0)
                {
                    Data.FocusEnemy = Data.Enemy[0].gameObject;
                }
                Data.FocusPlayerStats = Data.Enemy[0].gameObject.GetComponent<OtherPlayerStats>();
                ClientApp.instance.SendMonsterFocusMessage(Data.monsterNumber, PlayerManager.instance.GetPlayerIndex(Data.FocusEnemy.gameObject));
                Data.TransferState(Data, AIData2.State.Chase);
            }
        }
        else {
            PosCheck(Data);
            RotateCheck(Data);
        }
    }
}
public class BossChaseState2 : MonsterState2
{
    public override void DoBeforeEnter(AIData2 Data)
    {
        Data.CanRun = true;
    }
    public override void DoBeforeLeave(AIData2 Data)
    {
        Data.rbody.velocity = new Vector3(0, 0, 0);
        Data.CanRun = false;
    }
    public override void Do(AIData2 Data)
    {
        if (Data.FocusEnemy != null && Vector3.Distance(Data.FocusEnemy.transform.position, Data.BornPoint) < Data.AbsDetectRadius)
        {
            //每秒根據角度大小轉身－角度越大轉越多－反之則越小
            MonsterRotate(Data, Data.Enemy[0].transform.position);
            if (Data.Distance(Data.Me_Object.transform, Data.FocusEnemy.transform) > Data.MiddleSkillRange && Data.CanCastFarRangeStomp)
            {
                Data.TransferState(Data, AIData2.State.FarRangStomp);
                return;
            }
            else
            if (Data.Distance(Data.Me_Object.transform, Data.FocusEnemy.transform) > Data.AttackRange)
            {
                if (Data.Monsteranim.anim.GetCurrentAnimatorStateInfo(0).IsName("Run"))
                {
                    Vector3 velocity = Vector3.Normalize(Data.Enemy[0].transform.position - Data.rbody.transform.position) * Data.MoveSpeed;
                    velocity.y = 0;
                    Data.rbody.velocity = velocity;
                }
                else {
                    Data.TransferState(Data, AIData2.State.Chase);
                }
            }
            else if (Data.AttackDelayCount <= 0)
            {
                Debug.Log("2");
                Data.TransferState(Data, AIData2.State.Attack);
            }
            else
            {
                Debug.Log("3");
                Data.rbody.velocity = new Vector3(0, 0, 0);
                Data.CanRun = false;
                Data.AttackDelayCount -= Time.deltaTime;
            }
        }
        else
        {
            ClientApp.instance.SendMonsterDisFocusMessage(Data.monsterNumber, false);
            Data.FocusEnemy = null;
            Data.FocusPlayerStats = null;
            Data.TransferState(Data, AIData2.State.Idle );
        }
    }
}
public class BossAttackState2 : MonsterState2
{
    public override void DoBeforeEnter(AIData2 Data)
    {
        Data.CanAttack = true;
        ClientApp.instance.SendMonsterAttackAnimMessage(Data.monsterNumber, (Data.CanAttack ? 1 : 0));
    }
    public override void DoBeforeLeave(AIData2 Data)
    {
        Data.CanAttack = false;
        ClientApp.instance.SendMonsterAttackAnimMessage(Data.monsterNumber,(Data.CanAttack ? 1:0));
    }
    public override void Do(AIData2 Data)
    {
        if (Data.Enemy.Length > 0)
        {
            if (Data.Distance(Data.Me_Object.transform, Data.Enemy[0].transform) > Data.AttackRange + 1f)
            {
                Data.TransferState(Data, AIData2.State.Chase);
            }
            else
            {
                if (((float)Data.m_AIStats.currentHp / (float)Data.m_AIStats.maxHp) <= 0.7 && Data.CanCastBigStomp)
                {
                    Data.TransferState(Data, AIData2.State.BigStomp);
                }
                else
                {
                    if (Data.AttackDelayCount > 0) {
                        Data.CanAttack = false;
                        Data.AttackDelayCount -= Time.deltaTime;
                    }
                    else{
                        Data.CanAttack = true;
                        Data.AttackDelayCount += Data.AttackDelay;
                    }
                    MonsterRotate(Data, Data.Enemy[0].transform.position);
                }
            }
        }
        else {
            Data.TransferState(Data, AIData2.State.Idle);
        }
    }
}
public class BossDeadState2 : MonsterState2 
{
    public override void DoBeforeEnter(AIData2 Data)
    {
        Data.rbody.velocity = new Vector3(0, Data.rbody.velocity.y, 0);
        Data.DeadDisapearTimeCount = Data.DeadDisapearTime;
        Data.Monsteranim.anim.SetTrigger("DeathTrigger");
        Data.InDeadState = true;
        Data.rbody.useGravity = true;
    }
    public override void DoBeforeLeave(AIData2 Data)
    {
        Data.m_AIStats.Reset();
        Data.IsDead = false;
        Data.InDeadState = false;
        Data.DeadDisapearTimeCount = Data.DeadDisapearTime;
        Data.MonsterMesh.enabled = true;
        Data.MonsterCollider.enabled = true;
    }
    public override void Do(AIData2 Data)
    {
        if (Data.DeadDisapearTimeCount > 0)
        {
            Data.DeadDisapearTimeCount -= Time.deltaTime;
            Data.rbody.velocity = new Vector3(0, Data.rbody.velocity.y, 0);
            //shaderdisapeartime = Data.DeadDisapearTime;
        }
        else if (Data.DeadDisapearTimeCount < 0)
        {
            //傳訊息給物件池
            Data.Me_Object.transform.position = Data.BornPoint;
            Data.TransferState(Data, AIData2.State.Idle);
        }
    }
}
public class BossCastMetorState2 : MonsterState2
{
    public override void DoBeforeEnter(AIData2 Data)
    {
        Data.CanCastMeteor = false;
        Data.monsterAI.StartCoroutine(Data.monsterAI.MeteorCDCount(Data, Data.MeteorCastCD));
        if (ClientApp.instance.host)
        {
            ClientApp.instance.SendMonsterAttackAnimMessage(Data.monsterNumber, 2);
        }
        Data.rbody.velocity = new Vector3(0, 0, 0);
        Data.Monsteranim.anim.SetTrigger("Meteor");
    }
    public override void DoBeforeLeave(AIData2 Data)
    {
    }
    public override void Do(AIData2 Data)
    {
    }
}


public class BossBigStompState2 : MonsterState2
{
    public override void DoBeforeEnter(AIData2 Data)
    {
        Data.CanCastBigStomp = false;
        Data.monsterAI.StartCoroutine(Data.monsterAI.BigStompCDCount(Data, Data.BigStompCastCD));
        if (ClientApp.instance.host)
        {
            ClientApp.instance.SendMonsterAttackAnimMessage(Data.monsterNumber, 3);
        }
        Data.rbody.velocity = new Vector3(0, 0, 0);
        Data.Monsteranim.anim.SetTrigger("BigStomp");
    }
    public override void DoBeforeLeave(AIData2 Data)
    {
    }
    public override void Do(AIData2 Data)
    {

    }
}
public class BossFarRangeStompState2 : MonsterState2
{
    public override void DoBeforeEnter(AIData2 Data)
    {
        Data.FarRangStompCDCount = Data.FarRangStompCD;
        if (ClientApp.instance.host) {
            ClientApp.instance.SendMonsterAttackAnimMessage(Data.monsterNumber, 4);
        }
        Data.monsterAI.StartCoroutine(Data.monsterAI.FarRangeStompCDCount(Data, Data.FarRangStompCD));
        Data.CanCastFarRangeStomp = false;
        Data.rbody.velocity = new Vector3(0, 0, 0);
        Data.Monsteranim.anim.SetTrigger("FarRangeStomp");
    }
    public override void DoBeforeLeave(AIData2 Data)
    {
        //Data.Monsteranim.anim.SetBool("CastComplete", false);
    }
    public override void Do(AIData2 Data)
    {
    }
}


