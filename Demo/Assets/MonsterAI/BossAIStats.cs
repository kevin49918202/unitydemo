﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAIStats : AIStats
{
    [SerializeField] AudioClip victoryAudio;
    BossMiniMapIcon miniMapIcon;

    protected override void Awake()
    {
        base.Awake();
        miniMapIcon = GameObject.FindGameObjectWithTag("BossIcon").GetComponent<BossMiniMapIcon>();
    }
    void OnEnable()
    {
        if(BossHpObserver.instance)
            BossHpObserver.instance.Enable(this);
        onHpChanged += CheckDamageRatio;
        miniMapIcon.SetTarget(transform);
        miniMapIcon.gameObject.SetActive(true);
    }

    void OnDisable()
    {
        if(BossHpObserver.instance)
            BossHpObserver.instance.Disable();
        onHpChanged -= CheckDamageRatio;
        miniMapIcon.SetTarget(null);
        miniMapIcon.gameObject.SetActive(false);
    }

    public override void TakeDamage(GameObject owner, int damage, bool critical, string damageType, bool sendToOther)
    {
        base.TakeDamage(owner, damage, critical, damageType, sendToOther);

        if (((float)currentHp / maxHp) <= 0.35 && currentHp > 0 && monsterai.m_Data.CanCastMeteor)
        {
            monsterai.m_Data.TransferState(monsterai.m_Data, monsterai.m_Data.bossAI.BCM);
            CoolDownManager.instance.cdRatio = 0.5f;
            StartCoroutine(AudioManager.audiomanager.ChangeAudio(victoryAudio));
        }
    }
    
    void CheckDamageRatio()
    {
        if((float)currentHp/maxHp <= 0.35)
        {
            if(damageRatio != 0.5f)
            {
                damageRatio = 0.5f;
                Debug.Log("boss血量低於傷害減半");
            }
        }
        else
        {
            if(damageRatio != 1)
            {
                damageRatio = 1;
                Debug.Log("Boss傷害比率1");
            }
        }
    }
}
