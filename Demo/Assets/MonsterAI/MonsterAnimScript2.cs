﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAnimScript2 : MonoBehaviour{
    [HideInInspector]
    public Animator anim;
    AIData2 m_Data;
    //[SerializeField] float DestroyTime = 30f;
    private void Awake()
    {
        anim = GetComponent<Animator>();
    }
    private void Start()
    {
        m_Data = GetComponent<OtherMonsterAI2>().m_Data;
    }
    public void MonsterAnimController(AIData Data)
    {
        anim.SetBool("CanRun", Data.CanRun);
        anim.SetBool("CanAttack", Data.CanAttack);
        anim.SetBool("Death", Data.IsDead);
        anim.SetFloat("MoveType", Data.HasEnemy ? 1 : 0);
    }
    public void MonsterAnimController(AIData2 Data)
    {
        anim.SetBool("CanRun", Data.CanRun);
        anim.SetBool("CanAttack", Data.CanAttack);
        anim.SetBool("Death", Data.IsDead);
        anim.SetFloat("MoveType", Data.HasEnemy ? 1 : 0);
    }
    void BugBornAnim() {
        Instantiate(MonsterManager.monsterInstance.BugBornPT, transform.position, transform.rotation);
    }
    void AttackAnim()
    {
        if (ClientApp.instance.host)
        {
            if (m_Data.FocusEnemy == null) {
                return;
            }
            m_Data.FocusPlayerStats.TakeDamage(m_Data.Me_Object, m_Data.m_AIStats.damage, false, "", true);
            if (m_Data.FocusPlayerStats.currentHp <= 0)
            {
                ClientApp.instance.SendMonsterDisFocusMessage(m_Data.monsterNumber, false);
                m_Data.FocusEnemy = null;
            }
        }
    }
    void MonsterAnimDone() {
        if (ClientApp.instance.host)
        {
            m_Data.TransferState(m_Data, AIData2.State.Chase);
        }
        else {
        }
    }
    void AttackType() {
        m_Data.Monsteranim.anim.SetFloat("AttackType", Random.Range(0, 2));
    }
    void DieAnim()
    {
        GameObject chest;
        if (m_Data.MonsterChest == null)
        {
            chest = MonsterManager.monsterInstance.DefaultChest;
        }
        else
        {
            chest = m_Data.MonsterChest;
        }
        Vector3 chestBornPoint;
        if (m_Data.Me_Object.CompareTag("Boss"))
        {
            chestBornPoint = new Vector3(m_Data.Me_Object.transform.position.x, m_Data.Me_Object.transform.position.y+15, m_Data.Me_Object.transform.position.z); ;
        }
        else {
            chestBornPoint = new Vector3(m_Data.Me_Object.transform.position.x, m_Data.Me_Object.transform.position.y, m_Data.Me_Object.transform.position.z); ;
        }
        ChestController chestcontrol = Instantiate(chest, chestBornPoint, m_Data.Me_Object.transform.rotation).GetComponent<ChestController>();
        foreach (ItemData chestItem in m_Data.MonsterDrop) {
            chestcontrol.ChestItem.Add(chestItem);
        }
        {
            Instantiate(m_Data.DieParticle, m_Data.Me_Object.transform.position, m_Data.MonsterMesh.transform.rotation);
            Instantiate(MonsterManager.monsterInstance.DieExplosion, m_Data.MonsterMesh.transform.position, m_Data.MonsterMesh.transform.rotation);
        }
        m_Data.MonsterMesh.enabled = false;
        m_Data.MonsterCollider.enabled = false;
    }
    void bossDieAnimStart() {
        m_Data.Monsteranim.anim.SetLayerWeight(1, 0);
    }
    void BugDieAnim() {
        if (MonsterManager.monsterInstance.BugDieParticle != null)
        {
            Instantiate(MonsterManager.monsterInstance.BugDieParticle, transform.position, transform.rotation);
        }
    }
}
