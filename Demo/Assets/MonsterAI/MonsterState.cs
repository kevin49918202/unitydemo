﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterState {
    public MonsterState() {
    }
    public virtual void DoBeforeEnter(AIData Data) {
        Debug.Log("In Monster State");

    }
    public virtual void DoBeforeLeave(AIData Data)
    {
        //Debug.Log("Leave Monster State");
    }
    public virtual void Do(AIData Data)
    {
        Debug.Log("Monster State");
    }
    /// <summary>
    /// 以下為大家可能都會用到的function
    /// </summary>
    /// <param name="Data"></param>
    public void MonsterRotate(AIData Data ,Vector3 target)
    {
        Vector3 monsterRotate = target - Data.Me_Object.transform.position;
        monsterRotate.y = 0;
        if(monsterRotate != Vector3.zero)
        {
            Data.targetRotation = Quaternion.LookRotation(monsterRotate);
            Data.Me_Object.transform.rotation = Quaternion.Slerp(Data.Me_Object.transform.rotation, Data.targetRotation, 5f * Time.deltaTime);
        }
    }
    protected void PosCheck(AIData Data)
    {
        if(Data.finalPosition == Vector3.zero){
            Data.finalPosition = Data.Me_Object.transform.position;
        }
        if (Vector3.Distance(Data.Me_Object.transform.position, Data.finalPosition) < 0.1f)
        {
            Data.rbody.velocity = Vector3.zero;
            Data.CanRun = false;
        }
        else
        {
            Vector3 PosVelocity = new Vector3(Vector3.Normalize(Data.finalPosition - Data.Me_Object.transform.position).x, 0/*Data.rbody.velocity.y*/, Vector3.Normalize(Data.finalPosition - Data.Me_Object.transform.position).z);
            if (Vector3.Distance(Data.Me_Object.transform.position, Data.finalPosition) > 1f)
            {
                Data.rbody.velocity = PosVelocity * Data.MoveSpeed * (1 + Data.OtherSpeedOffset);
            }
            else
            {
                Data.rbody.velocity = PosVelocity * Data.MoveSpeed ;
            }
            Data.CanRun = true;
            
        }
    }
    protected void RotateCheck(AIData Data)
    {
        if (Data.Me_Object.transform.rotation.y != Data.Angle)
        {
            Data.Me_Object.transform.rotation = Quaternion.Slerp(Data.Me_Object.transform.rotation, Quaternion.Euler(0, Data.Angle, 0), 5f * Time.deltaTime);
        }
    }
    protected bool DetectEnemy(AIData Data) {
        Data.Enemy = Physics.OverlapSphere(Data.Me_Object.transform.position, Data.DetectRadius, Data.EnemyLayer);
        if (Data.Enemy.Length > 0 && (Vector3.Distance(Data.Enemy[0].transform.position, Data.BornPoint) < Data.AbsDetectRadius))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
    
public class MonsterIdleState : MonsterState
{
    public override void DoBeforeEnter(AIData Data)
    {
        Data.steeringPoint = Data.Me_Object.transform.position;
        Data.steeringIntervalCount = Time.time + Random.Range(Data.steeringInterval / 2, Data.steeringInterval);
    }
    public override void DoBeforeLeave(AIData Data)
    {
    }
    public override void Do(AIData Data) {

        if (Data.steeringIntervalCount < Time.time && Data.rbody.velocity == Vector3.zero) {
            Data.steeringIntervalCount = Time.time + Random.Range(Data.steeringInterval/2, Data.steeringInterval);
            if (Data.SteeringRange == 0) {
                Debug.LogError(Data.Me_Object+"沒有SteeringRange");
                return;
            }

            Data.steeringPoint = Data.BornPoint + Random.insideUnitSphere * Data.SteeringRange;
            Data.steeringPoint.y = Data.Me_Object.transform.position.y;
            Data.OriginalPoint = Data.rbody.position;
        }
        if (Vector3.Distance(Data.steeringPoint, Data.Me_Object.transform.position) > 0.15f)
        {
            MonsterRotate(Data, Data.steeringPoint);
            Data.rbody.velocity = Vector3.Normalize(Data.steeringPoint - Data.rbody.transform.position) * Data.MoveSpeed;
            Data.CanRun = true;
        }
        else {
            Data.rbody.velocity = new Vector3(0,0,0);
            Data.Me_Object.transform.position = Data.steeringPoint;
            Data.OriginalPoint = Data.Me_Object.transform.position;
            Data.CanRun = false;
        }
        if (DetectEnemy(Data) || Data.FocusEnemy!=null) {
            if (Data.Enemy.Length > 0)
            {
                Data.FocusEnemy = Data.Enemy[0].gameObject;
                if (Data.Me_Object.CompareTag("EarthElement"))
                {
                    Collider[] callingfriendtest = Physics.OverlapSphere(Data.Me_Object.transform.position, 30f, 1 << LayerMask.NameToLayer("Monster"));
                    foreach (Collider callingfriend in callingfriendtest)
                    {
                        OtherMonsterAI callingfriendSc = callingfriend.gameObject.GetComponent<OtherMonsterAI>();
                        callingfriendSc.m_Data.FocusEnemy = Data.FocusEnemy;
                        ClientApp.instance.SendMonsterFocusMessage(callingfriendSc.m_Data.monsterNumber,PlayerManager.instance.GetPlayerIndex(Data.FocusEnemy));
                    }
                }
            }
            Data.FocusPlayerStats = Data.FocusEnemy.GetComponent<OtherPlayerStats>();
            ClientApp.instance.SendMonsterFocusMessage(Data.monsterNumber, PlayerManager.instance.GetPlayerIndex(Data.FocusEnemy.gameObject));
            Data.TransferState(Data, Data.monsterAI.MCS);
        };
    }
}
public class MonsterChaseState : MonsterState
{
    public override void DoBeforeEnter(AIData Data)
    {
        Data.Monsteranim.anim.SetFloat("MoveType",1);
        //MonsterManager.monsterInstance.DetectEnemyIcon(Data);
    }
    public override void DoBeforeLeave(AIData Data)
    {
        ClientApp.instance.SendMonsterMovementMessage(Data.monsterNumber, Data.Me_Object.transform.position, Data.Me_Object.transform.eulerAngles.y,Data.m_AIStats.currentHp);
        Data.rbody.velocity = new Vector3(0, 0, 0);
        Data.CanRun = false;
        Data.Monsteranim.anim.SetFloat("MoveType", 0);
    }
    public override void Do(AIData Data)
    {
        if (Data.FocusEnemy != null)
        if (Vector3.Distance(Data.FocusEnemy.transform.position,Data.BornPoint)<Data.AbsDetectRadius)
        {
            //每秒根據角度大小轉身－角度越大轉越多－反之則越小
            MonsterRotate(Data, Data.FocusEnemy.transform.position);
            if (Data.Distance(Data.Me_Object.transform, Data.FocusEnemy.transform) > Data.AttackRange)
            {
                Data.CanRun = true;
                if (Data.Monsteranim.anim.GetCurrentAnimatorStateInfo(0).IsName("Run"))
                {
                    Vector3 ChaseVel = Vector3.Normalize(Data.FocusEnemy.transform.position - Data.rbody.transform.position);
                    ChaseVel.y = 0;
                    Data.rbody.velocity = ChaseVel * Data.MoveSpeed;
                }
            }
            else if (Data.AttackDelayCount <= 0)
            {
                Data.TransferState(Data, Data.monsterAI.MAS);
            }
            else {
                Data.rbody.velocity = new Vector3(0, 0, 0);
                Data.CanRun = false;
            }
        }
        else {
            ClientApp.instance.SendMonsterDisFocusMessage(Data.monsterNumber, false);
            Data.FocusEnemy = null;
            Data.FocusPlayerStats = null;
            //Data.m_AIStats.Reset();
            Data.TransferState(Data, Data.monsterAI.MIS);
        }
    }
}
public class MonsterAttackState : MonsterState
{
    public override void DoBeforeEnter(AIData Data)
    {
        Data.AttackDelayCount = Data.AttackDelay;
        Data.monsterAI.StartCoroutine(Data.monsterAI.monsterattackCount(Data,Data.AttackDelayCount));
        Data.CanAttack = true;
        ClientApp.instance.SendMonsterAttackAnimMessage(Data.monsterNumber,(Data.CanAttack ? 1:0));
    }
    public override void DoBeforeLeave(AIData Data)
    {
        Data.CanAttack = false;
        ClientApp.instance.SendMonsterAttackAnimMessage(Data.monsterNumber, (Data.CanAttack ? 1 : 0));
    }
    public override void Do(AIData Data)
    {
        if (Data.Enemy.Length<=0) {
            Debug.Log(Data.AttackDelayCount);
            Data.TransferState(Data, Data.monsterAI.MIS);
        }
        //if (Data.Enemy.Length > 0)
        //{
        //    if (Data.Distance(Data.Me_Object.transform, Data.FocusEnemy.transform) > Data.AttackRange)
        //    {
        //        Data.TransferState(Data, Data.monsterAI.MCS);
        //    }
        //    else
        //    {
        //        MonsterRotate(Data, Data.FocusEnemy.transform.position);
        //        //if (Data.AttackDelayCount > 0)
        //        //{
        //           //Data.CanAttack = false;
        //        //    Data.AttackDelayCount -= Time.deltaTime;
        //        //}
        //        //else
        //        //{
        //            //Data.CanAttack = true;
        //        //}
        //    }
        //}
        //else {
        //    Data.TransferState(Data, Data.monsterAI.MIS);
        //}
    }
}
public class MonsterDeadState : MonsterState
{
    public override void DoBeforeEnter(AIData Data)
    {
        Data.rbody.velocity = new Vector3(0,0,0);
        Debug.Log(Data.DeadDisapearTime);
        Data.DeadDisapearTimeCount = Data.DeadDisapearTime;
        Data.FocusEnemy = null;
        Data.Monsteranim.anim.SetTrigger("DeathTrigger");

    }
    public override void DoBeforeLeave(AIData Data)
    {
        Data.m_AIStats.Reset();
        Data.IsDead = false;
        Data.InDeadState = false;
        Data.DeadDisapearTimeCount = Data.DeadDisapearTime;
        Data.MonsterMesh.enabled = true;
        Data.MonsterCollider.enabled = true;
        Data.FocusEnemy = null;
    }
    public override void Do(AIData Data)
    {
        if (Data.DeadDisapearTimeCount > 0)
        {
            Data.DeadDisapearTimeCount -= Time.deltaTime;
            //shaderdisapeartime = Data.DeadDisapearTime;
        }
        else if (Data.DeadDisapearTimeCount <= 0) {

            //傳訊息給物件池
            Data.Me_Object.transform.position = Data.BornPoint;
            if (ClientApp.instance.host)
            {
                Debug.Log("切換mis");
                Data.TransferState(Data, Data.monsterAI.MIS);
            }
            else {
                Data.TransferState(Data, Data.monsterAI.OMS);
            }
        }
    }
}

public class OtherMonsterState : MonsterState
{
    public override void DoBeforeEnter(AIData Data)
    {

    }
    public override void DoBeforeLeave(AIData Data)
    {
    }
    public override void Do(AIData Data)
    {
        PosCheck(Data);
        RotateCheck(Data);
    }
}