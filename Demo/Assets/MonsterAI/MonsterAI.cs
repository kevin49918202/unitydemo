﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAI : OtherMonsterAI {


    public override void Start()
    {
        m_Data.TransferState(m_Data, MIS);
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        if (m_Data.Enemy !=null&& m_Data.Enemy.Length > 0)
        {
            Gizmos.color = Color.red;
        }
        if (m_Data.Me_Object == null) {
            return;
        }
        Gizmos.DrawWireSphere(m_Data.Me_Object.transform.position, m_Data.DetectRadius);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(m_Data.BornPoint, m_Data.SteeringRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(m_Data.Me_Object.transform.position, m_Data.AttackRange);
        Gizmos.color = Color.gray;
        Gizmos.DrawWireSphere(m_Data.BornPoint,m_Data.AbsDetectRadius);
    }

    public void MonPre() {

        MIS = new MonsterIdleState();
        MCS = new MonsterChaseState();
        MAS = new MonsterAttackState();
        MDS = new MonsterDeadState();

    }
}
