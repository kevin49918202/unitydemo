﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class AIData2 {
    [HideInInspector]
    public MonsterState2 m_currentState;
    
    public int monsterNumber;
    //public BossState m_currentBossState;

    [HideInInspector]
    public OtherMonsterAI2 monsterAI;
    //[HideInInspector]
    public BossAI bossAI;
    //public MFSM FSM;
    [HideInInspector]
    public GameObject Me_Object;
    [HideInInspector]
    public Rigidbody rbody;
    [HideInInspector]
    public MonsterAnimScript2 Monsteranim;
    /// <summary>
    /// 遊走(Steering Behavior)
    /// </summary>
    [HideInInspector]
    public Vector3 BornPoint;
    [HideInInspector]
    public Vector3 OriginalPoint;
    [HideInInspector]
    public Vector3 steeringPoint;
    public float steeringInterval = 5f;
    public float SteeringRange = 5f;
    [HideInInspector]
    public float steeringIntervalCount;

    /// <summary>
    /// 偵測範圍敵人
    /// </summary>
    [HideInInspector]
    public bool HasEnemy;
    //[HideInInspector]
    public Collider[] Enemy;
    //[HideInInspector]
    public GameObject FocusEnemy;
    public GameObject NewFocusEnemy;
    public int enemyIndex;
    public float DetectRadius = 10f;
    public float AbsDetectRadius = 20f;
    public LayerMask EnemyLayer = 9;
    /// <summary>
    /// AI素質
    /// </summary>
    [HideInInspector]
    public bool CanAttack;
    public float AttackDelay;
    [HideInInspector]
    public float AttackDelayCount;
    public float AttackRange = 1.2f;
    [HideInInspector]
    public bool CanRun;
    public float MoveSpeed = 3;
    //otherMonster跟上主monster的offset速度;
    public float OtherSpeedOffset = 0.1f;
    public float WalkSpeed = 3;

    public float RotateSpeed = 40f;
    /// <summary>
    /// 技能
    /// </summary>
    public float MiddleSkillRange;

    public int BigStompCastCD;
    public bool CanCastBigStomp = true;
    public float FarRangStompCD;
    public bool CanCastFarRangeStomp = true;
    [HideInInspector]
    public float BigStompCDCount;
    [HideInInspector]
    public float FarRangStompCDCount;

    public int CastSpellTime = 10;
    [HideInInspector]
    public float CastSpellCount;
    //[HideInInspector]
    public bool CanCastMeteor = true;
    public float MeteorCastCD;
    [HideInInspector]
    public Quaternion targetRotation;

    [HideInInspector]
    public float EnemyAngle;
    /// <summary>
    /// 死亡
    /// </summary>
    [HideInInspector]
    public bool IsDead;
    public float DeadDisapearTime;
    [HideInInspector]
    public float DeadDisapearTimeCount;
    [HideInInspector]
    public bool InDeadState;

    /// <summary>
    /// Other素質
    /// </summary>
    /// <returns></returns>
    [HideInInspector]
    public Vector3 finalPosition;
    [HideInInspector]
    public float Angle;
    [HideInInspector]
    public CharacterStats m_AIStats;
    [HideInInspector]
    public OtherPlayerStats FocusPlayerStats;
    public GameObject DieParticle;
    [HideInInspector]
    public Renderer MonsterMesh;
    [HideInInspector]
    public Collider MonsterCollider;
    public GameObject MonsterChest;
    public ItemData[] MonsterDrop;
    [HideInInspector]
    public float BeAttackStunTime = 0.5f;
    [HideInInspector]
    public float BeAttackStunCount = 0.5f;
    public Dictionary<State, MonsterState2> StateDic = new Dictionary<State, MonsterState2>();
    public float DetectIconOffset;
    //public bool HasFocusEnemy() {
    //    if (FocusEnemy != null)
    //    {
    //        if (Vector3.Distance(FocusEnemy.transform.position, BornPoint) > AbsDetectRadius)
    //        {
    //            FocusEnemy = null;
    //            HasEnemy = false;
    //        }
    //        else {
    //            HasEnemy = true;
    //            FocusCheck();
    //        }
    //    }
    //    else {
    //        Enemy = Physics.OverlapSphere(Me_Object.transform.position, DetectRadius, EnemyLayer);
    //        if (Enemy.Length > 0)
    //        {
    //            HasEnemy = true;
    //            FocusCheck();
    //        }
    //        else
    //        {
    //            HasEnemy = false;
    //        }
    //    }
    //    return HasEnemy;
    //}
    //void FocusCheck() {
    //    if (Enemy.Length > 0)
    //    {
    //        if (FocusEnemy != Enemy[0].gameObject)
    //        {
    //            Debug.Log("吸引同伴!");
    //            FocusPlayerStats = Enemy[0].gameObject.GetComponent<OtherPlayerStats>();
    //            if (FocusPlayerStats.currentHp <= 0)
    //            {
    //            }
    //            FocusEnemy = Enemy[0].gameObject;
    //            ClientApp.instance.SendMonsterFocusMessage(monsterNumber, PlayerManager.instance.GetPlayerIndex(FocusEnemy.gameObject),true);
    //        }
    //    }
    //}

    public enum State {
        Idle,
        Chase,
        Attack,
        Dead,
        Other,
        FarRangStomp,
        BigStomp,
        Meteor,
    }

    public void TransferState(AIData2 Data, State ChangeMS)
    {
        if (!StateDic.ContainsKey(ChangeMS)) {
            Debug.Log("I don't have this state");
            return;
        }
        Data.m_currentState.DoBeforeLeave(Data);
        Data.m_currentState = StateDic[ChangeMS];
        Data.m_currentState.DoBeforeEnter(Data);
    }
    //public void TransferState(AIData Data, BossState ChangeMS)
    //{
    //    Data.m_currentBossState.DoBeforeLeave(Data);
    //    Data.m_currentBossState = ChangeMS;
    //    Data.m_currentBossState.DoBeforeEnter(Data);
    //}
    public float Distance(Transform monstertrans,Transform Enemytrans) {
        float distance = Vector3.Distance(monstertrans.position, Enemytrans.position);
        return distance;
    }

}
