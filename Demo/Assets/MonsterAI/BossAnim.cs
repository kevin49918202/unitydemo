﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAnim : MonoBehaviour {
    [SerializeField]
    GameObject effectSavePoint;
    [SerializeField]
    GameObject meteor;
    GameObject[] meteorList;
    bool meteorStart;
    [SerializeField]
    int meteorListCount = 0;
    [SerializeField]
    GameObject stompEffect;
    GameObject[] stompEffectList;
    [SerializeField]
    int stompEffectCount = 0;
    [SerializeField]
    GameObject farRangeStomp;
    GameObject[] farRangeStompList;
    [SerializeField]
    int farRangeStompListCount = 0;
    [SerializeField]
    GameObject BossGround;
    [SerializeField]
    float Area;
    public bool jumpMove;
    BossAI bossai;
    Vector3 farRangeFocusTrans;
    Material[] Bossmat;
    /// <summary>
    /// 0.Meteor
    /// 1.Stomp
    /// 2.FarRangeStomp
    /// </summary>
    [SerializeField]SkillData[] BossSkill;
    void Start()
    {
        Bossmat = GetComponentInChildren<Renderer>().materials;
        effectSavePoint = GameObject.FindGameObjectWithTag("GameManager");
        bossai = GetComponent<BossAI>();
        meteorList = new GameObject[meteorListCount];
        for (int i = 0; i < meteorList.Length; i++)
        {
            meteorList[i] = Instantiate(meteor, effectSavePoint.transform);
            AreaSpell areaspell = meteorList[i].GetComponent<AreaSpell>();
            areaspell.Set(bossai.gameObject.GetComponent<CharacterStats>(), BossSkill[0]);
            meteorList[i].SetActive(false);
            meteorList[i].transform.parent = effectSavePoint.transform;
        }

        stompEffectList = new GameObject[stompEffectCount];
        for (int i = 0; i < stompEffectList.Length; i++)
        {
            stompEffectList[i] = Instantiate(stompEffect, effectSavePoint.transform.position, effectSavePoint.transform.rotation);
            AreaSpell areaspell = stompEffectList[i].GetComponent<AreaSpell>();
            areaspell.Set(bossai.gameObject.GetComponent<CharacterStats>(), BossSkill[1]);
            stompEffectList[i].SetActive(false);
            stompEffectList[i].transform.parent = effectSavePoint.transform;
        }
        farRangeStompList = new GameObject[farRangeStompListCount];
        for (int i = 0; i < farRangeStompList.Length; i++)
        {
            farRangeStompList[i] = Instantiate(farRangeStomp, effectSavePoint.transform.position, effectSavePoint.transform.rotation);
            AreaSpell areaspell = farRangeStompList[i].GetComponent<AreaSpell>();
            areaspell.Set(bossai.gameObject.GetComponent<CharacterStats>(), BossSkill[2]);
            farRangeStompList[i].SetActive(false);
            farRangeStompList[i].transform.parent = effectSavePoint.transform;
        }
    }
    void MeteorStart() {
        //for (int i = 0; i < 3; i++)
        //{
        //    if (meteorListCount >= meteorList.Length)
        //    {
        //        meteorListCount = 0;
        //    }
        //    meteorList[meteorListCount].transform.position = bossai.transform.position + new Vector3(Random.Range(Area, -Area), 15f, Random.Range(Area, -Area));
        //    meteorList[meteorListCount].SetActive(true);
        //    meteorListCount++;
        //}
        meteorStart = true;
        StartCoroutine(MeteorCast());
    }
    void MeteorEnd() {
        meteorStart = false;
    }
    IEnumerator MeteorCast() {
        while (meteorStart)
        {
            for (int i = 0; i < 2; i++)
            {
                if (meteorListCount >= meteorList.Length)
                {
                    meteorListCount = 0;
                }
                Vector3 newPos = bossai.transform.position + new Vector3(Random.Range(Area, -Area), 15f, Random.Range(Area, -Area));
                newPos.y = 50.1f;
                meteorList[meteorListCount].transform.position = newPos;
                meteorList[meteorListCount].SetActive(true);
                meteorListCount++;
            }
            yield return new WaitForSeconds(0.3f);
        }
    }
    void MatColorChange() {

        foreach (Material bossmat in Bossmat) {
            bossmat.color = Color.red;
        }
    }
    //IEnumerator MatColorChangeSmooth()
    //{
    //    foreach (Material bossmat in Bossmat)
    //    {
    //        bossmat.color = Color.red;
    //    }
    //    yield return new WaitForFixedUpdate();
    //}
    void MatColorReturn() {
        foreach (Material bossmat in Bossmat)
        {
            bossmat.color = Color.white;
        }
    }
    void CastSpellCount() {
        bossai.m_Data.CastSpellCount -= 1;
        if (bossai.m_Data.CastSpellCount <= 0) {
            bossai.m_Data.Monsteranim.anim.SetBool("CastComplete", true);
        }
    }
    void StompEffect() {
        //Instantiate(stompEffect, BossGround.transform.position, BossGround.transform.rotation);
        if (stompEffectCount >= stompEffectList.Length) {
            stompEffectCount = 0;
        }
        stompEffectList[stompEffectCount].transform.position = BossGround.transform.position;
        stompEffectList[stompEffectCount].SetActive(true);
        stompEffectCount++;
    }
    void FarRangeFocus() {
        farRangeFocusTrans = bossai.m_Data.FocusEnemy.transform.position;
    }
    void FarRangeStomp()
    {
        if (farRangeStompListCount >= farRangeStompList.Length) {
            farRangeStompListCount = 0;
        }
        farRangeStompList[farRangeStompListCount].transform.position = farRangeFocusTrans;
        farRangeStompList[farRangeStompListCount].SetActive(true);
        farRangeStompListCount++;
    }
    void AnimDone() {
        bossai.m_Data.TransferState(bossai.m_Data, bossai.BIS);
    }
}
