﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAI2 : OtherMonsterAI2 {
    public MonsterIdleState2 MIS;
    public MonsterChaseState2 MCS;
    public MonsterAttackState2 MAS;

    public override void Awake()
    {
        base.Awake();
        MIS = new MonsterIdleState2();
        MCS = new MonsterChaseState2();
        MAS = new MonsterAttackState2();

        m_Data.StateDic.Add(AIData2.State.Idle, MIS);
        m_Data.StateDic.Add(AIData2.State.Attack, MAS);
        m_Data.StateDic.Add(AIData2.State.Chase, MCS);
    }
    public override void Start()
    {
        m_Data.TransferState(m_Data, AIData2.State.Idle);
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        if (m_Data.Enemy !=null&& m_Data.Enemy.Length > 0)
        {
            Gizmos.color = Color.red;
        }
        if (m_Data.Me_Object == null) {
            return;
        }
        Gizmos.DrawWireSphere(m_Data.Me_Object.transform.position, m_Data.DetectRadius);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(m_Data.BornPoint, m_Data.SteeringRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(m_Data.Me_Object.transform.position, m_Data.AttackRange);
        Gizmos.color = Color.gray;
        Gizmos.DrawWireSphere(m_Data.BornPoint,m_Data.AbsDetectRadius);
    }

}
