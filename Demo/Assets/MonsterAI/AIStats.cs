﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIStats : CharacterStats {
    public List<GameObject> owners;
    public int exp;
    public event System.Action missionCheck = () =>{};
    protected OtherMonsterAI monsterai;

    bool isReceiveDie;

    protected override void Awake()
    {
        base.Awake();
        monsterai = GetComponent<OtherMonsterAI>();
    }
    public override void TakeDamage(GameObject owner,int damage, bool critical, string damageType, bool sendToOhter)
    {
        if (damage == 0 || currentHp <= 0) return;
        if (!owners.Contains(owner))
        {
            owners.Add(owner);
        }
        if (monsterai.m_Data.FocusEnemy == null) {

            monsterai.m_Data.FocusEnemy = owners[0];
            monsterai.m_Data.FocusPlayerStats = owners[0].GetComponent<OtherPlayerStats>();
            ClientApp.instance.SendMonsterFocusMessage(monsterai.m_Data.monsterNumber, PlayerManager.instance.GetPlayerIndex(monsterai.m_Data.FocusEnemy.gameObject));
        }
        if (sendToOhter)
        {
            ClientApp.instance.SendMonsterTakeDamageMessage(gameObject, owner, damage, critical, damageType);
        }
        if (currentHp > 0)
        {
            if ((float)damage  > maxHp * 0.05)
            {
                BeDamageAnim();
            }
            else if(Random.Range(0,10)>0.7){
                BeDamageAnim();
            }
        }
        base.TakeDamage(owner, damage, critical, damageType, sendToOhter);
    }

    public override void Die()
    {
        base.Die();
        monsterai.m_Data.IsDead = true;
        if (monsterai.m_Data.bossAI != null) {
            monsterai.m_Data.TransferState(monsterai.m_Data, monsterai.m_Data.bossAI.BDS);
        }
        else {
            monsterai.m_Data.TransferState(monsterai.m_Data, monsterai.MDS);
        }
        missionCheck();

        //是主玩家才給exp
        if(owners.Count > 0)
        {
            PlayerStats playerStats = owners[0].GetComponent<PlayerStats>();
            if (playerStats != null)
            {
                playerStats.AddExp(TeamManager.instance.ShareExp(exp));
            }
        }

        if (!isReceiveDie)
        {
            ClientApp.instance.SendMonsterDieMessage(gameObject);
            isReceiveDie = true;
        }
    }

    public virtual void OnMonsterDieReceive()
    {
        if(currentHp != 0 && !isReceiveDie)
        {
            currentHp = 0;
            isReceiveDie = true;
            Die();
        }
    }

    public override void Reset()
    {
        InitialHpMp();
        owners = new List<GameObject>();
        isReceiveDie = false;
    }
    protected void BeDamageAnim() {
        if (monsterai.m_Data.Monsteranim.anim.GetCurrentAnimatorStateInfo(0).IsName("Attack")) {
            return;
        }
        else {
            monsterai.m_Data.Monsteranim.anim.Play("BeAttack",1,0);
        }
    }
}
