﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossState : MonsterState
{
    public BossState()
    {
    }
    public override void DoBeforeEnter(AIData Data)
    {
        //Debug.Log("In Monster State");

    }
    public override void DoBeforeLeave(AIData Data)
    {
        //Debug.Log("Leave Monster State");
    }
    public override void Do(AIData Data)
    {
        //Debug.Log("Monster State");
    }
}

public class BossIdleState : MonsterState
{
    public override void DoBeforeEnter(AIData Data)
    {
        Data.steeringPoint = Data.Me_Object.transform.position;
        Data.steeringIntervalCount = Time.time + Data.steeringInterval;
    }
    public override void DoBeforeLeave(AIData Data)
    {
    }
    public override void Do(AIData Data)
    {
        if (ClientApp.instance.host)
        {
            if (Data.FocusEnemy == null)
            {
                if (Data.steeringIntervalCount < Time.time && Data.rbody.velocity == new Vector3(0, Data.rbody.velocity.y, 0))
                {
                    Data.steeringIntervalCount = Time.time + Data.steeringInterval;
                    //if (Data.SteeringRange == 0)
                    //{
                    //    Debug.LogError("沒有SteeringRange");
                    //    return;
                    //}

                    Data.steeringPoint = Data.BornPoint + Random.insideUnitSphere * Data.SteeringRange;
                    Data.steeringPoint.y = Data.Me_Object.transform.position.y;
                    Data.OriginalPoint = Data.rbody.position;
                }
                if (Vector3.Distance(Data.steeringPoint, Data.Me_Object.transform.position) > 0.15f)
                {
                    MonsterRotate(Data, Data.steeringPoint);
                    Data.rbody.velocity = Vector3.Normalize(Data.steeringPoint - Data.rbody.transform.position) * Data.MoveSpeed * Mathf.Clamp(Vector3.Distance(Data.steeringPoint, Data.rbody.transform.position) / Vector3.Distance(Data.steeringPoint, Data.OriginalPoint) * 2, 0.6f, 1f);
                    Data.rbody.velocity = new Vector3(Data.rbody.velocity.x, Data.rbody.velocity.y, Data.rbody.velocity.z);
                    Data.CanRun = true;
                }
                else
                {
                    Data.rbody.velocity = new Vector3(0, Data.rbody.velocity.y, 0);
                    Data.Me_Object.transform.position = Data.steeringPoint;
                    Data.OriginalPoint = Data.Me_Object.transform.position;
                    Data.CanRun = false;
                }
            }
            //if (Data.HasFocusEnemy() == true)
            //{
            //    Data.TransferState(Data, Data.bossAI.BCS);
            //}
            if (DetectEnemy(Data) || Data.FocusEnemy != null)
            {
                if (Data.Enemy.Length > 0)
                {
                    Data.FocusEnemy = Data.Enemy[0].gameObject;
                }
                Data.FocusPlayerStats = Data.Enemy[0].gameObject.GetComponent<OtherPlayerStats>();
                ClientApp.instance.SendMonsterFocusMessage(Data.monsterNumber, PlayerManager.instance.GetPlayerIndex(Data.FocusEnemy.gameObject));
                Data.TransferState(Data, Data.bossAI.BCS);
            }
        }
        else {
            PosCheck(Data);
            RotateCheck(Data);
        }
    }
}
public class BossChaseState : MonsterState
{
    public override void DoBeforeEnter(AIData Data)
    {
        Data.CanRun = true;
    }
    public override void DoBeforeLeave(AIData Data)
    {
        Data.rbody.velocity = new Vector3(0, 0, 0);
        Data.CanRun = false;
    }
    public override void Do(AIData Data)
    {
        if (Data.FocusEnemy != null && Vector3.Distance(Data.FocusEnemy.transform.position, Data.BornPoint) < Data.AbsDetectRadius)
        {
            //每秒根據角度大小轉身－角度越大轉越多－反之則越小
            MonsterRotate(Data, Data.Enemy[0].transform.position);
            if (Data.Distance(Data.Me_Object.transform, Data.FocusEnemy.transform) > Data.MiddleSkillRange && Data.CanCastFarRangeStomp)
            {
                Data.TransferState(Data, Data.bossAI.BFR);
                return;
            }
            else
            if (Data.Distance(Data.Me_Object.transform, Data.FocusEnemy.transform) > Data.AttackRange)
            {
                if (Data.Monsteranim.anim.GetCurrentAnimatorStateInfo(0).IsName("Run"))
                {
                    Vector3 velocity = Vector3.Normalize(Data.Enemy[0].transform.position - Data.rbody.transform.position) * Data.MoveSpeed;
                    velocity.y = 0;
                    Data.rbody.velocity = velocity;
                }
                else {
                    Data.TransferState(Data, Data.bossAI.BCS);
                }
            }
            else if (Data.AttackDelayCount <= 0)
            {
                Debug.Log("2");
                Data.TransferState(Data, Data.bossAI.BAS);
            }
            else
            {
                Debug.Log("3");
                Data.rbody.velocity = new Vector3(0, 0, 0);
                Data.CanRun = false;
                Data.AttackDelayCount -= Time.deltaTime;
            }
        }
        else
        {
            ClientApp.instance.SendMonsterDisFocusMessage(Data.monsterNumber, false);
            Data.FocusEnemy = null;
            Data.FocusPlayerStats = null;
            Data.TransferState(Data, Data.bossAI.BIS);
        }
    }
}
public class BossAttackState : MonsterState
{
    public override void DoBeforeEnter(AIData Data)
    {
        Data.CanAttack = true;
        ClientApp.instance.SendMonsterAttackAnimMessage(Data.monsterNumber, (Data.CanAttack ? 1 : 0));
    }
    public override void DoBeforeLeave(AIData Data)
    {
        Data.CanAttack = false;
        ClientApp.instance.SendMonsterAttackAnimMessage(Data.monsterNumber,(Data.CanAttack ? 1:0));
    }
    public override void Do(AIData Data)
    {
        if (Data.Enemy.Length > 0)
        {
            if (Data.Distance(Data.Me_Object.transform, Data.Enemy[0].transform) > Data.AttackRange + 1f)
            {
                Data.TransferState(Data, Data.bossAI.BCS);
            }
            else
            {
                if (((float)Data.m_AIStats.currentHp / (float)Data.m_AIStats.maxHp) <= 0.7 && Data.CanCastBigStomp)
                {
                    Data.TransferState(Data, Data.bossAI.BBS);
                }
                else
                {
                    if (Data.AttackDelayCount > 0) {
                        Data.CanAttack = false;
                        Data.AttackDelayCount -= Time.deltaTime;
                    }
                    else{
                        Data.CanAttack = true;
                        Data.AttackDelayCount += Data.AttackDelay;
                    }
                    MonsterRotate(Data, Data.Enemy[0].transform.position);
                }
            }
        }
        else {
            Data.TransferState(Data, Data.bossAI.BIS);
        }
    }
}
public class BossDeadState : MonsterState 
{
    public override void DoBeforeEnter(AIData Data)
    {
        Data.rbody.velocity = new Vector3(0, Data.rbody.velocity.y, 0);
        Data.DeadDisapearTimeCount = Data.DeadDisapearTime;
        Data.Monsteranim.anim.SetTrigger("DeathTrigger");
        Data.InDeadState = true;
        Data.rbody.useGravity = true;
    }
    public override void DoBeforeLeave(AIData Data)
    {
        Data.m_AIStats.Reset();
        Data.IsDead = false;
        Data.InDeadState = false;
        Data.DeadDisapearTimeCount = Data.DeadDisapearTime;
        Data.MonsterMesh.enabled = true;
        Data.MonsterCollider.enabled = true;
    }
    public override void Do(AIData Data)
    {
        if (Data.DeadDisapearTimeCount > 0)
        {
            Data.DeadDisapearTimeCount -= Time.deltaTime;
            Data.rbody.velocity = new Vector3(0, Data.rbody.velocity.y, 0);
            //shaderdisapeartime = Data.DeadDisapearTime;
        }
        else if (Data.DeadDisapearTimeCount < 0)
        {
            //傳訊息給物件池
            Data.Me_Object.transform.position = Data.BornPoint;
            Data.TransferState(Data, Data.bossAI.BIS);
        }
    }
}
public class BossCastMetorState : MonsterState
{
    public override void DoBeforeEnter(AIData Data)
    {
        Data.CanCastMeteor = false;
        Data.bossAI.StartCoroutine(Data.bossAI.MeteorCDCount(Data, Data.MeteorCastCD));
        if (ClientApp.instance.host)
        {
            ClientApp.instance.SendMonsterAttackAnimMessage(Data.monsterNumber, 2);
        }
        Data.rbody.velocity = new Vector3(0, 0, 0);
        Data.Monsteranim.anim.SetTrigger("Meteor");
    }
    public override void DoBeforeLeave(AIData Data)
    {
    }
    public override void Do(AIData Data)
    {
    }
}


public class BossBigStompState : MonsterState
{
    public override void DoBeforeEnter(AIData Data)
    {
        Data.CanCastBigStomp = false;
        Data.bossAI.StartCoroutine(Data.bossAI.BigStompCDCount(Data, Data.BigStompCastCD));
        if (ClientApp.instance.host)
        {
            ClientApp.instance.SendMonsterAttackAnimMessage(Data.monsterNumber, 3);
        }
        Data.rbody.velocity = new Vector3(0, 0, 0);
        Data.Monsteranim.anim.SetTrigger("BigStomp");
    }
    public override void DoBeforeLeave(AIData Data)
    {
    }
    public override void Do(AIData Data)
    {

    }
}
public class BossFarRangeStompState : MonsterState
{
    public override void DoBeforeEnter(AIData Data)
    {
        Data.FarRangStompCDCount = Data.FarRangStompCD;
        if (ClientApp.instance.host) {
            ClientApp.instance.SendMonsterAttackAnimMessage(Data.monsterNumber, 4);
        }
        Data.bossAI.StartCoroutine(Data.bossAI.FarRangeStompCDCount(Data, Data.FarRangStompCD));
        Data.CanCastFarRangeStomp = false;
        Data.rbody.velocity = new Vector3(0, 0, 0);
        Data.Monsteranim.anim.SetTrigger("FarRangeStomp");
    }
    public override void DoBeforeLeave(AIData Data)
    {
        //Data.Monsteranim.anim.SetBool("CastComplete", false);
    }
    public override void Do(AIData Data)
    {
    }
}


