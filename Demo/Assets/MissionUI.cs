﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionUI : DraggableObject {
    public static MissionUI missionui;
    public  MissionClass mission;
    [SerializeField] GameObject MissionPanel;
    [SerializeField] Text amountText;
    [SerializeField] Text title; 
    [SerializeField] Text Description;

    [SerializeField] Text tip_Text;
    [SerializeField] Animator tip_Animator;
    [SerializeField] CanvasGroup canvasGroup;

    // Use this for initialization
    void Awake () {
        missionui = this;
	}

    public void UpdateMission()
    {
        if (mission == null || mission.missionData == null)
        {
            title.text = " ";
            Description.text = "目前無任何任務";
            amountText.text = " ";
            tip_Text.text = "";
        }
        else
        {
            title.text = mission.missionData.objectName;
            Description.text = mission.missionData.MissionDescription;
            Description.text = Description.text.Replace("n", "\n");
            Description.text = Description.text.Replace('s', ' ');
            amountText.text = "目前殺了：" + mission.amount +"/"+ mission.goalamount + "隻";

            string sDescription = "";
            switch (mission.missionData.name)
            {
                case "BugMission":
                    sDescription = "擊殺毒爆蟲解除屏障\n目前殺了：" + mission.amount + "/" + mission.goalamount + "隻";
                    break;
                case "EathElementRitual":
                    sDescription = "擊殺土元素阻止儀式完成\n目前殺了：" + mission.amount + "/" + mission.goalamount + "隻";
                    break;
                case "WolfAssault":
                    sDescription = "擊殺野狼獲取最終裝備\n目前殺了：" + mission.amount + "/" + mission.goalamount + "隻";
                    break;
                case "BossMission":
                    sDescription = "打敗最後龍王\n結束這場災難";
                    break;
            }

            tip_Text.text = sDescription;
        }
    }
    public void OpenMissionUI() {
        if (mission.missionData == null)
        {
            UpdateMission();
        }
        if (MissionPanel.activeSelf == true)
        {
            MissionPanel.SetActive(false);
            canvasGroup.blocksRaycasts = false;
        }
        else {
            MissionPanel.SetActive(true);
            canvasGroup.blocksRaycasts = true;
        }
    }
    public void CloseMissionUI() {
        MissionPanel.SetActive(false);
    }

    public void OnMissionChanged(MissionClass mission)
    {
        this.mission = mission;
            UpdateMission();
    }

    public void OnMissionCompleted(MissionClass mission)
    {
        tip_Animator.Play("MissionTipFadeOut");
        string sDescription = "";
        switch (mission.missionData.name)
        {
            case "BugMission":
                sDescription = "擊殺毒爆蟲解除屏障\n目前殺了：" + mission.amount + "/" + mission.goalamount + "隻";
                break;
            case "EathElementRitual":
                sDescription = "擊殺土元素阻止儀式完成\n目前殺了：" + mission.amount + "/" + mission.goalamount + "隻";
                break;
            case "WolfAssault":
                sDescription = "擊殺野狼獲取最終裝備\n目前殺了：" + mission.amount + "/" + mission.goalamount + "隻";
                break;
        }

        tip_Text.text = sDescription;
    }

    public void OnAddMission(MissionClass mission)
    {
        OnMissionChanged(mission);
        tip_Animator.SetTrigger("FadeIn");
    }
}
