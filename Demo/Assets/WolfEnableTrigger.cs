﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WolfEnableTrigger : MonoBehaviour {
    [SerializeField] GameObject explosionef;
    [SerializeField] float ParticleDestroyTime = 5f;
    [SerializeField] float WolfSpawnSpeed = 3f;
    bool isQuitting;
    [SerializeField]bool firstCreate = true;

    void Awake()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnApplicationQuit()
    {
        isQuitting = true;
    }

    public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        isQuitting = true;
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnEnable()
    {
        if (firstCreate == true) {  return; }
        GameObject explosion = Instantiate(explosionef, transform.position, transform.rotation);
        Destroy(explosion, ParticleDestroyTime);
    }

    private void OnDisable()
    {
        if (isQuitting) return;
        if (firstCreate == true) { firstCreate = false; return; }
        GameObject explosion = Instantiate(explosionef, transform.position, transform.rotation);
        Destroy(explosion, ParticleDestroyTime);
    }
    void DisableWolf() {
        gameObject.SetActive(false);
    }
    void SpawnWolf() {
        Invoke("SpawnWolfCount", WolfSpawnSpeed);
    }
    void SpawnWolfCount() {
        MonsterManager.monsterInstance.SpawnWolf();
        MonsterManager.monsterInstance.DisEarth_Element();
    }
}
