﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTrigger : MonoBehaviour {
    private void OnDisable()
    {
        MonsterManager.monsterInstance.SpawnBoss();
    }
}
