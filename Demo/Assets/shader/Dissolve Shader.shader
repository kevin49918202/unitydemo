﻿Shader "Custom/Dissolve Shader" {
 Properties {
      _MainTex ("Texture (RGB)", 2D) = "white" {}
	  
      _SliceGuide ("Slice Guide (RGB)", 2D) = "white" {}
      _SliceAmount ("Slice Amount", Range(0.0, 1.0)) = 0.5
      _Color("Color",Color) = (0,0,0,0)
	  _HDR("HDR",Range(0.0, 40.0)) = 1
	  _Alpha("Alpha",Range(0.0, 1)) = 1
    }
    SubShader {
      Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }
      Cull Off
      CGPROGRAM
      //if you're not planning on using shadows, remove "addshadow" for better performance
      #pragma surface surf Lambert alpha
      struct Input {
          float2 uv_MainTex;
          float2 uv_SliceGuide;
          float _SliceAmount;
      };
      sampler2D _MainTex;
      sampler2D _SliceGuide;
      float _SliceAmount;
	  float4 _Color;
	  float _HDR;
	  float _Alpha;
      void surf (Input IN, inout SurfaceOutput o) {
		  half4 c = tex2D(_MainTex, IN.uv_MainTex);
		  float _SlicPic = tex2D (_SliceGuide, IN.uv_SliceGuide).r -_SliceAmount*1.01;
		  if(_SlicPic<0){
		  discard;
		  }
          //clip(tex2D (_SliceGuide, IN.uv_SliceGuide).r - _SliceAmount*1.02);
          o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb;
		  o.Albedo.rgb = dot(tex2D(_MainTex, IN.uv_MainTex).rgb, (0.299, 0.587, 0.114));
		  o.Albedo.rgb *= _Color;
		  o.Albedo.rgb *= (o.Albedo.rgb*_HDR) / (o.Albedo.rgb);
		  o.Alpha = tex2D(_MainTex, IN.uv_MainTex).a*_Alpha;
		  //o.Alpha = 1;
      }
      ENDCG
    } 
    Fallback "Diffuse"
  }
