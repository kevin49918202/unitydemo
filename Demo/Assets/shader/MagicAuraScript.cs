﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicAuraScript : MonoBehaviour {
    public Material MaicAuraMat;
    public float magicauraCount = 1f;
    public bool magichappen;
    [SerializeField]GameObject MagicAura;
    // Use this for initialization
    // Update is called once per frame
    private void Awake()
    {
        //MagicAura.SetActive(false);
        magicauraCount = 1f;
    }
    void LateUpdate()
    {
            dotheaura();
    }
    void dotheaura() {
            magicauraCount = magicauraCount - Time.deltaTime * .425f;
            MaicAuraMat.SetFloat("_SliceAmount", magicauraCount);
            MaicAuraMat.SetFloat("_TintAlpha", 1-magicauraCount*2f);
    }
    public void stopTheAura() {
        magichappen = true;
        magicauraCount = 1f;
        this.gameObject.SetActive(false);
    }
}
