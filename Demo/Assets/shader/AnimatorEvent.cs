﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorEvent : MonoBehaviour {
    public GameObject MagicAuraObject;
    void MagicAura() {
        //MagicAuraScript magicaurascript = MagicAuraObject.GetComponentInChildren<MagicAuraScript>();
        MagicAuraObject.SetActive(true);
    }
    void stopTheAura() {
        MagicAuraScript magicaurascript = MagicAuraObject.GetComponentInChildren<MagicAuraScript>();
        magicaurascript.stopTheAura();
    }
}
