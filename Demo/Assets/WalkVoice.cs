﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkVoice : MonoBehaviour {
    AudioSource audiosource;
    [SerializeField]AudioClip FootSound;
    [SerializeField] LayerMask GroundLayer;
    bool makeSound;
    private void Start()
    {
        audiosource = GetComponent<AudioSource>();
        audiosource.volume = 0.2f;
    }
    private void Update()
    {
        if (Physics.OverlapSphere(transform.position, 0.3f, GroundLayer).Length > 0 && makeSound == false)
        {
            makeSound = true;
            audiosource.PlayOneShot(FootSound);
        }
        else if (Physics.OverlapSphere(transform.position, 0.3f, GroundLayer).Length == 0) {
            makeSound = false;
        }
    }
}
