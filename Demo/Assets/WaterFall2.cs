﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterFall2 : MonoBehaviour
{
    [SerializeField] Material water;
    [SerializeField] float fallspeed = -0.5f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector2 offset = new Vector2(Time.time * fallspeed, 0);
        water.mainTextureOffset = offset;
    }
}
