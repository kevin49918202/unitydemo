﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wolfTimelineTrigger : MonoBehaviour {
    WolfEnableTrigger[] wolftrig;
	// Use this for initialization
	void Awake () {
        wolftrig = GetComponentsInChildren<WolfEnableTrigger>();
        GetComponentInParent<MapTrigger>().resetTrigger += ResetWolfTrig;
    }
    private void ResetWolfTrig()
    {
        foreach (WolfEnableTrigger Wolftrig in wolftrig) {
            Wolftrig.gameObject.SetActive(true);
        }
    }
}
