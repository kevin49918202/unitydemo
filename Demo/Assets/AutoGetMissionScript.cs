﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoGetMissionScript : MonoBehaviour {
    MapTrigger maptrig;
    [SerializeField] MissionData mission;
	// Use this for initialization
	void Start () {
        maptrig = GetComponentInParent<MapTrigger>();
        maptrig.onTrigger += addMission;
    }

    void addMission() {
        MissionManager.missionmanager.AddMission(mission);
    }
}
