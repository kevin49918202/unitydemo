﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class ChatBoxUIFunc : MonoBehaviour {
    public static ChatBoxUIFunc chatboxuifunc;
    //要送出的字
    [SerializeField]
    InputField InputMessageField;
    [SerializeField]
    Text input_Text;
    //聊天室GameObject
    [SerializeField]
    GameObject ChatBoxPanel;
    //聊天訊息gameObject && 聊天室訊息
    [SerializeField]
    GameObject MessageBar;
    [SerializeField]
    Text MessageBarText;
    //聊天室排版欄位
    [SerializeField]
    GameObject Content_ChatBox;

    [SerializeField]
    float MessageMaxAmount;
    List<GameObject> messagebarArray = new List<GameObject>();


    [SerializeField] GameObject inputFieldGameObject;
    [SerializeField] Image viewportBackground_Image;
    [SerializeField] Image scrollBarBackground_Image;
    [SerializeField] Image scrollBarHandle_Image;
    [SerializeField] Scrollbar scrollbar;
    [SerializeField] CanvasGroup canvasGroup;
    bool enable = false;

    private void Awake()
    {
        chatboxuifunc = this;
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<PlayerManager>().onMainPlayerCreated += RegisterLevelUp;
    }

    void Start()
    {
        Inventory.instance.onAddItem += OnAddItem;
        DisableUI();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (enable)
            {
                SendChatMessageUI();
            }
            else
            {
                EnableUI();
            }
        }

        if (enable)
        {
            if((Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)) && !EventSystem.current.IsPointerOverGameObject())
            {
                DisableUI();
            }
        }
    }
    public void SendChatMessageUI() {
        if(InputMessageField.text == "/whoisyourdaddy")
        {
            Inventory.instance.WhoIsYourDaddy();
            InputMessageField.text = "";
            EnableUI();
        }
        else if(InputMessageField.text != "")
        {
            ClientApp.instance.SendMessageBoard(InputMessageField.text);
            InputMessageField.text = "";
            EnableUI();
        }
        else
        {
            DisableUI();
        }
    }
    public void AcceptMessageToBoard(string Name,string Message)
    {
        if (messagebarArray.Count >= MessageMaxAmount)
        {
            Destroy(messagebarArray[0].gameObject);
            messagebarArray.Remove(messagebarArray[0]);
        }
        GameObject message = Instantiate(MessageBar, Content_ChatBox.transform);
        message.GetComponentInChildren<Text>().text = Name + ":" + Message;
        messagebarArray.Add(message);
        scrollbar.value = 0;
    }

    public void SetSystemLog(string text)
    {
        if (messagebarArray.Count >= MessageMaxAmount)
        {
            Destroy(messagebarArray[0].gameObject);
            messagebarArray.Remove(messagebarArray[0]);
        }
        GameObject message = Instantiate(MessageBar, Content_ChatBox.transform);
        Text messageText = message.GetComponentInChildren<Text>();
        messageText.text = text;
        messageText.color = Color.green;
        messagebarArray.Add(message);
        scrollbar.value = 0;
    }

    public void EnableUI()
    {
        inputFieldGameObject.SetActive(true);
        viewportBackground_Image.color = new Color(1, 1, 1, 1);
        scrollBarBackground_Image.enabled = true;
        scrollBarHandle_Image.enabled = true;
        canvasGroup.blocksRaycasts = true;
        enable = true;
        InputMessageField.ActivateInputField();
    }

    public void DisableUI()
    {
        inputFieldGameObject.SetActive(false);
        viewportBackground_Image.color = new Color(0, 0, 0, 0.5f);
        scrollBarBackground_Image.enabled = false;
        scrollBarHandle_Image.enabled = false;
        canvasGroup.blocksRaycasts = false;
        enable = false;
    }

    void OnAddItem(int itemID, int itemAmount)
    {
        string itemName = UseableObjectDataManager.instance.GetItemData(itemID).objectName;
        string text = "獲得 " + itemName + " * " + itemAmount;
        SetSystemLog(text);
    }
    
    void RegisterLevelUp(GameObject player)
    {
        player.GetComponent<PlayerStats>().onLevelUp += OnLevelUp;
    }

    void OnLevelUp(PlayerStats stats)
    {
        string text = "升級了! 當前等級:" + stats.level + "級!";
        SetSystemLog(text);
        text = "血量+" + stats.perLevelHpValue + " 魔力+" + stats.perLevelMpValue;
        SetSystemLog(text);
        text = "攻擊力+" + stats.perLevelDamageValue + " 防禦力+" + stats.perLevelArmorValue;
        SetSystemLog(text);
    }
}
