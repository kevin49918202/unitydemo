﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraRecord_Delete : MonoBehaviour {
    bool StartMove;
    Vector3 MoveDir;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.K)) {
            StartMove = true;
            MoveDir = new Vector3(1, 0, 0);
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            StartMove = false;
        }
        if (StartMove) {
            transform.position += MoveDir*Time.deltaTime*2;
        }
	}
}
