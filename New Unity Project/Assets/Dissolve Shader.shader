﻿Shader "Custom/Dissolve Shader" {
 Properties {
      _MainTex ("Texture (RGB)", 2D) = "white" {}
	  
      _SliceGuide ("Slice Guide (RGB)", 2D) = "white" {}
      _SliceAmount ("Slice Amount", Range(0.0, 100.0)) = 0.5
      _Color("Color",Color) = (0,0,0,0)
	  _HDR("HDR",Range(0.0, 40.0)) = 1
    }
    SubShader {
      Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }
      Cull Off
      CGPROGRAM
      //if you're not planning on using shadows, remove "addshadow" for better performance
      #pragma surface surf Lambert alpha
      struct Input {
          float2 uv_MainTex;
          float2 uv_SliceGuide;
          float _SliceAmount;
      };
      sampler2D _MainTex;
      sampler2D _SliceGuide;
      float _SliceAmount;
	  float4 _Color;
	  float _HDR;
      void surf (Input IN, inout SurfaceOutput o) {
		  half4 c = tex2D(_MainTex, IN.uv_MainTex);

          clip(tex2D (_SliceGuide, IN.uv_SliceGuide).rgb - _SliceAmount/100*1.02);
          //o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb;
		  float3 test = (0.3, 0.6, 0.1);
		  o.Albedo.rgb = dot(tex2D(_MainTex, IN.uv_MainTex).rgb, (0.299, 0.587, 0.114));
		  o.Albedo.rgb *= _Color;
		  o.Albedo.rgb *= (o.Albedo.rgb*_HDR) / (o.Albedo.rgb);
		  o.Alpha = tex2D(_MainTex, IN.uv_MainTex).a;
      }
      ENDCG
    } 
    Fallback "Diffuse"
  }
