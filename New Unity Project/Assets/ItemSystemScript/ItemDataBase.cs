﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class ItemDataBase : MonoBehaviour {
    public static ItemDataBase Instance { get; set; }
    private List<Item> Items { get; set; }
	// Use this for initialization
	void Start () {
        Instance = this;

	}
	
	// Update is called once per frame
	void Update () {
        Items = JsonConvert.DeserializeObject<List<Item>>(Resources.Load<TextAsset>("JSON/Items").ToString());

    }
}
