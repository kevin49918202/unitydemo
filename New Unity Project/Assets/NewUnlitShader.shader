﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/PlanToWave" {
	Properties{
		_TintColor("Tint Color", Color) = (0.5,0.5,0.5,0.5)
		_TintAlpha("_TintAlpha",  Range(0, 1.5)) = 1
		_Color("Color", Color) = (0, 0, 0, 1)
		_MainTex("Particle Texture", 2D) = "white" {}
		_Amplitude("Amplitude", Range(0,4)) = 1.0
		_Movement("Movement", Range(-100,100)) = 0
	}

		SubShader{
			Tags { "RenderType" = "transparent" }

			Pass
			{
				Cull Off

				CGPROGRAM

				#pragma vertex vert
				#pragma fragment frag

				sampler2D _MainTex;
				float4 _Color;
				float _Amplitude;
				float _Movement;

				sampler2D _MainTex2;
				float _SliceAmount;
				float _TintAlpha;
				fixed4 _TintColor;

				struct vertexInput
				{
					float4 vertex : POSITION;
				};

				struct vertexOutput
				{
					float4 pos : SV_POSITION;
				};

				struct v2f {
					float4 vertex : SV_POSITION;
					fixed4 color : COLOR;
					float2 texcoord : TEXCOORD0;
				};


				vertexOutput vert(vertexInput input)
				{
					float4x4 Matrice = unity_ObjectToWorld;
					vertexOutput output;

					float4 posWorld = mul(Matrice, input.vertex);

					float displacement = (cos(posWorld.y) + cos(posWorld.x + _Movement * _Time));
					posWorld.y = posWorld.y + _Amplitude * displacement;

					output.pos = mul(UNITY_MATRIX_VP, posWorld);
					return output;
				}

				float4 frag(v2f i) : COLOR
				{
				 #ifdef SOFTPARTICLES_ON
				 float sceneZ = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)));
				 float partZ = i.projPos.z;
				 float fade = saturate(_InvFade * (sceneZ - partZ));
				 //i.color.a *= fade;
				 #endif
				 _TintColor.a += _TintAlpha;
				 fixed4 col = 2.0f  * tex2D(_MainTex, i.texcoord) *_TintColor;

				 clip((1 - tex2D(_MainTex2,i.texcoord).rgb) - _SliceAmount);
				 return col;
				}
				ENDCG
			}
	}
		FallBack "Diffuse"
}