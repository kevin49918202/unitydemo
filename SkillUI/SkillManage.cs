﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SkillManage : MonoBehaviour {
    public GameObject SkillPanel;
    public SkillT[] SkillDictionary;
    SkillUIController[] skilluicontrol;
    // Use this for initialization
    void Start () {
        skilluicontrol = SkillPanel.GetComponentsInChildren<SkillUIController>();
        for (int i = 0; i < skilluicontrol.Length; i++)
        {
            if (SkillDictionary[i] != null) {
                skilluicontrol[i].UISkill = SkillDictionary[i];
                skilluicontrol[i].BuildSkill();
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
