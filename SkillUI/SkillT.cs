﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Create/Skill")]
//目前4種技能－範圍/指定/指向/指向範圍
//Four Skill Area of Effect(AOE)（範圍）
//Unit-targeted(範圍)
//skillshot（指定範圍/距離）
//SkillShot-AOE（範圍/距離）
public class SkillT : ScriptableObject {
    public string SkillName;

    public string Descrption;
    public string SkillType;
    
    public Sprite Artwork;

    public float ManaCost;
    public float attack;
    public float CD;
    
    public float Bound;
    public float Range;
}
