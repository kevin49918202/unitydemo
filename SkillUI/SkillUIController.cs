﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillUIController : MonoBehaviour {
    public int SkillNumber;
    public SkillT UISkill;
    [SerializeField]Image UIsprite;
    PlayerController player;

    [SerializeField]KeyCode keycode;
    bool StartCountTimer;
    float CoolDown;
    bool IsCoolDown;
    public Image CoolDownImage;
    private void Awake()
    {
        UIsprite = GetComponent<Image>();
        StartCountTimer = false;
    }
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
        if (UISkill != null) {
            if (Input.GetKeyDown(keycode) && IsCoolDown)
            {
                StartCountTimer = true;
                CoolDown = UISkill.CD;
                IsCoolDown = false;
                PlayerController.Instance.useSkill(UISkill);
            }
            if (StartCountTimer == true)
            {
                CoolDown -= Time.deltaTime;
            }
            if (CoolDown <= 0)
            {
                CoolDown = 0;
                IsCoolDown = true;
            }
            CoolDownImage.fillAmount = CoolDown / UISkill.CD;
        }
    }
    public void BuildSkill() {
        UIsprite.sprite = UISkill.Artwork;
    }
}
