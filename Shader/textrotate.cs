﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class textrotate : MonoBehaviour {
    [SerializeField] Material mat;
    Vector2 offset;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        offset += new Vector2(Time.deltaTime * 0.075f, 0);
        mat.mainTextureOffset = offset;
    }
}
